

AI.NotEnoughStockMessages = {
	"I don't have that many available."
}

function Merchant.DoNotEnoughStock(buyer)
	QuickDialogMessage(this,buyer, AI.NotEnoughStockMessages[math.random(1, #AI.NotEnoughStockMessages)])
end

function Merchant.HasStock(item, amount)
	if (item == nil) or (amount == nil) then return false end
	if not(item:IsValid()) then return false end
	if IsStackable(item) then
		local availableStock = GetStackCount(item)
		if(amount <= availableStock) then
			return true
		end
	else
	-- shouldn't have to check for non-stackable items,
		-- since if out of stock the item won't be there to click.
		return true
	end
	return false
end

-- used only after a purchase, will update merchants current stock
function Merchant.UpdateStock(merchant, item, amount)
	--local unlimitedStock = item:GetObjVar("UnlimitedStock")
	-- DAB HACK: UNLIMITED STOCK HACK
	local unlimitedStock = true
	if IsStackable(item) then
		local stackCount = item:GetObjVar("StackCount")
		if( amount > stackCount ) then
			return false
		end
		if ( unlimitedStock ~= true ) then
			if( amount == stackCount ) then
				-- bought them all, will re-stock later.
				if ( RemoveFromStock(merchant, item) ) then
					item:Destroy()
				end
			else
				-- update the stack count for stocked item
				MerchantUpdateStackCount(item, stackCount - amount)
			end
		end
		return true
	else
		if ( unlimitedStock ~= true ) then

			-- non-stackable are removed and will be re-stocked later.
			if ( RemoveFromStock(merchant, item) ) then
				item:Destroy()
			end
		end
		return true
	end
	return false
end