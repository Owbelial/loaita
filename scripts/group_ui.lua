
function CleanUp()
    this:CloseDynamicWindow("GroupWindow")
    this:DelModule("group_ui")
end

local lootStrategiesDescriptions = {
    FreeForAll = "No loot privileges will be assigned to any mobs killed by the group.",
    RoundRobin = "Assign loot privileges to the next group member in line, must be within "..ServerSettings.Group.RewardRange.." units when mob dies to be eligible.",
    Random = "Randomly picks a group member to assign loot privileges to, must be within "..ServerSettings.Group.RewardRange.." units when mob dies to be eligible.",
    Master = "All loot privileges go to the group leader, leader must be within "..ServerSettings.Group.RewardRange.." units when mob dies to be eligible. If leader is not eligible, falls back to Random",
}

local lootDisplayStrs = {
    FreeForAll = "Free For All",
    RoundRobin = "Round Robin",
    Random = "Random",
    Master = "Master"
}

mSelectedMemberId = nil
mContextMember = nil

function GroupWindow()
    local groupId = GetGroupId(this)
    if ( groupId == nil ) then
        CleanUp()
        return
    end

    local windowWidth = 380
    local windowHeight = 284
    
    local dynamicWindow = DynamicWindow(
        "GroupWindow", --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
        "Group", --(string) Title of the window for the client UI
        windowWidth, --(number) Width of the window
        windowHeight --(number) Height of the window
        --startX, --(number) Starting X position of the window (chosen by client if not specified)
        --startY, --(number) Starting Y position of the window (chosen by client if not specified)
        --windowType, --(string) Window type (optional)
        --windowAnchor --(string) Window anchor (default "TopLeft")
    )

    local groupRecord = GetGroupRecord(groupId)

    local leader = groupRecord.Leader
    local members = groupRecord.Members
    local loot = groupRecord.Loot or GroupLootStrategies[1]
    local karma = groupRecord.Karma    

    dynamicWindow:AddImage(8,27,"BasicWindow_Panel",344,130,"Sliced")
    dynamicWindow:AddImage(10,5,"HeaderRow_Bar",340,21,"Sliced")
    dynamicWindow:AddLabel(
            25, --(number) x position in pixels on the window
            10, --(number) y position in pixels on the window
            "Name", --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16, --(number) font size (default specific to client)
            "left" --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )

        dynamicWindow:AddLabel(
            280, --(number) x position in pixels on the window
            10, --(number) y position in pixels on the window
            "Status", --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16, --(number) font size (default specific to client)
            "center" --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )

    local y = 32    
    for i=1,#members do
        local member = members[i]
        
        local name = GlobalVarReadKey("User.Name", member) or "Unknown"
        local online = GlobalVarReadKey("User.Online", member)

        local isSelected = mSelectedMemberId == member.Id

        local buttonState = isSelected and "pressed" or ""
        dynamicWindow:AddButton(12,y,"Select|"..tostring(member.Id),"",336,24,"","",false,"ThinFrameHover",buttonState)

        local nameStr = name
        if(leader == member) then
            nameStr = "(L) "..nameStr
        end
        dynamicWindow:AddLabel(
            25, --(number) x position in pixels on the window
            y+5, --(number) y position in pixels on the window
            nameStr, --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16 --(number) font size (default specific to client)
            --"center", --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )

        local status = "[32CD32]Online[-]"
        if not( online ) then status = "[708090]Offline[-]" end
        dynamicWindow:AddLabel(
            280, --(number) x position in pixels on the window
            y+5, --(number) y position in pixels on the window
            status, --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16, --(number) font size (default specific to client)
            "center" --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )

        y = y + 24
    end
       
    if ( this == leader ) then
        dynamicWindow:AddButton(
            12, --(number) x position in pixels on the window
            200, --(number) y position in pixels on the window
            "GroupLoot", --(string) return id used in the DynamicWindowResponse event
            lootDisplayStrs[loot], --(string) text in the button (defaults to empty string)
            113, --(number) width of the button (defaults to width of text)
            24,--(number) height of the button (default decided by type of button)            
            loot..": "..lootStrategiesDescriptions[loot], --(string) mouseover tooltip for the button (default blank)
            "", --(string) server command to send on button click (default to none)
            false,--closeWindowOnClick, --(boolean) should the window close when this button is clicked? (default true)
            "Default"--buttonType, --(string) button type (default "Default")
            --buttonState, --(string) button state (optional, valid options are default,pressed,disabled)
            --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
        )

        if ( karma == nil ) then
            dynamicWindow:AddButton(
                125, --(number) x position in pixels on the window
                200, --(number) y position in pixels on the window
                "GroupKarma", --(string) return id used in the DynamicWindowResponse event
                "Disable Karma", --(string) text in the button (defaults to empty string)
                113, --(number) width of the button (defaults to width of text)
                24,--(number) height of the button (default decided by type of button)
                "So you can spar and loot eachother freely.", --(string) mouseover tooltip for the button (default blank)
                "", --(string) server command to send on button click (default to none)
                false,--closeWindowOnClick, --(boolean) should the window close when this button is clicked? (default true)
                "Default"--buttonType, --(string) button type (default "Default")
                --buttonState, --(string) button state (optional, valid options are default,pressed,disabled)
                --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
            )
        end
    else
        dynamicWindow:AddButton(
            15, --(number) x position in pixels on the window
            200, --(number) y position in pixels on the window
            "Nada", --(string) return id used in the DynamicWindowResponse event
            lootDisplayStrs[loot], --(string) text in the button (defaults to empty string)
            113, --(number) width of the button (defaults to width of text)
            24,--(number) height of the button (default decided by type of button)
            loot..": "..lootStrategiesDescriptions[loot], --(string) mouseover tooltip for the button (default blank)
            "", --(string) server command to send on button click (default to none)
            false,--closeWindowOnClick, --(boolean) should the window close when this button is clicked? (default true)
            "Default", --buttonType, --(string) button type (default "Default")
            "disabled" --(string) button state (optional, valid options are default,pressed,disabled)
            --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
        )
    end

    dynamicWindow:AddButton(
        238, --(number) x position in pixels on the window
        200, --(number) y position in pixels on the window
        "GroupLeave", --(string) return id used in the DynamicWindowResponse event
        "Leave Group", --(string) text in the button (defaults to empty string)
        113, --(number) width of the button (defaults to width of text)
        24,--(number) height of the button (default decided by type of button)
        "Leave your group", --(string) mouseover tooltip for the button (default blank)
        "", --(string) server command to send on button click (default to none)
        false,--closeWindowOnClick, --(boolean) should the window close when this button is clicked? (default true)
        "Default"--buttonType, --(string) button type (default "Default")
        --buttonState, --(string) button state (optional, valid options are default,pressed,disabled)
        --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
    ) 

    local karmaStr = "Members of this group are restricted from performing negative karma actions towards each other."
    
    if ( karma ) then
        karmaStr = "All group members can attack and loot you freely, leave this group to opt out."            
    end
    dynamicWindow:AddLabel(
            15, --(number) x position in pixels on the window
            162, --(number) y position in pixels on the window
            karmaStr, --(string) text in the label
            360, --(number) width of the text for wrapping purposes (defaults to width of text)
            40, --(number) height of the label (defaults to unlimited, text is not clipped)
            16, --(number) font size (default specific to client)
            "left" --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )

    this:OpenDynamicWindow(dynamicWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "GroupWindow", function(user, button)
    if(button == nil or button == "") then
        this:CloseDynamicWindow("GroupWindow")
        return
    end

    local groupId = GetGroupId(this)
    if ( groupId == nil ) then
        this:SystemMessage("You are not in a group.", "info")
        CleanUp()
        return
    end

    if ( this:HasTimer("AnitSpamTimer") ) then
        return
    end
    this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(500), "AnitSpamTimer")

    if ( button == "GroupLeave" ) then
        GroupRemoveMember(groupId, this)
    elseif ( GetGroupVar(groupId, "Leader") == this ) then
        if ( button == "GroupLoot") then
            local current = 1
            local loot = GetGroupVar(groupId, "Loot") or "FreeForAll"
            for i=1,#GroupLootStrategies do
                if ( GroupLootStrategies[i] == loot ) then
                    current = i
                end
            end
            local next = current + 1
            if ( next > #GroupLootStrategies ) then
                next = 1
            end
            SetGroupLoot(groupId, GroupLootStrategies[next])
        elseif ( button == "GroupKarma" ) then
            SetGroupKarma(groupId, true)
        else
            local option, arg = string.match(button, "(.+)|(.+)")
            if ( option == "Select" ) then
                mContextMember = GameObj(tonumber(arg))
                if(mContextMember ~= this) then
                    this:OpenCustomContextMenu("GroupOptions","Action",{"Promote","Kick"})
                end
            elseif ( option == "Kick" ) then
                local member = GameObj(tonumber(arg))
                GroupRemoveMember(groupId, member)
            elseif ( option == "Promote" ) then
                local member = GameObj(tonumber(arg))
                SetGroupLeader(groupId, member)
            else
                CleanUp()
            end
        end    
    end
end)

RegisterEventHandler(EventType.Message, "GroupUpdate", function()
    GroupWindow()
end)

RegisterEventHandler(EventType.Message, "GroupClose", function()
    CallFunctionDelayed(TimeSpan.FromMilliseconds(1), CleanUp)
end)

RegisterEventHandler(EventType.ContextMenuResponse,"GroupOptions",
    function(user,optionStr)
        local groupId = GetGroupId(this)

        if ( groupId and mContextMember and GetGroupVar(groupId, "Leader") == this ) then
            if(optionStr == "Promote") then
                SetGroupLeader(groupId, mContextMember)
            elseif ( optionStr == "Kick" ) then
                GroupRemoveMember(groupId, mContextMember)          
            end
        end
    end)

GroupWindow()