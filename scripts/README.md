# Shards Online - Lua Scripting

This document describes the structure of /rundir/scripts and the conventions adopted by the development team and modding community.

## Style Guide

Shards Online adopts the standard Lua style-guide available here: http://lua-users.org/wiki/LuaStyleGuide

## Design Patterns and Sample Code Structures

The Lua community has many examples of re-usable modules and design patterns that make the Lua code consistent, secure, and easy to use: http://lua-users.org/wiki/SampleCode
