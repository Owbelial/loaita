require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript",
	function()
		this:DelModule("en_balance")
	end) 

RegisterSingleEventHandler(EventType.ModuleAttached, "en_balance", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)
		
		if(enName ~= "Balance") then return end
		AlterEquipmentBonusStat(this, "BonusSpeedOffset", -.1)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)