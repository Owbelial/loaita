
--- List of all possible Mobile Mods, 
-- using any of these options you can modify the status of a mobile, like buff them with strength, or debuff them with less wisdom, or freeze them in place, etc..
-- they are stored in the local memory space of base_mobile so when each mod is changed there is zero IO interaction, this also means they do not persist.

-- helpers/mobile.lua contains a list of what stat (if any) to recalculate when any of these change


-- incase you're wondering why there is a Plus and Times for each mod; it's very optimized to grab the specific static table by name in all the dozens of places
	-- we call GetMobileMod(), and it's one less parameter to SetMobileMod() so that's a plus.  - KH
MobileMod = {
	-- stats
	AccuracyPlus = { },
	AccuracyTimes = { },
	AgilityPlus = { },
	AgilityTimes = { },
	AttackPlus = { },
	AttackTimes = { },
	ConstitutionPlus = { },
	ConstitutionTimes = { },
	CritChancePlus = { },
	CritChanceTimes = { },
	DefensePlus = { },
	DefenseTimes = { },
	EvasionPlus = { },
	EvasionTimes = { },
	ForcePlus = { },
	ForceTimes = { },
	IntelligencePlus = { },
	IntelligenceTimes = { },
	PowerPlus = { },
	PowerTimes = { },
	StrengthPlus = { },
	StrengthTimes = { },
	WillPlus = { },
	WillTimes = { },
	WisdomPlus = { },
	WisdomTimes = { },

	-- regen stats
	MaxHealthPlus = { },
	MaxHealthTimes = { },
	MaxManaPlus = { },
	MaxManaTimes = { },
	MaxStaminaPlus = { },
	MaxStaminaTimes = { },
	MaxVitalityPlus = { },
	MaxVitalityTimes = { },

	-- regen stats rates
	HealthRegenPlus = { },
	HealthRegenTimes = { },
	ManaRegenPlus = { },
	ManaRegenTimes = { },
	StaminaRegenPlus = { },
	StaminaRegenTimes = { },
	VitalityRegenPlus = { },
	VitalityRegenTimes = { },

	-- other stats
	MoveSpeedPlus = { },
	MoveSpeedTimes = { },
	MountMoveSpeedPlus = { },
	MountMoveSpeedTimes = { },
	-- damage
	MagicDamageTakenPlus = { },
	MagicDamageTakenTimes = { },
	PhysicalDamageTakenPlus = { }, -- This should probably be removed when bow/bashing/slashing is implemented?
	PhysicalDamageTakenTimes = { }, -- This should probably be removed when bow/bashing/slashing is implemented?

	--healing
	HealingReceivedPlus = { },
	HealingReceivedTimes = { },

	BowDamageTakenPlus = { }, -- !!not implemented!!
	BowDamageTakenTimes = { }, -- !!not implemented!!
	BashingDamageTakenPlus = { },  -- !!not implemented!!
	BashingDamageTakenTimes = { },  -- !!not implemented!!
	PiercingDamageTakenPlus = { },  -- !!not implemented!!
	PiercingDamageTakenTimes = { },  -- !!not implemented!!
	SlashingDamageTakenPlus = { },  -- !!not implemented!!
	SlashingDamageTakenTimes = { },  -- !!not implemented!!

	-- movement restriction
	Freeze = { }, -- stop movement, stop spin, allow items/combat
    Disable = { }, -- stop movement, stop spin, disallow items/combat
	Busy = { }, -- disallow items/combat, no change to movement
	Root = { }, -- !!not implemented!! stop movement, allow spin, allow items/combat
}

function HandleMobileMod(modName, modId, modValue)
	if ( MobileMod[modName] == nil ) then
		LuaDebugCallStack("[Invalid MobileMod] Provided modName "..modName.." is invalid.")
		return
	end
	--DebugMessage("HandleMobileMod", modName, modId, tostring(modValue))

	-- adding or removing we clear any existing expires.
	ClearMobileModExpire(modName, modId)
	
	if ( MobileMod[modName] ~= nil ) then
		-- prevent unneccessary cpu cycles if nothing changed.
		if ( MobileMod[modName][modId] == modValue ) then
			return
		end

		-- save the data of this mobile mod (memory only)
		MobileMod[modName][modId] = modValue

		if ( MobileModRecalculateStat[modName] ~= nil ) then
			MarkStatsDirty(MobileModRecalculateStat[modName])
		end

		if ( modName == "Freeze" or modName == "Disable" or modName == "Busy" ) then
			-- force value to be either true or false
			if ( modValue ~= true ) then modValue = false end
            ApplyModMoveLock(modName, modId, modValue)
		end
	end
end

function ApplyModMoveLock(name, id, val)

    local freezes = false
	local disables = false
	
	for i,v in pairs(MobileMod.Disable) do
		if ( v == true and i ~= id ) then
			freezes = true
			disables = true
			break
		end
	end

	if ( freezes == false ) then
		-- check all Freeze mods that only use SetMobileFrozen
		for i,v in pairs(MobileMod.Freeze) do
			if ( v == true and i ~= id ) then freezes = true break end
		end
	end
	
	if ( disables == false ) then
		-- check all Busy mods that only use Disabled objVar
		for i,v in pairs(MobileMod.Busy) do
			if ( v == true and i ~= id ) then disables = true break end
		end
	end

    -- if no effects are present that effect freeze, then since this is the only lock, we can do exactly as asked.
    if ( freezes == false ) then
        this:SetMobileFrozen(val,val) -- era val,val
    end

	-- if the mod is Disable or Busy and there are no other disables, do exactly as asked.
	if ( (name == "Disable" or name == "Busy") and disables == false ) then
		if ( val ) then
			this:SetObjVar("Disabled", true)
		else
			this:DelObjVar("Disabled")
		end
	end
end

function ClearMobileModExpire(modName, modId)
	local timer = modName..modId.."Expire"
	if ( this:HasTimer(timer) ) then
		UnregisterEventHandler("base_mobile_mods", EventType.Timer, timer)
		this:RemoveTimer(timer)
	end
end

RegisterEventHandler(EventType.Message, "MobileMod", HandleMobileMod)

RegisterEventHandler(EventType.Message, "MobileModExpire", function(modName, modId, modValue, modExpire)
	-- apply the mod
	HandleMobileMod(modName, modId, modValue)
	local timer = modName..modId.."Expire"
	-- handle the expire
	RegisterSingleEventHandler(EventType.Timer, timer, function()
		HandleMobileMod(modName, modId, nil)
	end)
	-- make it expire eventually.
	this:ScheduleTimerDelay(modExpire, timer)
end)
