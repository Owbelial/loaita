require 'base_ai_mob'
require 'base_ai_casting'
require 'incl_regions'
require 'base_ai_intelligent'
require 'base_ai_conversation'
require 'incl_faction'
require 'incl_run_path'
--require 'base_ai_sleeping'

AI.Settings.Debug = false
-- set charge speed and attack range in combat ai

AI.Settings.AggroRange = 10.0
AI.Settings.ChaseRange = 10.0
AI.Settings.LeashDistance = 20
AI.Settings.CanConverse = true
AI.Settings.ScaleToAge = false
AI.Settings.CanWander = false
AI.Settings.CanUseCombatAbilities = true
AI.Settings.CanCast = true
AI.Settings.ChanceToNotAttackOnAlert = 2
AI.Settings.SpeechTable = "Cultist"

if (this:GetObjVar("controller") ~= nil) then
    AI.Settings.CanWander = true
end
function ShouldPursue(target)
    if target:IsInRegion("LowerRuins") then return false end
    return true
end
--Function that determine's what team I'm on. Override this for custom behaviour.
function IsFriend(target)
    if (not AI.IsValidTarget(target)) then return true end
    
    --Override if this is my "target"
    --DebugMessage(1)
    if (AI.InAggroList(target)) then
        return false
    end

    --DebugMessage(2)
    if (target == nil) then
        DebugMessage("[ai_cultist|IsFriend] ERROR: target is nil")
        return true
    end

    if (target:HasObjVar("controller")) then
        --DebugMessage("Controller is "..target:GetObjVar("controller"):GetName())
        return IsFriend(target:GetObjVar("controller"))
    end

    local otherTeam = target:GetObjVar("MobileTeamType")
    local myTeam = this:GetObjVar("MobileTeamType")
    local targetFaction = GetFaction(target,this:GetObjVar("MobileTeamType"))
    if (targetFaction ~= nil and myTeam ~= otherTeam) then 
        --DebugMessage("A")
        if (targetFaction < 0) then 
            return false
        end
        if (target:IsInRegion("TopRuins") and targetFaction < 10 ) then
            return false
        end
        if (target:IsInRegion("RuinsThroneRoom") and targetFaction < 40 ) then
            --DebugMessage("TargetFaction =",targetFaction)
            return false
        end
        if (target:IsInRegion("LowerRuins")) then
            return true
        end
        if (targetFaction > 5) then 
            return true 
        end
    elseif (myTeam ~= otherTeam) then
        --DebugMessage("B")
        if (target:IsInRegion("TopRuins") ) then
            return false
        end
        if (target:IsInRegion("RuinsThroneRoom") ) then
            return false
        end
        if (target:IsInRegion("LowerRuins")) then
            return true
        end
    end
    --DebugMessage(3)
    if (target:HasObjVar("RuinsEntryBribe") ) then return true end

   --DebugMessage(4)
    if (target:HasModule("ai_slave") and not target:HasObjVar("Escaping")) then
        return true
    elseif (target:HasObjVar("Escaping")) then
        return false 
    end

   --DebugMessage(5)
    if (myTeam == nil) then --If I have no team, then attack everyone!
        DebugMessageA(this,"NO TEAM")
        return false
    end

    if (this:HasObjVar("NaturalEnemy") ~= nil)  then
        if (this:GetObjVar("NaturalEnemy") == otherTeam and otherTeam ~= nil) then
            return false
        end
    end
   --DebugMessage(6)
    if (target:GetMobileType() == "Animal" ) then --Animals don't usually attack animals
        if (this:DistanceFrom(target) < AI.Settings.AggroRange or math.random(AI.GetSetting("AggroChanceAnimals")) == 1) then            
            --AI.AddThreat(damager,-1)--Don't aggro them
            return (myTeam == otherTeam) 
        else
            return true
        end
    end
    if (otherTeam == nil) then
        return false
    end
   --DebugMessage(7)
    return (myTeam == otherTeam) --Return true if they have the same team, false if not.
end

AI.StateMachine.AllStates.Dead = {
        OnEnterState = function()   
           -- --DebugMessage("Dead start")   
            local campController = this:GetObjVar("controller")
            if( campController ~= nil ) then
                campController:SendMessage("cultist_death",this)
            end    
        end,

        OnExitState = function()

        end,
    }

AI.StateMachine.AllStates.Alert = {       
        OnEnterState = function() 
            this:PlayAnimation("alarmed")
            
            alertTarget = FindAITarget()
            if( alertTarget == nil ) then
               DebugMessageA(this,"Changing to idle from alert")
                AI.StateMachine.ChangeState("Idle")
            else
                FaceObject(this,alertTarget)
            end
            ----AI.StateMachine.--DebugMessage("ENTER ALERT")
        end,

        GetPulseFrequencyMS = function() return 1700 end,

        AiPulse = function()    
            --AI.StateMachine.--DebugMessage("Alert pulse")
            --this:NpcSpeech("[f70a79]*Alert!*[-]")
            alertTarget = FindAITarget()
            if( alertTarget == nil ) then
               DebugMessageA(this,"Changing to idle from alert")
                AI.StateMachine.ChangeState("Idle")
            else
                FaceObject(this,alertTarget)
                --We found a new mobile, handle it

                --if I should attack then
                local shouldAttack = true --should I attack
                local shouldAttackAnyway = true
                local chanceAttackAnyway = 10 --should I attack anyway
                local targetFaction = GetFaction(alertTarget,this:GetObjVar("MobileTeamType"))

                --if I'm unencountered attack
                if (targetFaction == nil) then
                    DebugMessageA(this,"targetFaction is nil")
                    targetFaction = 0
                end

                --if my faction chance is less than ten then increase chance of randomized attack
                if (targetFaction <= 10) then
                    DebugMessageA(this,"randomizng")
                    chanceAttackAnyway = 300*targetFaction
                else 
                    chanceAttackAnyway = 1000
                end

                if (targetFaction == 0) then
                    DebugMessageA(this,"newly encountered")
                    chanceAttackAnyway = 100
                end

                --don't attack in ruins
                local regions = GetRegionsAtLoc(this:GetLoc())
                for i,j in pairs(regions) do
                    if j == "CultistRuins" or j == "LowerRuins" or j == "UpperRuins" then
                        DebugMessageA(this,"In ruins")
                        shouldAttack = false
                    end
                end
                for i,j in pairs(regions) do
                    if (j == "TopRuins" and targetFaction <= 10) then
                        DebugMessageA(this,"In top ruins")
                        shouldAttack = true
                    end
                end

                --if I'm an enemy of the cult attack
                if (targetFaction < 0) then
                    DebugMessageA(this,"Enemy of teh cult")
                    shouldAttack = true
                end
                --if I'm a member of the cult don't attack
                if (targetFaction > 10) then
                    DebugMessageA(this,"Member of teh cult")
                    shouldAttack = false
                    shouldAttackAnyway = false
                end

                for i,j in pairs(regions) do
                    if (j == "RuinsThroneRoom" and targetFaction <= 50) then
                        DebugMessageA(this,"In throne room")
                        shouldAttack = true
                    end
                end

                DebugMessageA(this,"CultistFavorability is " .. tostring(targetFaction))
                DebugMessageA(this,"shouldAttackAnyway is " .. tostring(shouldAttackAnyway))
                DebugMessageA(this,"shouldAttack is "..tostring(shouldAttack))
                DebugMessageA(this,"chanceAttackAnyway is "..tostring(chanceAttackAnyway))

                --I treat it as if it's something I should attack
                if (shouldAttack) then
                    if (math.random(1,AI.GetSetting("ChanceToNotAttackOnAlert")) == 1  or alertTarget:DistanceFrom(this) < AI.GetSetting("AggroRange")) then
                        if (not IsFriend(alertTarget)) then
                            AttackEnemy(alertTarget)
                        else --not a threat, go to idle
                            AI.StateMachine.ChangeState("Idle")
                        end
                    end
                else --I'm inside the ruins or I have a bit of cultist faction
                    if (shouldAttackAnyway) then
                        --if (math.random(1,chanceAttackAnyway) == 1 ) then
                        --     if (not IsFriend(alertTarget)) then --I'm going to bop you to show you a thing or two
                        --        AttackEnemy(alertTarget)
                        --    else --not a threat, go to idle
                        --        AI.StateMachine.ChangeState("Idle")
                        --    end
                        --else
                        if (math.random(1,chanceAttackAnyway/3) == 1 ) then --three times more likely to insult you rather than attack
                            InsultTarget(alertTarget)
                        end
                        --end
                    else --greeting a member of the cult
                        if math.random(1,AI.GetSetting("ChanceToNotAttackOnAlert")) == 1 then
                            this:NpcSpeech("Hau-Kohmen, bretheren.")
                            AI.IdleTarget = alertTarget
                            AI.StateMachine.ChangeState("Converse")
                        else
                            AI.StateMachine.ChangeState("Idle")
                        end
                    end
                end
            end
        end,
    }

--Have them leash only if they have a home location
if (initializer ~= nil ) then
    if( initializer.CultistNamed ~= nil ) then    
        local name = initializer.CultistNamed[math.random(#initializer.CultistNamed)]
        this:SetName(name.." the "..this:GetName())
    end
    
    if( this:HasObjVar("homeLoc")) then
        AI.Settings.Leash = true
        AI.Settings.StationedLeash = true
        --DebugMessage("Has HomeLoc")
        -- set home facing
        local campController = this:GetObjVar("controller")
        if( campController ~= nil ) then
            --DebugMessage("Has controller")
            local facing = this:GetLoc():YAngleTo(campController:GetLoc())
            this:SetObjVar("homeFacing", facing)
        end
    end
    if( this:HasObjVar("homeRegion")) then
        AI.SetSetting("CanWander",true)
        --DebugMessage("Wanders")
    else
        AI.Settings.Leash = true  
        AI.Settings.StationedLeash = true
        AI.SetSetting("CanWander",false)     
        --DebugMessage("Doesn't wander") 
    end
end

function IsInRuins()
    local inRuins = false
    local regions = GetRegionsAtLoc(this:GetLoc())
    for i,j in pairs(regions) do
        if j == "CultistRuins" then
            inRuins = true
        end
    end
    return inRuins
end

if (IsInRuins()) then
    path = GetPath("CultistGuardPath")
else
    this:SetObjVar("DoesNotNeedPath",true)
end

--On the target coming into range have a chance to alert to his presense
RegisterEventHandler(EventType.EnterView, "chaseRange", 
    function (mobileObj)
        if (not IsInRuins()) then return end
        if (math.random(1,5) <= 4) then return end
        if (AI.IsValidTarget(mobileObj)) then
            local otherTeam = mobileObj:GetObjVar("MobileTeamType")
            local myTeam = this:GetObjVar("MobileTeamType")
            if (myTeam ~= otherTeam) then
                this:SendMessage("AlertEnemy",mobileObj)
            end
        end
    end)

--reduce faction for damaging cultists
--When I get hit.
RegisterEventHandler(EventType.Message, "DamageInflicted", 
function (damager,damageAmt)    
    if (damager == nil or not damager:IsValid()) then return end
    if (IsFriend(damager) and AI.Anger < 100) then return end
    --if I'm outside the ruins don't change faction
    damager:DelObjVar("RuinsEntryBribe")
        --but attack anyone that attack's my bretheren
    local nearbyCultists = FindObjects(SearchMulti(
    {
        SearchMobileInRange(AI.GetSetting("ChaseRange")), --in 10 units
        SearchObjVar("MobileTeamType","Cultists"), --find cultists
    }))
    for i,j in pairs (nearbyCultists) do
        if (not IsInCombat(j) and not j:HasTimer("RecentlyAlerted") and not j:HasModule("ai_slave")) then
            j:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"RecentlyAlerted")
            --DebugMessage("Sending message to j")
                --DebugMessage("attacking enemy")
            j:SendMessage("AttackEnemy",damager,true) --defend me
        end
    end
    --end
    if (damager:HasObjVar("controller")) then
        damager = damager:GetObjVar("controller")
    end
    if (IsInRuins()) then
        if (damager ~= nil and damager:IsValid()) then
            local targetFaction = GetFaction(alertTarget,this:GetObjVar("MobileTeamType")) 

            if (targetFaction == nil) then targetFaction = 0 end
     

            --bottoms out at -30
            if (targetFaction < -30) then
                targetFaction = -30
            end
            DebugMessageA(this,"CultistFavorability is "..targetFaction)
            ChangeFactionByAmount(damager,-4,this:GetObjVar("MobileTeamType"))
        end
    end
end)
--reduce faction for killing cultists
RegisterEventHandler(EventType.Message,"HasDiedMessage",
    function (killer)

    if (killer ~= nil and killer:IsValid()) then

        --if I'm outside the ruins don't change faction
        if (not IsInRuins()) then
            return
        end

        local targetFaction = GetFaction(alertTarget,this:GetObjVar("MobileTeamType"))  or 0
        --bottoms out at -30
        if (targetFaction < -30) then
            targetFaction = -30
        end
        if (targetFaction == nil) then targetFaction = 0 end

        targetFaction = targetFaction - 10  
        DebugMessageA(this,"CultistFavorability is "..targetFaction)
        ChangeFactionByAmount(killer,-10)
    end
end)
