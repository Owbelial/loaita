
local guideObj = nil
local queuedEvent = nil
local isDismissing = false

function IsGuideSummoned()
	return guideObj and not(guideObj:ContainedBy())
end

function UpdateGuideWindow()
	local summonGuideWindow = DynamicWindow("GuideButton","",30,30,-180,144,"Transparent","TopRight")
	-- todo make it pulsing
	local buttonState = ""
	if(IsGuideSummoned()) then
		buttonState = "pressed"
	elseif(queuedEvent) then
		buttonState = "activated"
	end

	summonGuideWindow:AddButton(0,0,"GuideButton","",40,40,"Summon or dismiss your magical guide.\n\nYour magical guide can help you find places and give you tips as you explore this new land. When your guide has something to tell you this button will pulse.",nil,false,"HelpButton",buttonState)
	this:OpenDynamicWindow(summonGuideWindow)
end

function InitializeGuide()
	guideObj = this:GetObjVar("MagicalGuideObj")
	if not(guideObj) or not(guideObj:IsValid()) then
		local spawnLoc = GetNearbyPassableLoc(this,360,2,4)
		CreateObj("npe_magical_guide",spawnLoc,"guide_created",this)
	end
end

function OnNPELoad()
	if( (this:GetObjVar("InitiateMinutes") or 0) < 1 ) then
		this:DelModule("npe_player")
		return false
	end

	InitializeGuide()

	UpdateGuideWindow()
end

function DoTriggerMessage(eventId,messageIndex)
	if(this:HasTimer("MessageExtended")) then
		this:RemoveTimer("MessageExtended")
	end

	messageIndex = messageIndex or 1

	local eventData = MagicalGuideEvents[eventId]
	guideObj:NpcSpeechToUser(eventData.Messages[messageIndex],this,"event")
	guideObj:SendMessage("RefreshAutoDimissTimer")
	if(#eventData.Messages > messageIndex) then
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(4),"MessageExtended",eventId,messageIndex+1)
	end
end
RegisterEventHandler(EventType.Timer,"MessageExtended", function(...) DoTriggerMessage(...)	end)

function DoEventTrigger(eventId,eventLoc)
	if not(IsGuideSummoned()) then
		eventLoc = eventLoc or this:GetLoc()
		queuedEvent = { Id = eventId, Loc = eventLoc, Time = DateTime.UtcNow }
		UpdateGuideWindow()

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"CheckQueuedEvent")
	else
		DoTriggerMessage(eventId)
	end
end

function DoQueuedEvent()
	if(IsGuideSummoned() and not(isDismissing) and queuedEvent) then 
		DoEventTrigger(queuedEvent.Id)
	end
	queuedEvent = nil
	UpdateGuideWindow()
end

RegisterEventHandler(EventType.Message,"DoGuideEvent",
	function (eventId,eventLoc)
		DoEventTrigger(eventId,eventLoc)
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		OnNPELoad()
	end)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function ( ... )		
		if ( ServerSettings.NewPlayer.InitiateSystemEnabled ) then
			this:SetObjVar("InitiateMinutes", ServerSettings.NewPlayer.InitiateDurationMinutes)
		end

		OnNPELoad()

		local dialogTitle = "Where am I?"
		local dialogText = "You awaken to find yourself in the strangest of places. You have no memory of how you got here. Or even of who you were in your former life. All you can remember is a blinding flash of light... and your own name. \n"
		local response = {{text="Ok.",handle="Ok"}}
		NPCInteraction(dialogText,nil,this,"NewbieWelcome",response,dialogTitle)		
	end)

RegisterEventHandler(EventType.DynamicWindowResponse, "NewbieWelcome",
	function ( ... )
		DoEventTrigger("Welcome")

		this:CloseDynamicWindow("NewbieWelcome")
	end)

RegisterEventHandler(EventType.DynamicWindowResponse, "GuideButton",
	function (user,buttonId)
		if(isDismissing) then return end 

		if(buttonId == "GuideButton") then
			if(IsGuideSummoned()) then
				guideObj:SendMessage("Interact")
			else
				guideObj:SendMessage("Summon")				
				if(queuedEvent) then
					this:RemoveTimer("CheckQueuedEvent")
					CallFunctionDelayed(TimeSpan.FromSeconds(1),
						function() 
							DoQueuedEvent()							
						end)
				else
					-- DAB THREADING: this only works because send message is executed immediately
					UpdateGuideWindow()
				end
			end
		end
	end)

RegisterEventHandler(EventType.CreatedObject, "guide_created",
	function (success,objRef)
		if(success) then
			guideObj = objRef
			guideObj:SendMessage("SetOwner",this)
			this:SetObjVar("MagicalGuideObj",guideObj)

			UpdateGuideWindow()
		end
	end)

RegisterEventHandler(EventType.Timer,"CheckQueuedEvent",
	function ()
		if((DateTime.UtcNow - queuedEvent.Time > ServerSettings.NewPlayer.MagicalGuideEventTimeout)
				or (this:GetLoc():Distance(queuedEvent.Loc) > ServerSettings.NewPlayer.MagicalGuideEventMaxDistance)) then
			queuedEvent = nil
			UpdateGuideWindow()
		else
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"CheckQueuedEvent")
		end
	end)

RegisterEventHandler(EventType.EnterView,"WorldTriggerView",
	function (targetObj)
		local eventId = targetObj:GetObjVar("GuideEventId")		
		
		DoEventTrigger(eventId,targetObj:GetLoc())
	end)

RegisterEventHandler(EventType.Message,"GuideDismissed",
	function ( ... )
		UpdateGuideWindow()
	end)

function EndNPE()	
	RemoveMapMarker(this,"GuideWaypoint")

	this:DelObjVar("InitiateMinutes")

	guideObj:Destroy()
	this:DelObjVar("MagicalGuideObj")
	this:CloseDynamicWindow("GuideButton")

	this:SendMessage("UpdateName")

	this:DelModule("npe_player")
end

RegisterEventHandler(EventType.Message,"SetGuideWaypoint",
	function (user,waypointName,waypointLoc,waypointObj)
		local mapMarker = {Map="NewCelador", Icon="marker_circle1", Location=waypointLoc, Obj=waypointObj, Tooltip=waypointName, RemoveDistance=5}
		AddMapMarker(user,mapMarker,"GuideWaypoint")
		user:FireTimer("UpdateMapMarkers")
	end)

RegisterEventHandler(EventType.Message,"EndNPE",
	function ( ... )
        ClientDialog.Show{
            TargetUser = this,
            DialogId = "InitiateExpired",
            TitleStr = "Protection Expired",
            DescStr = "[$1945]",
            Button1Str = "Ok",
        }        

		isDismissing = true

		CallFunctionDelayed(TimeSpan.FromSeconds(2),function()
			EndNPE()
		end)	
	end)

-- we put the view on the player because we want it to fire even when the guide is dismissed
AddView("WorldTriggerView",SearchHasObjVar("GuideEventId",5))

this:RemoveTimer("CheckQueuedEvent")