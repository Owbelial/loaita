MobileEffectLibrary.PotionCure = 
{

	OnEnterState = function(self,root,target,args)
	
		if ( self.ParentObj:HasTimer("PotionCure") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		
		if not( IsPoisoned(self.ParentObj) ) then
			self.ParentObj:SystemMessage("You are not poisoned.", "info")
			EndMobileEffect(root)
			return false
		end
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(5.0), "PotionCure")
		AddBuffIcon(self.ParentObj,"PotionCure","Potion Cure Cooldown","Poison Splash","Cannot cure again yet",true,5)

		self.ParentObj:PlayEffect("HealEffect")

		self.ParentObj:SendMessage("CurePoison")

		self.ParentObj:SystemMessage("You have been cured.", "info")

		EndMobileEffect(root)
	end,

}