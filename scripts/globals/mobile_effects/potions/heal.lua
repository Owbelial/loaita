MobileEffectLibrary.PotionHeal = 
{

	OnEnterState = function(self,root,target,args)
		self.Amount = args.Amount or self.Amount
		if ( self.ParentObj:HasTimer("RecentHeal") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		
		if ( GetCurHealth(self.ParentObj) >= GetMaxHealth(self.ParentObj) ) then
			self.ParentObj:SystemMessage("You seem fine.", "info")
			EndMobileEffect(root)
			return false
		end
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromMinutes(4), "RecentHeal")

		self.ParentObj:PlayEffect("HealEffect")

		self.ParentObj:SendMessage("HealRequest", self.Amount, target, true)
		AddBuffIcon(self.ParentObj,"PotionHeal","Potion Heal Cooldown","restore","Cannot use again yet",true,240)
		EndMobileEffect(root)
	end,

	Amount = 1,
}