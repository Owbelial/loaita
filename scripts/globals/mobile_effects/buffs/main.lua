-- general buffs
require 'globals.mobile_effects.buffs.immune'
require 'globals.mobile_effects.buffs.stun_immune'
require 'globals.mobile_effects.buffs.heal_over_time'

-- player buffs
require 'globals.mobile_effects.buffs.player.vitality_hearth'
require 'globals.mobile_effects.buffs.player.campfire'
require 'globals.mobile_effects.buffs.player.tinderbox'

-- field mage
require 'globals.mobile_effects.buffs.fieldmage.stasis'
require 'globals.mobile_effects.buffs.fieldmage.empower'
require 'globals.mobile_effects.buffs.fieldmage.empoweraoe'

-- gladiator
require 'globals.mobile_effects.buffs.gladiator.pursuit' -- works
require 'globals.mobile_effects.buffs.gladiator.noremorse' -- works
require 'globals.mobile_effects.buffs.gladiator.stunstrike' -- works
-- hamstring is general debuff -- works

-- knight
require 'globals.mobile_effects.buffs.knight.shieldbash' -- works
require 'globals.mobile_effects.buffs.knight.heroism' -- works
require 'globals.mobile_effects.buffs.knight.righteousweapon' -- works
require 'globals.mobile_effects.buffs.knight.vanguard' -- works
require 'globals.mobile_effects.buffs.knight.charge'

-- rogue
require 'globals.mobile_effects.buffs.rogue.dart' -- works
require 'globals.mobile_effects.buffs.rogue.evasion' -- works
require 'globals.mobile_effects.buffs.rogue.backstab'

-- scout
require 'globals.mobile_effects.buffs.scout.snipe' --works

-- sorcerer
require 'globals.mobile_effects.buffs.sorcerer.spellchamber' --works
require 'globals.mobile_effects.buffs.sorcerer.spellshield'
--require 'globals.mobile_effects.buffs.sorcerer.energyvortex' -- works
require 'globals.mobile_effects.buffs.sorcerer.destruction' -- needs testing

-- spells
require 'globals.mobile_effects.buffs.spells.attack'
require 'globals.mobile_effects.buffs.spells.defense'
require 'globals.mobile_effects.buffs.spells.recall'
require 'globals.mobile_effects.buffs.spells.summonportal'
