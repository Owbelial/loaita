MobileEffectLibrary.SummonPortal = 
{
	OnEnterState = function(self,root,target,args)
		if(GetKarmaLevel(GetKarma(self.ParentObj)).GuardHostilePlayer) then
			self.ParentObj:SystemMessage("You must attone for your negative actions before the gods will allow you to do this.","info")
						---- edit twotowers
		elseif GetRegionAddress() == "TwoTowers" then
			self.ParentObj:SystemMessage("This action is disabled here, use /exitarena.","info")
		---- edit twotowers
		elseif(target == self.ParentObj) then
			local bindLoc = GetPlayerSpawnPosition(self.ParentObj)
			local bindRegionAddress = GetRegionAddress()

			local spawnPosEntry = self.ParentObj:GetObjVar("SpawnPosition")
			if ( spawnPosEntry ~= nil ) then
				bindLoc = spawnPosEntry.Loc
				bindRegionAddress = spawnPosEntry.Region
			end

			self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"TeleportDelay")
			OpenRemoteTwoWayPortal(self.ParentObj:GetLoc(),bindLoc,bindRegionAddress,60)
		elseif(target:HasObjVar("Destination")) then
			local destRegionAddress = target:GetObjVar("RegionAddress")			
			local targetLoc = target:GetObjVar("Destination")

			self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"TeleportDelay")
			OpenRemoteOneWayPortal(self.ParentObj:GetLoc(),targetLoc,destRegionAddress,60)
		end

		EndMobileEffect(root)
		-- return false to prevent any timers from being started.
		return false

	end,
}