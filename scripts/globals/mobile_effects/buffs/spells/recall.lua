MobileEffectLibrary.Recall = 
{
	OnEnterState = function(self,root,target,args)
		if(GetKarmaLevel(GetKarma(self.ParentObj)).GuardHostilePlayer) then
			self.ParentObj:SystemMessage("You must attone for your negative actions before the gods will allow you to do this.","info")
						---- edit twotowers
		elseif GetRegionAddress() == "TwoTowers" then
			self.ParentObj:SystemMessage("This action is disabled here, use /exitarena.","info")
		---- edit twotowers
		elseif(target == self.ParentObj) then
			local bindLoc = GetPlayerSpawnPosition(self.ParentObj)
			local bindRegionAddress = GetRegionAddress()

			local spawnPosEntry = self.ParentObj:GetObjVar("SpawnPosition")
			if ( spawnPosEntry ~= nil ) then
				bindLoc = spawnPosEntry.Loc
				bindRegionAddress = spawnPosEntry.Region
			end

			-- send them home
			TeleportUser(self.ParentObj, self.ParentObj, bindLoc, bindRegionAddress, self.ParentObj:GetFacing())
		elseif(target:HasObjVar("Destination")) then			
			local destRegionAddress = target:GetObjVar("RegionAddress")			
			local targetLoc = target:GetObjVar("Destination")

			TeleportUser(self.ParentObj,self.ParentObj,target:GetObjVar("Destination"),target:GetObjVar("RegionAddress"),target:GetObjVar("DestinationFacing"))
		end

		EndMobileEffect(root)
		-- return false to prevent any timers from being started.
		return false

	end,
}