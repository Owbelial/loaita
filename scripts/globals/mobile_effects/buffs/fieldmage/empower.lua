MobileEffectLibrary.Empower = 
{
	PersistSession = true, -- so the arguments are saved and we can access them when we fire the spell.

	OnEnterState = function(self,root,target,args)
		self.Modifier = args.Modifier or self.Modifier

		self.PrestigeAbilityPosition = GetPrestigeAbilityPosition(self.ParentObj, "Empower")

		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "EmpowerBuff", "Empower", "Spectral Ball", "Next heal will also heal in an 8 yard radius for "..(self.Modifier*100).."%.")
			self.ParentObj:PlayEffect("DestructionBlueEffect",20.0,"Bone=Ground")
		end
	end,

	OnExitState = function(self,root)
		-- spell was fired (probably), start the real cooldown.
		StartPrestigePositionCooldown(self.ParentObj, self.PrestigeAbilityPosition, self.Cooldown)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "EmpowerBuff")
			self.ParentObj:StopEffect("DestructionBlueEffect")
			--self.ParentObj:StopEffect("FireballEffect")
		end
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(10),
	Cooldown = TimeSpan.FromMinutes(0.1),
	
	Modifier = 0.1,
}