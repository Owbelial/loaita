--[[
 Handles the AoE of Empower
]]

MobileEffectLibrary.EmpowerAoE = 
{

	OnEnterState = function(self,root,target,args)
		self.Heal = args.Heal or self.Heal
		local nearbyMobiles = FindObjects(SearchMobileInRange(self.Radius))
        for i,mobile in pairs (nearbyMobiles) do
        	if ( mobile ~= target and not ValidCombatTarget(target, mobile) ) then
				mobile:SendMessage("HealRequest", self.Heal, target)
				mobile:PlayEffect("Restoration")
        	end
        end
		EndMobileEffect(root)

	end,

	Heal = 1,
	Radius = 8
}