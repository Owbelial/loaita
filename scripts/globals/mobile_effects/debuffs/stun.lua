MobileEffectLibrary.Stun = 
{

	OnEnterState = function(self,root,target,args)
		args = args or {}

		if ( self.Resist(self,root) ) then 
			EndMobileEffect(root)
			return false
		end

		self._isPlayer = IsPlayerCharacter(self.ParentObj)

		-- always set this (other effects pass PlayerDuration as Duration)
		self.Duration = args.Duration or self.Duration
		if ( self._isPlayer ) then
			self.Duration = args.PlayerDuration or self.Duration
		end

		SpellCastInterrupt(self.ParentObj)

		SetMobileMod(self.ParentObj, "Disable", "Stun", true)

		self.ParentObj:NpcSpeech("[FF0000]*stunned*[-]", "combat")
		if ( self._isPlayer ) then
			AddBuffIcon(self.ParentObj, "DebuffStunned", "Stunned", "Force Push 02", "Cannot move or cast.", true)
		end
		self.ParentObj:PlayEffect("StunnedEffectObject")
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "Disable", "Stun", nil)

		self.ParentObj:SendMessage("ResetSwingTimer", 0)

		if ( self._isPlayer ) then
			RemoveBuffIcon(self.ParentObj, "DebuffStunned")
		end

		self.ParentObj:StopEffect("StunnedEffectObject")
	end,

	OnStack = function(self,root,target,args)
		if ( self.Resist(self,root) ) then return end
		local timerId = root.PulseId.."-"..root.CurStateName
		local duration = args.Duration or TimeSpan.FromSeconds(0.5)
		local timerDelay = self.ParentObj:GetTimerDelay(timerId)
		-- if the new stun applied has a duration greater than the duration that's remaining in the current stun
		if ( timerDelay ~= nil and duration.TotalSeconds > timerDelay.TotalSeconds ) then
			-- set timer to the duration of the new stun
			root.ParentObj:ScheduleTimerDelay(duration, timerId)
		end
	end,

	Resist = function(self,root)
		if Success(0.5*((GetWill(self.ParentObj) - ServerSettings.Stats.IndividualStatMin) / (ServerSettings.Stats.IndividualPlayerStatCap - ServerSettings.Stats.IndividualStatMin))) then
			self.ParentObj:NpcSpeech("[99ffbb]*resisted*[-]", "combat")
			return true
		end
		return false
	end,

	GetPulseFrequency = function(self,root) 
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(0.5),

	_isPlayer = false
}