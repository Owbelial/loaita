require 'globals.mobile_effects.debuffs.stun'
require 'globals.mobile_effects.debuffs.daze'
require 'globals.mobile_effects.debuffs.concus'
require 'globals.mobile_effects.debuffs.sunder'
require 'globals.mobile_effects.debuffs.hamstring'
require 'globals.mobile_effects.debuffs.eviscerate'
require 'globals.mobile_effects.debuffs.bleed'
require 'globals.mobile_effects.debuffs.dismount'
require 'globals.mobile_effects.debuffs.poison'
require 'globals.mobile_effects.debuffs.scavengable'

-- player
require 'globals.mobile_effects.debuffs.player.hungry'
require 'globals.mobile_effects.debuffs.player.lowvitality'

--skills
require 'globals.mobile_effects.debuffs.skills.heavyarmordebuff'


-- field mage
require 'globals.mobile_effects.debuffs.fieldmage.silence'

-- sorcerer
require 'globals.mobile_effects.debuffs.sorcerer.destructionaoe'

-- mounts
require 'globals.mobile_effects.debuffs.dazeddebuff'
require 'globals.mobile_effects.debuffs.nomount'