

		--this old af bandages


-- this mobile effect is ran at the end of BandageBeginApply
MobileEffectLibrary.BandageApply = 
{

	OnEnterState = function(self,root,bandagee,duration,wasDead)				
		self.Bandagee = bandagee
		self.BandageDuration = duration or self.BandageDuration
		self.WasDead = wasDead

		self.ParentObj:SendMessage("BreakInvisEffect", "Bandage")

		-- prevent using bandages while stunned or whatever.
		if( IsMobileDisabled(self.ParentObj) ) then
			self.ParentObj:SystemMessage("Cannot use that right now.", "info")
			EndMobileEffect(root)
			return false
		end

		CheckKarmaBeneficialAction(self.ParentObj, bandagee)

		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(180), "BandageCooldownTimer")
		
		-- reset swing timer with no delay
		self.ParentObj:SendMessage("ResetSwingTimer", 0)

		-- if we aren't bandaging ourself we need to monitor range from target
		if(bandagee ~= self.ParentObj) then
			RegisterEventHandler(EventType.LeaveView,"EffectRange", function()
				EndMobileEffect(root)
			end)
			AddView("EffectRange",SearchSingleObject(bandagee,SearchMobileInRange(SkillData.AllSkills.HealingSkill.Options.BandageRange)))
		end

		-- handle damage interrupting the heal
		RegisterEventHandler(EventType.Message, "DamageInflicted", function(damager, amount)
			self.ParentObj:SystemMessage("Bandage interrupted.", "info")
			EndMobileEffect(root)
		end)

		AddBuffIcon(self.Bandagee,"Bandage"..self.UniqueId,"Bandages","Cold Mastery","You are being bandaged.",true,self.BandageDuration.TotalSeconds)

	end,

	OnExitState = function(self,root)
		RemoveBuffIcon(self.Bandagee,"Bandage"..self.UniqueId)
		if(self.Bandagee ~= self.ParentObj) then
			DelView("EffectRange")
			UnregisterEventHandler("", EventType.LeaveView, "EffectRange")
		end
		UnregisterEventHandler("", EventType.Message, "DamageInflicted")		
	end,

	GetPulseFrequency = function(self,root) 
		return self.BandageDuration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
		BandageFinishApply(self.ParentObj, self.Bandagee, self.WasDead)
	end,

	Bandagee = nil,
	BandageDuration = TimeSpan.FromSeconds(0),
	WasDead = nil,
}

--- Begin the process of applying a bandage
-- @param bandager mobileObj applying bandage
-- @param bandagee mobileObj being bandaged
function BandageBeginApply(bandager, bandagee)
	if ( bandager:HasTimer("BandageCooldownTimer") ) then
		bandager:SystemMessage("You must wait "..TimeSpanToWords(bandager:GetTimerDelay("BandageCooldownTimer")).." to use again.")
		return
	end

	if ( GetCurHealth(bandagee) >= GetMaxHealth(bandagee) and not IsDead(bandagee) and not IsPoisoned(bandagee) ) then
		bandager:SystemMessage("The patient seems just fine.", "info")
		return
	end

	if not( ValidateRangeWithError(SkillData.AllSkills.HealingSkill.Options.BandageRange, bandager, bandagee, "Not close enough to heal that target.") ) then
		return
	end

	local isDead = IsDead(bandagee)

	local firstaidSkillName = nil
	if ( bandagee:IsPlayer() ) then
		firstaidSkillName = "HealingSkill"
	end
	if ( IsTamedPet(bandagee) ) then
		firstaidSkillName = "VeterinarySkill"
	end
	-- disallow healing anything but players and player's pets.
	if ( firstaidSkillName == nil ) then
		return
	end

	local firstaidSkill = GetSkillLevel(bandager, firstaidSkillName)

	-- determine how long the heal will take
	local healTime = 0
	local agi = GetAgi(bandager) or 10
	-- check for dead so resurrect takes longer
	if ( isDead or bandager == bandagee ) then
		healTime = SkillData.AllSkills.HealingSkill.Options.HealTimeBaseSelf + (500 * ((120 - agi) / 10))
	else
		healTime = SkillData.AllSkills.HealingSkill.Options.HealTimeBaseOther - (500 * ((120 - agi) / 10))
	end

	healTime = math.max(healTime, SkillData.AllSkills.HealingSkill.Options.MinHealTime)

	if ( isDead ) then
		if ( firstaidSkill >= SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect ) then
			bandager:SystemMessage("Attempting to resurrect your target.", "info")
			if ( bandagee:IsPlayer() ) then
				bandagee:SystemMessage("" .. bandager:GetName() .. " is attempting to resurrect you.", "info")
			end
		else
			bandager:SystemMessage("Not skilled enough to resurrect this corpse.", "info")
			return
		end
	else
		if ( bandager == bandagee ) then
			--bandager:SystemMessage("You begin applying a bandage.")
		else
			bandager:SystemMessage("Applying a bandage to " .. bandagee:GetName() .. ".", "info")
			if ( bandagee:IsPlayer() ) then
				bandagee:SystemMessage("" .. bandager:GetName() .. " is applying a bandage to you.", "info")
			end
		end
		bandagee:PlayEffect("PotionHealEffect")
	end

	bandager:PlayAnimation("kneel")

	-- apply our state-machine ran mobile effect to allow for registering and un registering of our events, etc.
	bandager:SendMessage("StartMobileEffect", "BandageApply", bandagee, TimeSpan.FromMilliseconds(healTime), isDead)
end

--- Do the actual application of bandage, calling this directy is as if a bandage has completed being applied.
-- @param bandager mobileObj applying bandage
-- @param bandagee mobileObj being bandaged
-- @param wasDead boolean was bandagee dead when the bandaging started? (ie, was this a resurrect attempt)
function BandageFinishApply(bandager, bandagee, wasDead)
	-- verify bandagee still legit
	if( bandagee == nil or not(bandagee:IsValid()) or not(bandagee:IsMobile())) then
		return
	end

	if ( IsDead(bandager) ) then
		return
	end

	if not( ValidateRangeWithError(SkillData.AllSkills.HealingSkill.Options.BandageRange, bandager, bandagee, "No longer close enough to heal that target.", "Too far to continue being healed.") ) then
		return
	end

	local firstaidSkillName = nil
	if ( bandagee:IsPlayer() ) then
		firstaidSkillName = "HealingSkill"
	end
	if ( IsTamedPet(bandagee) ) then
		firstaidSkillName = "VeterinarySkill"
	end
	-- disallow healing anything but players and player's pets.
	if ( firstaidSkillName == nil ) then
		return
	end

	local skillDictionary = GetSkillDictionary(bandager)
	local firstaidSkill = GetSkillLevel(bandager, firstaidSkillName, skillDictionary)

	if ( wasDead and IsDead(bandagee) ) then
		if ( firstaidSkill >= SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect ) then
			if ( Success(0.25) ) then
				-- resurrect the corpse
				bandagee:SetObjVar("sp_resurrect_effectSource", bandager)
				bandagee:AddModule("sp_resurrect_effect")
				-- skill gain check
				CheckSkill(bandager, firstaidSkillName, SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect)
			else
				bandager:SystemMessage("Fail to stir the corpse.", "info")
			end
		else
			bandager:SystemMessage("Not skilled enough to resurrect this corpse.", "info")
		end
	elseif ( wasDead and not(IsDead(bandagee))) then
		bandager:SystemMessage("Your patient is already alive.", "info")
	else
		if ( IsDead(bandagee) ) then
			-- don't feel like we need a message? oh ok whatever.
			bandager:SystemMessage("The patient has died.", "info")
			return false
		end
		-- check for poison
		if ( IsPoisoned(bandagee) ) then
			local chance = ( firstaidSkill / 2 ) + ( firstaidSkill / 2 ) + 10.5
			if ( Success(chance/100) ) then
				bandagee:PlayEffect("PotionHealEffect",0,"Color=ff9900")
				bandagee:SendMessage("CurePoison")
				bandagee:NpcSpeech("[0000FF]*cured*[-]", "combat")
				if ( bandagee == bandager ) then
					if ( bandager:IsPlayer() ) then
						bandagee:SystemMessage("You are cured.", "info")
					end
				else
					if ( bandager:IsPlayer() ) then
						bandager:SystemMessage("You cured "..bandagee:GetName()..".", "info")
					end
					if ( bandagee:IsPlayer() ) then
						bandagee:SystemMessage(bandager:GetName().." has cured you.", "info")
					end
				end
			else
				if ( bandagee == bandager ) then
					if ( bandager:IsPlayer() ) then
						bandagee:SystemMessage("Failed to cure yourself.", "info")
					end
				else
					if ( bandager:IsPlayer() ) then
						bandager:SystemMessage("Failed to cure "..bandagee:GetName()..".", "info")
					end
					if ( bandagee:IsPlayer() ) then
						bandagee:SystemMessage(bandager:GetName().." failed to cure you.", "info")
					end
				end
			end
		else
			-- determine healing amount
			local minHeal = ( firstaidSkill / 2 ) + ( firstaidSkill / 2 )
			local maxHeal = ( firstaidSkill * 2 )
			local healAmount = math.random(minHeal, maxHeal)
			local finalHealValue = 0

			if ( Success(((firstaidSkill + 10) / 100)) ) then
				finalHealValue = math.floor(healAmount)
				if ( finalHealValue < minHeal ) then
					finalHealValue = minHeal
				end
				local bandageeCurHealth = GetCurHealth(bandagee)
				local bandageeMaxHealth = GetMaxHealth(bandagee)
				if ( bandageeCurHealth >= bandageeMaxHealth ) then
					bandager:SystemMessage("The patient seems just fine.", "info")
				else
					if ( bandageeCurHealth + finalHealValue >= bandageeMaxHealth ) then
						finalHealValue = bandageeMaxHealth - bandageeCurHealth
					end
					if ( finalHealValue < 1 ) then
						bandager:SystemMessage("The patient seems just fine.", "info")
					else
						finalHealValue = math.round(finalHealValue)
						bandagee:PlayEffect("PotionHealEffect",0)
						bandagee:SendMessage("Heal", finalHealValue, bandager)
						bandager:SystemMessage("Bandage has healed for " .. finalHealValue, "info")
					end
				end
			else
				bandager:SystemMessage("Bandage fails to have much effect.", "info")
				-- toss them a bone (too much bone)
				--bandagee:SendMessage("Heal", math.random(1,4), bandager)
			end
		end
		-- skill gain first aid
		CheckSkillChance(bandager, firstaidSkillName, firstaidSkill)
	end
end