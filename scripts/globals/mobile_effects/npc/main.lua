require 'globals.mobile_effects.npc.basic'

require 'globals.mobile_effects.npc.dragonfire'

require 'globals.mobile_effects.npc.cerberus.charge'
require 'globals.mobile_effects.npc.cerberus.poisonbreath'

require 'globals.mobile_effects.npc.death.voidteleport'
require 'globals.mobile_effects.npc.death.deathwave'