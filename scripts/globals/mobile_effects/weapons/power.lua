MobileEffectLibrary.Power = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.WeaponAttackModifier = args.WeaponAttackModifier or self.WeaponAttackModifier

		self._Weapon = GetPrimaryWeapon(self.ParentObj)
		if ( not self._Weapon or not target ) then
			EndMobileEffect(root)
			return false
		end

		self._AttackPlus = Weapon.GetStat(Weapon.GetType(self._Weapon), "Attack")
		if ( self._AttackPlus == nil ) then
			EndMobileEffect(root)
			return false
		end

		self.ParentObj:PlayEffect("BuffEffect_M")

		SetCombatMod(self.ParentObj, "AttackPlus", "Power", self._AttackPlus * self.WeaponAttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackPlus", "Power", nil)

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,

	Range = 20,

	AttackPlus = 10,
}