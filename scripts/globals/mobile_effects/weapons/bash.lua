MobileEffectLibrary.Bash = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.WeaponAttackModifier = args.WeaponAttackModifier or self.WeaponAttackModifier

		self._Weapon = GetPrimaryWeapon(self.ParentObj)
		if ( not self._Weapon or not target ) then
			EndMobileEffect(root)
			return false
		end

		self._AttackPlus = Weapon.GetStat(Weapon.GetType(self._Weapon), "Attack")
		if ( self._AttackPlus == nil ) then
			EndMobileEffect(root)
			return false
		end

		self.ParentObj:PlayAnimation("pummel")
		self.ParentObj:PlayEffect("BuffEffect_B")
		self.ParentObj:PlayObjectSound("Stab")

		SetCombatMod(self.ParentObj, "AttackPlus", "Bash", self._AttackPlus * self.WeaponAttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackPlus", "Bash", nil)

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,

	WeaponAttackModifier = 0,

	Range = 1,

	AttackPlus = 10,
}