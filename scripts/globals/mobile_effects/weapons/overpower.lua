MobileEffectLibrary.Overpower = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.WeaponAttackModifier = args.WeaponAttackModifier or self.WeaponAttackModifier

		self._Weapon = GetPrimaryWeapon(self.ParentObj)
		if ( not self._Weapon or not target ) then
			EndMobileEffect(root)
			return false
		end

		self._AttackPlus = Weapon.GetStat(Weapon.GetType(self._Weapon), "Attack")
		if ( self._AttackPlus == nil ) then
			EndMobileEffect(root)
			return false
		end

		self.ParentObj:PlayEffect("BuffEffect_L")
		target:PlayEffect("BloodEffect_A")

		SetCombatMod(self.ParentObj, "AttackPlus", "Overpower", self._AttackPlus * self.WeaponAttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackPlus", "Overpower", nil)

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,
	
	WeaponAttackModifier = 0.0,

	Range = 1,

	_AttackPlus = 0,
}