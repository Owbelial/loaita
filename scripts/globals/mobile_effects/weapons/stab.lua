MobileEffectLibrary.Stab = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.WeaponAttackModifier = args.WeaponAttackModifier or self.WeaponAttackModifier
		self.HitChanceFront = args.HitChanceFront or self.HitChanceFront
		self.HitChanceBack = args.HitChanceBack or self.HitChanceBack
		
		self.ParentObj:PlayAnimation("impale")
		self.ParentObj:PlayEffect("BuffEffect_K")

		self._Weapon = GetPrimaryWeapon(self.ParentObj)
		if ( not self._Weapon or not target ) then
			EndMobileEffect(root)
			return false
		end

		self._AttackPlus = Weapon.GetStat(Weapon.GetType(self._Weapon), "Attack")
		if ( self._AttackPlus == nil ) then
			EndMobileEffect(root)
			return false
		end

		self._HitChance = self.HitChanceFront
		if ( IsBehind(self.ParentObj, target, 60) ) then
			self._HitChance = self.HitChanceBack
		end

		if ( Success(self._HitChance) ) then
			self.ParentObj:PlayObjectSound("Stab")
			SetCombatMod(self.ParentObj, "AttackPlus", "Stab", self._AttackPlus * self.WeaponAttackModifier)
			self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
			SetCombatMod(self.ParentObj, "AttackPlus", "Stab", nil)
		else
			target:NpcSpeech("[08FFFF]*Miss*[-]","combat")
		end

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,

	Range = 1,

	WeaponAttackModifier = 0.01,
	HitChanceFront = 0.4,
	HitChanceBack = 0.6,

	_Weapon = nil,
	_HitChance = nil,
	_AttackPlus = nil,
}