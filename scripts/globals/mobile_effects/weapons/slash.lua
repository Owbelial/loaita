MobileEffectLibrary.Slash = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.WeaponAttackModifier = args.WeaponAttackModifier or self.WeaponAttackModifier

		self._Weapon = GetPrimaryWeapon(self.ParentObj)
		if ( not self._Weapon or not target ) then
			EndMobileEffect(root)
			return false
		end

		self._AttackPlus = Weapon.GetStat(Weapon.GetType(self._Weapon), "Attack")
		if ( self._AttackPlus == nil ) then
			EndMobileEffect(root)
			return false
		end
		
		if(HasHumanAnimations(self.ParentObj)) then
			self.ParentObj:PlayAnimation("sunder")
		else
			-- impale is the "heavy attack for mobs"
			self.ParentObj:PlayAnimation("impale")
		end

		self.ParentObj:PlayEffect("BuffEffect_K")
		self.ParentObj:PlayObjectSound("Stab")

		SetCombatMod(self.ParentObj, "AttackPlus", "Slash", self._AttackPlus * self.WeaponAttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackPlus", "Slash", nil)

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,

	Range = 1,

	_AttackPlus = 0,
}