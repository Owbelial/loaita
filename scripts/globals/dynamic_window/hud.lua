function ShowStatusElement(mobileObj,args)
	args = {
	    ScreenX = args.ScreenX or 300,
		ScreenY = args.ScreenY or 300,
		IsExpanded = args.IsExpanded or false,
		IsSelf = args.IsSelf or false,
		User = args.User or mobileObj
	}

	local windowId = "Status"..mobileObj.Id
	local width = 166
	local height = 58 

	local statusWindow = DynamicWindow(windowId,"",width,height,args.ScreenX,args.ScreenY,"Transparent")

	--if(args.IsSelf and IsInCombat(mobileObj)) then
	--	statusWindow:AddImage(-24,-20,"CombatStatus")
	--end

	-- this is a special command that handles the click client side by targeting the mob with the id of the buttonid
	statusWindow:AddButton(0,0,"","",width,height,"","$target "..mobileObj.Id,false,"Invisible")

	statusWindow:AddImage(0,0,"UtilityBar_Frame")

	local modifiedName = StripColorFromString(mobileObj:GetName())
	statusWindow:AddLabel(16,6,modifiedName,0,0,15,"left",false,true,"SpectralSC-SemiBold")

	statusWindow:AddImage(14,20,"UtilityBar_StatusFrame",135,0,"Sliced")


	statusWindow:AddStatBar(
		166,
		48,			
		36,
		5,
		"Vitality",		
		"00ff00",
		mobileObj,
		true)

	statusWindow:AddStatBar(
		17,
		23,			
		129,
		7,
		"Health",		
		"FF0000",
		mobileObj)

	if(args.IsSelf) then
		statusWindow:AddStatBar(
			17,
			33,			
			129,
			4,
			"Mana",		
			"3388ff",
			mobileObj)

		statusWindow:AddStatBar(
			17,
			40,			
			129,
			4,
			"Stamina",		
			"fffd52",
			mobileObj)
	
	end

	args.User:OpenDynamicWindow(statusWindow)
end

function UpdateItemBar(mobileObj)
	local itemSlotStartIndex = 21

	--local curSlot = itemSlotStartIndex
	--local backpack = this:GetEquippedObject("Backpack")
	--for i,objRef in pairs(backpack:GetContainedObjects()) do
	--	if(i <= 8) then
	--		local curAction = GetItemUserAction(objRef,this) 
	--		curAction.Slot = curSlot			
	--		AddUserActionToSlot(curAction)

	--		curSlot = curSlot + 1
	--	end
	--end	

	-- two rows of 4
	local itemSize = 43
	local itembarHeight = itemSize * 4
	
	local spellBarWindow = DynamicWindow("itembar","",0,0,0,-(itembarHeight/2),"Transparent","Right")
	for itemIndex = 0,7 do
		local yIndex = ((itemIndex) % 4)
		local xIndex = math.floor((itemIndex) / 4)

		spellBarWindow:AddHotbarAction(-(2+itemSize) - (xIndex*itemSize),(yIndex*itemSize),itemSlotStartIndex + itemIndex,0,0,"Circle",true)
	end

	mobileObj:OpenDynamicWindow(spellBarWindow)
end

function UpdateSpellBar(mobileObj)
	--local curAction = GetSpellUserAction("Fireball") 
	--curAction.Slot = 1
	--AddUserActionToSlot(curAction)
	--curAction = GetSpellUserAction("Greaterheal") 
	--curAction.Slot = 2
	--AddUserActionToSlot(curAction)
	--curAction = GetSpellUserAction("Lightning") 
	--curAction.Slot = 3
	--AddUserActionToSlot(curAction)

	local spellItemSize = 48
	local spellbarHeight = spellItemSize * 10
	local spellSlotStartIndex = 1
	
	local spellBarWindow = DynamicWindow("spellbar","",0,0,0,-(spellbarHeight/2),"Transparent","Left")
	local curX = 2
	for x=0,1 do		
		for y=0,9 do
			local index = (x * 10) + y
			spellBarWindow:AddHotbarAction(curX,y*spellItemSize,index+spellSlotStartIndex,0,0,"Square",true)
		end
		curX = curX + spellItemSize
	end

	mobileObj:OpenDynamicWindow(spellBarWindow)
end

function ShouldShowUtilitySlots(mobileObj)
	return true
end

function UpdateHotbar(mobileObj)		
	local abilitySlotStartIndex = 29	

	local actionWindow = DynamicWindow("actionbar","",643,78,-321,-78,"Transparent","Bottom")

	local hasUtilitySlots = ShouldShowUtilitySlots(mobileObj)

	local xOffset = 120
	local skillButtonX = 442
	local characterButtonX = 476
	local settingsButtonX = 400
	local bgRightX = 431
	if not(hasUtilitySlots) then
		xOffset = xOffset + 80
		skillButtonX = xOffset +103
		characterButtonX = xOffset +136
		settingsButtonX = xOffset +160
		bgRightX = 212
	end

	-- x = 0, y = 0, hotbar slot = 0
	--actionWindow:AddImage(xOffset,0,"AbilityHotbar_Sides_L")
	--actionWindow:AddImage(xOffset+bgRightX,0,"AbilityHotbar_Sides_R")

	--actionWindow:AddButton(xOffset+42,65,"","",0,0,"","",false,"PvPButton")
	actionWindow:AddButton(xOffset+67,65,"","",0,0,"","",false,"StealthButton")
	actionWindow:AddButton(xOffset+101,52,"","",0,0,"","",false,"CombatToggle")
	
	if(hasUtilitySlots) then
		--actionWindow:AddImage(xOffset+211,0,"AbilityHotbar_Middle")
		--actionWindow:AddImage(xOffset+285,0,"AbilityHotbar_Middle")
		--actionWindow:AddImage(xOffset+358,0,"AbilityHotbar_Middle")
	end

	actionWindow:AddHotbarAction(xOffset+137,78,abilitySlotStartIndex,0,0,"Diamond",false)
	actionWindow:AddHotbarAction(xOffset+211,78,abilitySlotStartIndex+1,0,0,"Diamond",false)
	if(hasUtilitySlots) then
		--actionWindow:AddHotbarAction(xOffset+285,78,abilitySlotStartIndex+2,0,0,"DiamondSilver",false)
		--actionWindow:AddHotbarAction(xOffset+359,78,abilitySlotStartIndex+3,0,0,"DiamondSilver",false)
		--actionWindow:AddHotbarAction(xOffset+433,78,abilitySlotStartIndex+4,0,0,"DiamondSilver",false)
	end
		
	actionWindow:AddButton(skillButtonX,57,"","",0,0,"","",false,"SkillButton")
	actionWindow:AddButton(characterButtonX,65,"","",0,0,"","",false,"CharacterButton")
	--actionWindow:AddButton(settingsButtonX,65,"","",0,0,"","",false,"SettingsButton")
		
	mobileObj:OpenDynamicWindow(actionWindow)
end

local guardProtectionStrs = {
		GuardTower = "[E1BB00]Protected[-]",
		Guard = "[00FF00]Protected[-]",
		InstaKill = "[00FF00]Protected[-]",
	}

function UpdateRegionStatus(playerObj,areaName,curProtection)		
	areaName = areaName or GetRegionalName(playerObj:GetLoc())
	curProtection = curProtection or playerObj:GetObjVar("GuardProtection") or ""

	local protectionStr = guardProtectionStrs[curProtection] or "[FF0000]Unprotected[-]"	

	local dynWindow = DynamicWindow("regionstatus","",0,0,-196,190,"Transparent","TopRight")
	dynWindow:AddLabel(100,0,areaName,0,0,20,"center",false,true,"SpectralSC-SemiBold")
	dynWindow:AddLabel(100,18,protectionStr,0,0,16,"center",false,true,"SpectralSC-SemiBold")

	-- not the best place to put the god button but it works
	if(IsImmortal(playerObj)) then
		dynWindow:AddButton(157,-77,"","",22,21,"","ToggleGodWindow",false,"God")
	end


		dynWindow:AddButton(12,-77,"","",22,21,"","ToggleQuestLog",false,"HelpButton")


	playerObj:OpenDynamicWindow(dynWindow)
end

function UpdateBackpackBar(mobileObj)
	local backpackBar = DynamicWindow("backpackbar","",300,88,-300,-88,"Transparent","BottomRight")

	local numWeaponSlots = 1
	local backgroundWidth = 147 + (numWeaponSlots * 30)
	backpackBar:AddImage(300-backgroundWidth,0,"DynamicBar",backgroundWidth,88,"Sliced")

	backpackBar:AddButton(262,32,"","",0,0,"","",false,"BackpackButton")
	--backpackBar:AddButton(202,73,"","",0,0,"","",false,"CharacterButton")

	mobileObj:OpenDynamicWindow(backpackBar)
end