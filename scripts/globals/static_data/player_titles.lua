AllTitles = {
	FactionTitles = {
		Water = {
			{"Clan Enemy"		,-50		,0,"Given to enemies of the clan."},
			{"Outsider"			,0 		,5, "[$2806]"},
			{"Clan Initiate"	,5 		,10, "Given to those who are initiated in the Water Clan"},
			{"Clan Member"		,10 	,20,"[$2807]"},
			{"Clan Devotee"		,20 	,30, "[$2808]"},
			{"Clan Elder"		,30 	,40, "Given to high ranking members of the Water Clan"},
			{"Clan Lord"		,40 	,50, "[$2809]"},
		},

		Earth = {
			{"Clan Enemy"		,-50		,0,"Given to enemies of the clan."},
			{"Outsider"			,0 		,5, "[$2810]"},
			{"Clan Initiate"	,5 		,10, "Given to those who are initiated in the Earth Clan"},
			{"Clan Member"		,10 	,20,"[$2811]"},
			{"Clan Devotee"		,20 	,30, "[$2812]"},
			{"Clan Elder"		,30 	,40, "Given to high ranking members of the Earth Clan"},
			{"Clan Lord"		,40 	,50, "[$2813]"},
		},

		Fire = {
			{"Clan Enemy"		,-50		,0,"Given to enemies of the clan."},
			{"Outsider"			,0 		,5, "Given to those who are not a part of the Fire Clan"},
			{"Clan Initiate"	,5 		,10, "Given to those who are initiated in the Fire Clan"},
			{"Clan Member"		,10 	,20,"[$2814]"},
			{"Clan Devotee"		,20 	,30, "[$2815]"},
			{"Clan Elder"		,30 	,40, "Given to high ranking members of the Fire Clan"},
			{"Clan Lord"		,40 	,50, "[$2816]"},
		},

		Cultists = {
			{"Enemy of the True Religion",-50,-40, "[$2817]"},
			{"Hostile"					,-40,  0, "[$2818]"},
			{"Untrusted"					, 0,  10, "[$2819]"},
			{"Cult Member"				, 10 ,20, "[$2820]"},
			{"Trusted Cult Member"		, 20 ,30, "[$2821]"},
			{"Cultist Cheif"				, 30 ,40, "[$2822]"},
			{"Cultist Lord"				, 40 ,50, "[$2823]"},
		},

		Villagers = {
			{"Traitor",-50,-40, 						"[$2824]"},
			{"Criminal"					,-40,  -30,	"[$2825]"},
			{"Untrusted"					,-40,  0, 	"[$2826]"},
			{"Newcomer"					, 0,  5, 	"[$2827]"},
			{"Resident"					, 5,  10,	"[$2828]"},
			{"Citizen"				, 10 ,20,		"[$2829]"},
			{"Noble"					, 20 ,30, 		"[$2830]"},
			{"Champion of Petra"				, 30 ,40, "[$2831]"},
			{"Hero of Petra"				, 40 ,50, "[$2832]"},
		},

		Rebels = {
			{"Enemy of the People",-50,-40, 						"[$2833]"},
			{"Loyalist"					,-40,  -30,	"Given to those who fight against the rebels"},
			{"Untrusted"					,-40,  0, 	"Given to those who are not trusted by the rebels"},
			{"Outsider"					, 0,  5, 	"Given to those who are outsiders to the rebels"},
			{"Comrade"					, 5,  10,	"[$2834]"},
			{"Freedom Fighter"				, 10 ,20,		"[$2835]"},
			{"The Revolutionary"					, 20 ,30, 		"[$2836]"},
			{"Champion of the People"				, 30 ,40, "[$2837]"},
			{"General of the Resistance"				, 40 ,50, "[$2838]"},
		},

		Bandits = {
			{"Bountied Target",-50,-40, 				"[$2839]"},
			{"Hostile"					,-40,  0,   	"[$2840]"},
			{"Chump"					, 0,  5,		"[$2841]"},
			{"Thief"						, 5,  10,	"[$2842]"},
			{"Associate"					, 10 ,20, "[$2843]"},
			{"Crime Lord"					, 20 ,30, "[$2844]"},
			{"Bandit Don"				, 30 ,40, "[$2845]"},
			{"Bandit Kingpin"				, 40 ,50, "[$2846]"},
		},

		Wayun = {
			{"Enemy of the Wayun",-50,-40, 				"Given to those who the Wayward hate."},
			{"Hostile"					,-40,  -5,   	"[$2847]"},
			{"Untrusted"					, 0,  10,		"Given to those who the Wayward do not trust."},
			{"Accepted"					, 10 ,20, "Given to those who are accepted by the Wayward."},
			{"Annointed"					, 20 ,30, "[$2848]"},
			{"Servant of the Strangers"				, 30 ,40, "[$2849]"},
			{"Saint of the Strangers"				, 40 ,50, "[$2850]"},
		},
	},
	SkillTitles = {

		MeleeSkill = {
		{"Fighter",20,50,"Given to those who have learned how to fight."},
		{"Journeyman Fighter",50,80,"Given to those who are powerful in the art of fighting."},
		{"Master Fighter",80,90,"Given to those who are among the most profound in the art of fighting."},
		{"Grandmaster Fighter",100,100,"Given to those who are the greatest in fighting."},
		},

		PiercingSkill = {
		{"Spearman",20,50,"Given to those who have learned how to pierce things."},
		{"Journeyman Spearman",50,80,"Given to those who are powerful in the art of piercing."},
		{"Master Spearman",80,90,"Given to those who are among the most profound in the art of piercing."},
		{"Grandmaster Spearman",100,100,"Given to those who are the greatest in piercing."},
		},

		SlashingSkill = {
		{"Swordsman",20,50,"Given to those who have skill in slashing."},
		{"Journeyman Swordsman",50,80,"Given to those who have skill in slashing."},
		{"Master Swordsman",80,90,"Given to those who are among the most profound swordsmen."},
		{"Grandmaster Swordsman",100,100,"Given to those who are among the greatest swordsmen."},
		},

		BashingSkill = {
		{"Maceman",20,50,"Given to those who have skill in bashing."},
		{"Journeyman Maceman",50,80,"Given to those who have exceptional skill in bashing."},
		{"Master Maceman",80,90,"Given to those who who could crush the skull of an elephant."},
		{"Grandmaster Maceman",100,100,"Given to those who could crush the skull of a dragon."},
		},

		MagicAffinitySkill = {
		{"Medium",20,50,"Given to those who can absorb the magic in the world."},
		{"Journeyman Medium",50,80,"Given to those who have are good at absorbing magic in the world."},
		{"Master Medium",80,90,"Given to those who have mastered the ability to absorb the magic in the world."},
		{"Grandmaster Medium",100,100,"Given to those who have become one with the magic of the world."},
		},

		ManifestationSkill = {
		{"Mage",20,50,"Given to those who have learned the art of magic."},
		{"Journeyman Mage",50,80,"Given to those who are good at the art of magic."},
		{"Master Mage",80,90,"Given to those who have mastered the art of magic."},
		{"Grandmaster Mage",100,100,"Given to those who are among the most profound mages."},
		},

		ChannelingSkill = {
		{"Sorcerer",20,50,"Given to those who can channel magic."},
		{"Journeyman Sorcerer",50,80,"Given to those who are good at channelling magic."},
		{"Master Sorcerer",80,90,"Given to those who have masted the art of channelling magic."},
		{"Grandmaster Sorcerer",100,100,"Given to those who are the greatest sorcerers who have ever lived."},
		},

		BlockingSkill = {
		{"Defender",20,50,"Given to those who have mastered the art of blocking blows."},
		{"Journeyman Defender",50,80,"Given to those who have mastered the art of blocking blows."},
		{"Master Defender",80,90,"Given to those who have mastered the art of blocking blows."},
		{"Grandmaster Defender",100,100,"Given to those who are among the most profound defenders."},
		},

		MetalsmithSkill = 
		{
		{"Blacksmith",20,50,"Given to those who have learned metalsmithing."},
		{"Journeyman Blacksmith",50,80,"Given to those who are good at metalsmithing."},
		{"Master Blacksmith",80,90,"Given to those who have mastered metalsmithing."},
		{"Grandmaster Blacksmith",100,100,"Given to those who are among the most profound metalsmiths."},
		},

		WoodsmithSkill = 
		{
		{"Carpenter",20,50,"Given to those who have learned woodsmithing."},
		{"Journeyman Carpenter",50,80,"Given to those are good at woodsmithing."},
		{"Master Carpenter",80,90,"Given to those who have mastered woodsmithing."},
		{"Grandmaster Carpenter",100,100,"Given to those who are among the most profound woodsmiths."},
		},

		RogueSkill = 
		{
		{"Thief",20,50,"Given to those who are learning roguery."},
		{"Journeyman Thief",50,80,"Given to those who are good at roguery."},
		{"Master Thief",80,90,"Given to those who have mastered the art of roguery."},
		{"Grandmaster Thief",100,100,"Given to those who are among the most nefarious rogues."},
		},

		AnimalKenSkill = 
		{
		{"Animal Trainer",20,50,"Given to those who are animal trainers."},
		{"Journeyman Animal Trainer",50,80,"Given to those who are decent animal tamers."},
		{"Master Animal Trainer",80,90,"Given to those who are masters and attuned to animals."},
		{"Grandmaster Animal Trainer",100,100,"Given to those who are renound for their ability with animals."},
		},

		AnimalLoreSkill = 
		{
		{"Animal Trainer",20,50,"Given to those who are animal trainers."},
		{"Journeyman Animal Trainer",50,80,"Given to those who are decent animal tamers."},
		{"Master Animal Trainer",80,90,"Given to those who are masters and attuned to animals."},
		{"Grandmaster Animal Trainer",100,100,"Given to those who are renound for their ability with animals."},
		},

		CookingSkill = {
		{"Chef",20,50,"Given to those who can cook."},
		{"Journeyman Chef",50,80,"Given to those who are good at cooking."},
		{"Master Chef",80,90,"Given to those who have mastered the art of cooking."},
		{"Grandmaster Chef",100,100,"Given to those who know everything there is about cooking."},
		},

		FishingSkill = {
		{"Fisherman",20,50,"Given to those who can fish."},
		{"Journeyman Fisherman",50,80,"Given to those who are good at the art of fishing."},
		{"Master Fisherman",80,90,"Given to those who have mastered the art of fishing."},
		{"Grandmaster Fisherman",100,100,"Given to those who know everything there is about fishing."},
		},
		
		LumberjackSkill = {
		{"Lumberjack",20,50,"Given to those who have learned to chop wood."},
		{"Journeyman Lumberjack",50,80,"Given to those who are good at lumberjacking."},
		{"Master Lumberjack",80,90,"Given to those who have mastered the art of lumberjacking."},
		{"Grandmaster Lumberjack",100,100,"Given to those who know everything there is about lumberjacking."},
		},

		MiningSkill = {
		{"Miner",20,50,"Given to those who have learned mining."},
		{"Journeyman Miner",50,80,"Given to those who are good at mining."},
		{"Master Miner",80,90,"Given to those who have mastered the art of mining."},
		{"Grandmaster Miner",100,100,"Given to those who know everything there is about mining."},
		},

		FabricationSkill = {
		{"Tailor",20,50,"Given to those who can spin a loom."},
		{"Journeyman Tailor",50,80,"Given to those who are good at fabrication."},
		{"Master Tailor",80,90,"Given to those who have mastered the art of fabrication."},
		{"Grandmaster Tailor",100,100,"Given to those who know everything there is about fabrication."},
		},

		AlchemySkill = 
		{
		{"Alchemist",20,50,"Given to those who have mastered the art of alchemy."},
		{"Journeyman Alchemist",50,80,"Given to those who have mastered the art of alchemy."},
		{"Master Alchemist",80,90,"Given to those who have mastered the art of alchemy."},
		{"The Chemist",100,100,"Given to those who are among the most profound alchemists."},
		},
	},

	ActivityTitles = {
	HouseBuilding = {
		{"Builder",2,5,"Given to those who have built at least 2 houses."},
		{"Master Builder",5,1000,"Given to those who have built at least 5 houses."},
		},
	},	
	
	FameTitles = {
		Fame = {
			{"Respectable",1250,"Trustworthy"},
			{"Great",2500,"Great"},
			{"Illustrious",5000,"Glorious"},
			{"Glorious Lord",10000,"Glorious Lord"},
		},

	},
	--MonsterTitles = {
			--[[boss_demon = {
				{"Slayer of Devils",1,1000,"Given to those who have slain the Greater Demon of Kho"}
			},
			cultist_king = {
				{"Bane of the Elder Gods",1,1000,"Given to those who have slain the Cultist King"}
			},
			spider_boss = {
				{"Arachnid Killer",1,1000,"Given to those who have slain the Weaver Queen"}
			},
			mountain_dragon = {
				{"Dragon-Slayer",1,1000,"Given to those who have slain a Dragon"}
			},
			giant = {
				{"Giant-Slayer",3,1000,"Given to those who have slain 3 Giants"}
			},
			ogre = {
				{"Ogre-Slayer",1,1000,"Given to those who have slain an Ogre"}
			},

			deer = {
				{"Deer Hunter",50,100,"Given to those who have killed 45 deer."},
			},

			bear = {
				{"Bear Hunter",50,100,"Given to those who have killed 45 bears."},
			},

			chicken = {
				{"Slayer of Chickens",50,100,"Given to those who have killed 45 chickens."},
			},

			vampire = {
				{"Vampire-Slayer",1,100,"Given to those who have killed a vampire."},
			},
			vampire_female = {
				{"Vampire-Slayer",1,100,"Given to those who have killed a vampire."},
			},

			boss_necromancer = {
				{"Banisher of the Void",1,100,"Given to those who have killed a arch-necromancer."},
			},
			boss_necromancer_female = {
				{"Banisher of the Void",1,100,"Given to those who have killed a arch-necromancer."},
			},

			fel_wraith = {
				{"Banisher of the Undying",1,100,"Given to those who have killed a fel-wraith."},
			},
			fel_wraith_female = {
				{"Banisher of the Undying",1,100,"Given to those who have killed a fel-wraith."},
			},

			boss_wraith_knight = {
				{"The Iron-Crusher",1,100,"Given to those who have killed a giant wraith knight."},
			},

			cerberus = {
				{"Guardian of the Underworld",1,100,"Given to those who have userped the Void Guardian."},
			},

			vampire_minion = {
				{"Vampire-Hunter",25,100,"Given to those who have killed 25 lesser vampires."},
			},

			skeleton_officer = {
				{"Skull-Crusher",1,100,"Given to those who have killed a skeleton officer."},
			},
			skeleton_officer_mage = {
				{"Skull-Crusher",1,100,"Given to those who have killed a skeleton officer."},
			},

			phantom = {
				{"Ghost-Buster",20,100,"Given to those who have killed 20 phantoms."},
			},
			grim_reaper = {
				{"The Undying",1,100,"Given to those who have bested death itself."},
			},
			werebat = {
				{"Blood-Guardian",10,100,"Given to those who have killed 10 werebats."},
			},

			catacombs_merchant_paul  = {
				{"Cleanser of the Temple",10,100,"Given to those who have killed the Greater Nephilim Wraith of Kho"},
			},

			PlayerVsPlayer = {
			{"The Warrior",25,50,"Given to those who have slain 25 or more players."},
			{"The Champion",50,100,"Given to those who have slain 50 or more players."},
			{"The Conquerer",100,1000,"Given to those who have slain 100 or more players."},
			},

			PlayerVsMonster = {
			{"The Slayer",150,400,"Given to those who have slain 150 or more monsters."},
			{"The Eradicator",400,1000,"Given to those who have slain 400 or more monsters."},
			{"The Annihilator",1000,10000,"Given to those who have slain 1000 or more monsters."},
			},

		},

	MobModuleTitles = {
		ai_skeleton ={
			{"Bone-Crusher",100,1000,"Given to those who have killed 100 skeletons."},
		},
		ai_demon = {
			{"Exorcist",75,1000,"Given to those who have killed 75 demons."},
		},
		ai_wraith = {
			{"Wraith-Bane",50,100,"Given to those who have killed 50 wraiths."},
		},

	},

	MobTeamTitles = {
				Bandits = {
				{"The Marshal",25,100,"Given to those who have killed 25 bandits."},
				{"The Enforcer",50,100,"Given to those who have killed 50 bandits."},
				{"The Judge",100,1000,"Given to those who have killed 100 bandits."},
				},
				Cultists = {
				{"The Infidel",50,100,"Given to those who have killed 50 cultists."},
				{"The Abolisher",100,200,"Given to those who have killed 100 cultists."},
				{"The Apostate",200,1000,"Given to those who have killed 200 cultists."},
				},
				UndeadGraveyard = {
				{"Sword of the Living",50,100,"Given to those who have killed 50 undead."},
				{"Slayer of the Dead",100,200,"Given to those who have killed 100 undead."},
				{"Destroyer of the Damned",200,1000,"Given to those who have killed 200 undead."},
				},
				Guards = {
				{"The Rebel",10,25,"Given to those who have killed 10 or more guards."},
				{"The Guerrilla",25,50,"Given to those who have killed 25 or more guards."},
				{"Leader of the Revolution",50,1000,"Given to those who have killed 50 or more guards."},
				}	
		},
	--other titles we should add -angel of death
	--raider, destroyer, demon-slayer, knight, overlord, imperator, arch-mage, warlock, necromancer, shaman
	--monk, priest, purger, cleanser, mercenary, hit-man, chieftan, paladin, gladiator, exarch, templar, deacon, sage]]
}