EquipmentStats = {
	BaseWeaponClass = {
		Fist = {
			Accuracy = 4.75,
			Critical = 40,
			Speed = 300,
			Variance = 0.05,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Tool = {
			Accuracy = -4,
			Critical = 0,
			Speed = 200,
			Variance = 0,
			WeaponSkill = "PiercingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Dagger = {
			Accuracy = -3.75,
			Critical = 120,
			Speed = 433,
			Variance = 0.05,
			WeaponSkill = "PiercingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Sword = {
			Accuracy = 0,
			Critical = 80,
			Speed = 379,
			Variance = 0.10,
			WeaponSkill = "SlashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Sword2H = {
			Accuracy = 0,
			Critical = 80,
			Speed = 325,
			Variance = 0.10,
			WeaponSkill = "SlashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		Blunt = {
			Accuracy = 4.75,
			Critical = 40,
			Speed = 379,
			Variance = 0.20,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Blunt2H = {
			Accuracy = 8,
			Critical = 40,
			Speed = 227,
			Variance = 0.20,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		PoleArm = {
			Accuracy = -3.75,
			Critical = 120,
			Speed = 155,
			Variance = 0.10,
			WeaponSkill = "LancingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
			PrimaryAbility = "Cleave"
		},

		Bow = {
			Accuracy = -3.75,
			Critical = 120,
			Speed = 500,
			Variance = 0.05,
			WeaponSkill = "ArcherySkill",
			WeaponDamageType = "Bow",
			WeaponHitType = "Ranged",
			TwoHandedWeapon = true,
			Range = 12,
		},

		Stave = {
			Accuracy = 8,
			Critical = 40,
			Speed = 325,
			Variance = 0.20,
			WeaponSkill = "MagicAffinitySkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		Offhand = {
			Accuracy = 4.75,
			Critical = 40,
			Speed = 379,
			Variance = 0.20,
			WeaponSkill = "MagicAffinitySkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},
		Tool = {
			Accuracy = 4.75,
			Critical = 40,
			Speed = 379,
			Variance = 0.20,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},
	},

	-- DAB COMBAT CHANGES IMPLEMENT MIN SKILL FOR WEAPONS
	BaseWeaponStats = {
		BareHand = {
			WeaponClass = "Fist",
			Attack = 3,
			MinSkill = 0,
		},
		-- Daggers
		LetterOpener = {
			WeaponClass = "Dagger",
			Attack = 5,
			MinSkill = 0,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Shiv = {
			WeaponClass = "Dagger",
			Attack = 10,
			MinSkill = 20,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Dagger = {
			WeaponClass = "Dagger",
			Attack = 13,
			MinSkill = 20,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Jambiya = {
			WeaponClass = "Dagger",
			Attack = 15,
			MinSkill = 50,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Kryss = {
			WeaponClass = "Dagger",
			Attack = 16,
			MinSkill = 50,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Gouge = {
			WeaponClass = "Dagger",
			Attack = 17,
			MinSkill = 50,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Crusader = {
			WeaponClass = "Dagger",
			Attack = 20,
			MinSkill = 70,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		LichBlade = {
			WeaponClass = "Dagger",
			Attack = 21,
			Attack = 21,
			MinSkill = 70,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Undeath = {
			WeaponClass = "Dagger",
			Attack = 22,
			MinSkill = 70,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		MithrilBlade = {
			WeaponClass = "Dagger",
			Attack = 24,
			MinSkill = 100,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		DemonsFang = {
			WeaponClass = "Dagger",
			Attack = 25,
			MinSkill = 100,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		
		DarkSword = {
			WeaponClass = "Dagger",
			Attack = 26,
			MinSkill = 100,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		Silence = {
			WeaponClass = "Dagger",
			Attack = 27,
			MinSkill = 100,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
		},
		-- Blunts
		Club = {
			WeaponClass = "Blunt",
			Attack = 5,
			MinSkill = 0,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		SpikedClub = {
			WeaponClass = "Blunt",
			Attack = 10,
			MinSkill = 20,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		Mace = {
			WeaponClass = "Blunt",
			Attack = 14,
			MinSkill = 20,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		MorningStar = {
			WeaponClass = "Blunt",
			Attack = 17,
			MinSkill = 50,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		IronHammer = {
			WeaponClass = "Blunt",
			Attack = 18,
			MinSkill = 50,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		GiantsBone = {
			WeaponClass = "Blunt",
			Attack = 20,
			MinSkill = 50,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		SpikedHammer = {
			WeaponClass = "Blunt",
			Attack = 24,
			MinSkill = 70,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		DwarvenMace = {
			WeaponClass = "Blunt",
			Attack = 25,
			MinSkill = 70,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		Smasher = {
			WeaponClass = "Blunt",
			Attack = 26,
			MinSkill = 70,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		FlangedMace = {
			WeaponClass = "Blunt",
			Attack = 28,
			MinSkill = 100,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		HandOfTethys = {
			WeaponClass = "Blunt",
			Attack = 29,
			MinSkill = 100,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		HammerOfTheAncients = {
			WeaponClass = "Blunt",
			Attack = 29,
			MinSkill = 100,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		Victory = {
			WeaponClass = "Blunt",
			Attack = 30,
			MinSkill = 100,
			PrimaryAbility = "Bash",
			SecondaryAbility = "Stun",
		},
		-- 2H Blunts
		Quarterstaff = {
			WeaponClass = "Blunt2H",
			Attack = 10,
			MinSkill = 0,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Followthrough",
		},
		SledgeHammer = {
			WeaponClass = "Blunt2H",
			Attack = 24,
			MinSkill = 20,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Followthrough",
		},
		GreatHammer = {
			WeaponClass = "Blunt2H",
			Attack = 33,
			MinSkill = 50,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Followthrough",
		},
		DwarvenHammer = {
			WeaponClass = "Blunt2H",
			Attack = 41,
			MinSkill = 70,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Followthrough",
		},
		Destruction = {
			WeaponClass = "Blunt2H",
			Attack = 54,
			MinSkill = 100,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Followthrough",
		},
		-- Polearms
		Warfork = {
			WeaponClass = "PoleArm",
			Attack = 13,
			MinSkill = 0,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Gladius = {
			WeaponClass = "PoleArm",
			Attack = 32,
			MinSkill = 20,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Vouge = {
			WeaponClass = "PoleArm",
			Attack = 36,
			MinSkill = 20,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Spear = {
			WeaponClass = "PoleArm",
			Attack = 46,
			MinSkill = 50,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		WarSpear = {
			WeaponClass = "PoleArm",
			Attack = 48,
			MinSkill = 50,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Halberd = {
			WeaponClass = "PoleArm",
			Attack = 50,
			MinSkill = 70,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		WarAxe = {
			WeaponClass = "PoleArm",
			Attack = 55,
			MinSkill = 70,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		TribalSpear = {
			WeaponClass = "PoleArm",
			Attack = 59,
			MinSkill = 100,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		RunedHalberd = {
			WeaponClass = "PoleArm",
			Attack = 61,
			MinSkill = 100,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Celeste = {
			WeaponClass = "PoleArm",
			Attack = 63,
			MinSkill = 100,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		Avenger = {
			WeaponClass = "PoleArm",
			Attack = 65,
			MinSkill = 100,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Dismount",
		},
		-- Bows
		ShortBow = {
			WeaponClass = "Bow",
			Attack = 9,
			MinSkill = 0,
			PrimaryAbility = "Power",
			SecondaryAbility = "DoubleShot",
			BowSpeedModifier = 500,
			DrawSpeed = 1500,
			DrawTime = 1500,
		},
		Longbow = {
			WeaponClass = "Bow",
			Attack = 23,
			MinSkill = 20,
			PrimaryAbility = "Power",
			SecondaryAbility = "DoubleShot",
			BowSpeedModifier = 1500,
			DrawSpeed = 1500,
			DrawTime = 1500,
		},
		EldeirBow = {
			WeaponClass = "Bow",
			Attack = 30,
			MinSkill = 50,
			PrimaryAbility = "Power",
			SecondaryAbility = "DoubleShot",
			BowSpeedModifier = 1000,
			DrawSpeed = 1500,
			DrawTime = 1500,
		},
		SavageBow = {
			WeaponClass = "Bow",
			Attack = 32,
			MinSkill = 70,
			PrimaryAbility = "Power",
			SecondaryAbility = "DoubleShot",
			BowSpeedModifier = 1500,
			DrawSpeed = 1500,
			DrawTime = 1500,
		},
		BoneBow = {
			WeaponClass = "Bow",
			Attack = 42,
			MinSkill = 100,
			PrimaryAbility = "Power",
			SecondaryAbility = "DoubleShot",
			BowSpeedModifier = 1500,
			DrawSpeed = 1500,
			DrawTime = 1500,
		},
		-- 2H Staffs
		
		Staff = {
			WeaponClass = "Stave",
			Attack = 4,
			Power = 11,
			MinSkill = 0,
			PrimaryAbility = "Focus",
		},
		TribalStaff = {
			WeaponClass = "Stave",
			Attack = 9,
			Power = 17,
			MinSkill = 20,
			PrimaryAbility = "Focus",
		},
		DarkwoodStaff = {
			WeaponClass = "Stave",
			Attack = 9,
			Power = 20,
			MinSkill = 20,
			PrimaryAbility = "Focus",
		},
		JourneymanStaff = {
			WeaponClass = "Stave",
			Attack = 12,
			Power = 22,
			MinSkill = 50,
			PrimaryAbility = "Focus",
		},
		IronStaff = {
			WeaponClass = "Stave",
			Attack = 12,
			Power = 23,
			MinSkill = 50,
			PrimaryAbility = "Focus",
		},
		WhiteStaff = {
			WeaponClass = "Stave",
			Attack = 15,
			Power = 27,
			MinSkill = 70,
			PrimaryAbility = "Focus",
		},
		StaffOfTheSun = {
			WeaponClass = "Stave",
			Attack = 15,
			Power = 28,
			MinSkill = 70,
			PrimaryAbility = "Focus",
		},
		Redemption = {
			WeaponClass = "Stave",
			Attack = 19,
			Power = 32,
			MinSkill = 100,
			PrimaryAbility = "Focus",
		},
		StaffOfTheMagi = {
			WeaponClass = "Stave",
			Attack = 19,
			Power = 33,
			MinSkill = 100,
			PrimaryAbility = "Focus",
		},
		StaffOfTheDead = {
			WeaponClass = "Stave",
			Attack = 19,
			Power = 33,
			MinSkill = 100,
			PrimaryAbility = "Focus",
		},
		-- Offhands
		Spellbook = {
			WeaponClass = "Fist",
			Attack = 5,
			MinSkill = 0,
			PrimaryAbility = "Focus",
		},
		Stave = {
			WeaponClass = "Offhand",
			Attack = 3,
			Power = 10,
			MinSkill = 20,
			PrimaryAbility = "Focus",
		},
		MageStave = {
			WeaponClass = "Offhand",
			Attack = 6,
			Power = 17,
			MinSkill = 50,
			PrimaryAbility = "Focus",
		},
		PriestScepter = {
			WeaponClass = "Offhand",
			Attack = 8,
			Power = 22,
			MinSkill = 70,
			PrimaryAbility = "Focus",
		},
		StaveOfTheFallen = {
			WeaponClass = "Offhand",
			Attack = 9,
			Power = 28,
			MinSkill = 100,
			PrimaryAbility = "Focus",
		},
		Benediction = {
			WeaponClass = "Offhand",
			Attack = 10,
			Power = 29,
			MinSkill = 100,
			PrimaryAbility = "Focus",
		},
		-- 1H Swords
		TrainingSword = {
			WeaponClass = "Sword",
			Attack = 6,
			MinSkill = 0,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Rapier = {
			WeaponClass = "Sword",
			Attack = 12,
			MinSkill = 20,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Axe = {
			WeaponClass = "Sword",
			Attack = 14,
			MinSkill = 20,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Broadsword = {
			WeaponClass = "Sword",
			Attack = 17,
			MinSkill = 50,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		KnightsSword = {
			WeaponClass = "Sword",
			Attack = 19,
			MinSkill = 50,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		BattleAxe = {
			WeaponClass = "Sword",
			Attack = 20,
			MinSkill = 50,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Scimitar = {
			WeaponClass = "Sword",
			Attack = 23,
			MinSkill = 70,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Falchion = {
			WeaponClass = "Sword",
			Attack = 24,
			MinSkill = 70,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		OrnateAxe = {
			WeaponClass = "Sword",
			Attack = 25,
			MinSkill = 70,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		Katana = {
			WeaponClass = "Sword",
			Attack = 28,
			MinSkill = 100,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		ElvenSword = {
			WeaponClass = "Sword",
			Attack = 29,
			MinSkill = 100,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		SwordOfValor = {
			WeaponClass = "Sword",
			Attack = 31,
			MinSkill = 100,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		AxeOfTheSun = {
			WeaponClass = "Sword",
			Attack = 31,
			MinSkill = 100,
			PrimaryAbility = "Slash",
			SecondaryAbility = "Bleed",
		},
		-- 2H Swords
		LargeAxe = {
			WeaponClass = "Sword2H",
			Attack = 8,
			MinSkill = 0,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
		},
		GreatAxe = {
			WeaponClass = "Sword2H",
			Attack = 18,
			MinSkill = 20,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
		},
		LongAxe = {
			WeaponClass = "Sword2H",
			Attack = 24,
			MinSkill = 50,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
		},
		Peacekeeper = {
			WeaponClass = "Sword2H",
			Attack = 30,
			MinSkill = 70,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
		},
		Justice = {
			WeaponClass = "Sword2H",
			Attack = 38,
			MinSkill = 100,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
		},
		-- Tools
		Hatchet = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Lumberjack",
		},
		MiningPick = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Mine",
		},
		FishingRod = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Fish",
		},
		HuntingKnife = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Skin",--KH TODO: Implement skin weapon ability.
		},
		Torch = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
		},
		Crook = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Command",
			SecondaryAbility = "Tame",
		},
	},

	BaseArmorClass = {
		Cloth = {
			Head = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
			Chest = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
			Legs = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
		},

		Light = {
			Head = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.35,
			},
			Chest = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.35,
			},
			Legs = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.35,
			},
		},

		Heavy = {
			Head = {
				StaRegenModifier = -0.05,
				ManaRegenModifier = 0.1,
			},
			Chest = {
				StaRegenModifier = -0.05,
				ManaRegenModifier = 0.1,
			},
			Legs = {
				StaRegenModifier = -0.05,
				ManaRegenModifier = 0.1,
			},
		},
	},

	BaseArmorStats = {
		Natural = { --naked
			ArmorClass = "Cloth",
			Head = {
				ArmorRating = 13
			},
			Chest = {
				ArmorRating = 17
			},
			Legs = {
				ArmorRating = 10
			}
		},

		Apprentice = {
			ArmorClass = "Cloth",
			SoundType = "Leather",
			Head = {
				ArmorRating = 13,
			},
			Chest = {
				ArmorRating = 17,
			},
			Legs = {
				ArmorRating = 10,
			}
		},
		Linen = {
			ArmorClass = "Cloth",
			SoundType = "Leather",
			Head = {
				ArmorRating = 11,
			},
			Chest = {
				ArmorRating = 18,
			},
			Legs = {
				ArmorRating = 16,
			}
		},
		Plant = {
			ArmorClass = "Cloth",
			SoundType = "Leather",
			Head = {
				ArmorRating = 13,
			},
			Chest = {
				ArmorRating = 21,
			},
			Legs = {
				ArmorRating = 19,
			}
		},
		Knowledge = {
			ArmorClass = "Cloth",
			SoundType = "Leather",
			Head = {
				ArmorRating = 16,
			},
			Chest = {
				ArmorRating = 25,
			},
			Legs = {
				ArmorRating = 23,
			}
		},
		Ritual = {
			ArmorClass = "Cloth",
			SoundType = "Leather",
			Head = {
				ArmorRating = 19,
			},
			Chest = {
				ArmorRating = 30,
			},
			Legs = {
				ArmorRating = 26,
			}
		},

		Padded = {
			ArmorClass = "Light",
			SoundType = "Leather",
			Head = {
				ArmorRating = 12
			},
			Chest = {
				ArmorRating = 40
			},
			Legs = {
				ArmorRating = 22
			}
		},

		Leather = {
			ArmorClass = "Light",
			SoundType = "Leather",
			Head = {
				ArmorRating = 15
			},
			Chest = {
				ArmorRating = 44
			},
			Legs = {
				ArmorRating = 24
			}
		},

		Bone = {
			ArmorClass = "Light",
			SoundType = "Leather",
			Head = {
				ArmorRating = 18
			},
			Chest = {
				ArmorRating = 47
			},
			Legs = {
				ArmorRating = 26
			}
		},

		Hardened = {
			ArmorClass = "Light",
			SoundType = "Leather",
			Head = {
				ArmorRating = 22
			},
			Chest = {
				ArmorRating = 50
			},
			Legs = {
				ArmorRating = 29
			}
		},

		Dragon = {
			ArmorClass = "Light",
			SoundType = "Leather",
			Head = {
				ArmorRating = 25
			},
			Chest = {
				ArmorRating = 53
			},
			Legs = {
				ArmorRating = 32
			}
		},

		Brigandine = {
			ArmorClass = "Heavy",
			SoundType = "Plate",
			Head = {
				ArmorRating = 13
			},
			Chest = {
				ArmorRating = 55
			},
			Legs = {
				ArmorRating = 35
			}
		},

		Chain = {
			ArmorClass = "Heavy",
			SoundType = "Plate",
			Head = {
				ArmorRating = 16
			},
			Chest = {
				ArmorRating = 59
			},
			Legs = {
				ArmorRating = 37
			}
		},

		Scale = {
			ArmorClass = "Heavy",
			SoundType = "Plate",
			Head = {
				ArmorRating = 19
			},
			Chest = {
				ArmorRating = 62
			},
			Legs = {
				ArmorRating = 39
			}
		},
		Plate = {
			ArmorClass = "Heavy",
			SoundType = "Plate",
			Head = {
				ArmorRating = 23
			},
			Chest = {
				ArmorRating = 65
			},
			Legs = {
				ArmorRating = 41
			}
		},
		FullPlate = {
			ArmorClass = "Heavy",
			SoundType = "Plate",
			Head = {
				ArmorRating = 26
			},
			Chest = {
				ArmorRating = 68
			},
			Legs = {
				ArmorRating = 43
			}
		},
	},

	BaseShieldStats = {
		Buckler = {
			BaseBlockChance = 20,
			ArmorRating = 47,
			MinSkill = 0,
		},

		SmallShield = {
			BaseBlockChance = 20,
			ArmorRating = 50,
			MinSkill = 20,
		},

		BoneShield = {
			BaseBlockChance = 20,
			ArmorRating = 52,
			MinSkill = 0,
		},
		CurvedShield = {
			BaseBlockChance = 20,
			ArmorRating = 67,
			MinSkill = 50,
		},
		MarauderShield = {
			BaseBlockChance = 20,
			ArmorRating = 70,
			MinSkill = 50,
		},
		LargeShield = {
			BaseBlockChance = 20,
			ArmorRating = 79,
			MinSkill = 70,
		},
		DwarvenShield = {
			BaseBlockChance = 20,
			ArmorRating = 82,
			MinSkill = 70,
		},
		HeaterShield = {
			BaseBlockChance = 20,
			ArmorRating = 90,
			MinSkill = 100,
		},
		Fortress = {
			BaseBlockChance = 20,
			ArmorRating = 91,
			MinSkill = 100,
		},
		DragonGuard = {
			BaseBlockChance = 20,
			ArmorRating = 92,
			MinSkill = 100,
		},
		Temper = {
			BaseBlockChance = 20,
			ArmorRating = 75,
			MinSkill = 70,
		},
		Guardian = {
			BaseBlockChance = 20,
			ArmorRating = 71,
			MinSkill = 70,
		},
	}
}