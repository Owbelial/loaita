SkillData = {
	AllSkills= {
		MeleeSkill = {
			DisplayName = "Vigor",
			SkillIcon = "Skill_Endurance",
			PrimaryStat = "Agility",
			Description = "Vigor determines the effectiveness of bandaging and offers a damage bonus with melee and ranged weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.3
		},
		PiercingSkill = {
			DisplayName = "Piercing",
			PrimaryStat = "Agility",
			Description = "Piercing determines your effeciency when fighting with piercing weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.25
		},
		SlashingSkill = {
			DisplayName = "Slashing",
			PrimaryStat = "Strength",
			Description = "Slashing determines your effeciency when fighting with slashing weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.3
		},
		LancingSkill = {
			DisplayName = "Lancing",
			PrimaryStat = "Strength",
			Description = "Lancing determines your effeciency when fighting with lancing weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4
		},
		BashingSkill = {
			DisplayName = "Bashing",
			PrimaryStat = "Strength",
			Description = "Bashing determines your effeciency when fighting with bashing weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.35
		},
		HealingSkill = {
			DisplayName = "Healing",
			Description = "Healing determines your effectiveness using bandages. You can attempt to resurrect a corpse with bandages at 80 skill.",
			PrimaryStat = "Agility",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Regeneration",
			GainFactor = 1,
			Options = {
				SkillRequiredToResurrect = 80,
				HealTimeBaseSelf = 8000,
				HealTimeBaseOther = 8000,
				MinHealTime = 3000,
				InterruptDamagePlayer = 19,
				InterruptDamageNPC = 26,
				BandageRange = 3.25
			}
		},
		-- MAGIC SKILLS
		MagicAffinitySkill = {
			DisplayName = "Magic Affinity",
			PrimaryStat = "Intelligence",
			NoStatGain = true,
			Description = "Magic Affinity determines your proficiency in magical staffs and staves, the source of your power.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1
		},
		ManifestationSkill = {
			DisplayName = "Manifestation",
			PrimaryStat = "Intelligence",
			Description = "Manifestation determines your knowledge of beneficial spells.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1
		},
		ChannelingSkill = {
			DisplayName = "Channeling",
			PrimaryStat = "Intelligence",
			Description = "Channeling determines your ability to regenerate mana and offers a damage bonus when casting offensive spells.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1
		},
		EvocationSkill = {
			DisplayName = "Evocation",
			PrimaryStat = "Intelligence",
			Description = "Evocation determines your knowledge of damage dealing spells.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1,
		},
		-- END MAGIC SKILLS
		BlockingSkill = {
			DisplayName = "Blocking",
			PrimaryStat = "Strength",
			Description = "Blocking determines your effectiveness using a shield.",
			SkillType = "CombatTypeSkill",
		},
		MetalsmithSkill = {
			DisplayName = "Blacksmithing",
			Description = "Blacksmithing is the capacity to be able to work metal and allows creation of metal items. Higher skill levels can produce better quality items.",
			PrimaryStat = "Strength",
			SkillType = "TradeTypeSkill",
			Practice = {
				ResourceType = "IronIngots",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		WoodsmithSkill = {
			DisplayName = "Carpentry",
			Description = "Carpentry is the capacity to work wood and allows creation of wooden items. Higher skill levels can produce better quality items.",
			PrimaryStat = "Agility",
			Description = "[$3280]",
			SkillType = "TradeTypeSkill",
			Practice = {
				ResourceType = "Boards",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		AlchemySkill = {
			DisplayName = "Alchemy",
			PrimaryStat = "Intelligence",
			Description = "[$3287]",
			SkillType = "TradeTypeSkill",
			Skip = false,
		},
		MarksmanshipSkill = {
			DisplayName = "Marksmanship",
			PrimaryStat = "Strength",
			Description = "Accuracy with projectile weapons",
			SkillType = "CombatTypeSkill",
			Skip = true,
		},
		NecromancySkill = {
			DisplayName = "Necromancy",
			PrimaryStat = "Intelligence",
			Description = "[$3289]",
			SkillType = "CombatTypeSkill",
			Skip = true,
		},
		CookingSkill = {
			DisplayName = "Cooking",
			PrimaryStat = "Intelligence",
			Description = "[$3290]",
			SkillType = "TradeTypeSkill",
		},
		FishingSkill = {
			DisplayName = "Fishing",
			PrimaryStat = "Intelligence",
			Description = "[$3291]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.5
		},	
		LumberjackSkill = {
			DisplayName = "Lumberjack",
			PrimaryStat = "Strength",
			Description = "[$3295]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.6,
			--Skip = true,
		},
		MiningSkill = {
			DisplayName = "Mining",
			PrimaryStat = "Strength",
			Description = "[$3296]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.6,
			--Skip = true,
		},
		FabricationSkill = {
			DisplayName = "Fabrication",
			PrimaryStat = "Intelligence",
			Description = "[$3297]",
			SkillType = "TradeTypeSkill",
			Practice = {
				ResourceType = "Fabric",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		MusicianshipSkill = {
			DisplayName = "Musicianship",
			PrimaryStat = "Intelligence",
			Description = "The ability to play a musical instrument well.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.3,
			Skip = true
		},
		InscriptionSkill = {
			DisplayName = "Inscription",
			PrimaryStat = "Intelligence",
			Description = "The ability to craft spell scrolls & spellbooks.",
			SkillType = "TradeTypeSkill",
			GainFactor = 1.5,
		},
		BardSkill = {
			DisplayName = "Bard",
			PrimaryStat = "Intelligence",
			Description = "Use music to accomplish more than sound.",
			SkillType = "CombatTypeSkill",
			Skip = true,
			GainFactor = 2.5,
		},
		AnimalTamingSkill = {
			DisplayName = "Animal Taming",
			PrimaryStat = "Intelligence",
			Description = "Animal Taming is your ability to control certain creatures.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_AnimalKen",
			GainFactor = 1.5,
		},
		BeastmasterySkill = {
			DisplayName = "Beastmastery",
			PrimaryStat = "Intelligence",
			Description = "Beastmastery is your ability to control beasts. Beasts are powerful creatures that aid in combat, You must master Animal Taming before you can learn to control beasts.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_BeastMastery",
			GainFactor = 1.5,
		},
		AnimalLoreSkill = {
			DisplayName = "Animal Lore",
			PrimaryStat = "Intelligence",
			Description = "Your knowledge of animals. Determines your ability to control and adds a bonus to healing your pets.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1.5,
		},
		ArcherySkill = {
			DisplayName = "Archery",
			PrimaryStat = "Agility",
			Description = "Your effective with a bow and arrow.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1.5,
		},
		HidingSkill = {
			DisplayName = "Hiding",
			SkillIcon = "Skill_Rogue",
			PrimaryStat = "Agility",
			Description = "Hiding determines how well you can conceal yourself from the sight of others.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1,
		},
		StealthSkill = {
			DisplayName = "Stealth",
			SkillIcon = "Skill_Rogue",
			PrimaryStat = "Agility",
			Description = "Stealth determines how effective you are at remaining concealed whilst moving. Start stop movement will reveal you quickly.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1,
		},
		LightArmorSkill = {
			DisplayName = "Light Armor Proficiency",
			SkillIcon = "Skill_LightArmorProficiency",
			PrimaryStat = "Agility",
			Description = "Determines your effectiveness using light armor. Affects evasion, critical hit reduction, and attack speed.",
			SkillType = "CombatTypeSkill",
			GainFactor = 1,
		},
		HeavyArmorSkill = {
			DisplayName = "Heavy Armor Proficiency",
			PrimaryStat = "Agility",
			Description = "Determines your effectiveness using heavy armor. Affects defense and critical hit reduction.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Knight",
			GainFactor = 1,
		},

		TreasureHuntingSkill = {
			DisplayName = "Treasure Hunting",
			PrimaryStat = "Intelligence",
			Description = "Your ability to decipher treasure maps and locate buried treasure.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Inscription",
			GainFactor = 1,
		},

		--[[
		ForensicEvaluationSkill = {
			DisplayName = "Forensic Evaluation",
			PrimaryStat = "Intelligence",
			Description = "Use clues to determine the perpetrator of a crime.",
			SkillType = "TradeTypeSkill",
			GainFactor = 1,
		}]]
	},
}