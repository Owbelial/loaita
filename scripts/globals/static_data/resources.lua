ResourceData = {
	ResourceSourceInfo = 
	{		
		Tree = 
		{
			ResourceType = "Wood",
			ToolType = "Axe",		
			SkillRequired = "LumberjackSkill",
			DepletedState = "Stump",			

			RareResources = {
				WoodDurable = {					
					VisualState = "HighQuality",
					MinSkill = 30,
				},
			}
		},

		BlackForestTree = 
		{
			ResourceType = "Wood",
			ToolType = "Axe",		
			SkillRequired = "LumberjackSkill",
			DepletedState = "Stump",

			RareResources = {
				BlightWood = {					
					VisualState = "HighQuality",
					MinSkill = 60,
				},
			}
		},

		Rock = 
		{ 
			ResourceType = "Stone",
			ToolType = "Pick",		
			SkillRequired = "MiningSkill",
			DepletedState = "Depleted",

			RareResources = {
				IronOre = {					
					VisualState = "IronVein",
					MinSkill = 0,
				},
				CobaltOre = {
					VisualState = "CobaltVein",
					MinSkill = 60,
				},
			}
		},

		Ginseng = 
		{
			SourceTemplate = "plant_ginseng",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Ginseng",
		},

		LemonGrass = 
		{
			SourceTemplate = "plant_lemon_grass",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "LemonGrass",
		},

		Cotton = 
		{
			SourceTemplate = "plant_cotton",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Cotton",
		},

		Mushrooms = 
		{
			SourceTemplate = "plant_mushrooms",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Mushrooms",
		},

		GiantMushrooms = {
			SourceTemplate = "plant_giant_mushrooms",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			HarvestBonusFactor = 1.0,
			ResourceType = "GiantMushrooms",
		},

		MushroomsPoison = 
		{
			SourceTemplate = "plant_mushrooms_poison",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "MushroomsPoison",
		},

		MushroomsPoisonNoxious = 
		{
			SourceTemplate = "ingredient_mushroom_poison_noxious",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "MushroomsPoisonNoxious",
		},

		HumanSkull = 
		{
			SourceTemplate = "item_human_skull",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "HumanSkull",
		},

		Bones = 
		{
			SourceTemplate = "item_bones",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Bones",
		},

		Cactus = 
		{
			SourceTemplate = "plant_cactus",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Cactus",
		},

		SacredCactus = 
		{
			SourceTemplate = "plant_sacred_cactus",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "SacredCactus",
		},

		Moss = 
		{
			SourceTemplate = "plant_moss",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
			ResourceType = "Moss",
		},

		HalloweenJackOLantern = 
		{
			SourceTemplate = "halloween_jack_o_lantern",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
		},

		HalloweenJackOLanternHappy = 
		{
			SourceTemplate = "halloween_jack_o_lantern_happy",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
		},

		HalloweenJackOLanternSad = 
		{
			SourceTemplate = "halloween_jack_o_lantern_sad",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
		},

		HalloweenJackOLanternSurprised = 
		{
			SourceTemplate = "halloween_jack_o_lantern_surprised",
			ToolType = "BareHands",
			DestroyWhenDepleted = true,
		},
	},

	ResourceInfo = {
		-- raw world resources
		Wood = {
			HarvestBonusSkill = "LumberjackSkill",
			
			Template = "resource_wood",
			SalvageValue = 1,
			Difficulty = {
				Min = 0,
				Max = 60
			}
		},
		BlightWood = {
			HarvestBonusSkill = "LumberjackSkill",
			Template = "resource_blightwood",
			SalvageValue = 1,	
			AlternateHarvestResources = {
				{
					ResourceType = "BlightWoodVile",
					SkillThreshold = 80,
					SkillThresholdMax = 100,
					MaxUpgradeChance = 30,
				},
			},
			SkillGainDifficultyMultiplier = 5,
		},
		WoodDurable = {
			HarvestBonusSkill = "LumberjackSkill",
			DisplayName = "Durable Wood",
			Template = "resource_wood_durable",
			SalvageValue = 1,
			SkillGainDifficultyMultiplier = 2,		
		},
		BlightWoodVile = {
			HarvestBonusSkill = "LumberjackSkill",
			DisplayName = "Vile Blightwood",
			Template = "resource_blightwood_vile",
			SalvageValue = 1,
			SkillGainDifficultyMultiplier = 10,		
		},

		Stone = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_stone",
			SalvageValue = 0.3,
			Difficulty = {
				Min = 0,
				Max = 30
			}
		},
		Sand = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_sand",
			SalvageValue = 0.3,
		},
		IronOre = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_iron_ore",
			DisplayName = "Iron Ore",
			SalvageValue = 1,
			SalvageResource = "MetalScraps",
			AlternateHarvestResources = {				
				{
					ResourceType = "Charcoal",
					SkillThreshold = 40,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 10,
				},
				{
					ResourceType = "Coal",
					SkillThreshold = 0,
					SkillThresholdMax = 60,
					MaxUpgradeChance = 30,
				},
				{
					ResourceType = "IronOreGleaming",
					SkillThreshold = 40,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 30,
				},
			},

			Difficulty = {
				Min = -5,
				Max = 65
			}
		},
		IronOreGleaming = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_iron_ore_gleaming",
			DisplayName = "Iron Ore Gleaming",
			SalvageValue = 1,
			SalvageResource = "MetalScrapsGleaming",
			SkillGainDifficultyMultiplier = 2,
		},
		CobaltOre = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_cobalt_ore",
			DisplayName = "Cobalt Ore",
			SalvageValue = 1,
			SalvageResource = "CobaltScraps",
			AlternateHarvestResources = {				
				{
					ResourceType = "Carbon",
					SkillThreshold = 80,
					SkillThresholdMax = 100,
					MaxUpgradeChance = 10,
				},				
				{
					ResourceType = "Anthracite",
					SkillThreshold = 60,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 10,
				},
				{
					ResourceType = "CobaltOreCrystallized",
					SkillThreshold = 80,
					SkillThresholdMax = 100,
					MaxUpgradeChance = 30,
				},
			},

			Difficulty = {
				Min = 55,
				Max = 100
			}
		},
		CobaltOreCrystallized = {
			HarvestBonusSkill = "MiningSkill",
			Template = "resource_cobalt_ore_crystallized",
			DisplayName = "Crystallized Cobalt Ore",
			SalvageValue = 1,
			SalvageResource = "CobaltScrapsCrystallized",
			SkillGainDifficultyMultiplier = 10,
		},
		MetalScraps = {
			Template = "resource_metalscraps",
			DisplayName = "Metal Scraps",
		},
		MetalScrapsGleaming = {
			Template = "resource_metalscraps_gleaming",
			DisplayName = "Gleaming Metal Scraps",
		},
		CobaltScraps = {
			Template = "resource_cobaltscraps",
			DisplayName = "Cobalt Scraps",
		},
		CobaltScrapsCrystallized = {
			Template = "resource_cobaltscraps_crystallized",
			DisplayName = "Crystallized Cobalt Scraps",
		},
		LeatherScraps = {
			Template = "resource_leatherscraps",
			DisplayName = "Leather Scraps",
		},
		BruteLeatherScraps = {
			Template = "resource_brute_leatherscraps",
			DisplayName = "Brute Leather Scraps",
		},
		BeastLeatherScraps = {
			Template = "resource_beast_leatherscraps",
			DisplayName = "Beast Leather Scraps",
		},
		FabledBeastLeatherScraps = {
			Template = "resource_fabled_beast_leatherscraps",
			DisplayName = "Fabled Beast Leather Scraps",
		},
		ClothScraps = {
			Template = "resource_clothscraps",
			DisplayName = "Cloth Scraps",
		},
		QuiltedClothScraps = {
			Template = "resource_clothscraps_quilted",
			DisplayName = "Quilted Cloth Scraps",
		},
		SilkScraps = {
			Template = "resource_silkscraps",
			DisplayName = "Silk Scraps",
		},
		RoyalSilkScraps = {
			Template = "resource_silkscraps_royal",
			DisplayName = "Silk Scraps",
		},
		Miasma = {
			Template = "animalparts_miasma",
			DisplayName = "Miasma",
		},
		MiasmaDeathly = {
			Template = "animalparts_miasma_deathly",
			DisplayName = "Miasma",
		},
		Cotton = {
			Template = "resource_cotton",
			DisplayName = "Cotton",
			AlternateHarvestResources = {
				{
					ResourceType = "Strand",
					SkillThreshold = 0,
					SkillThresholdMax = 60,
					MaxUpgradeChance = 15,
				},
				{
					ResourceType = "CottonFluffy",
					SkillThreshold = 40,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 30,
				},				
			},
		},
		CottonFluffy = {
			Template = "resource_cotton_fluffy",
			DisplayName = "Fluffy Cotton",
			SkillGainDifficultyMultiplier = 10,
		},

		-- intermediate world resources
		Iron = {
			Template = "resource_iron",
			DisplayName = "Iron",
			CraftedItemPrefix = {Flimsy="Iron", Stout="Bolstered Iron"},
			SalvageValue = 7,
			SalvageResource = "MetalScraps",
		},
		Cobalt = {
			Template = "resource_cobalt",
			DisplayName = "Cobalt",
			CraftedItemPrefix = "Cobalt",
			SalvageValue = 7,
			SalvageResource = "CobaltScraps",
			SkillGainDifficultyMultiplier = 5,
		},
		Steel = {
			Template = "resource_steel",
			DisplayName = "Steel",
			CraftedItemPrefix = "Steel",
			SalvageValue = 7,
			SalvageResource = "MetalScrapsSteel",
			SkillGainDifficultyMultiplier = 2,
		},
		CobaltCrystallized = {
			Template = "resource_cobalt_crystallized",
			DisplayName = "Crystallized Cobalt",
			CraftedItemPrefix = "Crystallized Cobalt",
			SalvageValue = 7,
			SalvageResource = "CobaltScrapsCrystallized",
			SkillGainDifficultyMultiplier = 10,
		},
		TannedLeather = {
			Template = "resource_leather_tanned",
			DisplayName = "Leather Tanned",
			CraftedItemPrefix = {Flimsy="Dried", Stout="Tanned"},
			SalvageValue = 5,
			SalvageResource = "LeatherScraps",
		},
		GildedLeather = {
			Template = "resource_gilded_leather",
			DisplayName = "Gilded Leather",
			CraftedItemPrefix = "Gilded",
			SalvageValue = 5,
			SalvageResource = "BruteLeatherScraps",
			SkillGainDifficultyMultiplier = 2,
		},
		BeastLeather = {
			Template = "resource_beast_leather",
			DisplayName = "Beast Leather",
			CraftedItemPrefix = "Beast",
			SalvageValue = 5,
			SalvageResource = "BeastLeatherScraps",
			SkillGainDifficultyMultiplier = 5,
		},
		FabledBeastLeather = {
			Template = "resource_fabled_beast_leather",
			DisplayName = "Fabled Beast Leather",
			CraftedItemPrefix = "Fabled Beast",
			SalvageValue = 5,
			SalvageResource = "FabledBeastLeatherScraps",
			SkillGainDifficultyMultiplier = 10,
		},
		Boards = {
			Template = "resource_boards",
			CraftedItemPrefix = {Stout="Stiff"},
			SalvageValue = 2,
			SalvageResource = "Wood",
		},
		BoardsDurable = {
			DisplayName = "Durable Boards",
			Template = "resource_boards_durable",
			CraftedItemPrefix = "Durable",
			SalvageValue = 2,
			SalvageResource = "Wood",
			SkillGainDifficultyMultiplier = 2,
		},
		BlightwoodBoards = {
			DisplayName = "Blightwood Boards",
			Template = "resource_blightwood_boards",
			CraftedItemPrefix = "Blightwood",
			SalvageValue = 2,
			SalvageResource = "Blightwood",
			SkillGainDifficultyMultiplier = 5,
		},
		BlightwoodBoardsVile = {
			DisplayName = "Vile Blightwood Boards",
			Template = "resource_blightwood_boards_vile",
			CraftedItemPrefix = "Vile Blightwood",
			SalvageValue = 2,
			SalvageResource = "Blightwood",
			SkillGainDifficultyMultiplier = 10,
		},
		Cloth = {
			Template = "resource_bolt_of_cloth",
		},
		QuiltedCloth = {
			DisplayName = "Quilted Cloth",
			CraftedItemPrefix = "Quilted",
			Template = "resource_bolt_of_cloth_quilted",			
		},
		SilkCloth = {
			DisplayName = "Silk Cloth",
			CraftedItemPrefix = "Silk",
			Template = "resource_silk_cloth",
		},
		WildSilkCloth = {
			DisplayName = "Wild Silk Cloth",
			CraftedItemPrefix = "Wild Silk",
			Template = "resource_wild_silk_cloth",
		},

		-- GEMS --

		Diamond = {
			Template = "diamond_gem",
			DisplayName = "Diamond",
		},

		Emerald = {
			Template = "emerald_gem",
			DisplayName = "Emerald",
		},

		Ruby = {
			Template = "ruby_gem",
			DisplayName = "Ruby",
		},

		Sapphire = {
			Template = "sapphire_gem",
			DisplayName = "Sapphire",
		},

		Topaz = {
			Template = "topaz_gem",
			DisplayName = "Topaz",
		},

		-- Composite --

		Coal = {
			Template = "resource_coal",
			DisplayName = "Coal",
		},
		Charcoal = {
			Template = "resource_charcoal",
			DisplayName = "Charcoal",
		},
		Anthracite = {
			Template = "resource_anthracite",
			DisplayName = "Anthracite",
		},
		Carbon = {
			Template = "resource_carbon",
			DisplayName = "Carbon",
		},

		Strand = {
			Template = "resource_strand",
			DisplayName = "Strand",
		},
		Thread = {
			Template = "resource_thread",
			DisplayName = "Thread",
		},
		FineThread = {
			Template = "resource_fine_thread",
			DisplayName = "Fine Thread",
		},
		Fibers = {
			Template = "resource_fibers",
			DisplayName = "Fibers",
		},
		
		-- WEAPON PARTS --
		WarSpearHaft = {
			Template = "resources_weapon_part_warspear",
			DisplayName = "War Spear Haft",
		},
		HalberdHaft = {
			Template = "resources_weapon_part_halberd",
			DisplayName = "Halberd Haft",
		},
		WarAxeHaft = {
			Template = "resources_weapon_part_waraxe",
			DisplayName = "War Axe Haft",
		},
		MageStaveShaft = {
			Template = "resources_weapon_part_magestave",
			DisplayName = "Mage Stave Shaft",
		},
		DarkwoodShaft = {
			Template = "resources_weapon_part_darkwood",
			DisplayName = "Darkwood Staff Shaft",
		},
		TribalShaft = {
			Template = "resources_weapon_part_tribal",
			DisplayName = "Tribal Staff Shaft",
		},
		JourneymanShaft = {
			Template = "resources_weapon_part_journeyman",
			DisplayName = "Journeyman Staff Shaft",
		},
		WhiteStaffShaft = {
			Template = "resources_weapon_part_white",
			DisplayName = "White Staff Shaft",
		},
		StaffOfTheSunShaft = {
			Template = "resources_weapon_part_sun",
			DisplayName = "Staff of the Sun Shaft",
		},

		JambiyaEdge = {
			Template = "resources_weapon_part_jambiya",
			DisplayName = "Jambiya Edge",
		},
		KryssEdge = {
			Template = "resources_weapon_part_kryss",
			DisplayName = "Kryss Edge",
		},
		GougeEdge = {
			Template = "resources_weapon_part_gouge",
			DisplayName = "Gouge Edge",
		},
		LichBladeEdge = {
			Template = "resources_weapon_part_lich",
			DisplayName = "Lich Blade Edge",
		},
		CrusaderEdge = {
			Template = "resources_weapon_part_crusader",
			DisplayName = "Crusader Edge",
		},
		UndeathEdge = {
			Template = "resources_weapon_part_undeath",
			DisplayName = "Undeath Edge",
		},
		KryssEdge = {
			Template = "resources_weapon_part_kryss",
			DisplayName = "Kryss Edge",
		},
		MithrilBladeEdge = {
			Template = "resources_weapon_part_mithrilblade",
			DisplayName = "Mithril Blade Edge",
		},
		DemonsFang = {
			Template = "resources_weapon_part_demonsfang",
			DisplayName = "Demon's Fang",
		},
		DarkSwordEdge = {
			Template = "resources_weapon_part_darksword",
			DisplayName = "Dark Sword Edge",
		},
		SilenceEdge = {
			Template = "resources_weapon_part_silence",
			DisplayName = "Silence Edge",
		},
		BroadswordBlade = {
			Template = "resources_weapon_part_broadsword",
			DisplayName = "Broadsword Blade",
		},
		KnightsSwordBlade = {
			Template = "resources_weapon_part_knightssword",
			DisplayName = "Knights Sword Blade",
		},
		BattleAxeHandle = {
			Template = "resources_weapon_part_battleaxe",
			DisplayName = "Battle Axe Handle",
		},
		ScimitarBlade = {
			Template = "resources_weapon_part_scimitar",
			DisplayName = "Scimitar Blade",
		},
		FalchionBlade = {
			Template = "resources_weapon_part_falchion",
			DisplayName = "Falchion Blade",
		},
		OrnateAxeHandle = {
			Template = "resources_weapon_part_ornateaxe",
			DisplayName = "Ornate Axe Handle",
		},
		SwordOfValorBlade = {
			Template = "resources_weapon_part_swordofvalor",
			DisplayName = "Sword Of Valor Blade",
		},
		ElvenSwordBlade = {
			Template = "resources_weapon_part_elvensword",
			DisplayName = "Elven Sword Blade",
		},
		KatanaBlade = {
			Template = "resources_weapon_part_katana",
			DisplayName = "Katana Blade",
		},
		AxeOfTheSunHandle = {
			Template = "resources_weapon_part_axeofthesun",
			DisplayName = "Axe Of The Sun Handle",
		},
		MorningStarHead = {
			Template = "resources_weapon_part_morningstar",
			DisplayName = "Morning Star Head",
		},
		IronHammerHead = {
			Template = "resources_weapon_part_ironhammer",
			DisplayName = "Iron Hammer Head",
		},
		GiantsBone = {
			Template = "resources_weapon_part_giantsbone",
			DisplayName = "Giant's Bone",
		},
		SpikedHammerHead = {
			Template = "resources_weapon_part_spikedhammer",
			DisplayName = "Spiked Hammer Head",
		},
		DwarvenMaceHead = {
			Template = "resources_weapon_part_dwarvenmace",
			DisplayName = "Dwarven Mace Head",
		},
		SmasherHead = {
			Template = "resources_weapon_part_smasher",
			DisplayName = "Axe Of The Sun Handle",
		},
		FlangedMaceHead = {
			Template = "resources_weapon_part_flangedmace",
			DisplayName = "Flanged Mace Head",
		},
		HandOfTethysHead = {
			Template = "resources_weapon_part_handoftethys",
			DisplayName = "Hand Of Tethys Head",
		},
		HammerOfTheAncientsHead = {
			Template = "resources_weapon_part_hammeroftheancients",
			DisplayName = "Hammer Of The Ancients' Head",
		},
		VictoryHead = {
			Template = "resources_weapon_part_victory",
			DisplayName = "Victory Head",
		},
		GreatHammerHead = {
			Template = "resources_weapon_part_greathammer",
			DisplayName = "Great Hammer Head",
		},
		DwarvenHammerHead = {
			Template = "resources_weapon_part_dwarvenhammer",
			DisplayName = "Dwarven Hammer Head",
		},
		DestructionHead = {
			Template = "resources_weapon_part_destruction",
			DisplayName = "Destruction Head",
		},
		CleaverHandle = {
			Template = "resources_weapon_part_cleaver",
			DisplayName = "Cleaver Handle",
		},
		PeacekeeperHandle = {
			Template = "resources_weapon_part_peacekeeperhandle",
			DisplayName = "Peacekeeper Handle",
		},
		JusticeHandle = {
			Template = "resources_weapon_part_justice",
			DisplayName = "Justice Handle",
		},
		SavageBowShaft = {
			Template = "resources_weapon_part_savagebow",
			DisplayName = "Savage Bow Shaft",
		},
		BoneBowShaft = {
			Template = "resources_weapon_part_bonebow",
			DisplayName = "Bone Bow Shaft",
		},
		RuneBowShaft = {
			Template = "resources_weapon_part_runebow",
			DisplayName = "Rune Bow Shaft",
		},

		-- Shields --
		CurvedShieldBoss = {
			Template = "resources_shield_part_curvedshield",
			DisplayName = "Curved Shield Boss",
		},
		DragonGuardBoss = {
			Template = "resources_shield_part_dragonguard",
			DisplayName = "Dragon Guard Boss",
		},
		DwarvenShieldBoss = {
			Template = "resources_shield_part_dwarvenshield",
			DisplayName = "Dwarven Shield Boss",
		},
		FortressBoss = {
			Template = "resources_shield_part_fortress",
			DisplayName = "Fortress Boss",
		},
		HeaterShieldBoss = {
			Template = "resources_shield_part_heatershield",
			DisplayName = "Heater Shield Boss",
		},
		LargeShieldBoss = {
			Template = "resources_shield_part_largeshield",
			DisplayName = "Large Shield Boss",
		},
		MarauderShieldBoss = {
			Template = "resources_shield_part_maraudershield",
			DisplayName = "Marauder Shield Boss",
		},
		TemperShieldBoss = {
			Template = "resources_shield_part_tempershield",
			DisplayName = "Marauder Shield Boss",
		},
		GuardianShieldBoss = {
			Template = "resources_shield_part_guardianshield",
			DisplayName = "Guardian Shield Boss",
		},

		-- Armor --
		ScaleArmorPart = {
			Template = "resources_armor_part_scale",
			DisplayName = "Scale Armor Part",
		},
		PlateArmorPart = {
			Template = "resources_armor_part_plate",
			DisplayName = "Plate Armor Part",
		},
		FullPlateArmorPart = {
			Template = "resources_armor_part_fullplate",
			DisplayName = "Full Plate Armor Part",
		},
		PlantRobeFabric = {
			Template = "resources_armor_fabric_plant",
			DisplayName = "Plant Robe Fabric",
		},
		KnowledgeRobeFabric = {
			Template = "resources_armor_fabric_knowledge",
			DisplayName = "Knowledge Robe Fabric",
		},
		RitualRobeFabric = {
			Template = "resources_armor_fabric_ritual",
			DisplayName = "Ritual Robe Fabric",
		},
		BoneArmorTexture = {
			Template = "resources_armor_texture_bone",
			DisplayName = "Bone Armor Texture",
		},
		HardenedArmorTexture = {
			Template = "resources_armor_texture_hardened",
			DisplayName = "Hardened Armor Texture",
		},
		DragonArmorTexture = {
			Template = "resources_armor_texture_dragon",
			DisplayName = "Dragon Armor Texture",
		},

		-- Offhand --
		PriestScepterHead = {
			Template = "resources_armor_part_priestscepter",
			DisplayName = "PriestScepterHead",
		},

		-- Recipes --

		RecipeChainHelm = {
			Template = "recipe_chain_helm",
			DisplayName = "Recipe: Chain Helm",
		},
		RecipeChainLeggings = {
			Template = "recipe_chain_leggings",
			DisplayName = "Recipe: Chain Leggings",
		},
		RecipeChainTunic = {
			Template = "recipe_chain_tunic",
			DisplayName = "Recipe: Chain Tunic",
		},

		RecipeGladius = {
			Template = "recipe_gladius",
			DisplayName = "Recipe: Gladius",
		},
		RecipeGreatAxe = {
			Template = "recipe_greataxe",
			DisplayName = "Recipe: Great Axe",
		},
		RecipeLongbow = {
			Template = "recipe_longbow",
			DisplayName = "Recipe: Longbow",
		},
		RecipeRapier = {
			Template = "recipe_rapier",
			DisplayName = "Recipe: Rapier",
		},
		RecipeSledgehammer = {
			Template = "recipe_sledgehammer",
			DisplayName = "Recipe: Sledgehammer",
		},
		RecipeSpikedClub = {
			Template = "recipe_spikedclub",
			DisplayName = "Recipe: Spiked Club",
		},
		RecipeAxe = {
			Template = "recipe_axe",
			DisplayName = "Recipe: Axe",
		},
		RecipeAxeOfTheSun = {
			Template = "recipe_axeofthesun",
			DisplayName = "Recipe: Axe of the Sun",
		},
		RecipeBattleAxe = {
			Template = "recipe_battleaxe",
			DisplayName = "Recipe: Battle Axe",
		},
		RecipeBenediction = {
			Template = "recipe_benediction",
			DisplayName = "Recipe: Benediction",
		},
		RecipeBoneBow = {
			Template = "recipe_bonebow",
			DisplayName = "Recipe: Bone Bow",
		},
		RecipeBoneHelm = {
			Template = "recipe_bonehelm",
			DisplayName = "Recipe: Bone Helm",
		},
		RecipeBoneLeggings = {
			Template = "recipe_boneleggings",
			DisplayName = "Recipe: Bone Leggings",
		},
		RecipeBoneShield = {
			Template = "recipe_boneshield",
			DisplayName = "Recipe: Bone Shield",
		},
		RecipeBoneTunic = {
			Template = "recipe_bonetunic",
			DisplayName = "Recipe: Bone Tunic",
		},
		RecipeBroadsword = {
			Template = "recipe_broadsword",
			DisplayName = "Recipe: Broadsword",
		},
		RecipeCeleste = {
			Template = "recipe_celeste",
			DisplayName = "Recipe: Celeste",
		},
		RecipeCleaver = {
			Template = "recipe_cleaver",
			DisplayName = "Recipe: Cleaver",
		},
		RecipeCrusader = {
			Template = "recipe_crusader",
			DisplayName = "Recipe: Crusader",
		},
		RecipeLichBlade = {
			Template = "recipe_lichblade",
			DisplayName = "Recipe: Lich Blade",
		},
		RecipeCurvedShield = {
			Template = "recipe_curvedshield",
			DisplayName = "Recipe: Curved Shield",
		},
		RecipeDagger = {
			Template = "recipe_dagger",
			DisplayName = "Recipe: Dagger",
		},
		RecipeDarksword = {
			Template = "recipe_darksword",
			DisplayName = "Recipe: Darksword",
		},
		RecipeDarkwoodStaff = {
			Template = "recipe_darkwoodstaff",
			DisplayName = "Recipe: Darkwood Staff",
		},
		RecipeTribalStaff = {
			Template = "recipe_tribalstaff",
			DisplayName = "Recipe: Tribal Staff",
		},
		RecipeDemonsFang = {
			Template = "recipe_demonsfang",
			DisplayName = "Recipe: Demon's Fang",
		},
		RecipeDestruction = {
			Template = "recipe_destruction",
			DisplayName = "Recipe: Destruction",
		},
		RecipeDragonGuard = {
			Template = "recipe_dragonguard",
			DisplayName = "Recipe: Dragon Guard",
		},
		RecipeDragonHelm = {
			Template = "recipe_dragonhelm",
			DisplayName = "Recipe: Dragon Helm",
		},
		RecipeDragonLeggings = {
			Template = "recipe_dragonleggings",
			DisplayName = "Recipe: Dragon Leggings",
		},
		RecipeDragonTunic = {
			Template = "recipe_dragontunic",
			DisplayName = "Recipe: Dragon Tunic",
		},
		RecipeDwarvenHammer = {
			Template = "recipe_dwarvenhammer",
			DisplayName = "Recipe: Dwarven Hammer",
		},
		RecipeDwarvenMace = {
			Template = "recipe_dwarvenmace",
			DisplayName = "Recipe: Dwarven Mace",
		},
		RecipeDwarvenShield = {
			Template = "recipe_dwarvenshield",
			DisplayName = "Recipe: Dwarven Shield",
		},
		RecipeEldeirBow = {
			Template = "recipe_eldeirbow",
			DisplayName = "Recipe: Eldeir Bow",
		},
		RecipeElvenSword = {
			Template = "recipe_elvensword",
			DisplayName = "Recipe: Elven Sword",
		},
		RecipeFalchion = {
			Template = "recipe_falchion",
			DisplayName = "Recipe: Falchion",
		},
		RecipeStaveOfTheFallen = {
			Template = "recipe_fallen",
			DisplayName = "Recipe: Stave of the Fallen",
		},
		RecipeFlangedMace = {
			Template = "recipe_flangedmace",
			DisplayName = "Recipe: Flanged Mace",
		},
		RecipeFortress = {
			Template = "recipe_fortress",
			DisplayName = "Recipe: Fortress",
		},
		RecipeFullPlateHelm = {
			Template = "recipe_fullplatehelm",
			DisplayName = "Recipe: Full Plate Helm",
		},
		RecipeFullPlateLeggings = {
			Template = "recipe_fullplateleggings",
			DisplayName = "Recipe: Full Plate Leggings",
		},
		RecipeFullPlateTunic = {
			Template = "recipe_fullplatetunic",
			DisplayName = "Recipe: Full Plate Tunic",
		},
		RecipeGiantsBone = {
			Template = "recipe_giantsbone",
			DisplayName = "Recipe: Giant's Bone",
		},
		RecipeGladius = {
			Template = "recipe_gladius",
			DisplayName = "Recipe: Gladius",
		},
		RecipeGouge = {
			Template = "recipe_gouge",
			DisplayName = "Recipe: Gouge",
		},
		RecipeGreatAxe = {
			Template = "recipe_greataxe",
			DisplayName = "Recipe: Great Axe",
		},
		RecipeGreatHammer = {
			Template = "recipe_greathammer",
			DisplayName = "Recipe: Great Hammer",
		},
		RecipeAvenger = {
			Template = "recipe_avenger",
			DisplayName = "Recipe: Avenger",
		},
		RecipeHammerOfTheAncients = {
			Template = "recipe_hammeroftheancients",
			DisplayName = "Recipe: Hammer of the Ancients",
		},
		RecipeHalberd = {
			Template = "recipe_halberd",
			DisplayName = "Recipe: Halberd",
		},
		RecipeHandOfTethys = {
			Template = "recipe_handoftethys",
			DisplayName = "Recipe: Hand of Tethys",
		},
		RecipeHardenedHelm = {
			Template = "recipe_hardenedhelm",
			DisplayName = "Recipe: Hardened Leather Helm",
		},
		RecipeHardenedLeggings = {
			Template = "recipe_hardenedleggings",
			DisplayName = "Recipe: Hardened Leather Leggings",
		},
		RecipeHardenedTunic = {
			Template = "recipe_hardenedtunic",
			DisplayName = "Recipe: Hardened Leather Tunic",
		},
		RecipeHeaterShield = {
			Template = "recipe_heatershield",
			DisplayName = "Recipe: Heater Shield",
		},
		RecipeIronHammer = {
			Template = "recipe_ironhammer",
			DisplayName = "Recipe: Iron Hammer",
		},
		RecipeIronStaff = {
			Template = "recipe_ironstaff",
			DisplayName = "Recipe: Iron Staff",
		},
		RecipeJambiya = {
			Template = "recipe_jambiya",
			DisplayName = "Recipe: Jambiya",
		},
		RecipeJourneymanStaff = {
			Template = "recipe_journeymanstaff",
			DisplayName = "Recipe: Journeyman Staff",
		},
		RecipeJustice = {
			Template = "recipe_justice",
			DisplayName = "Recipe: Justice",
		},
		RecipeKatana = {
			Template = "recipe_katana",
			DisplayName = "Recipe: Katana",
		},
		RecipeKnightSword = {
			Template = "recipe_knightsword",
			DisplayName = "Recipe: Knight Sword",
		},
		RecipeKryss = {
			Template = "recipe_kryss",
			DisplayName = "Recipe: Kryss",
		},
		RecipeLargeShield = {
			Template = "recipe_largeshield",
			DisplayName = "Recipe: Large Shield",
		},
		RecipeLeatherHelm = {
			Template = "recipe_leatherhelm",
			DisplayName = "Recipe: Leather Helm",
		},
		RecipeLeatherLeggings = {
			Template = "recipe_leatherleggings",
			DisplayName = "Recipe: Leather Leggings",
		},
		RecipeLeatherTunic = {
			Template = "recipe_leathertunic",
			DisplayName = "Recipe: Leather Tunic",
		},
		RecipeMace = {
			Template = "recipe_mace",
			DisplayName = "Recipe: Mace",
		},
		RecipeMagesStave = {
			Template = "recipe_magestave",
			DisplayName = "Recipe: Mage's Stave",
		},
		RecipeMarauderShield = {
			Template = "recipe_maraudershield",
			DisplayName = "Recipe: Marauder Shield",
		},
		RecipeMithrilBlade = {
			Template = "recipe_mithril",
			DisplayName = "Recipe: Mithril Blade",
		},
		RecipeMorningStar = {
			Template = "recipe_morningstar",
			DisplayName = "Recipe: Morning Star",
		},
		RecipeOrnateBlade = {
			Template = "recipe_ornate",
			DisplayName = "Recipe: Ornate Blade",
		},
		RecipeOrnateAxe = {
			Template = "recipe_ornateaxe",
			DisplayName = "Recipe: Ornate Axe",
		},
		RecipePeacekeeper = {
			Template = "recipe_peacekeeper",
			DisplayName = "Recipe: Peacekeeper",
		},
		RecipePlateHelm = {
			Template = "recipe_platehelm",
			DisplayName = "Recipe: Plate Helm",
		},
		RecipePlateTunic = {
			Template = "recipe_platetunic",
			DisplayName = "Recipe: Plate Tunic",
		},
		RecipePlateLeggings = {
			Template = "recipe_plateleggings",
			DisplayName = "Recipe: Plate Leggings",
		},
		RecipeKnowledgeHelm = {
			Template = "recipe_knowledgehelm",
			DisplayName = "Recipe: Knowledge Helm",
		},
		RecipeKnowledgeTunic = {
			Template = "recipe_knowledgetunic",
			DisplayName = "Recipe: Knowledge Tunic",
		},
		RecipeKnowledgeLeggings = {
			Template = "recipe_knowledgeleggings",
			DisplayName = "Recipe: Knowledge Leggings",
		},
		RecipePriestsScepter = {
			Template = "recipe_priestsscepter",
			DisplayName = "Recipe: Priest's Scepter",
		},

		RecipeGuardianShield = {
			Template = "recipe_guardianshield",
			DisplayName = "Recipe: Guardian",
		},
		RecipeTemper = {
			Template = "recipe_tempershield",
			DisplayName = "Recipe: Temper",
		},
		RecipeRedemption = {
			Template = "recipe_redemption",
			DisplayName = "Recipe: Redemption",
		},
		RecipeRitualHelm = {
			Template = "recipe_ritualhelm",
			DisplayName = "Recipe: Ritual Helm",
		},
		RecipeRitualTunic = {
			Template = "recipe_ritualtunic",
			DisplayName = "Recipe: Ritual Tunic",
		},
		RecipeRitualLeggings = {
			Template = "recipe_ritualleggings",
			DisplayName = "Recipe: Ritual Leggings",
		},
		RecipePlantHelm = {
			Template = "recipe_planthelm",
			DisplayName = "Recipe: Ritual Helm",
		},
		RecipePlantTunic = {
			Template = "recipe_planttunic",
			DisplayName = "Recipe: Ritual Tunic",
		},
		RecipePlantLeggings = {
			Template = "recipe_plantleggings",
			DisplayName = "Recipe: Ritual Leggings",
		},
		RecipeEldeirBow = {
			Template = "recipe_eldeirbow",
			DisplayName = "Recipe: Eldeir Bow",
		},
		RecipeRunedHalberd = {
			Template = "recipe_runedhalberd",
			DisplayName = "Recipe: Runed Halberd",
		},
		RecipeSavageBow = {
			Template = "recipe_savagebow",
			DisplayName = "Recipe: Savage Bow",
		},
		RecipeScaleHelm = {
			Template = "recipe_scalehelm",
			DisplayName = "Recipe: Scale Helm",
		},
		RecipeScaleTunic = {
			Template = "recipe_scaletunic",
			DisplayName = "Recipe: Scale Tunic",
		},
		RecipeScaleLeggings = {
			Template = "recipe_scaleleggings",
			DisplayName = "Recipe: Scale Leggings",
		},
		RecipeScimitar = {
			Template = "recipe_scimitar",
			DisplayName = "Recipe: Scimitar",
		},
		RecipeShiv = {
			Template = "recipe_shiv",
			DisplayName = "Recipe: Shiv",
		},
		RecipeSilence = {
			Template = "recipe_silence",
			DisplayName = "Recipe: Silence",
		},
		RecipeSmasher = {
			Template = "recipe_smasher",
			DisplayName = "Recipe: Smasher",
		},
		RecipeSpear = {
			Template = "recipe_spear",
			DisplayName = "Recipe: Spear",
		},
		RecipeSpikedHammer = {
			Template = "recipe_spikedhammer",
			DisplayName = "Recipe: Spiked Hammer",
		},
		RecipeStaffOfTheDead = {
			Template = "recipe_staffofthedead",
			DisplayName = "Recipe: Staff of the Dead",
		},
		RecipeStaffOfTheMagi = {
			Template = "recipe_staffofthemagi",
			DisplayName = "Recipe: Staff of the Magi",
		},
		RecipeStaffOfTheSun = {
			Template = "recipe_staffofthesun",
			DisplayName = "Recipe: Staff of the Sun",
		},
		RecipeLongSword = {
			Template = "recipe_longsword",
			DisplayName = "Recipe: Long Sword",
		},
		RecipeTribalSpear = {
			Template = "recipe_tribalspear",
			DisplayName = "Recipe: Tribal Spear",
		},
		RecipeUndeath = {
			Template = "recipe_undeath",
			DisplayName = "Recipe: Undeath",
		},
		RecipeVictory = {
			Template = "recipe_victory",
			DisplayName = "Recipe: Victory",
		},
		RecipeVouge = {
			Template = "recipe_vouge",
			DisplayName = "Recipe: Vouge",
		},
		RecipeWarAxe = {
			Template = "recipe_waraxe",
			DisplayName = "Recipe: War Axe",
		},
		RecipeWarSpear = {
			Template = "recipe_warspear",
			DisplayName = "Recipe: War Spear",
		},
		RecipeWhiteStaff = {
			Template = "recipe_whitestaff",
			DisplayName = "Recipe: White Staff",
		},

		-- FOOD RESOURCES --

		KhoToken = {		
			Template = "kho_token",
		},
		Wine = {
			Template = "ingredient_wine",
			AlternateHarvestResources = {
				{
					ResourceType = "OliveOil",
					SkillThreshold = 80,
					SkillThresholdMax = 100,
					MaxUpgradeChance = 30,
				},
			},
		},

		Ginseng = {
			Template = "ingredient_ginsengroot",
			AlternateHarvestResources = {
				{
					ResourceType = "AncientGinseng",
					SkillThreshold = 40,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 30,
				},
			},
		},
		Moss = {
			Template = "ingredient_moss",
		},
		AncientGinseng = {
			Template = "ingredient_ancient_ginsengroot",
		},
		LemonGrass = {
			Template = "ingredient_lemongrass",
			AlternateHarvestResources = {
				{
					ResourceType = "ingredient_lemongrass_spirited",
					SkillThreshold = 20,
					SkillThresholdMax = 40,
					MaxUpgradeChance = 30,
				},
			},
		}, 
		LemonGrassSpirited = {
			Template = "ingredient_lemongrass_spirited",
		},

		MushroomsPoison = {
			Template = "ingredient_mushroom_poison",
			AlternateHarvestResources = {
				{
					ResourceType = "MushroomsPoisonNoxious",
					SkillThreshold = 80,
					SkillThresholdMax = 100,
					MaxUpgradeChance = 30,
				},
			},
		},

		MushroomsPoisonNoxious = {
			Template = "ingredient_mushroom_poison_noxious",
		},

		Mushrooms = {
			Template = "ingredient_mushroom",
			AlternateHarvestResources = {
				{
					ResourceType = "FragrantMushrooms",
					SkillThreshold = 20,
					SkillThresholdMax = 60,
					MaxUpgradeChance = 30,
				},
			},	
		},		

		GiantMushrooms = {
			Template = "ingredient_giant_mushrooms",
		},

		Cactus = {
			Template = "ingredient_cactus",
			AlternateHarvestResources = {
				{
					ResourceType = "SacredCactus",
					SkillThreshold = 40,
					SkillThresholdMax = 80,
					MaxUpgradeChance = 30,
				},
			},
		},

		SacredCactus = {
			Template = "ingredient_sacred_cactus",
		},
		FragrantMushrooms = {
			Template = "ingredient_mushroom_fragrant",
		},
		OliveOil = {
			Template = "ingredient_olive_oil",
		},
		Apple = {
			Template = "item_apple",
		},
		Pumpkin = {
			Template = "resource_pumpkin",
		},
		FruitCatacombs = {
			Template = "fruit_catacombs",
		},
		FruitCatacombsNectar = {
			Template = "fruit_catacombs_nectar",
		},
		PlantFabric = {
			Template = "resource_plant_fabric",
		},
		Bread = {
			Template = "item_bread",
		},
		Broccoli = {
			Template = "ingredient_broccoli",
		},
		Cabbage = {
			Template = "ingredient_cabbage",
		},
		Cucumber = {
			Template = "ingredient_cucumber",
		},
		GreenPepper = {
			Template = "ingredient_green_pepper",
		},
		Onion = {
			Template = "ingredient_onion",
		},
		Potato = {
			Template = "ingredient_potato",
		},
		Tomato = {
			Template = "ingredient_tomato",
		},
		Beer = { --BEEEEEEERRR
			Template = "item_beer",
		},
		Mead = { --BEEEEEEERRR
			Template = "item_mead",
		},
		FishFilletBarrel = { --BEEEEEEERRR
			Template = "animalparts_barrel_fish_fillet",
		},
		FishFilletTero = { --BEEEEEEERRR
			Template = "animalparts_tero_fish_fillet",
		},
		FishFilletSpottedTero = { --BEEEEEEERRR
			Template = "animalparts_spotted_tero_fish_fillet",
		},
		FishFilletFourEyedSalar = { --BEEEEEEERRR
			Template = "animalparts_foureyed_salar_fish_fillet",
		},
		FishFilletRazor = { --BEEEEEEERRR
			Template = "animalparts_razor_fish_fillet",
		},
		FishFilletGoldenAether = { --BEEEEEEERRR
			Template = "animalparts_golden_aether_fish_fillet",
		},

		-- ANIMAL PARTS ---
		AnimalMeat = {
			Template = "animalparts_meat",
		},
		FishFilet = {
			Template = "animalparts_meat",
		},
		BearClaw = {
			Template = "animalparts_bear_claw",
		},
		FishScale = {
			Template = "animalparts_fish_scale",
		},
		LeatherHide = {
			Template = "animalparts_leather_hide",
		},
		BruteLeatherHide = {
			Template = "animalparts_brute_leather_hide",
		},
		BeastLeatherHide = {
			Template = "animalparts_beast_leather_hide",
		},
		FabledBeastLeatherHide = {
			Template = "animalparts_fabled_beast_leather_hide",
		},
		HumanFlesh = {
			Template = "animalparts_human_flesh",
		},
		VampireBlood = 
		{
			Template = "animalparts_vampire_blood",
		},
		WereBatBlood = 
		{
			Template = "animalparts_werebat_blood",
		},
		HumanMeat = {
		},
		OgreTooth = {
			Template = "animalparts_ogre_tooth",
		},
		HumanSkull = {
			Template = "animalparts_human_skull",
		},
		RabbitFoot = {
			Template = "animalparts_rabbit_foot",
		},
		RatEar = {
			Template = "animalparts_rat_ear",
		},
		BatEar = {
			Template = "animalparts_bat_ear",
		},
		SnakeSkin = {
			Template = "animalparts_snake_skin",
			DisplayName = "Snake Skin",
		},
		ViperSkin = {
			Template = "animalparts_viper_skin",
		},
		SpiderEye = {
			Template = "animalparts_spider_eye",
		},
		SpiderSilk = {
			Template = "animalparts_spider_silk",
			DisplayName = "Spider Silk",
		},
		SpiderSilkGolden = {
			Template = "animalparts_spider_silk_golden",
			DisplayName = "Golden Spider Silk",
		},
		CochinealExtract = {
			Template = "animalparts_beetle_extract",
		},
		WormExtract = {
			Template = "animalparts_worm_extract",
		},
		Feather = {
			Template = "animalparts_feather",
		},
		WolfFang = {
			Template = "animalparts_wolf_fang",
		},
		AncientBearClaw = {
			Template = "animalparts_ancient_bear_claw",
		},
		AncientBearFang = {
			Template = "animalparts_ancient_bear_fang",
		},
		DireWolfFang = {
			Template = "animalparts_dire_wolf_fang",
		},
		GossamerSilk = {
			Template = "animalparts_gloss_silk",
		},
		BeetleExtract = {
			Template = "animalparts_beetle_extract",
		},
		PlagueRatEar = {
			Template = "animalparts_plague_rat_ear",
		},
		PurpleSpiderEye = {
			Template = "animalparts_purple_spider_eye",
		},
		Blood = {
			Template = "animalparts_beast",
		},
		BoneMarrow = {
			Template = "animalparts_bone_marrow",
		},
		BeastEye = {
			Template = "animalparts_beast_eye",
		},
		ToxicSaliva = {
			Template = "animalparts_toxic_saliva",
		},		

		-- MISC RESOURCES --
		Crystal = {
			Template = "resource_crystal",
		},
		Essence = {
			Template = "resource_essence",
		},

		Bones = {
			CraftedItemPrefix = {Stout="Fibrous"},
			Template = "animalparts_bone",
		},
		CursedBones = {
			DisplayName = "Cursed Bones",
			CraftedItemPrefix = "Cursed",
			Template = "animalparts_bone_cursed",
		},
		ToxicBones = {
			DisplayName = "Toxic Bones",
			CraftedItemPrefix = "Toxic",
			Template = "animalparts_bone_cursed",
		},
		SpectralBones = {
			DisplayName = "Spectral Bones",
			CraftedItemPrefix = "Spectral",
			Template = "animalparts_bone_cursed",
		},
		ArcaneScroll = {
			Template = "ingredient_arcane_scroll",
		},
		BlankScroll = {
			Template = "resource_blankscroll",
			DisplayName = "Blank Scroll",
		},
-----------------------------------------------------
		EssenzaMagica = {
			Template = "essenza_magica",
			DisplayName = "Magic Essence",
		},
		EssenzaMagicaBrill = {
			Template = "essenza_magicabrill",
			DisplayName = "Brilliant Magic Essence",
		},
		EssenzaMagicaInf = {
			Template = "essenza_magicainf",
			DisplayName = "Light Magic Essence",
		},
		EssenzaMagicaDiv = {
			Template = "essenza_magicadiv",
			DisplayName = "Divine Magic Essence",
		},
		EssenzaMagicaMerav = {
			Template = "essenza_magicamerav",
			DisplayName = "Wonderful Magic Essence",
		},
----------------------------------------------------
		StrongRitualPotion = {
			Template = "poz_ritualeforte",
			DisplayName = "Strong Ritual Potion",
		},
		InferiorRitualPotion = {
			Template = "poz_ritualeinferiore",
			DisplayName = "Light Ritual Potion",
		},
		AverageRitualPotion = {
			Template = "poz_ritualemedia",
			DisplayName = "Average Ritual Potion",
		},
		SuperiorRitualPotion = {
			Template = "poz_ritualesuperiore",
			DisplayName = "Superior Ritual Potion",
		},
		SupremeRitualPotion = {
			Template = "poz_ritualesuprema",
			DisplayName = "Supreme Ritual Potion",
		},
-----------------------------------------------------
		DemonBlood = {
			Template = "sangue_demoniaco",
			DisplayName = "Demonic Blood",
		},
		DragonBlood = {
			Template = "sangue_drago",
			DisplayName = "Dragon Blood",
		},
		GorgonBlood = {
			Template = "sangue_gorgone",
			DisplayName = "Gorgon Blood",
		},
		NormalBlood = {
			Template = "sangue_normale",
			DisplayName = "Blood",
		},
		UndeadBlood = {
			Template = "sangue_undead",
			DisplayName = "Undead Blood",
		},
------------------------------------------------------
		StrongRitualScroll = {
			Template = "scroll_ritualeforte",
			DisplayName = "Strong Ritual Scroll",
		},
		LightRitualScroll = {
			Template = "scroll_ritualeinferiore",
			DisplayName = "Light Ritual Scroll",
		},
		AverageRitualScroll = {
			Template = "scroll_ritualemedia",
			DisplayName = "Average Ritual Scroll",
		},
		SuperiorRitualScroll = {
			Template = "scroll_ritualesuperiore",
			DisplayName = "Superior Ritual Scroll",
		},
		SupremeRitualScroll = {
			Template = "scroll_ritualesuprema",
			DisplayName = "Supreme Ritual Scroll",
		}
	}
}