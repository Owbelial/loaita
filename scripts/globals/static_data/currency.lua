Denominations = {
	{ Name = "Copper"  , Value = (1)              , Abbrev = "c", Color = "[B87333]" },
	{ Name = "Silver"  , Value = (1 * 100)        , Abbrev = "s", Color = "[C0C0C0]" },
	{ Name = "Gold"    , Value = (100 * 100)      , Abbrev = "g", Color = "[FFD700]" },
	{ Name = "Platinum", Value = (100 * 100 * 100), Abbrev = "p", Color = "[E0E0E0]" },
}