AbilityData = {
	AllAbilities = {
		---------------------------------------------------------------------------------------------------------------
		-- Slot 1 Weapon Class Abilities
		---------------------------------------------------------------------------------------------------------------
		--[[Quickstrike = {
			AbilityType = "InstantAttack",
			AbilityTriggertype= {
				Instant = true,
				Triggered = false
			},			
			PrimaryAbility = {
				Halberd = true,
				Flail = true,
				ClawFlail = true,
				SpikedFlail = true,
				WarStaff = true,
				HammerSpike = true,
				ShortSpear = true,
				LongSpear = true,
				PoleAxe = true,
			},

			AutoCooldown = true,
			CooldownTimer = 10,
			MinDamModInst = 75,
			MaxDamModInst = 90,
			StaminaCost = 20,
			nextSwingDelay= .4,
			isTargetRequired = true,
			--AttackerFX = "BuffEffect_B",
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2657]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			ResetSwingTime = true,
			SwingAnimation = "lunge",
			Icon = "Charge",
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 0,
			triggerGlobalCoolOnAttack = true,		
		},

		Chargedbolt = {
			DisplayName = "Charged Bolt",
			AbilityTriggertype= {
				Instant = true,
				Triggered = false
			},			
			PrimaryAbility = {
				WizardStaff = true,
				Wand = true,
			},
			isTargetRequired = true,
			checkRangeForExecute = true,
			combatAbilityScriptOnUser = "ca_charged_bolt_script",
			AutoCooldown = true,
			CooldownTimer = 20,
			MinDamModTrig = 250,
			MaxDamModTrig = 300,			
			StaminaCost = 20,
			nextSwingDelay= 2,
			doNotPlayProjectile = true,
			ForceRanged = true,
			--AttackerFX = "BuffEffect_B",
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2658]",
			AbilityEnabled = true,
			AssociatedSkill = "ChannelingSkill",
			ResetSwingTime = true,
			SwingAnimation = "lunge",
			Icon = "Hellstorm 03",
			AssociatedSkill = "ChannelingSkill",
			LevelRequired = 10,	
			abilityRangeBonus = 8,		
	
		},
		Followthrough = { 
			AbilityTriggertype= {
				Instant = true,
				Triggered = false,
			},
			PrimaryAbility = {
				Sickle = true,
				Scythe = true,
				Hatchet = true,
				Cutlass = true,
				Gladius = true,
				CrossMace = true,
				Maul = true,
				MiningPick = true,
				Scythe = true,
				Glaive = true,
				Saber = true,
				WarChissel = true,
				Flange = true,
				GreatHammer = true,
				BattleAxe = true,
				LongSword = true,
				QuarterStaff = true,
			},
			isTargetRequired = true,
			checkRangeForExecute = true,
			AutoCooldown = true,
			continueOnMiss = true,
			continueOnParry = true,
			continueOnDodge = true,
			continueOnBlock = false,
			CooldownTimer = 20,
			StaminaCost = 55,
			combatAbilityTriggerScriptUser = "ca_followthrough_script",
			MinDamModInst = 80,
			MaxDamModInst = 95,
			MinDamModTrig = 75,
			MaxDamModTrig = 90,
			nextSwingDelay= 2,
			AttackerSFX = "FollowThrough",
			TargetFX = "BloodEffect_A",		
			TooltipString = "[$2659]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 10,
			StaminaCostDivisor = 2,
			ResetSwingTime = true,
			SwingAnimation = "followthrough",
			AbilityType = "InstantAttack",
			triggerGlobalCoolOnAttack = true,		
		},	

		Sunder = { 
			AbilityType = "InstantAttack",
			AbilityTriggertype= {
				Instant = true,
				Triggered = false,
			},
			PrimaryAbility = {
				Dagger = true,
				BareHand = true,
				Spade = true,
				Spikes = true,
				Cleaver = true,
				Talon = true,
				Dirk = true,
				Leafblade = true,
				Rapier = true,
				Truncheon = true,
				Stiletto = true,

			},
			AutoCooldown = true,
			CooldownTimer = 20,
			isTargetRequired = true,
			MinDamModInst = 110,
			MaxDamModInst = 130,
			StaminaCost = 30,
			nextSwingDelay= .4,
			combatAbilityTriggerScriptTarget = "ca_sunder_target_effect",
			AttackerFX = "BuffEffect_D",
			AttackerFXBone = "Ground",
			AttackerSFX = "SunderEffect",
			TooltipString = "[$2660]",
			AbilityEnabled = true,
			ResetSwingTime = true,
			SwingAnimation = "sunder",
			triggerGlobalCoolOnAttack = true,		
		},	

		---------------------------------------------------------------------------------------------------------------
		-- Slot 2 (W) --Damage Type Abilities
		---------------------------------------------------------------------------------------------------------------
	
		Pulverize = { 
			SecondaryAbility = {
				BareHand = true,
				CrossMace = true,
				Truncheon = true,
				Flail = true,
				Flange = true,
				Maul = true,
				MiningPick = true,
				QuarterStaff = true,
				WarStaff = true,
				GreatHammer = true,
			},	
			AbilityTriggertype= {
				Triggered = true
			},
			AutoCooldown = true,
			CooldownTimer = 8,
			MinDamModTrig = 120,
			MaxDamModTrig = 130,
			StaminaCost = 25,
			nextSwingDelay= .5,
			AttackerFX = "BuffEffect_C",
			AttackerFXBone ="Ground",
			AttackerSFX = "Pulverize",
			TargetFX = "BuffEffect_J",
			TargetFXBone = "Ground",
			TooltipString = "[$2661]",
			AbilityEnabled = true,
			AssociatedSkill = "BashingSkill",
			LevelRequired = 20,
			SwingAnimation = "pummel",
			AbilityBuffIcon = "pulverize",
		},			

		Rend = { 
			SecondaryAbility = {
				Axe = true,
				Cleaver = true,
				Talon = true,
				Sickle = true,
				Scythe = true,
				Hatchet = true,
				Cutlass = true,
				Saber = true,
				WarChissel = true,
				Gladius = true,
				ClawFlail = true,
				BattleAxe = true,
				LongSword = true,
				Glaive = true,
				Halberd = true,
				PoleAxe = true,
			},	
			AbilityTriggertype= {
				Triggered = true
			},
			AutoCooldown = true,
			CooldownTimer = 10,
			MinDamModTrig = 100,
			MaxDamModTrig = 100,
			StaminaCost = 30,
			nextSwingDelay= 0,
			combatAbilityTriggerScriptTarget = "ca_rend_target_effect",
			AttackerFX = "BuffEffect_G",
			AttackerFXBone = "Ground",
			TargetFX = "BloodEffect_A",			
			AttackerIdObjvar = "RendInflicter",
			TooltipString = "[$2662]",
			AbilityEnabled = true,
			AssociatedSkill = "SlashingSkill",
			LevelRequired = 20,
			SwingAnimation = "rend",
			AbilityBuffIcon = "rend",
		},	
		Staffward = { 
			SecondaryAbility = {
				WizardStaff = true,
			},	

			AutoCooldown = true,
			CooldownTimer = 65,
			StaminaCost = 30,
			AutoCooldown = true,
			AbilityTriggertype = {
					Instant = true,
					Triggered = false
			},
			MaxCharges = 0,
			Duration = 6,
			CompletionDelay = 1,
			combatAbilityScriptOnUser = "ca_staff_ward_user_effect",
			TooltipString = "[$2663]",
			AbilityEnabled = true,
			AssociatedSkill = "ChannelingSkill",
			LevelRequired = 50,	
			Icon = "Spell Shield",
		},	
		Impale = { 
			AbilityTriggertype= {
				Triggered = true
			},
			SecondaryAbility = {
				Dagger = true,
				Dirk = true,
				Stiletto = true,
				Spade = true,
				Spikes = true,
				Rapier = true,
				SpikedFlail = true,
				HammerSpike = true,
				ShortSpear = true,
				LongSpear = true,
			},	
			AutoCooldown = true,
			CombatAbilityCritChanceModifier = 1.4,
			CooldownTimer = 10,
			MinDamModTrig = 110,
			MaxDamModTrig = 135,
			StaminaCost = 25,
			nextSwingDelay= 1,
			combatAbilityTriggerScriptTarget = "ca_impale_target_effect",
			AttackerFX = "BuffEffect_O",
			AttackerSFX = "Impale",
			AttackerFXBone = "Ground",
			TooltipString = "[$2664]",
			AbilityEnabled = true,
			AssociatedSkill = "PiercingSkill",
			LevelRequired = 20,
			SwingAnimation = "impale",
			AbilityBuffIcon = "impale",
		},	
		---------------------------------------------------------------------------
		-- Slot 4 ( R ) -- Stance Abilities
		---------------------------------------------------------------------------
		Charge = {
			DisplayName = "Charge",
			AbilityTriggertype={
				Instant = true,
			},
			DependentStance = {
				Aggressive = true,
			},
			AutoCooldown = false,
			MinDamModTrig = 120,
			MinDamModTrig = 130,
			CombatAbilityCritChanceModifier = 1.1,
			CooldownTimer = 50,
			Duration = 6,
			CompletionDelay = 1,
			combatAbilityScriptOnUser = "ca_charge_script",
			combatAbilityTriggerScriptTarget = "ca_charge_target_effect",
			ResetSwingTime = true,
			StaminaCost = 40,
			TooltipString = "[$2665]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 30,
			StaminaCostDivisor = 2,
			--AbilityType = "InstantAttack",
			triggerGlobalCoolOnAttack = true,		
		},		
		Defend = {
			AbilityTriggertype={
				Instant = true,
			},
			DependentStance = {
				Defensive = true,
			},
			AutoCooldown = true,
			CombatAbilityCritChanceModifier = .7,
			CooldownTimer = 20,
			Duration = 5,
			CompletionDelay = 1,
			combatAbilityScriptOnUser = "ca_defend_script",
			AttackerSFX = "Defend",
			ResetSwingTime = false,
			StaminaCost = 15,
			DependentUserCombatState = {
				Defending = false,
				Blocking = false,
				Countering = false,
			},
			TooltipString = "[$2666]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 0,
		},
		Leap = {
			--AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			CooldownTimer = 40,
			MinDamModTrig = 45,
			MaxDamModTrig = 45,
			RangeBonus = 3,
			StaminaCost = 45,
			combatAbilityScriptOnUser ="ca_leap_script",
			ResetSwingTime = true,
			isTargetRequired = false,
			TooltipString = "[$2667]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 60,
			Icon = "Leap",
			triggerGlobalCoolOnAttack = true,		
		},

		Lunge = {
			--AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			DependentStance = {
				Passive = true,
			},
			AutoCooldown = true,
			CooldownTimer = 15,
			MinDamModTrig = 60,
			MaxDamModTrig = 75,
			RangeBonus = 3,
			StaminaCost = 25,
			combatAbilityScriptOnUser ="ca_lunge_script",
			combatAbilityTriggerScriptTarget = "ca_lunge_target_effect",
			ResetSwingTime = true,
			isTargetRequired = true,
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2668]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 20,
			StaminaCostDivisor = 2,
			SwingAnimation = "lunge",
			triggerGlobalCoolOnAttack = true,		
		},

		Flurry = { 
			CooldownTimer=20,
			AutoCooldown = true,
			AbilityTriggertype = {
					Instant = true,
					Triggered = false
			},
			RequiredRangeGroup = "Short",
			MaxCharges = 0,
			Duration = 6,
			CompletionDelay = 1,
			StaminaCost = 16,
			combatAbilityScriptOnUser = "ca_flurry_script",
			TooltipString = "[$2669]",
			AbilityEnabled = true,
			DependentUserCombatState = {
				Cloaked = false,
			},		
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 0,	
		},
		---------------------------------------------------------------------------------------------------------------
		--Slashing abilities
		---------------------------------------------------------------------------------------------------------------
		Forcestab = { 
			DisplayName = "Force Stab",
			AbilityType = "InstantAttack",			
			--AlwaysUsable = true,
			AbilityTriggertype= {
				Instant = true
			},
			ApplicableWepDamClasses = { 
				Slashing = true,
			},
			CooldownTimer = 45,
			isTargetRequired = true,
			CooldownTimer = 30,
			StaminaCost = 55,
			MinDamModInst = 110,
			MaxDamModInst = 120,
			nextSwingDelay= .7,
			combatAbilityTriggerScriptTarget = "ca_knockback_effect",
			--combatAbilityTriggerScriptUser = "ca_forcestab_effect",
			TooltipString = "[$2670]",
			AbilityEnabled = true,
			AssociatedSkill = "SlashingSkill",
			LevelRequired = 50,
			SwingAnimation = "impale",
			triggerGlobalCoolOnAttack = true,		
		},	

		Eviscerate = { 
				ApplicableWepDamClasses = { 
					Slashing = true,
				},
				CooldownTimer=20,
				AbilityTriggertype = {
						Instant = true,
						Triggered = false
				},
				AutoCooldown = true,
				EffectMinDamageModifier = 1.05,
				EffectMaxDamageModifier = 1.15,
				EffectCritChanceBonus = 100,
				EffectDuration = 6,
				CompletionDelay = 1,
				StaminaCost = 20,
				combatAbilityScriptOnUser = "ca_eviscerate_user_effect",
				TooltipString = "[$2671]",
				AbilityEnabled = true,
				AssociatedSkill = "SlashingSkill",	
				LevelRequired = 70,
			},	
		---------------------------------------------------------------------------------------------------------------
		--Piercing abilities
		---------------------------------------------------------------------------------------------------------------

		Perforate = { 
			Icon='barrage',
			ApplicableWepDamClasses = { 
				Piercing = true,
			},
			AutoCooldown = true,
			CooldownTimer=40,
			AbilityTriggertype = {
				Instant = true,
				Triggered = false
			},
			MaxCharges = 0,
			EffectMinDuration = 2,
			EffectMaxDuration = 7,
			EffectCritChanceBonus = 100,			
			CompletionDelay = 1,
			StaminaCost = 50,
			combatAbilityScriptOnUser = "ca_perforate_user_effect",
			TooltipString = "[$2672]",
			AbilityEnabled = true,
			AssociatedSkill = "PiercingSkill",	
			LevelRequired = 70,
			DependentUserCombatState = {
				Cloaked = false,
			},			
		},

		Puncture = {
			ApplicableWepDamClasses = { 
				Piercing = true,
			},
			AbilityTriggertype= {
				Triggered = true
			},
			AutoCooldown = true,
			CombatAbilityCritChanceModifier = 3,
			CooldownTimer = 10,
			MinDamModTrig = 110,
			MaxDamModTrig = 125,
			StaminaCost = 40,
			nextSwingDelay= 1,
			AttackerFX = "BuffEffect_M",
			AttackerSFX = "Puncture",
			AttackerFXBone = "Ground",
			TargetFX = "HitSwipe",
			TooltipString = "[$2673]",
			AbilityEnabled = true,
			AssociatedSkill = "PiercingSkill",
			LevelRequired = 50,
			SwingAnimation = "impale",
			BonusPenetration = 5,
		},
		---------------------------------------------------------------------------------------------------
		--Bashing abilities
		---------------------------------------------------------------------------------------------------
		Onslaught = { 
			--Icon='Hammer Smash',
			ApplicableWepDamClasses = { 
				Bashing = true,
			},
			AutoCooldown = true,
			CooldownTimer=30,
			AbilityTriggertype = {
				Instant = true,
				Triggered = false
			},
			MaxCharges = 0,
			CompletionDelay = 1,
			StaminaCost = 20,
			combatAbilityScriptOnUser = "ca_onslaught_user_effect",
			TooltipString = "[$2674]",
			AbilityEnabled = true,
			AssociatedSkill = "BashingSkill",	
			LevelRequired = 50,
			DependentUserCombatState = {
				Cloaked = false,
			},			
		},
		--DFB TODO: Add leap.
		Crushingstrike = { 
				ApplicableWepDamClasses = { 
					Bashing = true,
				},
				CooldownTimer=30,
				AbilityTriggertype = {
						Instant = true,
						Triggered = false
				},
				AutoCooldown = true,
				EffectMinDamageBonus = 3,
				EffectMaxDamageBonus = 12,
				EffectCritChanceBonus = 100,
				EffectDuration = 10,
				CompletionDelay = 1,
				StaminaCost = 30,
				combatAbilityScriptOnUser = "ca_crushing_strike_user_effect",
				TooltipString = "Type: Instant\nIncreases crit chance to 100%. Bonus Damge dealt to target 5-20. Target stamina drained by 10-30",
				AbilityEnabled = true,
				AssociatedSkill = "BashingSkill",	
				LevelRequired = 70,
				Icon ="Thunder Strike",
			},	

		---------------------------------------------------------------------------------------------------
		--Other abilities
		---------------------------------------------------------------------------------------------------
		Pommel = {
			AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			CooldownTimer = 5,
			MinDamModInst = 30,
			MaxDamModInst = 50,
			StaminaCost = 5,
			DependentUserCombatState = {
				Countering = true
			},
			CanMiss = false,
			nextSwingDelay= .2,
			isTargetRequired = true,
			OverrideDamageClass = "Bashing",
			TargetFX = "BloodEffect_A",
			AttackerFX = "BuffEffect_B",
			AttackerSFX = "Pommel",
			TooltipString = "[$2675]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 30,
			SwingAnimation = "pommel",
			triggerGlobalCoolOnAttack = true,		
			GlobalCooldownReduction = .8,		
		},

		Shield_Bash = {
			DisplayName = "Shield Bash",
			AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			CooldownTimer = 6,
			MinDamModInst = 100,
			MaxDamModInst = 125,
			StaminaCost = 10,
			ShieldAbility = true,
			DependentUserCombatState = {
				Blocking = true,
			},
			OverrideAttackSlot = "LeftHand",
			CanMiss = false,
			nextSwingDelay= .2,
			isTargetRequired = true,
			OverrideDamageClass = "Bashing",
			TargetFX = "ShieldBashEffect",
			TargetSFX = "ShieldBash",
			TooltipString = "[$2676]",
			AbilityEnabled = true,
			AssociatedSkill = "BlockingSkill",
			LevelRequired = 20,
			SwingAnimation = "shield_bash",
			triggerGlobalCoolOnAttack = true,		
			GlobalCooldownReduction = .2,

		},
				

		-------------------------------------------------------------------------------------
		--AOE combat abilities
		-------------------------------------------------------------------------------------
		Shockwave = {
			DisplayName = "Shockwave",
			AbilityTriggertype={
				Instant = true,
			},
			CooldownTimer = 50,
			Duration = 6,
			CompletionDelay = 2,
			combatAbilityScriptOnUser = "ca_shockwave_script",
			ResetSwingTime = true,
			isTargetRequired = false,
			StaminaCost = 55,
			TooltipString = "[$2677]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 70,
			StaminaCostDivisor = 1,
			SwingAnimation = "sword_in_ground",
			triggerGlobalCoolOnAttack = true,		

		},		

		Shout = {
			DisplayName = "Shout",
			AbilityTriggertype={
				Instant = true,
			},
			isTargetRequired = false,
			CooldownTimer = 45,
			Duration = 6,
			CompletionDelay = 2,
			combatAbilityScriptOnUser = "ca_shout_effect",
			ResetSwingTime = true,
			isTargetRequired = false,
			StaminaCost = 30,
			TooltipString = "[$2678]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 45,
			StaminaCostDivisor = 1,
			SwingAnimation = "cast_heal",
			IsPeacefulAbility = true,
			Icon = "Flame Mark",
		},		

		Wideswipe = { 
			Icon = "wideswept",
			DisplayName = "Wideswipe",
			--AbilityType = "InstantAttack",
			--AlwaysUsable = true,
			AbilityTriggertype= {
					--Instant = true,
					Triggered = true,
			},
			RequiredRangeGroup = "Long",
			MinDamModTrig = 65,
			MaxDamModTrig = 85,
			isTargetRequired = true,
			ResetSwingTime = true,
			CooldownTimer = 20,
			StaminaCost = 45,
			nextSwingDelay = .4,
			combatAbilityTriggerScriptUser = "ca_wideswipe_effect",
			FreezeDuration = 2000,
			AttackerFX = "BuffEffect_F",
			AttackerFXBone = "Ground",
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2679]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 60,
			SwingAnimation = "wideswept_attack",
		},	


		-------------------------------------------------------------------------------------
		--Disabled
		-------------------------------------------------------------------------------------

		--We don't have an animation for this yet.
		Spinattack = {
			DisplayName = "Spin Attack",
		--[[	ApplicableWepDamClasses = { 
				Slashing = true,
			},
		
			AbilityTriggertype={
			Instant = true,
			},
			CooldownTimer = 40,
			Duration = 6,
			CompletionDelay = 1,
			combatAbilityScriptOnUser = "ca_spinattack_script",
			ResetSwingTime = true,
			isTargetRequired = false,
			StaminaCost = 45,
			TooltipString = "[$2680]",
			AbilityEnabled = false,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 80,
			StaminaCostDivisor = 1,
			AbilityType = "InstantAttack",
			SwingAnimation = "sword_in_ground",

		},

		-- Slot 3 (E) --Counter Abilities	
		Crosscut = {
			DisplayName = "Cross Cut",
			ApplicableWepDamClasses = { 
				Slashing = true,
			},
			AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			CooldownTimer = 6,
			isTargetRequired = true,
			MinDamModInst = 110,
			MaxDamModInst = 125,
			StaminaCost = 15,
			nextSwingDelay= .4,
			combatAbilityScriptOnUser = "ca_crosscut_script",
			TargetFX = "SunderEffect",
			TooltipString = "[$2681]",
			AbilityEnabled = false,
			AssociatedSkill = "SlashingSkill",
			LevelRequired = 30,
			DependentUserCombatState = {
				Countering = true
			},
			triggerGlobalCoolOnAttack = true,		
			GlobalCooldownReduction = .5,
		},

		Riposte = {
			ApplicableWepDamClasses = { 
				Piercing = true,
			},
			AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			MinDamModInst = 100,
			MaxDamModInst = 110,
			CooldownTimer = 6,
			isTargetRequired = true,
			CombatAbilityCritChanceModifier = 3,
			StaminaCost = 15,
			nextSwingDelay= .2,
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2682]",
			AbilityEnabled = false,
			AssociatedSkill = "PiercingSkill",
			LevelRequired = 30,
			SwingAnimation = "lunge",
			DependentUserCombatState = {
				Countering = true
			},
			triggerGlobalCoolOnAttack = true,		
			GlobalCooldownReduction = .5,

		},

		DrivingStrike = {
			DisplayName = "Driving Strike",
			ApplicableWepDamClasses = { 
				Bashing = true,
			},
			AbilityType = "InstantAttack",
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = true,
			CooldownTimer = 6,
			isTargetRequired = true,
			CombatAbilityCritChanceModifier = 1.2,
			MinDamModInst = 105,
			MaxDamModInst = 115,
			StaminaCost = 20,
			nextSwingDelay= .2,
			combatAbilityScriptOnTarget = "ca_drivingstrike_effect",
			combatAbilityTriggerScriptUser = "ca_drivingstrike_user",
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2683]",
			AbilityEnabled = false,
			AssociatedSkill = "BashingSkill",
			LevelRequired = 30,
			DependentUserCombatState = {
				Countering = true
			},
			triggerGlobalCoolOnAttack = true,		
			GlobalCooldownReduction = .5,
		},
		-------------------------------------------------------------------------------------
		--Probablly shouldn't be combat abilities
		-------------------------------------------------------------------------------------

		Taunt = {
			AbilityTriggertype={
				Instant = true
			},
			CooldownTimer = 8,
			StaminaCost = 0,
			combatAbilityScriptOnUser = "ca_taunt_target_effect",
			ResetSwingTime = false,
			isTargetRequired = true,
			TooltipString = "[$2684]",
			AbilityEnabled = true,
			AssociatedSkill = "MeleeSkill",
			LevelRequired = 15,
			StaminaCostDivisor = 2,
			SwingAnimation = "ShowingOff",
		},

		Evasion = { 
			CooldownTimer=40,
			AbilityTriggertype = {
					Instant = true,
					Triggered = false
			},
			AutoCooldown = true,
			MaxCharges = 0,
			EffectMinDuration = 2,
			EffectMaxDuration = 7,
			CompletionDelay = 1,
			StaminaCost = 35,
			AttackerSFX = "Evasion",
			combatAbilityScriptOnUser = "ca_evasion_user_effect",
			TooltipString = "[$2685]",
			AbilityEnabled = true,
			AssociatedSkill = "DodgeSkill",	
			LevelRequired = 40,
			DependentUserCombatState = {
				Cloaked = false,
			},			
		},

		Backstab = {
			AbilityTriggertype={
				Instant = true
			},
			RequiredRangeGroup = "Short",
			ApplicableWeapons = {
				Dagger = true,
				Dirk = true,
				Talon = true,
				Stiletto = true,
				Spade = true,
				Spikes = true,
				Leafblade = true,

			},
			AutoCooldown = false,
			--AbilityType = "InstantAttack",
			CooldownTimer = 10,
			MinDamModTrig = 250,
			MaxDamModTrig = 500,
			RangeModifier = 3,
			StaminaCost = 10,
			combatAbilityScriptOnUser ="sca_backstab",
			combatAbilityTriggerScriptTarget = "sca_backstab_effect",
			CombatAbilityCritChanceModifier = 10,
			ResetSwingTime = true,
			isTargetRequired = false,
			CanMiss = false,
			TargetFX = "BloodEffect_A",
			TooltipString = "[$2686]",
			AbilityEnabled = true,
			AssociatedSkill = "RogueSkill",
			LevelRequired = 10,
			StaminaCostDivisor = 2,
			SwingAnimation = "impale",
			OverrideDamageClass = "TrueDamage",
			abilityRangeBonus = 1,	
			SkillRequirement = {
					RogueSkill = 10,
				},	
			DependentUserCombatState = {
				Cloaked = true,
			},	
			-- this prevents you from entering combat before the backstab initiates
			IsPeacefulAbility = true,
		},

		Sprint = {
			AlwaysUsable = true,
			AbilityTriggertype={
				Instant = true
			},
			AutoCooldown = false,
			StaminaCost = 0,
			combatAbilityScriptOnUser = "ca_sprint_script",
			TooltipString = "[$2687]",
			IsPeacefulAbility = true,
			AbilityEnabled = true,
			AssociatedSkill = "",
		}--]]
	}
}