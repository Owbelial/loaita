StatStats = {
	Strength = {
		GainFactor = 0.15,
	},
	Agility = {
		GainFactor = 0.15,
	},
	Intelligence = {
		GainFactor = 0.15,
	},
	Constitution = {
		GainFactor = 0.1,
	},
	Wisdom = {
		GainFactor = 0.15,
	},
	Will = {
		GainFactor = 0.15
	},
}