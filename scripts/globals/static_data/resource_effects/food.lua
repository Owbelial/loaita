ResourceEffectData.MushroomsPoison = {
    MobileEffect = "Poison",
    MobileEffectArgs = {
        PulseMax = 12,
        PulseFrequency = TimeSpan.FromSeconds(2),
    },
    SelfOnly = true,
    Tooltip = {
        "Will make you sick.",
    },
}


--useful for stunning yourself when testing.
ResourceEffectData.StunBread = {
    MobileEffect = "Stun",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(5),
    },
    SelfOnly = true,
    Tooltip = {
        "Will stun you.",
    },
}