ResourceEffectData.PotionGreaterHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        Amount = 200,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 200 health.",
    }
}

ResourceEffectData.PotionHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        Amount = 100,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 100 health.",
    }
}

ResourceEffectData.PotionLesserHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        Amount = 50,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 50 health.",
    }
}

ResourceEffectData.PotionMana = {
    MobileEffect = "PotionMana",
    MobileEffectArgs = {
        Amount = 60,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 60 mana.",
    }
}

ResourceEffectData.PotionCure = {
    MobileEffect = "PotionCure",
    SelfOnly = true,
    Tooltip = {
        "Instantly cure yourself of poison.",
    }
}