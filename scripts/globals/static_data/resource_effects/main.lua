
ResourceEffectData = {}

require 'globals.static_data.resource_effects.healing'
require 'globals.static_data.resource_effects.food'
require 'globals.static_data.resource_effects.tinderbox'
require 'globals.static_data.resource_effects.potions'
require 'globals.static_data.resource_effects.scrolls'