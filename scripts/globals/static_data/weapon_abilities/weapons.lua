
WeaponAbilitiesData.Stun = {
    TargetMobileEffect = "Stun",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(1.5),
        PlayerDuration = TimeSpan.FromSeconds(1),
    },
    Stamina = 50,
    Action = {
        DisplayName = "Stun",
        Tooltip = "Stun your opponent for 1.5 seconds. Reduced to 1 second against players.",
        Icon = "Flame Mark",
        Enabled = true
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Cleave = {		
    MobileEffect = "Cleave",
    MobileEffectArgs = {
        Range = 4
    },
    Stamina = 20,
    Action = {
        DisplayName = "Cleave",
        Tooltip = "Damage all targets 4 yards in front of you.",
        Icon = "Cleave",
        Enabled = true
    }
}

WeaponAbilitiesData.Knock = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_I"
        }
    },
    TargetMobileEffect = "Daze",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(3),
    },
    Stamina = 50,
    Action = {
        DisplayName = "Knock",
        Tooltip = "Daze your target for up to 3 seconds.",
        Icon = "Sure Strike",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Concus = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_I"
        }
    },
    TargetMobileEffect = "Concus",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(15),
        Modifier = -0.25
    },
    Stamina = 50,
    Action = {
        DisplayName = "Concus",
        Tooltip = "Your opponent's mana is reduced by 25% for 15 seconds.",
        Icon = "Sure Strike",
        Enabled = true,
    }
}

WeaponAbilitiesData.FollowThrough = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_L"
        }
    },
    TargetMobileEffect = "AdjustStamina",
    TargetMobileEffectArgs = {
        Amount = 20,
    },
    Stamina = 20,
    Action = {
        DisplayName = "Follow Through",
        Tooltip = "Remove 20 stamina from your opponent.",
        Icon = "Hammer Smash",
        Enabled = true,
    },
}

WeaponAbilitiesData.Eviscerate = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_M"
        }
    },	
    TargetMobileEffect = "Eviscerate",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromMilliseconds(1500),
        WeaponDamageModifier = 1.35
    },
    Stamina = 20,
    Action = {
        DisplayName = "Eviscerate",
        Tooltip = "Cause 35% additional weapon damage 1.5 seconds after a strike.",
        Icon = "Sap2",
        Enabled = true
    }
}

WeaponAbilitiesData.Bleed = {		
    TargetMobileEffect = "Bleed",
    TargetMobileEffectArgs = {
        PulseFrequency = TimeSpan.FromSeconds(2),
        PulseMax = 5,
        DamageMin = 20,
        DamageMax = 40,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Bleed",
        Tooltip = "Cause 20-40 damage a second for 20 seconds.",
        Icon = "Shred",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Dismount = {		
    TargetMobileEffect = "Dismount",
    Stamina = 20,
    Action = {
        DisplayName = "Dismount",
        Tooltip = "Remove your foe from his mount. Cannot use while mounted.",
        Icon = "Revenge2",
        Enabled = true
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Bash = {
    MobileEffect = "Bash",
    MobileEffectArgs = {
        WeaponAttackModifier = 1,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Bash",
        Tooltip = "Increase your attack by 100% of your weapon's attack rating for this swing.",
        Icon = "Blessed Hammer",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Power = {
    MobileEffect = "Power",
    MobileEffectArgs = {
        WeaponAttackModifier = 0.5,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Power Shot",
        Tooltip = "Increase your attack by 50% of your weapon's attack rating for this shot.",
        Icon = "Wild Shot",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Slash = {
    MobileEffect = "Slash",
    MobileEffectArgs = {
        WeaponAttackModifier = 1,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Slash",
        Tooltip = "Increase your attack by 100% of your weapon's attack rating for this swing.",
        Icon = "sunder",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Overpower = {
    MobileEffect = "Overpower",
    MobileEffectArgs = {
        WeaponAttackModifier = 0.7,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Overpower",
        Tooltip = "Increase your attack by 70% of your weapon's attack rating for this swing.",
        Icon = "Hammer Smash",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Stab = {
    MobileEffect = "Stab",
    MobileEffectArgs = {
        WeaponAttackModifier = 3.5,
        HitChanceFront = 0.4,
        HitChanceBack = 0.7,
    },
    Stamina = 50,
    Action = {
        DisplayName = "Stab",
        Tooltip = "Increase your attack by 350% of your weapon's attack rating for this swing.",
        Icon = "Fatal Strike",
        Enabled = true,
    },
    SkipHitAction = true
}