CharacterCustomization = {
	BodyTypesMale = {
		{Name = "Male A",},
		{Name = "Male B",Template = "head_male02"},
		{Name = "Male C",Template = "head_male03"},
		{Name = "Male D",Template = "head_male04"},
		{Name = "Male E",Template = "head_male05"},
	},
	BodyTypesFemale = {
		{Name = "Female A",Template = "head_female01"},
		{Name = "Female B",Template = "head_female02"},
		{Name = "Female C",Template = "head_female03"},
		{Name = "Female D",Template = "head_female04"},
		{Name = "Female E",Template = "head_female05"},
	},

	HairTypesMale = {
		{Name = "Regular",Template = "hair_male"},
		{Name = "Bangs",Template = "hair_male_bangs"},
		{Name = "Buzzcut",Template = "hair_male_buzzcut"},
		{Name = "Bald"},
		{Name = "Messy",Template = "hair_male_messy"},
		{Name = "Nobleman",Template = "hair_male_nobleman"},
		{Name = "Roguish",Template = "hair_male_rougish"},
		{Name = "Side-Undercut",Template = "hair_male_sideundercut"},
		{Name = "Undercut",Template = "hair_male_undercut"},
		{Name = "Windswept",Template = "hair_male_windswept"},
		--{Name = "Long",Template = "hair_male_long"},
	},

	HairTypesFemale = {
		{Name = "Pony-Tail A",Template = "hair_female"},
		{Name = "Bob",Template = "hair_female_bob"},
		{Name = "Bun",Template = "hair_female_bun"},
		{Name = "Buzzcut",Template = "hair_female_buzzcut"},
		{Name = "Pigtails",Template = "hair_female_pigtails"},
		{Name = "Pony-Tail B",Template = "hair_female_ponytail"},
		{Name = "Shaggy",Template = "hair_female_shaggy"},
		{Name = "Bald"},
	},

	HairColorTypes = {
		{Name = "Dark Brown", Color = "0xFF554838"},
		{Name = "Brown", Color = "0xFFA56B46"},
		{Name = "Light-Brown", Color = "0xFFA7856A"},
		{Name = "Red", Color = "0xFFB55239"},
		{Name = "Black", Color = "0xFF191919"},
		{Name = "Gray", Color = "0xFFB7A69E"},
		{Name = "Blonde", Color = "0xFFB9B184"},
		{Name = "Dark Red", Color = "0xFF380000"},
		{Name = "Orange", Color = "0xFFFF7F00"},
		{Name = "White", Color = "0xFFFFFFFF"},
		{Name = "Purple", Color = "0xFF990099"},
		{Name = "Dark Blue", Color = "0xFF012265"},
		{Name = "Green", Color = "0xFF339933"},
		{Name = "Blue", Color = "0xFF3399FF"},
		{Name = "Pink", Color = "0xFFFF66A3"},
	},

	StartingGear = {
		{ Name = "Warrior", Template = "startinggear_warrior" },
		{ Name = "Rogue", Template = "startinggear_rogue" },
		{ Name = "Archer", Template = "startinggear_archer" },
		{ Name = "Mage", Template = "startinggear_mage" },
		{ Name = "Tamer", Template = "startinggear_tamer" },
		--{ Name = "Bard", Template = "startinggear_bard" },
		{ Name = "Craftsman", Template = "startinggear_craftsman" }
	},
}