function JumbleContainerContents(lootObjects)
	for index, item in pairs(lootObjects) do
		if(item:GetLoc() == Loc.Zero) then
			local randomLoc = GetRandomDropPosition(this)
			item:UpdateContainerPosition(randomLoc)
		end
	end	
end

function TryStack(objToStack, containerObj, testOnly)
	-- if this is a stackable object look to see if we can stack it
	local resourceType = objToStack:GetObjVar("ResourceType")
	if( resourceType ~= nil and IsStackable(objToStack) ) then
		local resourceObj = FindResourceInContainer(containerObj,resourceType)
		if( resourceObj ~= nil and IsStackable(resourceObj) ) then
			if not(testOnly) then
				RequestStackOnto(resourceObj,objToStack)
			end
			return true
		end
	end

	return false
end

function GetContainerMaxWeight(container)	
	if(container ~= nil and container:IsEquipped()) then
		if(GetEquipSlot(container) == "Backpack") then
			local mobileObj = container:TopmostContainer()
			if(mobileObj ~= nil and mobileObj:IsMobile()) then
				return ServerSettings.Misc.BackpackBaseWeightLimit + GetStr(mobileObj)
			end
		elseif(container:IsEquipped() and GetEquipSlot(container) == "Bank") then
			return ServerSettings.Misc.BankWeightLimit			
		end
	end

	return ServerSettings.Misc.DefaultContainerWeightLimit
end

function CanAddWeightToContainer(container,objWeight)
	-- check weight requirement on all parent containers with a max weight
	local weightCont = nil
	local maxWeight = 0
	local canAdd = true

	ForEachParentContainerRecursive(container,true,
		function (parentCont)			
			maxWeight = GetContainerMaxWeight(parentCont)

			if(maxWeight ~= nil) then
				local curWeight = GetContentsWeight(parentCont)
				--DebugMessage("CHECKING WEIGHT",curWeight,objWeight,maxWeight)
		
				if(curWeight + objWeight > maxWeight) then
					canAdd = false
					weightCont = parentCont
					maxWeight = maxWeight
					return false
				end				
			end

			return true
		end)

	return canAdd,weightCont,maxWeight
end

function CanCreateItemInContainer(template,container,amount)
	amount = amount or 1

	local reason = nil

	if(container == nil) then
		return false,"invalidcontainer"
	end

	local templateResource = GetTemplateObjVar(template,"ResourceType")
	if (GetCapacity(container) <= GetItemCount(container) and not CanAddToStack(templateResource,container)) then
		return false, "full"
	end

	local templateWeight = GetCreationWeight(template,amount)
	if(templateWeight ~= -1) then
		local canAdd = CanAddWeightToContainer(container,templateWeight)
		if not(canAdd) then
			return false, "overweight"
		end
	end

	return true
end

function CanObjectFitInContainer(obj, container)
	return TryPutObjectInContainer(obj, container, Loc(0,0,0), false, false, true)
end

function TryPutObjectInContainer(obj, container, locInContainer, canOverfill, tryStack, testOnly)
	if(obj == container or container:IsContainedBy(obj)) then
		return false,"You can't put an object inside itself!"
	end	

	local objWeight = obj:GetSharedObjectProperty("Weight") or -1
	if(objWeight == nil or objWeight == -1) then
		return false,"That is too heavy for that container."
	end

	-- make sure this container or its parents are not for sale
	local isSaleContainer = false
    ForEachParentContainerRecursive(container,true,
        function (parentObj)
            if(parentObj:HasModule("hireling_merchant_sale_item")) then                
                isSaleContainer = true
                return false
            end
            return true
        end)

    if(isSaleContainer) then
        return false,"[$1853]"
    end

	if not(canOverfill) then
		local canAdd,weightCont,maxWeight = CanAddWeightToContainer(container,objWeight)

		-- if this would put any of our parent containers over their max weight then fail
		if not(canAdd) then
			return false,"The "..StripColorFromString(weightCont:GetName()).." cannot support any more weight. (Max: " .. tostring(maxWeight) .. " stones)"
		end
	end

	if( tryStack ) then
		if( TryStack(obj,container,testOnly) ) then
			return true
		end
	end

	if(locInContainer == nil) then
		locInContainer = GetRandomDropPosition(container)
	end

	if( canOverfill or container:CanHold(obj) ) then
		if not(testOnly) then
			obj:MoveToContainer(container, locInContainer)
		end
		return true
	end	

	return false,"There is not enough room for that object."
end

function CanAddToStack(resourceType, containerObj)
	local resourceObj = FindResourceInContainer(containerObj,resourceType)
	if( resourceObj ~= nil and IsStackable(resourceObj) ) then
		return true	
	end

	return false
end

function TryAddToStack(resourceType, containerObj, count)	
	if (containerObj == nil) then return false end
	if (resourceType == nil) then return false end
	if (count == nil) then return false end
	local resourceObj = FindResourceInContainer(containerObj,resourceType)
	if( resourceObj ~= nil and IsStackable(resourceObj) ) then
		local addWeight = GetUnitWeight(resourceObj,count)
		if(CanAddWeightToContainer(containerObj,addWeight)) then
			RequestAddToStack(resourceObj,count)
			return true, resourceObj
		else
			return false, "Weight"
		end
	end

	return false, "NotFound"
end

function GetCapacity(containerObj)
	if (containerObj == nil) then return 0 end
	return containerObj:GetSharedObjectProperty("Capacity") or 0;
end

function GetItemCount(containerObj)
	if (containerObj == nil) then return 0 end
	return #containerObj:GetContainedObjects()
end

function GetItemCountRecursive(containerObj)
	if (containerObj == nil) then return 0 end
	
	local count = 0
	ForEachItemInContainerRecursive(containerObj,function (contObj)
		count = count + 1
		return true
	end)

	return count
end

function FindObjectInContainer(containerObj,creationTemplate)
	local contObjects = containerObj:GetContainedObjects()
  	for index, contObj in pairs(contObjects) do
  		if(contObj:GetCreationTemplateId() == creationTemplate) then
  			return contObj
  		end
  	end	
end

-- close every sub container as well as the container
function CloseContainerRecursive(user,containerObj)
	local contObjects = containerObj:GetContainedObjects()
  	for index, contObj in pairs(contObjects) do
  		if(contObj:IsContainer()) then
  			CloseContainerRecursive(user,contObj)  			
  		end
  	end

  	containerObj:SendCloseContainer(user)
end

--- Consumes resource objects from a lua list (array) that match the specified resource type.
-- @param contents array ( the contents of a container normally ) DOES NOT GET CONTENTS OF CONTAINERS IN THE LIST
-- @param resourceType string The ResourceType to consume
-- @param amount(optional) double The amount of the resource to consume, defaults to 1
-- @param objVarName(optional) string Can be used to check against a different objvar than 'ResourceType' (defaults to ResourceType)
-- @return true or false
function ConsumeResource(contents, resourceType, amount, objVarName)
	amount = amount or 1
	objVarName = objVarName or "ResourceType"
	
	local resourceObjs = {}
	local total = 0
	for i,resourceObj in pairs(contents) do
		if ( resourceObj:GetObjVar(objVarName) == resourceType ) then
			table.insert(resourceObjs, resourceObj)
			total = total + GetStackCount(resourceObj)
		end
	end

	if( total >= amount ) then
		-- sort stackable objects from smallest to largest
		table.sort(resourceObjs,function(a,b) return GetStackCount(a)<GetStackCount(b) end)
		local remainingAmount = amount
		for index, resourceObj in pairs(resourceObjs) do
			local resourceCount = GetStackCount(resourceObj)
			if( resourceCount > remainingAmount ) then				
				RequestSetStackCount(resourceObj,resourceCount - remainingAmount)
				remainingAmount = 0
			else
				remainingAmount = remainingAmount - resourceCount
				resourceObj:Destroy()
			end

			if( remainingAmount == 0 ) then
				break
			end
		end
		return true
	end
	return false
end

--- Convenience function to consume resources directly from a container recursively
-- @param container
-- @param resourceType string
-- @param amount(optional) double The amount of the resource to consume, defaults to 1
-- @param objVarName(optional) defaults to "ResourceType"
-- @return true or false (success/fail)
function ConsumeResourceContainer(container, resourceType, amount, objVarName)
	local contents = GetResourcesInContainer(container, resourceType, objVarName)
	return ( contents and ConsumeResource(contents, resourceType, amount, objVarName) )
end

--- Convenience function to consume resources directly from a mobile's backpack, recursively.
-- @param mobileObj
-- @param resourceType string
-- @param amount(optional) double The amount of the resource to consume, defaults to 1
-- @param objVarName(optional) defaults to "ResourceType"
-- @return true or false (success/fail)
function ConsumeResourceBackpack(mobileObj, resourceType, amount, objVarName)
	local backpack = mobileObj:GetEquippedObject("Backpack")
	return ( backpack and ConsumeResourceContainer(backpack, resourceType, amount, objVarName) )
end