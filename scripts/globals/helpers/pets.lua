PetStance = {
	Passive = "Passive",
	Aggressive = "Aggressive"
}


--- Get the chance a master has to control a creature.
-- @param master mobileObj
-- @param creature mobileObj
-- @return from 0 to 1 percent based chance
function ChanceToControlCreature(master, creature)
	if ( creature == nil or not creature:IsValid() or IsDead(creature) ) then return 0 end
	-- prevent any chance to control on players and if the master somehow has too many pets.
	if ( IsPlayerCharacter(creature) ) then return 0 end
	if ( GetRemainingActivePetSlots(master) < 0 ) then
		if ( IsPlayerCharacter(master) ) then
			master:SystemMessage("Too many pets to control.", "info")
		end
		return 0
	end

	local petDifficulty = creature:GetObjVar("TamingDifficulty")

	if ( petDifficulty == nil ) then return 0 end

	local skillDictionary = GetSkillDictionary(master)

	local creatureChance = nil
	if ( petDifficulty >= 100 ) then
		petDifficulty = petDifficulty - 100
		creatureChance = ChanceToControl(skillDictionary, petDifficulty)
		if ( creatureChance > 0 ) then
			return ChanceToControl(skillDictionary, petDifficulty, "BeastmasterySkill")
		end
	end

	return creatureChance or ChanceToControl(skillDictionary, petDifficulty)
end

function ChanceToControl(masterSkillDictionary, petDifficulty, tamingSkill)
	if ( petDifficulty <= 29.1 ) then return 1 end
	tamingSkill = tamingSkill or "AnimalTamingSkill"

	local taming = (GetSkillLevel(nil,tamingSkill,masterSkillDictionary) or 0) - petDifficulty
	local lore = (GetSkillLevel(nil,"AnimalLoreSkill",masterSkillDictionary) or 0) - petDifficulty

	if ( taming < 0 ) then taming = taming * 28 else taming = taming * 6 end
	if ( lore < 0 ) then lore = lore * 14 else lore = lore * 6 end

	local chance = 700 + ( ( taming + lore) / 2 )

	if ( chance > 0 ) then
		if ( chance < 20 ) then
			chance = 20
		end
		chance = chance / 100
	end

	return chance
end

--- Send a pet command to a specific pet
-- @param pet mobileObj
-- @param command string The command to send
-- @param target(optional) mobileObj/Location
-- @param force(optional) boolean
function SendPetCommandTo(pet, command, target, force)
	if ( pet ~= nil and ValidPetCommand(command) ) then
		pet:SendMessage("UserPetCommand", command, target, force)
	end
end

--- Send a pet command to all pets in a list
-- @param pets luaArray Use the returned value from something like GetActivePets()
-- @param command string The command to send
-- @param target(optional) mobileObj/Location
-- @param force(optional) boolean
function SendPetCommandToAll(pets, command, target, force)
	if not( ValidPetCommand(command) ) then return end
	if not( pets ) then
		LuaDebugCallStack("Pets list not provided, use GetActivePets().")
		return
	end
    if ( #pets > 0 ) then
		for i,pet in pairs(pets) do
			if ( command == "go" and #pets > 1 ) then
				target = target:Project(72*i, 2)
			end
    		SendPetCommandTo(pet, command, target, force)
    	end
    end
end

--- Processes a pet command string
-- @param master mobileObj
-- @param cmd string First word in command string
-- @param args luaArray The rest of the words in the command string
function ProcessPetCommand(master, cmd, args)
	if ( #args == 1 or #args == 2 ) then
		-- should do better alias handling, but this works.
		if ( args[1] == "kill" ) then args[1] = "attack" end
		
		local command = args[1]
		local target = nil

		if ( PetCommands[command].target ) then
			if ( args[2] == "me" ) then
				target = master
			elseif ( args[2] == "target" or args[2] == "them" ) then
				target = master:GetObjVar("CurrentTarget")
				if ( not target or not target:IsValid() ) then
					return
				end
			else
				local eventId = "ProcessPetCommandRequestTarget"
				RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, eventId, function(targ)
					if ( targ == nil ) then return end
					if ( targ ~= master ) then
						FaceObject(master, targ)
						master:PlayAnimation("point")
					end
					FinalizePetCommand(master, cmd, command, targ)
				end)
				master:RequestClientTargetGameObj(master, eventId)
				return
			end
		end
		FinalizePetCommand(master, cmd, command, target)
	end
end

--- Finialize a pet command, extracted ending to ProcessPetCommand so we can request a target between start and finish
-- @param master mobileObj
-- @param name string Name of the creature you want to send this command to, send 'all' for all pets.
-- @param command string The pet command, check PetCommands
-- @param target(optional)
function FinalizePetCommand(master, name, command, target)
	if ( name == "all" ) then
		SendPetCommandToAll(GetActivePets(master), command, target)
	else
		SendPetCommandTo(GetActivePetByCommandName(GetActivePets(master), name), command, target)
	end
end

-- Determine if a given command for a pet is a valid
-- @param cmd string
-- @return true/false
function ValidPetCommand(cmd)
	return PetCommands[cmd] ~= nil
end

--- Determine if a mobileObj is tamable
-- @param mobileObj
-- @return true if mobileObj is tamable
function IsTamable(mobileObj)
	return mobileObj and mobileObj:HasObjVar("TamingDifficulty") and not mobileObj:HasModule("merchant_sale_item")
end

--- Determine if a mobileObj is a pet
-- @param mobileObj
-- @return true if mobileObj is a pet
function IsPet(mobileObj)
	return mobileObj and mobileObj:HasModule("pet_controller")
end

--- Determine if a mobileObj is a tamed pet
-- @param mobileObj
-- @return true if mobileObj is a tamed pet
function IsTamedPet(mobileObj)
	return ( IsPet(mobileObj) and IsTamable(mobileObj) )
end

function IsController(controller,mobileObj)
	return controller ~= nil and (mobileObj:GetObjectOwner() == controller or mobileObj:GetObjVar("controller") == controller)
end

function GetPetStance(mobileObj)
	if ( mobileObj == nil ) then return nil end
	return mobileObj:GetObjVar("PetStance") or PetStance.Passive
end

function ValidPetStance(stance)
	local found = false
	for key,val in pairs(PetStance) do
		if ( key == stance ) then
			found = true
			break
		end
	end
	if not( found ) then
		LuaDebugCallStack("Invalid Pet Stance: '"..tostring(stance).."' provided")
	end
	return found
end

function SetPetStance(mobileObj, stance)
	stance = stance or PetStance.Passive
	if ( not mobileObj or not ValidPetStance(stance) ) then return end
	if ( stance == PetStance.Passive ) then
		mobileObj:DelObjVar("PetStance")
	else
		mobileObj:SetObjVar("PetStance", stance)
	end
end

--- Get the maximum stable pet slots a master can have, each pet counts as 1 stable slot.
-- @param master mobileObj
-- @return number of maximum stable pet slots for master.
function MaxStabledPetSlots(master)
	return 6 -- all pets count as 1
end

--- Get the maximum active pet slots a master can have, each pet has individual slot count toward active pets slots.
-- @param master mobileObj
-- @return number of maximum active pet slots for master.
function MaxActivePetSlots(master)
	return 5
end

--- Get a master's stabled pet by a pet's gameObj id
-- @param master mobileObj
-- @param id double
-- @return stabledPet, totalStabledSlots
function GetStabledPetById(master, id)
	local stabledPets, slots = GetStabledPets(master)
	for i,pet in pairs(stabledPets) do
		if pet.Id == id then
			return pet, slots
		end
	end
	return nil, slots
end

--- Get a list of master's pets currently in stable
-- @param master mobileObj
-- @return stabledPets, totalStabledSlots
function GetStabledPets(master)
	local tempPack = master:GetEquippedObject("TempPack")
	if ( tempPack ) then
        local stabledPets = FindItemsInContainerRecursive(tempPack, IsPet)
        return stabledPets, #stabledPets
	end
	return {}, MaxStabledPetSlots(master) -- failed to lookup how many are stabled (missing temp pack?) so default
end

--- Get a master's active pet by a pet's gameObj id
-- @param master mobileObj
-- @param id double
-- @param stance(optional) string Get only pets with this stance.
-- @return activePet
function GetActivePetById(master, id, stance)
	local activePets = GetActivePets(master, stance)
	for i,pet in pairs(activePets) do
		if ( pet.Id == id ) then
			return pet
		end
	end
	return nil
end

--- Get an active pet by the command name of the pet (see SetCommandName for rules)
-- @param pets luaArray Use the return value from GetActivePets()
-- @param commandName string The command name of the pet you seek
-- @return mobileObj or nil
function GetActivePetByCommandName(pets, commandName)
	if not ( commandName ) then return end
	if not( pets ) then
		LuaDebugCallStack("Pets list not provided, use GetActivePets().")
		return
	end
	if ( #pets > 0 ) then
		for i,pet in pairs(pets) do
			if ( commandName == pet:GetObjVar("CommandName") ) then
				return pet
			end
		end
	end
	return nil
end

--- Get a list of active pets for a master, and also the slots taken by all active pets.
-- @param master mobileObj
-- @param stance, string(optional) Only return pets in this stance
-- @param includeMount, boolean(optional), set true to include the mount in the returned list
-- @return activePets, totalActiveSlots
function GetActivePets(master, stance, includeMount)
	if ( master == nil ) then
		LuaDebugCallStack("[GetActivePets] Nil master provided.")
		return {}
	end
	local pets = master:GetOwnedObjects() or {}
	-- account for mounted pet as well since they are technically in player's body container and not an Owned Object.
	if ( includeMount == true and IsMounted(master) ) then
		table.insert(pets, GetMount(master))
	end

	local slots = 0
	local out = {}
	for i,pet in pairs(pets) do
		if ( pet ~= master and not IsDead(pet) and IsPet(pet) and (stance == nil or GetPetStance(pet) == stance) ) then
			table.insert(out, pet)
			-- default to MaxActivePetSlots, if a pet doesn't have PetSlots set we don't want it to glitch and allow unlimited.
			slots = slots + ( pet:GetObjVar("PetSlots") or MaxActivePetSlots(master) )
		end
	end

	--master:NpcSpeech("Active pet slots: "..slots)
	return out, slots
end

--- Returns the number of active pet slots left for a master, ie how many more active pet slots can be filled.
-- @param master mobileObj
-- @return number of pet slot
function GetRemainingActivePetSlots(master)
	local pets, slots = GetActivePets(master, nil, true)
	return MaxActivePetSlots(master) - slots
end

--- Set's a pets command name, this is used in text commands when giving commands to specific pets.
-- @param pet mobileObj
-- @param name string
function UpdatePetCommandName(pet, name)
	pet:SetObjVar("CommandName", string.match( string.lower( StripColorFromString(name) ), "(%w+)") )
end

--- Set's a creature as a pet to a master, doesn't enforce valid Tamable creatures.
-- @param creature mobileObj
-- @param master mobileObj
-- @return boolean true if success
function SetCreatureAsPet(creature, master)
	if ( master == nil or creature == nil or not master:IsValid() or not creature:IsValid() or creature:IsPlayer() ) then return false end

	local myOldTeam = creature:GetObjVar("MobileTeamType")
	if(myOldTeam ~= nil) then
		creature:DelObjVar("MobileTeamType")
		creature:SetObjVar("OldMobileTeamType", myOldTeam)
	end
	--===============================================================
	-- QUEST EDIT
	GizmosCustomQuests.Helpers.CheckQuestObjectives(master, creature)
	--===============================================================
	creature:SetObjVar("LivingKarma", GetKarma(creature))

	local nameStr = StripColorFromString(creature:GetName())
	creature:SetObjVar("LivingName", nameStr)
	UpdatePetCommandName(creature, nameStr)
	
	creature:AddModule("pet_controller")
	creature:SendMessage("SetPetOwner", master)

	creature:SetObjVar("MobileTeamType","Villagers")
	creature:SetMobileType("Friendly")

	creature:SetObjVar("NoReset",true)
	
	if(creature:HasModule("decay")) then
		creature:DelModule("decay")
	end

	creature:SetObjVar("HasSkillCap",true)
	creature:SendMessage("EndCombatMessage")
	creature:PlayObjectSound("Pain")

	master:SystemMessage("You have tamed "..StripColorFromString(nameStr),"info")

	return true
end

function UpdatePetName(mobileObj, newName, colorized)
	mobileObj:SetName(colorized)
	UpdatePetCommandName(mobileObj, newName)
	mobileObj:SetSharedObjectProperty("DisplayName", colorized)
end


--- Determine if a master can control a creature
-- @param master mobileObj
-- @param creature mobileObj
-- @return true/false
function CanControlCreature(master, creature)
	return ChanceToControlCreature(master, creature) > 0
end

--- Get the chance to tame a animal/beast, to tame a beast both successes must pass
-- @param tamingSkillRequired double
-- @param animalTamingSkillLevel double
-- @param beastmasterSkillLevel double
-- @return animalChance(double), beastChance(double) (or nil if not beast)
function ChanceToTameAnimalBeast(tamingSkillRequired, animalTamingSkillLevel, beastmasterSkillLevel)
	if ( tamingSkillRequired == nil or animalTamingSkillLevel == nil or beastmasterSkillLevel == nil ) then return 0 end

	local beastChance = nil
	if ( tamingSkillRequired > 100 ) then
		beastChance = tamingSkillRequired - 100
		beastChance = SkillValueMinMax( beastmasterSkillLevel, beastChance, beastChance + 50 )
	end

	return SkillValueMinMax( animalTamingSkillLevel, tamingSkillRequired, tamingSkillRequired + 50 ), beastChance
end

--- given a target and a potential master, returns true/false for a successful tame, also skill checks proper.
-- @param creature mobileObj
-- @param master mobileObj
-- @return boolean true is success, otherwise false
function CheckAnimalTamingSuccess(creature, master)
	if ( creature == nil or master == nil or not creature:IsValid() or not master:IsValid() ) then return false end
	
	local previousOwners = creature:GetObjVar("PreviousOwners") or {}
	for i,id in pairs(previousOwners) do
		if ( id == master.Id ) then
			-- master has previously tamed this creature, 100% chance
			return true
		end
	end

	local requiredSkillToTame = creature:GetObjVar("TamingDifficulty")
	if ( requiredSkillToTame == nil ) then return false end

	local skillDictionary = GetSkillDictionary(master)
	local animalTamingSkillLevel = GetSkillLevel(master, "AnimalTamingSkill", skillDictionary)
	local animalLoreSkillLevel = GetSkillLevel(master, "AnimalLoreSkill", skillDictionary)
	local beastmasterSkillLevel = GetSkillLevel(master, "BeastmasterySkill", skillDictionary)

	local animalChance, beastChance = ChanceToTameAnimalBeast(requiredSkillToTame, animalTamingSkillLevel, beastmasterSkillLevel)
	--master:NpcSpeech("animalChance: "..animalChance)
	if ( beastChance ) then
		--master:NpcSpeech("beastChance: "..beastChance)
		-- skill check animal lore ( for gains )
		CheckSkillChance(master, "AnimalLoreSkill", animalLoreSkillLevel, animalChance)
		-- skill check taming ( for gains)
		CheckSkillChance(master, "AnimalTamingSkill", animalTamingSkillLevel, animalChance)
		return CheckSkillChance(master, "BeastmasterySkill", beastmasterSkillLevel, beastChance)
	else
		-- skill check animal lore ( for gains )
		CheckSkillChance(master, "AnimalLoreSkill", animalLoreSkillLevel, animalChance)
		return CheckSkillChance(master, "AnimalTamingSkill", animalTamingSkillLevel, animalChance)
	end

end

--- Given a creature and master will determine if a creature can be tamed in the given moment.
-- @param creature mobileObj
-- @param master mobileObj
-- @return boolean true if valid taming target, string Error message if not success
function ValidAnimalTamingTarget(creature, master)
	if ( creature == nil or not(IsTamable(creature)) or creature:HasModule('pet_controller') ) then
		return false, "That cannot be tamed."
	end
	if not( CanAddToActivePets(master, creature) ) then
		return false, "Cannot control that many creatures at once."
	end
	if not( CanControlCreature(master, creature) ) then
		return false, "You have no chance of controlling that creature."
	end
	if ( HasMobileEffect(creature, "BeingTamed") ) then
		return false, "That creature is already being tamed."
	end
	if ( master:GetLoc():Distance(creature:GetLoc()) > ServerSettings.Pets.Taming.Distance ) then
		return false, "Too far away to tame that."
	end
	return true
end

function CanAddToActivePets(master, creature)
	return (creature:GetObjVar("PetSlots") or MaxActivePetSlots(master)) <= GetRemainingActivePetSlots(master)
end