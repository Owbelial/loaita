

Currencies = {
	coins = 1,
	KhoToken = 12,
}

function GetNumBonuses(item)
	local objVarDict = item:GetAllObjVars()

	local bonusCount = 0
	for name,varValue in pairs(objVarDict) do
		if( name:match("Bonus") ) then
			bonusCount = bonusCount + 1
		end
	end

	return bonusCount
end

function GetItemBaseBonusValueMod(item)
	local baseStart = item:GetObjVar("Durability")
	if(baseStart == nil) then return 0 end

	local basePriceMod = baseStart * DURABILTY_VALUE_MULTIPLIER * (baseStart * 0.06)^2
	for i,v in pairs(ItemPropertyValues) do
		local curVal = item:GetObjVar(i)
		if(curVal ~= nil) then
			local curPot = curVal / ItemPropertyValues[i].gradient
			local modiEff = 1
			if(curPot < 0) then modiEff = .5 end
			local newVal = curPot * ItemPropertyValues[i].value * modiEff
			basePriceMod = basePriceMod + newVal
		end
	end

	-- when the mod is 0 that means the item has no bonuses
	if(basePriceMod == 0) then
		return 0
	end

	return basePriceMod /100
end


function GetBaseItemValue(item)
	-- first check for custom value by template id
	--LuaDebugCallStack("tf")
	local itemTemplateId = item:GetCreationTemplateId()
	if(itemTemplateId == nil) then
		DebugMessage("WARNING: Item has no creation template! "..tostring(item:GetName()))
		return 0
	end
	
	if (item:HasObjVar("Worthless")) then
		return 0 
	end

	if (item:HasModule("packed_object")) then
		itemTemplateId = item:GetObjVar("UnpackedTemplate")
	end

	if ( item:IsValid() and ( item:HasObjVar("lockObject") or item:HasObjVar("lockUniqueId") ) ) then
		return 0
	end
	
	if( CustomItemValues[itemTemplateId] ~= nil ) then
		return CustomItemValues[itemTemplateId]
	end

	-- most animal parts are worth 5
	if( itemTemplateId:match("animalparts") ) then
		return 3
	end

	if (item:HasObjVar("Valuable")) then
		return item:GetObjVar("Valuable")
	end

	-- some resource types have value
	local resourceType = item:GetObjVar("ResourceType")
	if( resourceType ~= nil ) then
		if( resourceType == "coins") then
			return 0
		-- refresh potions are worth more
		elseif( resourceType == "potion_refresh_regen") then
			return 5		
		elseif( resourceType:match("potion") ) then
			return 3
		elseif( resourceType:match("bandages") ) then
			return 3
		elseif( resourceType:match("Wood") ) then
			return 1
		elseif( resourceType:match("Stone") ) then
			return 1
		elseif( resourceType:match("Iron") ) then
			return 10
		end
	end

	local foodValue = item:GetObjVar("FoodValue")
	if( foodValue ~= nil ) then
		return math.max(1,math.floor(foodValue/4))
	end

	-- scrolls and skill books
	if( item:HasModule("spell_scroll") or item:HasModule("skill_book") ) then
		return 5
	end
	local itemBaseTemp = item:GetObjVar("BaseItemObjVar") or item:GetCreationTemplateId()
	if(itemBaseTemp ~= nil) then
		-- no explicit price for this base tempalte so lets
		-- extrapolate from the resource values
		local dur = item:GetObjVar("Durability")
		local maxDur = GetMaxDurabilityValue(item)
		local durRatio = 1
		if (dur ~= nil and maxDur ~= 0 ) then
			durRatio = dur/maxDur
		end

		local sourceRecipe = GetRecipeForBaseTemplate(itemBaseTemp)
		if(sourceRecipe ~= nil) then
			local canUseResourceMethod = true
			baseRet = 0
			local quality = GetQualityString(item)			
			local resourceTable = GetQualityResourceTable(sourceRecipe.Resources,quality)
			if(resourceTable == nil) then
				DebugMessage("WARNING: Recipe based item has invalid resource table "..tostring(quality),tostring(sourceRecipe.DisplayName),item:GetCreationTemplateId(),itemBaseTemp)				
			else
				for resourceName,recipeAmount in pairs (resourceTable) do
					if(ResourceData.ResourceInfo[resourceName] ~= nil) then
						local templateName = ResourceData.ResourceInfo[resourceName].Template
						if(CustomItemValues[templateName] ~= nil) then
							baseRet = baseRet + (CustomItemValues[templateName] * recipeAmount)
						else
							-- if one or more of the resources do not have a value specified we can not use this method
							canUseResourceMethod = false
							DebugMessage("WARNING: Recipe based item contains a resource which has no CustomItemValue "..tostring(resourceType),templateName)
						end
					else
						canUseResourceMethod = false
						DebugMessage("WARNING: Recipe based item contains a resource which has no Resource entry in ResourceData.ResourceInfo table. "..resourceName,sourceRecipe.DisplayName)
					end
				end

				if(canUseResourceMethod) then
					--DebugMessage("Price evaluation from resources "..baseRet,itemBaseTemp,quality)
					return baseRet * durRatio
				end
			end
		end		
	end
	local equipClass = GetEquipmentClass(item)
	if( equipClass == "WeaponClass" ) then
		local baseValue = 0
		local weaponClass = GetWeaponClass(item)
		if( weaponClass == "Vshort" ) then
			baseValue = 2
		elseif( weaponClass == "Short" or weaponClass == "ShortMid" ) then
			baseValue = 3
		elseif( weaponClass == "Mid" or weaponClass == "MidLong" ) then
			baseValue = 4
		elseif( weaponClass == "Long" ) then
			baseValue = 5
		end
		return baseValue + (GetNumBonuses(item) * 2)
	elseif( equipClass == "ArmorClass" ) then
		local baseValue = 0
		local baseMult = 1
		local armorClass = GetArmorClass(item)
		-- padded
		if( armorClass == "VeryLight") then
			baseMult = 1
		-- leather
		elseif( armorClass == "Light") then
			baseMult = 1.5
		-- chain and scale
		elseif( armorClass == "Medium" or armorClass == "Heavy") then
			baseMult = 2
		-- plate
		elseif( armorClass == "VeryHeavy") then
			baseMult = 2.5
		end

		local equipSlot = GetEquipSlot(item)
		if( equipSlot == "Head" or equipSlot == "Legs") then
			baseValue = 2 * baseMult
		elseif( equipSlot == "Chest" ) then
			baseValue = 4 * baseMult		
		end

		local combinedValue = baseValue + (GetNumBonuses(item) * 2)

		-- cultist armor is worth half
		if(itemTemplateId:match("cultist")) then
			combinedValue = combinedValue / 2
		end

		return math.floor(combinedValue)

	elseif( equipClass == "ShieldClass" ) then
		local baseValue = 0
		local shieldClass = GetShieldType(item)
		if( shieldClass == "Buckler") then
			baseValue = 2
		elseif( shieldClass == "Small" or shieldClass == "Medium") then
			baseValue = 3
		elseif( shieldClass == "Large" or armorClass == "ExtraLarge") then
			baseValue = 5		
		end
		--DebugMessage(GetNumBonuses(item))
		--DebugMessage(tostring(baseValue + (GetNumBonuses(item) * 2)))
		return baseValue + (GetNumBonuses(item) * 2)
	end
	return 0
end

function GetCurrencyValue(currency)
	return Currencies[currency]
end

function GetItemValue(item,currency)
	currency = currency or "coins"
	--DebugMessage("ItemValue",tostring(item),GetBaseItemValue(item),GetItemBaseBonusValueMod(item))
	return math.round((GetBaseItemValue(item) + GetItemBaseBonusValueMod(item))/GetCurrencyValue(currency))
end