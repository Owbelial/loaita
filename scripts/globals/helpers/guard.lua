
SuperGuardThingsToSay = {
	"Violence is not tolerated here!",
	"Enough! Slay them!",
	"The justice of the Guardian Order is swift.",
}


--- Get the protection type for a mobile
-- @param mobile
-- @param damager(optional)
-- @return "None", "InstaKill", or "Guard"
function GetGuardProtection(mobile, damager)
	if( mobile:IsInRegion("Arena")) then
		return "None"
	end

	if (damager ~= nil) then
		if( damager:IsInRegion("Arena")) then
			return "None"
		end
	end

	if(ServerSettings.PlayerInteractions.SlayImmediateProtectionZones) then
		for i,j in pairs(ServerSettings.PlayerInteractions.SlayImmediateProtectionZones) do
			if (mobile:IsInRegion(j)) then
				return "InstaKill"
			end
		end
	end

    --if this setting is enabled do for the entire map
    if (ServerSettings.PlayerInteractions.GuaranteedGuardProtectionFullMap) then
        return "Guard"
    end

	if(ServerSettings.PlayerInteractions.GuaranteedGuardProtectionZones) then
		for i,j in pairs(ServerSettings.PlayerInteractions.GuaranteedGuardProtectionZones) do
			if (mobile:IsInRegion(j) and (not(damager) or (damager and damager:IsInRegion(j)))) then
				return "Guard"
			end
		end
	end

	local curMap = GetWorldName()
	if(ServerSettings.PlayerInteractions.SlayImmediateProtectionMaps) then
		for i,j in pairs(ServerSettings.PlayerInteractions.SlayImmediateProtectionMaps) do
			if(curMap == j) then
				return "Guard"
			end
		end
	end

	local mobileLoc = mobile:GetLoc()
	local guardTower = FindObjectWithTagInRange("GuardTowerObject",mobileLoc,ServerSettings.PlayerInteractions.GuardTowerProtectionRange)
	if(guardTower) then
		return "GuardTower",guardTower
	end
	
	local teleportTower = FindObjectWithTagInRange("TeleportTowerObject",mobileLoc,ServerSettings.PlayerInteractions.GatekeeperProtectionRange)
	if(teleportTower) then
		return "GuardTower",guardTower
	end

    return "None"
end


--- Calling this on a lua vm context (attached module) will return the all guards near the gameObj of the lua VM context.
-- @return guards(luaTable of mobileObjs) or empty table
function GetNearbyGuards(neutral)
	local nearbyGuards = nil
	
	if ( neutral ) then
		return FindObjects(SearchMulti(
		{
			SearchMobileInRange(ServerSettings.PlayerInteractions.GuardTowerProtectionRange),
			SearchHasObjVar("IsNeutralGuard")
		})) or {}
	else
		return FindObjects(SearchMulti(
		{
			SearchMobileInRange(ServerSettings.PlayerInteractions.GuardTowerProtectionRange),
			SearchHasObjVar("IsGuard"),
		})) or {}
	end
end

--- Calling this on a lua vm context (attached module) will return the nearest guard to the gameObj of the lua VM context.
-- @param mobileObj guard closest to this mobile
-- @return guard(mobileObj) or nil
function GetNearestGuard(mobileObj)
	local nearbyGuards = GetNearbyGuards()

	local nearestGuard = nil
	local nearestDistance = 0
	for index, guardObj in pairs(nearbyGuards) do
		local distance = mobileObj:DistanceFrom(guardObj)
		if( nearestGuard == nil or distance < nearestDistance ) then
			nearestGuard = guardObj
			nearestDistance = distance
		end
	end

	return nearestGuard
end

--- Trigger guards to protect a victim from an aggressor
-- @param victim(mobileObj)
-- @param aggressor(mobileObj)
-- @param karmaLevelProtected(boolean) true is the karma level of vicitm is guard protected
-- @return none
function GuardProtect(victim, aggressor, karmaLevelProtected)
	if ( IsDead(aggressor) ) then return end
	
	if ( aggressor:HasObjVar("IsGuard") or aggressor:HasObjVar("Invulnerable") ) then
		return
	end

	if ( victim:HasTimer("EnteredProtection") ) then return end

	for i,pet in pairs(GetActivePets(aggressor)) do
		GuardProtect(victim, pet, karmaLevelProtected)
	end

	-- if the karma level is not protected, we call nearby neutral guards only
	if ( karmaLevelProtected ~= true ) then
		CallNearbyGuardsOn(aggressor, true)
		return
	end
	
	--if they're both players and victim is in a place karma matters
	if ( IsPlayerCharacter(aggressor) and IsPlayerCharacter(victim) and WithinKarmaArea(victim) ) then

		local guardProtectionType = GetGuardProtection(victim, aggressor)

		-- instantly kill the attacking player in certain situations
		if ( guardProtectionType == "InstaKill" and not IsDemiGod(aggressor) ) then
			aggressor:SendMessage("ProcessTrueDamage", aggressor, 5000, true)
			aggressor:PlayEffect("LightningCloudEffect")
			aggressor:SystemMessage("[$1820]")
			aggressor:SystemMessage("[$1820]","info")
			return
		end

		-- spawn super guards in other situations
		if ( guardProtectionType == "Guard" ) then
			for i=0,1 do
				local spawnPosition = GetNearbyPassableLoc(victim,360,6,7)
				CreateObj("super_guard", spawnPosition, "super_guard", aggressor)
			end
			return
		end

	end

	-- not guaranteed protection/not both players, add threat to all nearby physical guards
	CallNearbyGuardsOn(aggressor)
end

function CallNearbyGuardsOn(aggressor, neutral)
	local nearbyGuards = GetNearbyGuards(neutral)
	for i,guard in pairs(nearbyGuards) do
		guard:SendMessage("AddThreat", aggressor, 100)
	end
end

--- Update the UI with the player's current protection status and message the user when changed.
-- @param player(playerObj)
-- @return none
function UpdatePlayerProtection(player)
	local curProtection = player:GetObjVar("GuardProtection")
	local newProtection = GetGuardProtection(player)
	
    if(newProtection ~= curProtection) then
        player:SetObjVar("GuardProtection",newProtection)
        if(newProtection == "None") then                
            player:SystemMessage("[$1821]","event")
        elseif(newProtection == "Guard" or newProtection == "GuardTower") then
        	player:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"EnteredProtection")
            player:SystemMessage("[$1822]","event")
        elseif(newProtection == "InstaKill") then
            player:SystemMessage("[$1823]","event")
        end
        UpdateRegionStatus(player,nil,newProtection)
    end    
end