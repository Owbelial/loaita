
function HungerUpdate(player, amount)
	if ( IsImmortal(player) and not player:HasObjVar("TestHunger") ) then return end
    if ( IsDead(player) or amount == nil ) then return end

    -- if Hunger isn't set, we need to negate (-) amount since we are just going to add it to its self on the next line, to produce a 0 regardless of amount passed.
    local hunger = player:GetObjVar("Hunger") or -amount
    hunger = math.clamp(hunger + amount, 0, ServerSettings.Hunger.MaxHunger)

    if ( ServerSettings.Hunger.WarnThreshold ~= nil ) then
        if ( hunger >= ServerSettings.Hunger.WarnThreshold ) then
            player:SystemMessage("You could use a bite to eat.", "info")
        end
    end

    -- apply hungry debuff if they are
    if ( hunger >= ServerSettings.Hunger.Threshold ) then
        if not( HasMobileEffect(player, "Hungry") ) then
            StartMobileEffect(player, "Hungry")
        end
    else
        if ( HasMobileEffect(player, "Hungry") ) then
            player:SendMessage("EndHungryEffect")
        end
    end

    player:SetObjVar("Hunger", hunger)
end