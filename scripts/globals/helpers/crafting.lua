

QualityNameColors = {
	Flimsy = "978161",
	Stout = "22d322",
	Sturdy = "4D80F6",
	Robust = "B65AF3",
	Stalwart = "EA821A",
}

QualityIndex = {
	Flimsy = 1,
	Stout = 2,
	Sturdy = 3,
	Robust = 4,
	Stalwart = 5,
}

ImprovementGuarantees = {
	Flimsy = 0,
	Stout = 1,
	Sturdy = 2,
	Robust = 3,
	Stalwart = 4,
}

-- this allows recipes to either specify a single set of resources (for items with no quality)
-- or a table of resource sets, one for each quality level
function GetQualityResourceTable(resourceTable,quality)	
	-- if the resource table has subtables, then this recipe supports multiple quality levels
	local hasSubtables = true
	for k,v in pairs(resourceTable) do
		if(hasSubtables and type(v)~="table") then
			hasSubtables = false
		end
	end	
	--DFB HACK: Treat fragile items like flimsy items.
	if (quality == "Fragile") then
		quality = "Flimsy"
	end
	if(hasSubtables) then
		return resourceTable[quality]
	else
		return resourceTable	
	end
end

function HasResources(resourceTable, user,quality)
	local backpackObj = user:GetEquippedObject("Backpack")
	if( backpackObj == nil ) then	
		user:SystemMessage("You have no backpack equipped.")
		return false
	end

	local qualityResourceTable = resourceTable
	--DebugMessage("qualityResourceTable is "..tostring(qualityResourceTable))
	if(quality ~= nil) then		
		if(CountTable(resourceTable) == 0) then
			return true
		end	

		qualityResourceTable = GetQualityResourceTable(resourceTable,quality)
		--DebugMessage("qualityResourceTable is now "..tostring(qualityResourceTable))
		if(qualityResourceTable == nil) then
			return false
		end
	end
	for resourceType,count in pairs(qualityResourceTable) do
		--DebugMessage("count is "..tostring(count))
		--DebugMessage(DumpTable(count))

		--DFB NOTE: For future reference, if you're scratching your head wondering why count is a table, then you're passing a nil quality for an item that has quality
		if( CountResourcesInContainer(backpackObj,resourceType) < count ) then
			--DebugMessage("Count is "..tostring(count))
			--user:SystemMessage("You need more "..resourceType..".")
			return false 
		end		
	end

	return true
end


function ConsumeResources(resourceTable,user,transactionId,quality)
	local backpackObj = user:GetEquippedObject("Backpack")  

   	if( backpackObj == nil ) then 
   		return false
	end

	if(transactionId == nil) then transactionId = user end	

	local qualityResourceTable = resourceTable
	if(quality ~= nil) then
		-- we have already confirmed we can consume the resourceTypes so just assume everything is peachy		
		qualityResourceTable = GetQualityResourceTable(resourceTable,quality)
		if(qualityResourceTable == nil) then
			return false
		end
	end

	for resourceType,count in pairs(qualityResourceTable) do
		--DebugMessage("resourceType is "..tostring(resourceType))
		--DebugMessage("Count="..tostring(count))
		if( CountResourcesInContainer(backpackObj,resourceType) < count ) then
			return false 
		end	
		RequestConsumeResource(user,resourceType,count,transactionId,user)
	end

	return true
end

