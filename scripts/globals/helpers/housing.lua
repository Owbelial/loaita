function GetHousePlot(template, location, angle)
	local bounds = GetTemplateObjectBounds(template)
	--DebugMessage(DumpTable(bounds))
	if(bounds == nil or #bounds == 0 ) then
		return nil
	end
	--DFB NOTE: This only takes the first bounds, if we want more object bounds later we have to change it
	bounds = bounds[1]

	if (angle) then
		bounds = bounds:Rotate(angle)
	end
	--DebugMessage(bounds:Add(location):Flatten())
	return bounds:Add(location):Flatten()
end

function GetHouseInterior(template, location)
	local bounds = GetTemplateRoofBounds(template)
	--DebugMessage(DumpTable(bounds))
	if(bounds == nil or #bounds == 0 ) then
		return nil
	end

	interior = {}
	for i,boundsEntry in pairs(bounds) do
		table.insert(interior,boundsEntry:Add(location):Flatten())
	end

	--DebugMessage(bounds:Add(location):Flatten())
	return interior
end

function GetUserHouse(user)
	local userId = user:GetAttachedUserId()
	if(userId) then
		local housingRecordId = user:GetAttachedUserId() .. ".Housing"
	 	local housingRecord = GlobalVarRead(housingRecordId)
	 	if(housingRecord ~= nil and housingRecord.Owned ~= nil and #housingRecord.Owned > 0) then
	 		return housingRecord.Owned[1].HouseObj
	 	end
	end
end

function UserHasHouse(user)
	return GetUserHouse(user) ~= nil
end

function SetUserHouse(user,house,region)
	region = region or GetRegionAddress()
	local newEntry = {HouseObj=house,Region=region} 

	local housingRecordId = user:GetAttachedUserId() .. ".Housing"
	GlobalVarWrite(housingRecordId,nil,
		function (record)
			if(record.Owned == nil) then
				record.Owned = {newEntry}
			else
				table.insert(record.Owned,newEntry)
			end
			return true
		end)
end

function GetHousePlot(template, location, angle)
	local bounds = GetTemplateObjectBounds(template)
	--DebugMessage(DumpTable(bounds))
	if(bounds == nil or #bounds == 0 ) then
		return nil
	end
	--DFB NOTE: This only takes the first bounds, if we want more object bounds later we have to change it
	bounds = bounds[1]

	if (angle) then
		bounds = bounds:Rotate(angle)
	end
	--DebugMessage(bounds:Add(location):Flatten())
	return bounds:Add(location):Flatten()
end

function GetHouseControlPlot(houseControlObj)
	local houseObj = houseControlObj:GetObjVar("HouseObject")
	if(houseObj == nil or not(houseObj:IsValid())) then return nil end

	local houseTemplate = houseObj:GetCreationTemplateId()
	return GetHousePlot(houseTemplate,houseObj:GetLoc(),houseObj:GetFacing())
end

function GetHouseControlInterior(houseControlObj)
	local houseObj = houseControlObj:GetObjVar("HouseObject")
	if(houseObj == nil or not(houseObj:IsValid())) then return nil end

	local houseTemplate = houseObj:GetCreationTemplateId()
	return GetHouseInterior(houseTemplate,houseObj:GetLoc())
end

function GetNearbyHouses(targetLoc,range)
	range = range or 30
	return FindObjects(SearchMulti({SearchRange(targetLoc, range),SearchModule("house_control")}),GameObj(0))
end

function IsObjInHouse(targetObj)
	return GetContainingHouseForObj(targetObj) ~= nil
end

function HasHouseAtLoc(targetLoc)
	return GetContainingHouseForLoc(targetLoc) ~= nil
end

function GetContainingHouseForObj(targetObj)
	local worldObj = targetObj:TopmostContainer() or targetObj

	return GetContainingHouseForLoc(worldObj:GetLoc())
end

function GetContainingHouseForLoc(targetLoc)
	local nearbyHouses = GetNearbyHouses(targetLoc)
	if(#nearbyHouses > 0) then
		for i,houseControlObj in pairs(nearbyHouses) do
			if(IsLocInHouse(houseControlObj,targetLoc)) then
				return houseControlObj
			end
		end
	end

	return nil
end

function IsObjInHouse(houseControlObj,targetObj)
	local worldObj = targetObj:TopmostContainer() or targetObj

	return IsLocInHouse(houseControlObj,worldObj:GetLoc())
end

function IsLocInHouse(houseControlObj,targetLoc)
	local housePlot = GetHouseControlPlot(houseControlObj)	
	if(housePlot == nil) then
		LuaDebugCallStack("ERROR: Control obj has no plot! " .. tostring(houseControlObj))
		return
	end
	return housePlot:Contains(targetLoc)		
end

function IsObjInInterior(houseControlObj,targetObj)
	local worldObj = targetObj:TopmostContainer() or targetObj

	return IsLocInInterior(houseControlObj,worldObj:GetLoc())
end

function IsLocInInterior(houseControlObj,targetLoc)
	local houseInteriorArray = GetHouseControlInterior(houseControlObj)	
	if(houseInteriorArray == nil) then
		LuaDebugCallStack("ERROR: Control obj has no interior! " .. tostring(houseControlObj))
		return
	end

	for i,bounds in pairs(houseInteriorArray) do
		if(bounds:Contains(targetLoc)) then
			return true
		end
	end

	return false
end

function IsHouseOwner(user,houseControlObj)	
	return GetHouseOwner(houseControlObj) == user
end

function GetHouseOwner(houseControlObj)
	if(houseControlObj ~= nil 
			and houseControlObj:IsValid()) then
		return houseControlObj:GetObjVar("Owner")
	end
end

function IsHouseOwnerForLoc(user,targetLoc)
	local containedHouse = GetContainingHouseForLoc(targetLoc)

	return IsHouseOwner(user,containedHouse)
end

function IsHouseLocked(houseControlObj)
	if(houseControlObj == nil or not(houseControlObj:IsValid())) then
		return false
	end

	local doorObj = houseControlObj:GetObjVar("DoorObject")
	return doorObj ~= nil and doorObj:IsValid() and doorObj:GetObjVar("locked")
end

function GetTreesInBounds(bounds)
	if(bounds == nil) then
		LuaDebugCallStack("ERROR: Bounds is nil")
		return {}
	end

	local searcher = PermanentObjSearchSharedStateEntry("ResourceSourceId","Tree")
	local nearbyTrees = FindPermanentObjects(searcher)
	local treesInBounds = {}
	for i,objRef in pairs(nearbyTrees) do		
		if(bounds:Contains(objRef:GetLoc())) then
			table.insert(treesInBounds,objRef)
		end
	end

	return treesInBounds
end

function LockDownObject(targetObj)
	if(targetObj:HasModule("hireling_merchant_sale_item")) then
		return
	end
	if (targetObj:HasObjVar("DisableLockDown")) then
		return 
	end
	targetObj:SetObjVar("LockedDown",true)
	targetObj:SetObjVar("NoReset",true)
	SetTooltipEntry(targetObj,"locked_down","Locked Down",98)	
	if(targetObj:HasModule("decay")) then
		targetObj:DelObjVar("DecayTime")
		targetObj:DelModule("decay")
	end
	local containedObjects = targetObj:GetContainedObjects()
	for i,j in pairs(containedObjects) do
		if (j:HasModule("decay")) then
			j:DelObjVar("DecayTime")
			j:DelModule("decay")
		end
	end
end

function ReleaseObject(targetObj)
	if(targetObj:HasObjVar("LockedDown")) then
		targetObj:DelObjVar("LockedDown")
		targetObj:DelObjVar("NoReset")
		RemoveTooltipEntry(targetObj,"locked_down")
		targetObj:AddModule("decay")

		if(targetObj:HasModule("hireling_merchant_sale_item")) then
			targetObj:SendMessage("RemoveFromSale")
		end
	end
end