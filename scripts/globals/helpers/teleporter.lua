function ValidateTeleport(user,targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if (user:IsInRegion("NoTeleport")) then
		user:SystemMessage("[$1902]")
		return false
	end

	if( not(IsPassable(targetLoc)) ) then
		user:SystemMessage("[FA0C0C] You cannot teleport to that location.[-]")
		return false
	end

	return true
end


function GetNearbyFollowers(user)
	return FindObjects(SearchMulti
	{
		SearchMobileInRange(10,true),
		SearchLuaFunction(function(targetObj)
			return (targetObj:GetObjVar("controller") == user
					or targetObj:GetObjVar("MySuperior") == user)
		end)
	})
end

function TeleportUser(teleporter,user,targetLoc,destRegionAddress,destFacing)
	if(user == nil or not(user:IsValid())) then
		return
	end

	-- prevent teleport loops from region transfers
	if(TimeSinceLogin(user) < TimeSpan.FromSeconds(5)) then
		return
	end

	user:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"TeleportDelay")

	if (teleporter:HasObjVar("TeleporterConsumeItems")) then
		local itemList = teleporter:GetObjVar("TeleporterConsumeItems")
		user:GetEquippedObject("Backpack"):ConsumeItemsInContainer(itemList,true)
	end

	-- if it's not passed in, try to grab it from the teleporter object
	destFacing = destFacing or teleporter:GetObjVar("DestinationFacing") or user:GetFacing()	
	
	if(destRegionAddress ~= nil and destRegionAddress ~= GetRegionAddress()) then							
		-- pets will be stabled automatically
		-- DAB TODO: Store which pets should be unstabled on the other side
		if(user:TransferRegionRequest(destRegionAddress,targetLoc)) then
			if( destFacing ~= nil ) then
				user:SetFacing(destFacing)
			end
			if not(teleporter:HasObjVar("NoTeleportEffect")) then
				user:PlayEffect("TeleportToEffect")
				user:PlayObjectSound("Teleport")	
			end

			if (teleporter:HasObjVar("CreatePortalOnExit")) then
	    		MessageRemoteClusterController(destRegionAddress,"CreateObject","spawn_portal",targetLoc)
	    	end	

	    	local bindOnTeleport = teleporter:GetObjVar("BindOnTeleport")
			if( bindOnTeleport ) then
				user:SetObjVar("SpawnPosition",{Region=destRegionAddress,Loc=targetLoc})
			end
		end
	else
		local objsToTeleport = {user}
		TableConcat(objsToTeleport,GetNearbyFollowers(user))

		for i, follower in pairs(objsToTeleport) do
			if not(teleporter:HasObjVar("NoTeleportEffect")) then
				follower:PlayEffect("TeleportToEffect")						
				follower:PlayObjectSound("Teleport")
			end
			follower:SetWorldPosition(targetLoc)
			if( destFacing ~= nil ) then
				follower:SetFacing(destFacing)
			end
		end

		if (teleporter:HasObjVar("CreatePortalOnExit")) then
	    	CreateObj("spawn_portal",targetLoc)
	    end

	    local bindOnTeleport = teleporter:GetObjVar("BindOnTeleport")
		if( bindOnTeleport ) then
			user:SendMessage("BindToLocation",targetLoc)
		end		
	end		
end

function OnPortalCreated(success,objRef,portalDuration,portalId)
	if(success) then
		objRef:SetObjVar("DecayTime",portalDuration)
		objRef:AddModule("decay")

		if(portalId ~= nil) then
			objRef:SetObjVar("Type",portalId)
		end
	end
end

function OpenTwoWayPortal(sourceLoc,destLoc,portalDuration)
	local portalId = uuid()

	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj("portal",sourceLoc,"source_portal_created",portalDuration,portalId)
	RegisterSingleEventHandler(EventType.CreatedObject,"source_portal_created",OnPortalCreated)

	PlayEffectAtLoc("TeleportFromEffect",destLoc)
	CreateObj("portal",destLoc,"dest_portal_created",portalDuration,portalId)	
	RegisterSingleEventHandler(EventType.CreatedObject,"dest_portal_created",OnPortalCreated)
end

-- DAB TODO: Validate destination location by creating a nodraw object at destination first
function OpenRemoteTwoWayPortal(sourceLoc,destLoc,destRegionAddress,portalDuration)
	if(not(destRegionAddress) or destRegionAddress == GetRegionAddress()) then
		OpenTwoWayPortal(sourceLoc,destLoc,portalDuration)
		return
	end

	portalDuration = portalDuration or 25

	local portalId = uuid()	

	-- create local source portal
	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj("portal",sourceLoc,portalId)
	RegisterSingleEventHandler(EventType.CreatedObject,portalId,
		function(success,objRef)
			if(success) then
				objRef:SetObjVar("Destination",destLoc)
				objRef:SetObjVar("RegionAddress",destRegionAddress)
				objRef:SetObjVar("DecayTime",portalDuration)
				objRef:AddModule("decay")
			end
		end)

	-- create remote dest portal
	local targetModules = {"decay"}
	local targetObjVars = { Destination=sourceLoc, RegionAddress=GetRegionAddress(), DecayTime = portalDuration }
	MessageRemoteClusterController(destRegionAddress,"CreateObject","portal",destLoc,targetModules,targetObjVars)
end

function OpenOneWayPortal(sourceLoc,destLoc,portalDuration,targetObj)
	local portalId = uuid()

	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj("portal",sourceLoc,"source_portal_created",portalDuration,portalId)
	RegisterSingleEventHandler(EventType.CreatedObject,"source_portal_created",
		function(success,objRef)
			objRef:SetObjVar("Destination",destLoc)
			objRef:SetObjVar("DecayTime",portalDuration)
			objRef:AddModule("decay")
		end)
end

function OpenRemoteOneWayPortal(sourceLoc,destLoc,destRegionAddress,portalDuration)
	if(not(destRegionAddress) or destRegionAddress == GetRegionAddress()) then
		OpenOneWayPortal(sourceLoc,destLoc,portalDuration)
		return
	end
	
	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj("portal",sourceLoc,"transfer_portal_created")	
	RegisterSingleEventHandler(EventType.CreatedObject,"transfer_portal_created",
		function (success,objRef)
			if(success) then
				objRef:SetObjVar("Destination",destLoc)
				objRef:SetObjVar("RegionAddress",destRegionAddress)
				objRef:SetObjVar("DecayTime",portalDuration)
				objRef:AddModule("decay")
			end
		end)
end