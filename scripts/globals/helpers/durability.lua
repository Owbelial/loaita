

function GetDurabilityValue(item)
	if(item == nil) then LuaDebugCallStack("nil item provided to GetDurabilityValue") end
	return item:GetObjVar("Durability") or GetMaxDurabilityValue(item)
end

function GetMaxDurabilityValue(item)
	if(item == nil) then LuaDebugCallStack("nil item provided to GetMaxDurabilityValue") end
	return item:GetObjVar("MaxDurability") or ServerSettings.Durability.DefaultMax
end

--- Get the string value of an item's durability
-- @param item
-- @param current(optional) double
-- @param max(optional) double
-- @return string
function GetDurabilityString(item, current, max)
	max = max or GetMaxDurabilityValue(item)
	current = current or GetDurabilityValue(item)
	current = math.min(current, max)

	if ( current == max ) then
	return ("Durability:" ..(current) .. "/" .. (max) .. "" )

	end

	local percent = current / max
	if ( percent <= 0.01 ) then
	return ("Durability:" ..(current) .. "/" .. (max) .. "" )
	end
	if ( percent <= 0.25 ) then
	return ("Durability:" ..(current) .. "/" .. (max) .. "" )
	end
	if ( percent <= 0.65 ) then
	return ("Durability:" ..(current) .. "/" .. (max) .. "" )
	end

	-- this is between New and Worn
	return ("Durability:" ..(current) .. "/" .. (max) .. "" )
end

--- Update the tooltip with durability information
-- @param item gameObj The equipment to update durability on
-- @param current(optional) current durability
-- @param max(optional) max durability
function UpdateDurabilityTooltip(item, current, max)
	max = max or GetMaxDurabilityValue(item)
	current = current or GetDurabilityValue(item)
	current = math.min(current, max)

	local durabilityStr = GetDurabilityString(item, current, max)
	if(durabilityStr) then
		SetTooltipEntry(item, "Durability", durabilityStr, -99999)
	else
		RemoveTooltipEntry(item, "Durability")
	end	
end

--- Adjust the durability of an item, if the final durability is zero the item will break.
-- @param item
-- @param amount double(optional) Amount to adjust by, defaults to 0 (ANYTHING LESS THAN 1 BREAKS ITEM!)
-- @param current(optiona) double Current item durability, will be ready from object if not provided.
-- @param max(optional) double Item's max durability, will be ready from object if not provided.
function AdjustDurability(item, amount, current, max)
	if ( item == nil ) then
		LuaDebugCallStack("ERROR: nil item provided.")
		return
	end
	amount = amount or 0
	max = max or GetMaxDurabilityValue(item)
	current = current or GetDurabilityValue(item)
	current = math.min(current, max)

	current = current + amount

	-- anything less than 1 BREAKS the item!
	if ( current < 1 ) then
		BreakItem(item)
		return
	end
	
	if ( current == max ) then
		-- when an item has full durability, it doesn't need this objvar.
		item:DelObjVar("Durability")
	else
		-- warn the player their item is about to break.
		if ( current <= ServerSettings.Durability.BreakWarnings ) then
			local holder = item:TopmostContainer()
			if ( holder and holder:IsPlayer() ) then
				holder:SystemMessage("Your " .. StripColorFromString(item:GetName()) .." is in danger of breaking!", "event")
			end
		end
			item:DelObjVar("Durability")
		item:SetObjVar("MaxDurability", current)
	end

	--if ( GetDurabilityString(item, current, max) ~= GetDurabilityString(item, current - amount, max) ) then
		UpdateDurabilityTooltip(item, current, max)
	--e-nd
end

--- Sets the maximum durability for an item
-- @param item
-- @param newMax
function SetMaxDurabilityValue(item, newMax)
	if ( item == nil ) then return LuaDebugCallStack("nil item provided.") end

	item:SetObjVar("MaxDurability", newMax)
	AdjustDurability(item, nil, nil, newMax)
end

function BreakItem(item)
	local holder = item:TopmostContainer()
	if ( holder ) then
		if ( holder:IsPlayer() ) then
			local weaponType = item:GetObjVar("WeaponType")
			if ( EquipmentStats.BaseWeaponStats[weaponType] and EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Tool" ) then
				item:SendMessage("CancelHarvesting", holder)
			end
			
			-- do all the things that accompany unequipping an item.
			local tempPack = holder:GetEquippedObject("TempPack")
			if ( tempPack ) then
				item:MoveToContainer(tempPack, Loc(0,0,0))
			else
				item:SetWorldPosition(holder:GetLoc())
			end

			-- this is important enough to log it in the chat window and do an alert event
			holder:SystemMessage("Your " .. StripColorFromString(item:GetName()) .." has been destroyed!")
			holder:SystemMessage("Your " .. StripColorFromString(item:GetName()) .." has been destroyed!", "event")
		end
		holder:PlayObjectSound("Sunder")		
	end

	item:Destroy()
end