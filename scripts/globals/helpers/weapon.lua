
Weapon = {}

Weapon.GetType = function(weaponObj)
	if ( weaponObj ~= nil ) then		
		local weaponType = weaponObj:GetObjVar("WeaponType") or "BareHand"
		if not( EquipmentStats.BaseWeaponStats[weaponType] ) then
			LuaDebugCallStack("Invalid weapon type: "..tostring(weaponType)..", Template: "..weaponObj:GetCreationTemplateId())
		else
			return weaponType
		end
	end
	return "BareHand"
end

Weapon.GetClass = function(weaponType)
	weaponType = weaponType or "BareHand"
	if ( EquipmentStats.BaseWeaponStats[weaponType] ) then
		return EquipmentStats.BaseWeaponStats[weaponType].WeaponClass
	end
	return "Fist"
end

Weapon.GetStat = function(weaponType, stat)
	weaponType = weaponType or "BareHand"
	if ( EquipmentStats.BaseWeaponStats[weaponType] ) then
		return EquipmentStats.BaseWeaponStats[weaponType][stat]
	end
	return nil
end

Weapon.GetSkill = function(weaponType)
	if ( weaponType and EquipmentStats.BaseWeaponStats[weaponType] 
		and EquipmentStats.BaseWeaponStats[weaponType].WeaponClass 
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass]
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].WeaponSkill ) then
		return EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].WeaponSkill
	end
	return "BashingSkill"
end

Weapon.GetDamageType = function(weaponType)
	if ( weaponType and EquipmentStats.BaseWeaponStats[weaponType] 
		and EquipmentStats.BaseWeaponStats[weaponType].WeaponClass 
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass]
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].WeaponDamageType ) then
		return EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].WeaponDamageType
	end
	return "Bashing"
end

Weapon.GetRange = function(weaponType)
	if ( weaponType and EquipmentStats.BaseWeaponStats[weaponType] 
		and EquipmentStats.BaseWeaponStats[weaponType].WeaponClass 
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass]
		and EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].Range ) then
		return EquipmentStats.BaseWeaponClass[EquipmentStats.BaseWeaponStats[weaponType].WeaponClass].Range
	end
	return ServerSettings.Stats.DefaultWeaponRange
end

Weapon.IsRanged = function(weaponType)
	weaponType = weaponType or "BareHand"
	return ( EquipmentStats.BaseWeaponStats[weaponType] and (
		EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Bow"
		--or EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Bow" -- as an example to check for a secondary Class
	))
end