ServerSettings.Combat = {
	LOSEyeLevel = 1.9,
	DefaultBodySize = 0.8,

	MaxMinions = 3,

	NoSelfDamageOnAOE = true,
	AoEGuildDamageDefault = true,

	PlayerEndCombatPulse = 5,
	NPCEndCombatPulse = 20,

	MinimumSwingSpeed = 0.5, --slowest possible swing speed

	NoHideRange = 3, -- can not hide if within X units of another mob

	MountedAttackersCanTriggerDaze = false,
	DazedOnDamageWhileMounted = true, --Apply Daze to mounted mobs when they receive damage
	DismountWhileDazed = true, --Check to see if dazed targets have a chance to be dismounted.
	DismountWhileDazedChance = 50, -- Chance to be dismounted when sustaining damage while dazed.
	MountedAttackersCanTriggerDismount = false,
}