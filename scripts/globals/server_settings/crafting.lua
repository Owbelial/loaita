ServerSettings.Crafting = {
    MakersMark = {
        Enabled = true, -- should item's have a "Created by" in the tooltip?
        MakersMark = "Created by %s" -- %s will be replaced with the crafter's name.
    }
}