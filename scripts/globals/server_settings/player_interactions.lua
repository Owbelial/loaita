ServerSettings.PlayerInteractions = {
	PlayerVsPlayerEnabled = true,
	PlayerVsPlayerConsensualOnly = false,

	GuardTowerProtectionRange = 30.0,
	GatekeeperProtectionRange = 15.0,
	GuaranteedGuardProtectionFullMap = false,
	GuaranteedGuardProtectionZones = {		
	}, 
	SlayImmediateProtectionZones = {
		"GuardZoneLarge","Subregion-SewerDungeon","Subregion-SewerDungeon2","Subregion-SewerDungeon3","Subregion-SewerDungeon4"
	},
	SlayImmediateProtectionMaps = {
		--"Corruption"
	},

	FullItemDropOnDeath = true, -- change player_corpse.xml template to adjust body decay time.
	ExileMurderCount = 5,
}