ServerSettings.Group = {
    -- maximum size a group can be
    MaximumSize = 20,

	-- the range a group member is eligible for xp/fame/group loot
	RewardRange = 25,
}