ServerSettings.Pets = {
    Follow = {
        Distance = 1, -- closest a following pet will get to leader.
        Speed = {
            OnFoot = 5, -- speed that pets will follow at while owner is on foot
            Mounted = 8, -- speed that pets will follow at while owner is mounted.
        },
    },
    Command = {
        Distance = 40, -- distance owner can command pet from
    },
    Combat = {
        Speed = 6, -- speed that pets will chase stuff to attack
    },
    Taming = {
        Distance = 7 -- distance a player can be from a pet to tame it
    }
}