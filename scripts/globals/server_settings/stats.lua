ServerSettings.Stats = {
	PlayerBaseHealth = 100,
	NPCBaseHealth = 100,
	IndividualStatMin = 10,
	IndividualPlayerStatCap = 50,
	TotalPlayerStatsCap = 150,
	IndividualNPCStatCap = 1000,
	TotalNPCStatsCap = 10000,
	WalkSpeedModifier = 1.5,
	RunSpeedModifier = 4.5,
	BaseMoveSpeed = 1, -- base speed for movement
	MountMoveSpeed = 1.75, -- speed set to exactly this when mounted
	ApplyOverCapStatPowerReduction = true,
	OverCapPowerFactor = 0.85,

	-- these are stats that can gain with the use of any skill and are not (and never will be) tied to a specific skill
	AllSkillStats = {
		"Constitution",
		"Wisdom",
		"Will"
	},

	ShieldEvasionPenalty = 8,
	WeaponSkillEvasionBonus = 0.2,
	WeaponSkillAccuracyBonus = 0.2,
	BaseHealthRegenRate = 1/12,
	BaseVitalityRegenRate = 0,
	BaseVitality = 100,
	BaseStamina = 75,
	BaseStaminaRegenRate = 4,
	BaseManaRegenRate = 0.2,
	DefaultMobAttack = 4,
	DefaultMobPower = 4, -- default magic power
	DefaultMobForce = 4, -- default magic beneficial
	DefaultMobArmorRating = 44,
	DefaultMobWeaponSpeed = 253,

	DefaultMobManaRegen = 4.4,
	DefaultMobStamRegen = 2,

	LightArmorCritChanceBonus = 0.2,
	DefaultWeaponRange = 1.3,

	LightArmorEvasionBonus = 0.1,
	LightArmorAttackSpeedMultiplier = 0.0005,

	HeavyArmorCritChanceReduction = 0.002,
	HeavyArmorDefenseBonus = 0.15,

	-- how long should a player remain debuffed after removing their heavy armor
	HeavyArmorDebuffDuration = TimeSpan.FromMinutes(1),

	-- when calculating MoveSpeed and given the mobile has any heavy armor (or debuff), 
		-- this number will act in place of their real agi (only for movement speed calculations!)
	HeavyArmorMoveSpeedAgi = 10,

	-- amount of time character is frozen while performing range attacks/abilities
	RangedRecoveryTime = TimeSpan.FromMilliseconds(1),
	AbilityRecoveryTime = TimeSpan.FromMilliseconds(1),

	-- configurable functions

	-- used for accuracy and evasion
	AgiHitModifierFunc = function(agi) return math.sqrt(agi) * 6 end,
	-- used for crit chance and swing speed
	AgiModifierFunc = function(agi) return 1+((agi-10)*0.00625) end,	
	-- determines hp bonus from con stat
	ConHpBonusFunc = function(con)
		local conHpBonus = 0
		if con > 40 then 
	        conHpBonus = (conHpBonus+400) + ((con-40)*5)
		elseif con > 30 then 
	        conHpBonus = (conHpBonus+300) + ((con-30)*10)
		elseif con > 20 then 
	        conHpBonus = (conHpBonus+150) + ((con-20)*15)
	    elseif con > 10 then 
	        conHpBonus = (con-10)*15   
	    end

		return conHpBonus
	end,

		
	-- determines attack bonus from str
	StrModifierFunc = function(str) 
		local strModifier = 0.45	
	    if str > 40 then 
	        strModifier = (strModifier+.75) + ((str-40)*0.005)
	    elseif str > 30 then 
	        strModifier = (strModifier+.4) + ((str-30)*0.035)
		elseif str > 20 then 
	        strModifier = (strModifier+.15) + ((str-20)*0.025)
		elseif str > 10 then 
	        strModifier = strModifier + ((str-10)*0.015)
	    end

	    return strModifier
	end,

	IntMod = function(int)
		local base = 0.45
		if( int > 10 ) then
			return base + ( ( int - 10 ) * 0.02)
		end
		return base
	end,

	WisMod = function(wis)
		local base = 30
		if( wis > 40 ) then
			return ( base + 28 ) + ( ( wis - 40 ) * 0.2)
		elseif( wis > 30 ) then
			return ( base + 13 ) + ( ( wis - 30 ) * 1.5)
		elseif( wis > 20 ) then
			return ( base + 5 ) + ( ( wis - 20 ) * 0.08)
		elseif( wis > 10 ) then
			return base + ( ( wis - 10 ) * 0.015)
		end
		return base
	end,
}