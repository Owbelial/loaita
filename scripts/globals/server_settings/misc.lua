ServerSettings.Misc = {
	MustLearnMaps = false,
	StarvationEnabled = true,
	HungerTickTimer = 240,
	LethalStarvation = true,
	RestEnabled = false,
	RestTimer = 20,
	GroupAutolootRange = 15,
	ObjectDecayTimeSecs = 300,
	FoodFillingnessCap = 40,
	FoodMaxStarvationLevel = -3,
	FoodStarvationDivder = 5,		
	
	CorpseDecayTimeSecs = 900,
	EnforceBadWordFilter = true,
	
	BankWeightLimit = 10000,
	BackpackBaseWeightLimit = 200,
	DefaultContainerWeightLimit = 2000,
	CoinWeight = 0.01,
	PetDecayTime = 3600,
	NonCombatMaps = {
		"Founders"
	},
}