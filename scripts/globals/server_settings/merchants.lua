ServerSettings.Merchants = {
	DefaultRestockInSeconds = 1800,
	DefaultStackSize = 100,

	-- number between 0 (full price) and 1 (free)
	GatekeeperDiscount = 1,
}