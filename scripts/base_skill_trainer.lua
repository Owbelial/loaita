
DEFAULT_TRAIN_PRICE = 300
-- DAB HACK: This must be the same number as STARTING_SKILL_VALUE from base_skill_sys
STARTING_SKILL_VALUE = 30
menuItemSkills = nil

AI.RefuseTrainMessage = {
	"Fine. I have better things to do anyway.",
}

AI.WellTrainedMessage = {
	"[$1723]",
}

AI.CannotAffordMessages = {
	"[$1724]",
}

AI.TrainScopeMessages = {
	"[$1725]",
}

SkillTrainer = {
	IgnoredSkills = 
	{
	
	}
}

function CanTrain()
	for skillName,skillEntry in pairs(SkillData.AllSkills) do
		--DebugMessage("-------------------------------------------------------")
		--DebugMessage("Skill is "..tostring(skillName))
		--DebugMessage("IsInTableArray = "..tostring(not(IsInTableArray(SkillTrainer.IgnoredSkills,skillName))))
		--DebugMessage("skillEntry.Skip = "..tostring( not(skillEntry.Skip)))
		--DebugMessage("This.GetSkillLevel = "..tostring(GetSkillLevel(this,skillName) >= 30))
		--DebugMessage(tostring(GetSkillLevel(this,skillName)))
		if( not(IsInTableArray(SkillTrainer.IgnoredSkills,skillName))
				and not(skillEntry.Skip)
				and GetSkillLevel(this,skillName) >= 30 ) then
			--DebugMessage(skillName)
			return true
		end
	end
	return false
end

function SkillTrainer.ShowTrainContextMenu(user)
	menuItems = {}

	-- train skills this mobile is an expert in
	for skillName,skillEntry in pairs(SkillData.AllSkills) do
		--DebugMessage("-------------------------------------------------------")
		--DebugMessage("Skill is "..tostring(skillName))
		--DebugMessage("IsInTableArray = "..tostring(not(IsInTableArray(SkillTrainer.IgnoredSkills,skillName))))
		--DebugMessage("skillEntry.Skip = "..tostring( not(skillEntry.Skip)))
		--DebugMessage("This.GetSkillLevel = "..tostring(GetSkillLevel(this,skillName) >= 30))
		--DebugMessage(tostring(GetSkillLevel(this,skillName)))
		if( not(IsInTableArray(SkillTrainer.IgnoredSkills,skillName))
				and not(skillEntry.Skip)
				and GetSkillLevel(this,skillName) >= 30 ) then
			local nextSkill = {
				SkillName = skillName,
				DisplayName = skillEntry.DisplayName
			}
			table.insert(menuItems,nextSkill)
		end
	end	

	text = AI.TrainScopeMessages[math.random(1,#AI.TrainScopeMessages)]

	response = {}
	local n = 0
	for i,j in pairs(menuItems) do
		if (i < 5) then
			local skillData = SkillData.AllSkills[j.SkillName]
			local nextResponse = {}
    		nextResponse = {}
    		nextResponse.text = "I want to learn ".. skillData.DisplayName
    		nextResponse.handle = j.SkillName .."SkillResponse"
    		table.insert(response,nextResponse)
    		n = i
    	else
    		DebugMessage("[base_skill_trainer|SkillTrainer.ShowTrainContextMenu] ERROR: No more than 4 skills supported! Implement Scroll Bars!")
    	end
	end

	response[n+1] = {}
	response[n+1].text = "Nevermind"
	response[n+1].handle = "Nevermind"

	NPCInteractionLongButton(text,this,user,"TrainDialog",response)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"TrainDialog", 	
	function (user,buttonID)
	    if( buttonID == nil or buttonID == "") then return end
	    if( user == nil or not(user:IsValid())) then return end
	    if( user:DistanceFrom(this) > OBJECT_INTERACTION_RANGE) then return end

	    local skillName = StripFromString(buttonID,"SkillResponse")
	    --DebugMessage("buttonID is '"..tostring(buttonID) .."'")

		if( buttonID == "Nevermind") then		
			if (AI.RefuseTrainMessage ~= nil) then	
				this:NpcSpeech(AI.RefuseTrainMessage[math.random(1,#AI.RefuseTrainMessage)])
				return
			else
				NevermindDialog()
				return
			end
		end

	    local skillData = SkillData.AllSkills[skillName]
		if (skillData == nil ) then 
			DebugMessage("[base_skill_trainer|TrainDialog] ERROR: skillData is nil for: "..this:GetName().." skillName: "..skillName)
			return
		end		
		
	    FaceObject(this,user)

	    if( GetSkillLevel(user,skillName) >= STARTING_SKILL_VALUE ) then

			response = {}

	    	text = AI.WellTrainedMessage[math.random(1,#AI.WellTrainedMessage)]

			response[1] = {}
			response[1].text = "Right"
			response[1].handle = "Nevermind"

			NPCInteractionLongButton(text,this,user,"Responses",response)

	    	return
		end

		if( CountCoins(user) < 30 ) then

			--DebugMessage("Error here 1")
			text = AI.CannotAffordMessages[math.random(1,#AI.CannotAffordMessages)].."[D7D700]("..tostring(DEFAULT_TRAIN_PRICE).." Copper)[-]"
		
			response = {}

			response[1] = {}
			response[1].text = "Oh..."
			response[1].handle = "Nevermind"

			NPCInteractionLongButton(text,this,user,"Responses",response)
			
			return
		end			
	    
		text = "I can teach you the basics of "..skillData.DisplayName.." for [D7D700]("..tostring(DEFAULT_TRAIN_PRICE).." Copper)[-]?"
		response = {}

		response[1] = {}
		response[1].text = "Yes, please."
		response[1].handle = "Teach"..skillName

		response[2] = {}
		response[2].text = "No, thank you."
		response[2].handle = "Nevermind"

		NPCInteractionLongButton(text,this,user,"TeachSkill",response,nil,nil,max_distance)
	    
	end)

RegisterEventHandler(EventType.DynamicWindowResponse,"TeachSkill", 
	function (user,buttonId)
	    --local skillName = StripFromString(buttonID,"Teach")

		if( buttonId == "Nevermind") then		
			if (NevermindDialog == nil) then	
				this:NpcSpeech(AI.RefuseTrainMessage[math.random(1,#AI.RefuseTrainMessage)])
			else
				NevermindDialog()
			end
		elseif (buttonId:match("Teach")) then
			RequestConsumeResource(user,"coins",DEFAULT_TRAIN_PRICE,buttonId,this)
		end
	end)

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", 
	function (success,transactionId,user)	
		if not(transactionId:match("Teach")) then return end	
		if not(success) then
			text = AI.CannotAffordMessages[math.random(1,#AI.CannotAffordMessages)].."[D7D700]("..tostring(DEFAULT_TRAIN_PRICE).." Copper)"

			response = {}
			
			response[1] = {}
			response[1].text = "Oh..."
			response[1].handle = "Nevermind"

			NPCInteractionLongButton(text,this,user,"Responses",response)
	    	return
		else
			-- cut train off the id	
	    	local skillName = StripFromString(transactionId,"Teach")		
			--DebugMessage(skillName)
			SetSkillLevel(user, skillName, 30, true)
		end
	end)

if (initializer ~= nil) then
	if (initializer.IgnoredSkills ~= nil) then
		SkillTrainer.IgnoredSkills = initializer.IgnoredSkills
	end
end
