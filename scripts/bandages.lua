--[[
	OLD FILE: Should be deleted soonish -KH 9/14/2017
]]

DEFAULT_HEAL_VALUE = 100
DEFAULT_APPLY_TIME_MS = 10000

function ValidateUse(user)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( this:TopmostContainer() ~= user ) then
		user:SystemMessage("[$1613]","info")
		return false
	end

	return true
end

RegisterEventHandler(EventType.Message, "UseObject", 
	function (user,usedType)
    	if(usedType ~= "Use" and usedType ~= "Apply") then return end

		if not(ValidateUse(user) ) then
			return
		end
		user:SystemMessage("Who do you wish to apply bandages to?","info")
		user:RequestClientTargetGameObj(this, "bandageTarget")
	end)
	
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "bandageTarget", 
	function(target,user)
		if not(ValidateUse(user) ) then
			return
		end
		-- validate target
		if( target == nil or not(target:IsValid())) then
			return
		end
		-- target must be a mobile
		if( not(target:IsMobile()) ) then
			user:SystemMessage("You cannot apply bandages to that.","info")
			return
		end
		-- no double apply
		if ( HasMobileEffect(user, "BandageApply") ) then
			user:SystemMessage("You are already applying a bandage.","info")
			return
		end

		local applyInitializer = 
		{
			Target = target 
		}
		
		BandageBeginApply(user,target)
		user:PlayObjectSound("Use",true)
		RequestRemoveFromStack(this,1)
	end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(),
	function ( ... )
		SetTooltipEntry(this,"bandages","Used to heal wounds.")
        AddUseCase(this,"Use",true,"HasObject")
	end)