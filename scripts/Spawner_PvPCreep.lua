require 'incl_PvPEvent'

RespawnTimer = 90
SpawnInfo = 
{
    Earth = {
        { ["Count"] = 2, ["TemplateId"] = "vs_pvp_creep_earth" }, -- PvP Creep
        { ["Count"] = 2, ["TemplateId"] = "vs_pvp_creep_earth" }, -- PvP Creep
        { ["Count"] = 1, ["TemplateId"] = "vs_pvp_mage_earth" }, -- PvP Creep
    },
    Fire = {
        { ["Count"] = 2, ["TemplateId"] = "vs_pvp_creep_fire" }, -- PvP Creep
        { ["Count"] = 1, ["TemplateId"] = "vs_pvp_mage_fire" }, -- PvP Creep
    },
    Water = {
        { ["Count"] = 2, ["TemplateId"] = "vs_pvp_creep_water" }, -- PvP Creep
        { ["Count"] = 1, ["TemplateId"] = "vs_pvp_mage_water" }, -- PvP Creep
    },
}

SpawnCounter = 1

function SpawnCreeps(spawnInfo, pathName)
	if(SpawnCounter <= spawnInfo[1].Count) then
        CreateObj(spawnInfo[1].TemplateId, this:GetLoc(), "Created", pathName)  
    else
        CreateObj(spawnInfo[2].TemplateId, this:GetLoc(), "Created", pathName)    
    end    
end

RegisterSingleEventHandler(EventType.ModuleAttached, "Spawner_PvPCreep", 
    function()  
        this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(500), "Spawn")
    end)

RegisterSingleEventHandler(EventType.LoadedFromBackup, "", 
    function()
        this:ScheduleTimerDelay(TimeSpan.FromSeconds(RespawnTimer), "Spawn")
    end)

RegisterEventHandler(EventType.CreatedObject, "Created", 
    function(success, objRef, callbackData)
        if success then
            teamHue = this:GetObjVar("Hue")
            pvpTeam = this:GetObjVar("MobileTeamType")

            objRef:SetObjVar("MobileTeamType", pvpTeam )
            objRef:SetName("[" .. tostring(string.sub(teamHue,3)) .. "]" .. objRef:GetName() .. "[-]")
            objRef:SetObjVar("PathName", callbackData)
            --objRef:SetHue(teamHue)
            objRef:SendMessage("StartPath", nil)        
        end
    end)

RegisterEventHandler(EventType.Timer, "Spawn", 
    function()

        if( GetPvPGameState() ~= "Match" ) then
            this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "Spawn")
            return
        end

        pvpTeam = this:GetObjVar("MobileTeamType")
        if( pvpTeam == nil or SpawnInfo[pvpTeam] == nil ) then
            return
        end

        local spawnInfo = SpawnInfo[pvpTeam]

        SpawnCreeps(spawnInfo, this:GetObjVar("Path1Name"))
        SpawnCreeps(spawnInfo, this:GetObjVar("Path2Name"))
        --SpawnCreeps(spawnInfo, this:GetObjVar("Path3Name"))

        if(SpawnCounter == (spawnInfo[1].Count + spawnInfo[2].Count)) then
            SpawnCounter = 1
            this:ScheduleTimerDelay(TimeSpan.FromSeconds(RespawnTimer), "Spawn")
        else
            SpawnCounter = SpawnCounter + 1
            this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(100), "Spawn")
        end
    end)
