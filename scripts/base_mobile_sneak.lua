

function BeginSneak()
	if ( IsDead(this) ) then return end

	if ( IsMobileDisabled(this) ) then
		this:SystemMessage("Cannot hide right now.", "info")
		return
	end

	local mountObj = GetMount(this)
	if ( mountObj ) then
		--DismountMobile(this, mountObj)
	end

	local nearbyMobs = FindObjects(SearchMobileInRange(ServerSettings.Combat.NoHideRange))
	for i,mob in pairs(nearbyMobs) do
		if not( ShareGroup(this,mob) or IsController(this,mob)) then
			this:SystemMessage("Cannot hide with others nearby.", "info")
			return
		end
	end

	-- disallow combat
	this:SendMessage("EndCombatMessage")
	this:SetSharedObjectProperty("IsSneaking", true)
	-- make them walk slower
	HandleMobileMod("MoveSpeedTimes", "Sneak", -0.00) -- 80% slower
	-- try to hide
	this:FireTimer("TryHideTimer")
end

function EndSneak()
	this:SetSharedObjectProperty("IsSneaking", false)
	HandleMobileMod("MoveSpeedTimes", "Sneak", nil)
	if ( this:HasTimer("TryHideTimer") ) then
		this:RemoveTimer("TryHideTimer")
	end
end

RegisterEventHandler(EventType.Message, "EndSneak", EndSneak)
RegisterEventHandler(EventType.Message, "BeginSneak", EndSneak)

RegisterEventHandler(EventType.Timer, "TryHideTimer", function()
	if not( HasMobileEffect(this, "Hide") ) then
		-- attempt to hide.
		StartMobileEffect(this, "Hide")
		-- auto attempt again shortly..
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(9.1), "TryHideTimer")
	end
end)

RegisterEventHandler(EventType.Timer, "HideCooldownTimer", function()
	if ( this:HasTimer("TryHideTimer") ) then
		this:FireTimer("TryHideTimer")
	end
end)