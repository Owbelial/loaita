require 'incl_container'


function GetSkillTokenProps()
	local skillTokensEarned = tonumber(this:GetAccountProp("SkillTokensEarned") or "0")
	local skillTokensRedeemed = 0
	if(skillTokensEarned > 0) then
		skillTokensRedeemed = tonumber(this:GetAccountProp("SkillTokensRedeemed") or "0")
	end

	return skillTokensEarned, skillTokensRedeemed
end

function ProcessSkillTokenReward()
	local skillTokensEarned, skillTokensRedeemed = GetSkillTokenProps()
	if(skillTokensEarned > 0 and skillTokensRedeemed < skillTokensEarned) then
		this:SetAccountProp("SkillTokensRedeemed",tostring(skillTokensRedeemed + 1))
		CreateObjInBackpackOrAtLocation(this,"skill_token")
		this:SystemMessage("[$2635]")
		DebugMessage("INFO: Skill token redeemed!")
	end	
end

RegisterEventHandler(EventType.ModuleAttached,"temp_skill_token_reward",
	function ()				
		if( IsOfficialShard() ) then			
			local skillTokensEarned, skillTokensRedeemed = GetSkillTokenProps()
			if(skillTokensEarned > 0 and skillTokensRedeemed < skillTokensEarned) then				
				ClientDialog.Show{
					TargetUser = this,
					DialogId = "SkillToken",
					TitleStr = "Skill Token Earned",
					DescStr = "[$2636]",
					Button1Str = "Confirm",
					Button2Str = "Cancel",
					ResponseFunc = function (user,buttonId)
						if(buttonId == 0) then
							ProcessSkillTokenReward()							
						end

						this:DelModule("temp_skill_token_reward")
					end,
				}
			else
				this:DelModule("temp_skill_token_reward")
			end
		end
	end)
