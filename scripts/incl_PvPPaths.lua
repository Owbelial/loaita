function GetPathNodesByName(pathName)
	local item = {}

	if(pathName == "WaterShrine") then
		local waterShrine = GetPath("BrownToBlue")
		item[1] = waterShrine[#waterShrine]
		return item
	elseif(pathName == "FireShrine") then
		local fireShrine = GetPath("BrownToRed")
		item[1] = fireShrine[#fireShrine]
		return item
	elseif(pathName == "EarthShrine") then
		local earthShrine = GetPath("RedToBrown")
		item[1] = earthShrine[#earthShrine]
		return item
	else
		return GetPath(pathName)
	end
end

function InvertNodes(pathTable)
	local items = {}

	for i = #pathTable, 0, -1 do
		items[#items+1] = pathTable[i]
	end

	return items
end