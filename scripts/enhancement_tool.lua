require 'incl_enhancement'

function ValidateUse(user)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( this:TopmostContainer() ~= user ) then
		user:SystemMessage("[$1807]")
		return false
	end

	return true
end


function DoEnhance(target, user)
	local myEnhancement = this:GetObjVar("EnhancementName")
	if (myEnhancement == nil) then 
		DebugMessage("ERROR: Invalid Enhancement")
		return 
	end

	local mySuccess = AddEnhancement(target, myEnhancement, this, user)
	if(mySuccess == true) then
		this:Destroy()
		return
	end
	user:SystemMessage("[FA0C0C] Enhancement Failed.")
	return
end


RegisterEventHandler(EventType.ClientTargetGameObjResponse, "enhanceTarget", 
	function(target,user)
		DoEnhance(target,user)
	end)

RegisterEventHandler(EventType.Message, "UseObject", 
	function(user)
		if not(ValidateUse(user) ) then
			return
		end

		user:SystemMessage("What do you wish to enhance?")
		user:RequestClientTargetGameObj(this, "enhanceTarget")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "enhancement_tool", 
	function()
		local enName = this:GetObjVar("EnhancementName")
		if( enName ~= nil ) then	
			SetTooltipEntry(this,"enhancement_tool","[F7CC0A] Applies " .. GetEnhancementName(enName) .. " giving " .. GetEnhancementEffectDescription(enName))		
		end		
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function()
        AddUseCase(this,"Enhance Item",true)        
    end)