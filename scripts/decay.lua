DEFAULT_DECAYTIME = 300

function ScheduleDecay(decayTime)
	if(decayTime == nil) then
		decayTime = this:GetObjVar("DecayTime")		
	end

	if( decayTime == nil ) then
		decayTime = DEFAULT_DECAYTIME
	end

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(decayTime), "decay")
end

RegisterSingleEventHandler(EventType.ModuleAttached, "decay", 
	function()
		ScheduleDecay()
	end)

RegisterEventHandler(EventType.Message, "UpdateDecay", 
	function(decayTime)
		ScheduleDecay(decayTime)
	end)

RegisterEventHandler(EventType.Timer, "decay", 
	function()
		this:Destroy()
	end)