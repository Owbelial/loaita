require 'base_magic_sys'
require 'incl_player_guild'
require 'weapon_cache'

--MINIMUM_RANGE_OFFSET
COMBAT_RANGE = 20
mDualWielding = false
mInCombatState = false
mCurrentTarget = nil
mQueuedWeaponAbility = nil
mIsMoving = false
mWeaponDrawn = false

--- CombatMods are here to facilitate a 'hidden' Mod, for a single weapon swing for example, and does not update stats.
CombatMod = {
	-- copies from base_mobile (These are used in QueuedWeaponAbilites)
	AttackPlus = { },
	AttackTimes = { },
}

RegisterEventHandler(EventType.Message, "CombatMod", function(modName, modId, modValue)
		ApplyCombatMod(modName, modId, modValue)
	end)

function ApplyCombatMod(modName, modId, modValue)
	if ( CombatMod[modName] ~= nil ) then
		CombatMod[modName][modId] = modValue
	end
end

function PerformWeaponAttack(atTarget, hand)
	if not( mInCombatState ) then return end

	if ( IsMobileDisabled(this) ) then
		return
	end

	hand = hand or "RightHand"

	--LuaDebugCallStack("PerformWeaponAttack")
	if ( _Weapon[hand].DrawSpeed ) then
		if ( mIsMoving ) then
			return
		end
		if not( mWeaponDrawn ) then
			DrawWeapon(hand)
			return
		end
	end

	if ( _Weapon[hand].IsRanged and IsPlayerCharacter(this) and CountResourcesInContainer(this, "Arrows") < 1 ) then
		this:SystemMessage("Out of arrows.", "info")
		ResetSwingTimer(0, hand)
		return
	end

	if( atTarget == nil or atTarget == this ) then 
		PrimeAttack(hand)
		return
	end

	
	LookAt(this, atTarget)

	if not( ValidCombatTarget(this, atTarget) )  then 
		SetCurrentTarget(nil)
		PrimeAttack(hand)
		return
	end

	if not( this:HasLineOfSightToObj(atTarget) ) then
		PrimeAttack(hand)
		return
	end

	if( this:HasTimer("SpellPrimeTimer") ) then
		DelaySwingTimer(1, hand)
		PrimeAttack(hand)
		return
	end

	if not( WithinCombatRange(this, atTarget, _Weapon[hand].Range) ) then
		PrimeAttack(hand)
		return
	end

	--- perform the actual swing/shoot/w.e.
	this:SendMessage("BreakInvisEffect", "Swing")
	
	if ( _Weapon[hand].IsRanged ) then
		ExecuteRangedWeaponAttack(atTarget, hand)
	else
		ExecuteWeaponAttack(atTarget, hand)
	end
end

function ExecuteRangedWeaponAttack(atTarget, hand, hitSuccessOverride, isCritOverride)
	if ( IsPlayerCharacter(this) ) then
		-- consume the arrow before any further calculations.
		if ( ConsumeResourceBackpack(this, "Arrows", 1) ) then
			ExecuteWeaponAttack(atTarget, hand, true, hitSuccessOverride, isCritOverride)
		else
			this:SystemMessage("Out of arrows.", "info")
			-- reset swing timer
			ResetSwingTimer(0, hand)
		end
	else
		-- non-players don't consume arrows on ranged attacks (That's loot!)
		ExecuteWeaponAttack(atTarget, hand, true, hitSuccessOverride, isCritOverride)
	end
end

RegisterEventHandler(EventType.Message, "ExecuteRangedWeaponAttack", ExecuteRangedWeaponAttack)

--- Performs the actual attack with a weapon, also consumes any queued weapon abilities
-- @param atTarget, mobileObj this weapon attack is being executed against
-- @param hand, string, weapon hand, LeftHand or RightHand
-- @param ranged, bool, (optional) is this a ranged attack?
-- @param hitSuccessOverride, bool, (optional) if supplied hit chance will be based on this value, nil or not provided will calculate the hit chance (or 100% hit chance for queued weapon abilities)
-- @param isCritOverride, bool, (optional) if supplied, this boolean will decide if it's a crit or not, nil will cause it to calculate a crit chance.
function ExecuteWeaponAttack(atTarget, hand, ranged, hitSuccessOverride, isCritOverride)	

	-- swing has been executed, reset timer.
	ResetSwingTimer(0, hand)

	if ( ranged ) then
		PerformClientArrowShot(this, atTarget, _Weapon.RightHand.Object)
	else
		-- PerformClientArrowShot calls this internally
		PlayAttackAnimation(this)
	end

	-- grunting and stuff
	RandomAttackSoundChance(this)

	if ( mQueuedWeaponAbility ~= nil ) then
		if ( PerformWeaponAbility(this, atTarget, mQueuedWeaponAbility) ) then
			if ( ServerSettings.Stats.AbilityRecoveryTime ) then
				SetMobileModExpire(this, "Freeze", "AbilityRecovery", true, ServerSettings.Stats.AbilityRecoveryTime)
			end

			if ( mQueuedWeaponAbility == nil ) then
				LuaDebugCallStack("mQueueWeaponAbility is nil where it shouldn't be, ExecuteWeaponAttack is probably being called multiple times in quick succession.")
			end

			-- successfully took the stamina required, apply any mods
			if ( mQueuedWeaponAbility.CombatMods ~= nil ) then
				for k,v in pairs(mQueuedWeaponAbility.CombatMods) do
					ApplyCombatMod(k, "WeaponAbility", v)			
				end
			end
		end
	end

	-- some queued abilities will bypass the normal execute hit action and call it manually, or do whatever is needed for the ability.
	if ( mQueuedWeaponAbility == nil or mQueuedWeaponAbility.SkipHitAction ~= true ) then
		-- queued abilities have 100% hit chance, otherwise go with the override or determine hit chance now.
		local hitSuccess = false
		if ( mQueuedWeaponAbility ~= nil ) then
			hitSuccess = true
		elseif ( hitSuccessOverride ~= nil ) then
			hitSuccess = hitSuccessOverride
		else
			hitSuccess = CheckHitSuccess(atTarget, hand)
		end
		if ( hitSuccess ) then
			local isCrit = false
			-- queued abilities can't crit.
			if ( mQueuedWeaponAbility == nil ) then
				if ( isCritOverride ~= nil ) then
					-- if a crit override was provided
					isCrit = isCritOverride
				else
					-- crit chance is the same for mobs and players
					isCrit = Success((this:GetStatValue("CritChance") or 0)/100)
				end
			end
			ExecuteHitAction(atTarget, hand, isCrit)
		else
			ExecuteMissAction(atTarget, hand)
		end
	end

	if ( mQueuedWeaponAbility ~= nil ) then
		-- remove any mods applied from the weapon ability.
		if ( mQueuedWeaponAbility.CombatMods ~= nil ) then
			for k,v in pairs(mQueuedWeaponAbility.CombatMods) do
				ApplyCombatMod(k, "WeaponAbility", nil)
			end
		end
		-- interrupt the target if this ability allows
		if ( mQueuedWeaponAbility.SpellInterrupt == true ) then
			CheckSpellCastInterrupt(atTarget)
		end
		ClearQueuedWeaponAbility()
	end
end

function ClearQueuedWeaponAbility()
	if ( mQueuedWeaponAbility and this:IsPlayer() ) then
		-- tell the client to stop 'highlighting' this button
		this:SendClientMessage("SetActionActivated",{"CombatAbility",mQueuedWeaponAbility.ActionId,false})
	end
	mQueuedWeaponAbility = nil
end

RegisterEventHandler(EventType.Message, "ClearQueuedWeaponAbility", ClearQueuedWeaponAbility)

RegisterEventHandler(EventType.Message,"RegisterAbilitySelectTarget",function (primary,weaponAbility)
		if(weaponAbility.QueueTarget == "Any") then
			RegisterSingleEventHandler(EventType.ClientTargetAnyObjResponse,"AbilitySelectTarget",
				function (target,user)
					weaponAbility.NoTarget = true
					weaponAbility.QueueTarget = nil
					weaponAbility.Target = target
					QueueWeaponAbility(this,primary,weaponAbility)
				end)
		elseif(weaponAbility.QueueTarget == "Loc") then
			RegisterSingleEventHandler(EventType.ClientTargetLocResponse,"AbilitySelectTarget",
				function (success,targetLoc,targetObj,user)
					weaponAbility.NoTarget = true
					weaponAbility.QueueTarget = nil
					weaponAbility.Target = targetObj
					-- if a target location is set, we need to pass it through the MobileEffectArgs
					if ( targetLoc ) then
						if ( weaponAbility.MobileEffect ) then
							if ( weaponAbility.MobileEffectArgs ) then
								weaponAbility.MobileEffectArgs.TargetLoc = targetLoc
							else
								weaponAbility.MobileEffectArgs = {
									TargetLoc = targetLoc
								}
							end
						end
						if ( weaponAbility.TargetMobileEffect ) then
							if ( weaponAbility.TargetMobileEffectArgs ) then
								weaponAbility.TargetMobileEffectArgs.TargetLoc = targetLoc
							else
								weaponAbility.TargetMobileEffectArgs = {
									TargetLoc = targetLoc
								}
							end
						end
					end
					QueueWeaponAbility(this,primary,weaponAbility)
				end)
		else
			RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"AbilitySelectTarget",
				function (target,user)
					weaponAbility.NoTarget = true
					weaponAbility.QueueTarget = nil
					weaponAbility.Target = target
					QueueWeaponAbility(this,primary,weaponAbility)
				end)
		end
	end)

function PerformMagicalAttack(spellName, spTarget, spellSource, doNotRetarget)
	--D*ebugMessage(tostring(spellName .. " | " ..tostring(spTarget).." | " .. tostring(spellSource).. "|" .. tostring(doNotRetarget)))
	if(doNotRetarget == nil) then doNotRetarget = false end
	if(not(doNotRetarget) and not(IsDead(spTarget))) then 
		mCurrentTarget = spTarget
	end

	if not ( ValidCombatTarget(this, spTarget) ) then return end

	if(not InCombat(this)) then 
		SetInCombat(true) 
		BeginCombat()
	end
	
	if(doNotRetarget == false ) then LookAt(this, spTarget) end

	ExecuteSpellHitActions(spTarget, spellName, spellSource) 

	return "AttackHit"
end

function ExecuteSpellHitActions(atTarget, spellName, spellSource)
	local damageAmount = 0
		
	local baseDam = GetSpellInformation(spellName, "SpellPower") or 0
	-- hack to make ruin damage based on distance
	if ( spellName == "Ruin" and atTarget:IsValid() ) then
		local distance = this:DistanceFrom(atTarget)
		if ( distance > 2 ) then
			-- alter the damage by the distance
			if ( distance  > 4 ) then
				baseDam = baseDam * 0.25
			else
				baseDam = baseDam * 0.50
			end
		end
	end
	local damageType = GetSpellDamageType(spellName)

	local damInfo = {}
	damInfo.Damage = baseDam
	damInfo.Type = "MAGIC"
	damInfo.Source = this
	damInfo.Attacker = this
	damInfo.SpellCircle = GetSpellInformation(spellName, "Circle") or 1

	CheckDestructionMobileEffect(this, atTarget, baseDam, spellName)

	ApplyDamageToTarget(atTarget, damInfo)
	ApplySpellEffects(spellName, atTarget, spellSource)
end

-- SWING ACTIONS--
--[[
Instant use specials or events like parrying can delay the swing. In these cases we want to retain the current swing time remaining
and add the delay to it, before recreating the timer
bonusDelay = <INT> Additional time in milliseconds to add to the current swing
timerId = <STRING> Identifier for which swing timer to reset
]]
function DelaySwingTimer(bonusDelay, timerId)
	--D*ebugMessage(tostring(this) .. "  SwingTimeDelay " .. tostring(timerId))
	local resetAlt = false
	if(timerId == nil) then timerId = "All" end
	local altTimerId = "LeftHand"
	if(timerId == "LeftHand") then 
		altTimerId = "RightHand"
	end
	if(timerId == "All") then
		resetAlt = true
		timerId = "RightHand"
	end

	local nextSwingTimer = this:GetTimerDelay("SWING_TIMER_" .. timerId)
	if not(nextSwingTime == nil) then 
		local newTime = nextSwingTime.TotalMilliseconds + (bonusDelay * 1000)
		if(newTime <= 0) then
			--D*ebugMessage(tostring(this) ..":Firing Timer")
			this:FireTime("SWING_TIMER_" ..timerId)
		else
			this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(nextSwingTime.TotalMilliseconds + (bonusDelay * 1000)), "SWING_TIMER_"..timerId)
		end
	else		
		if(bonusDelay < .1) then
			--D*ebugMessage(tostring(this) ..":Firing Timer")
			this:FireTime("SWING_TIMER_" ..timerId)
		else
			this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(bonusDelay * 1000), "SWING_TIMER_"..timerId)
		end
	end

	if(resetAlt and mDualWielding) then
		local altTimer = this:GetTimerDelay("SWING_TIMER_" .. altTimerId)
		if not(altTimer == nil) then
			local newAlt = altTimer.TotalMilliseconds + (bonusDelay * 1000)
			if(newAlt <= 0 ) then
				this:FireTimer("SWING_TIMER_" .. altTimerId)
			else
				this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(altTimer.TotalMilliseconds + (bonusDelay * 1000)), "SWING_TIMER_"..altTimerId)
			end
		else
			if(bonusDelay < .1) then
				this:FireTimer("SWING_TIMER_" .. altTimerId)
			else
				this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(bonusDelay * 1000), "SWING_TIMER_"..altTimerId)
			end
		end
	end
		
end

function GetSwingTimerName(weaponHand)
	return "SWING_TIMER_"..(weaponHand or "RightHand")
end

function ResetSwingTimer(timeToDelayNextSwing, weaponHand)
	weaponHand = weaponHand or "RightHand"
	timeToDelayNextSwing = timeToDelayNextSwing or 0

	mWeaponDrawn = false

	local timerList = {weaponHand}
	if ( weaponHand == "All" ) then
		timerList = {
			"RightHand",
			"LeftHand"
		}
	end
	
	for i,weaponHand in pairs(timerList) do
		-- left hand should only work given there's an object equipped that's not a shield.
		if ( weaponHand == "LeftHand" and (not _Weapon.LeftHand.Object or _Weapon.LeftHand.ShieldType ~= nil) ) then break end
		local swingTimer = GetSwingTimerName(weaponHand)
		
		swingSpeed = this:GetStatValue("AttackSpeed")
		if ( _Weapon[weaponHand].DrawSpeed ) then
			swingSpeed = math.max(swingSpeed - (_Weapon[weaponHand].DrawSpeed/1000), ServerSettings.Combat.MinimumSwingSpeed)
		end

		if ( swingSpeed < ServerSettings.Combat.MinimumSwingSpeed ) then
			LuaDebugCallStack("Swing speed of "..swingSpeed.." detected, setting to "..ServerSettings.Combat.MinimumSwingSpeed.." seconds.")
			swingSpeed = ServerSettings.Combat.MinimumSwingSpeed
		end
		--DebugMessage("ScheduleSwingTimer",tostring(swingSpeed),tostring(timeToDelayNextSwing))
		--LuaDebugCallStack("ScheduleSwingTimer")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(swingSpeed + timeToDelayNextSwing), swingTimer)
	end
end

------------------------------------------

-- Evalutators
function ValidateCurrentTarget()
	if( mCurrentTarget == nil ) then return false end
	if not( mCurrentTarget:IsValid() ) then return false end
	return true
end


function CheckHitSuccess(victim, hand)
	local isPlayer = IsPlayerCharacter(this)	

	--Ensure you only gain off valid targets
	if (isPlayer or IsTamedPet(this)) then
		local damageSkill = "MeleeSkill"
		if ( ValidCombatGainTarget(victim,this) ) then
			local victimWeaponSkillLevel = GetSkillLevel(victim, GetPrimaryWeaponSkill(victim))
			CheckSkill(this, damageSkill, victimWeaponSkillLevel)
			local weaponClass = _Weapon[hand].Class
			if ( EquipmentStats.BaseWeaponClass[_Weapon[hand].Class] and EquipmentStats.BaseWeaponClass[_Weapon[hand].Class].WeaponSkill ) then
				CheckSkill(this, EquipmentStats.BaseWeaponClass[_Weapon[hand].Class].WeaponSkill, victimWeaponSkillLevel)
			end
		end
	end

	local accuracy = this:GetStatValue("Accuracy")
	local evasion = victim:GetStatValue("Evasion")

	local hitChance = 90.5
	local hitDiff = accuracy - evasion
	if hitDiff < -20 then 
        hitChance = hitChance - 15 + ((hitDiff+20) * 5)
    elseif hitDiff < -15 and hitDiff > -21 then
        hitChance = hitChance - 10 + ((hitDiff+15) * 3)
    elseif hitDiff < -10 and hitDiff > -16 then 
        hitChance = hitChance - 5 + ((hitDiff+10) * 2)
    elseif hitDiff < 0 and hitDiff > -11 then
        hitChance = hitChance + (hitDiff * 1.5)
    elseif hitDiff == 0 then
        hitChance = hitChance
    elseif hitDiff > 0 and hitDiff < 6 then
        hitChance = hitChance + hitDiff
    elseif hitDiff > 5 then
        hitChance = hitChance + 5 + ((hitDiff-5) * 0.5)
    end

	return Success(hitChance/100)

end

-- Get Combat Status
function InCombat(obj)
	if( obj == nil or obj == this) then
		return mInCombatState
	else
		return IsInCombat(obj)
	end
end

function ExecuteMissAction(atTarget, hand)
	--LuaDebugCallStack("ExecuteMissAction")
	atTarget:NpcSpeech("[08FFFF]*Miss*[-]","combat")
	PlayWeaponSound(this, "Miss", _Weapon[hand].Object)
end

--- This comes after a successful (hitchance) ExecuteWeaponAttack, and applies the weapon damage
-- @param atTarget, mobileObj, target to execute the hit action against
-- @param hand, string, hand of weapon, LeftHand or RightHand
-- @param isCrit, bool, (optional) is this a critical hit?
function ExecuteHitAction(atTarget, hand, isCrit)

	local damageInfo = {
		Attack = ( this:GetStatValue("Attack") + GetCombatMod(CombatMod.AttackPlus) ) * GetCombatMod(CombatMod.AttackTimes, 1),
		Type = _Weapon[hand].DamageType,
		Source = _Weapon[hand].Object,
		Attacker = this,
		IsCrit = isCrit,
	}

	if ( isCrit ) then
		damageInfo.CritDamageBonus = GetCritDamageBonus(this, _Weapon[hand].Object)
		atTarget:PlayEffect("BuffEffect_I", 1)
	end

	-- damage the weapon that's being swung.
	if ( _Weapon[hand].Object and Success(ServerSettings.Durability.Chance.OnSwing) and IsPlayerCharacter(this) ) then
		AdjustDurability(_Weapon[hand].Object, -1)
	end

	ApplyDamageToTarget(atTarget, damageInfo)				
end

RegisterEventHandler(EventType.Message, "ExecuteHitAction", ExecuteHitAction)

function CalculateBlockDefense(victim)
	local shield = victim:GetEquippedObject("LeftHand")
	if not( shield ) then return 0 end
	local shieldType = GetShieldType(shield)
	if ( shieldType and EquipmentStats.BaseShieldStats[shieldType] ) then
		-- skill gain dependant on the attacker's weapon skill level
		CheckSkill(victim, "BlockingSkill", GetSkillLevel(this,EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].WeaponSkill))
		if ( Success(GetSkillLevel(victim,"BlockingSkill")/200) ) then
			victim:PlayAnimation("block")
			if ( Success(ServerSettings.Durability.Chance.OnHit) ) then
				AdjustDurability(shield, -1)
			end
			return (EquipmentStats.BaseShieldStats[shieldType].ArmorRating or 0) + GetMagicItemArmorBonus(_Weapon.LeftHand.Object)
		end
	end
	return 0
end

function ApplyDamageToTarget(victim, damageInfo)
	if damageInfo == nil then return end

	damageInfo.Attacker = damageInfo.Attacker or this

	if not(damageInfo.Type) then
		if(damageInfo.Source) then
			damageInfo.Type = GetWeaponDamageType(damageInfo.Source)		
		else
			damageInfo.Type = "TrueDamage"
		end
	end

	if damageInfo.Attacker ~= victim and not( ValidCombatTarget(damageInfo.Attacker, victim) ) then
		return
	end

	if ( damageInfo.Type == "MAGIC" or damageInfo.IsCrit ) then CheckSpellCastInterrupt(victim) end

	local finalDamage = damageInfo.Damage or 0
	local blockDefense = 0
	damageInfo.PhysicalAbsorb = 0

	if ( damageInfo.Type ~= "TrueDamage" and damageInfo.Type ~= "Poison" ) then
		local isPlayer = IsPlayerCharacter(victim)

		--Get stats for the armor piece at the slot being hit OR the natural armor type the mob posses
		if ( damageInfo.Type == "MAGIC" ) then
			finalDamage = (damageInfo.Attacker:GetStatValue("Power") * finalDamage) / 8

			-- calculate variance if not passed in
			if not( damageInfo.Variance ) then
				if ( damageInfo.Source and isPlayer ) then
					-- KH TODO: Make this configurable
					damageInfo.Variance = 0.05
				else
					-- DAB COMBAT CHANGES: Make this configurable
					damageInfo.Variance = 0.05
				end
			end

			finalDamage = randomGaussian(finalDamage, finalDamage * damageInfo.Variance)

			if ( CheckActiveSpellResist(victim) ) then
				-- successful magic resist, half base damage
				finalDamage = finalDamage * 0.5
			end

			--victim:NpcSpeech("Base Damage: "..damageInfo.Damage)
			--victim:NpcSpeech("Final Damage: "..finalDamage)
		else
			if not( damageInfo.Attack ) then
				if ( damageInfo.Attacker and damageInfo.Attacker:IsValid() ) then
					damageInfo.Attack = damageInfo.Attacker:GetStatValue("Attack")
				else
					DebugMessage("ERROR: Attempting to perform weapon damage with no attacker")
				end
			end
			
			local critMultiplier = 1
			if ( damageInfo.IsCrit ) then
				critMultiplier = 2
			else
				damageInfo.CritDamageBonus = 0
			end

			blockDefense = CalculateBlockDefense(victim)
			local defense = victim:GetStatValue("Defense") + blockDefense
			finalDamage = (damageInfo.Attack * 70 * critMultiplier / defense) + damageInfo.CritDamageBonus
			--DebugMessage("DO IT",tostring(finalDamage),tostring(damageInfo.Attack),tostring(critMultiplier),tostring(defense),tostring(damageInfo.CritDamageBonus))

			-- calculate variance if not passed in
			if not( damageInfo.Variance ) then
				if ( damageInfo.Source and isPlayer ) then
					damageInfo.Variance = EquipmentStats.BaseWeaponClass[GetWeaponClass(damageInfo.Source)].Variance
				else
					-- DAB COMBAT CHANGES: Make this configurable
					damageInfo.Variance = 0.20
				end
			end

			--DebugMessage("DAMAGE: "..tostring(finalDamage),tostring(damageInfo.IsCrit),tostring(damageInfo.CritDamageBonus))
			--DebugMessage("DAMAGE: "..tostring(finalDamage),tostring(damageInfo.Attack),tostring(defense),tostring(victimArmorRating),tostring(GetCombatMod(CombatMod.DefenseTimes,1)),tostring(GetCombatMod(CombatMod.DefensePlus)),tostring(victimArmorBonus))
	
			finalDamage = randomGaussian(finalDamage,finalDamage * damageInfo.Variance)
			--DebugMessage("VARIANCE",tostring(finalDamage),tostring(damageInfo.Variance))
		end

		-- all but true damage/poison will also hurt the equipment.
		damageInfo.Slot = damageInfo.Slot or GetHitLocation()
		local hitItem = victim:GetEquippedObject(damageInfo.Slot)
		local soundType = "Leather"
		if ( hitItem ~= nil ) then
			local hitArmorType = GetArmorType(hitItem)
			soundType = GetArmorSoundType(hitArmorType)	

			if ( isPlayer or IsTamedPet(victim) ) then
				-- damage equipment that was hit
				if ( Success(ServerSettings.Durability.Chance.OnHit) ) then
					AdjustDurability(hitItem, -1)
				end

				-- perform proficiency check
				local armorClass = GetArmorClassFromType(hitArmorType)
				local armorSkill = armorClass.."ArmorSkill"
				if ( (armorClass == "Light" or armorClass == "Heavy") and ValidCombatGainTarget(damageInfo.Attacker,victim) ) then																	
					CheckSkill(victim, armorSkill, GetSkillLevel(damageInfo.Attacker, EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].WeaponSkill))					
				end
			end
		end

		if ( damageInfo.Type ~= "MAGIC" ) then
			PlayWeaponSound(damageInfo.Attacker, "Impact" .. soundType, damageInfo.Source)
		end
	else
		finalDamage = damageInfo.Damage
	end

	--victim:NpcSpeech("Raw Damage: "..damageInfo.Damage)
	--victim:NpcSpeech("Physical Absorb: "..damageInfo.PhysicalAbsorb)
	--victim:NpcSpeech("Damage Final         :"..damageInfo.FinalDamage)
	victim:SendMessage("DamageInflicted", damageInfo.Attacker, finalDamage, damageInfo.Type, damageInfo.IsCrit, blockDefense > 0, damageInfo.IsReflected)
end

function SetCurrentTarget(newTarget, fromClient)
	if(newTarget == nil ) then
		mCurrentTarget = nil
		this:DelObjVar("CurrentTarget")
		UpdateSpellTarget(mCurrentTarget)
		if ( this:IsPlayer() ) then
			this:SendClientMessage("ChangeTarget", mCurrentTarget)
		end
		return
	end
	if(mCurrentTarget ~= newTarget) then
		mCurrentTarget = newTarget

		if(newTarget ~= nil) then
			this:SetObjVar("CurrentTarget", newTarget)
			if ( mInCombatState ) then
				LookAt(this, newTarget)
				SendPetCommandToAll(GetActivePets(this, PetStance.Aggressive), "autoattack", newTarget)
			end
		else
			this:DelObjVar("CurrentTarget")
		end

		if(((not skipClientUpdate) and (not fromClient)) and this:IsPlayer()) then
			this:SendClientMessage("ChangeTarget",mCurrentTarget)
		end

		PrimeAttack("RightHand")

		UpdateSpellTarget(newTarget)
	end
end

function EndCombat()
	DelView("PrimaryAttackRange")
	DelView("SecondaryAttackRange")
	SetInCombat(false)
end

function PrimeAttack(hand, atTarget)	
	if not( InCombat(this) ) then return end
	if ( atTarget == nil ) then atTarget = mCurrentTarget end

	--LuaDebugCallStack("PrimeAttack")

	local viewName = "PrimaryAttackRange"
	if ( hand == "LeftHand" ) then
		viewName = "SecondaryAttackRange"
	end

	AddView( viewName, SearchObjectInRange( GetCombatRange(this, atTarget, _Weapon[hand].Range) ) )
end

function BeginCombat()
	if(IsDead(this)) then return end
	ScheduleEndCombatTimer()
	-- make sure we are in combat! (this function does nothing if you are already in combat)
	SetInCombat(true)
	InitiateCombatSequence()
end

function InitiateCombatSequence()
	if( IsDead(this) ) then return end

	if not( this:HasTimer("SWING_TIMER_RightHand") ) then 
		PerformWeaponAttack(atTarget, "RightHand")
	end
	if( mDualWielding and not this:HasTimer("SWING_TIMER_LeftHand") ) then
		PerformWeaponAttack(atTarget, "LeftHand")
	end
	
	if ( mCurrentTarget ~= nil ) then
		LookAt(this, mCurrentTarget)
		SendPetCommandToAll(GetActivePets(this, PetStance.Aggressive), "autoattack", mCurrentTarget)
	end
end

function DrawWeapon(hand)
	--LuaDebugCallStack("DrawWeapon")
	if ( this:HasTimer("DrawSpeedTimer") ) then return end
	if ( _Weapon[hand].DrawSpeed ) then
		if not ( mIsMoving ) then
			this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(_Weapon[hand].DrawSpeed),"DrawSpeedTimer")
			this:PlayAnimation("draw_bow")
			PlayWeaponSound(this, "Load", _Weapon[hand].Object)
		end
		return true
	end

	return false
end

--EVENT HANDLERS
function HandleAttackTarget(target)
	if(target ~= nil) then 
		SetCurrentTarget(target)		
	end
	SetInCombat(true)
	BeginCombat()
end

function HandleScriptCommandToggleCombat()
	if(IsDead(this)) then return end

	if ( CancelCastPrestigeAbility(this) ) then return end

	-- Enter combat mode if not already
	if( mInCombatState == false ) then
		BeginCombat()
	else
		SetInCombat(false)
	end
end


function HandleEnterPrimaryAttackRange(obj)
	if( ValidCombatTarget(this, obj) and obj ~= mCurrentTarget ) then return end
	if( this:HasTimer("SWING_TIMER_RightHand") ) then
		return
	end
	PerformWeaponAttack(mCurrentTarget, "RightHand")
end


function HandleEnterSecondaryAttackRange(obj)
	if( ValidCombatTarget(this, obj) and obj ~= mCurrentTarget ) then return end
	if(this:HasTimer("SWING_TIMER_LeftHand")) then
		return
	end
	PerformWeaponAttack(mCurrentTarget, "LeftHand")
end

function HandleLeavePrimaryAttackRange(obj)
	--if(obj == mCurrentTarget) then this:SendMessage("TargetLeftMeleeAttackRangeMessage", { ["target"] = obj}) end
end

function HandleScriptCommandTargetObject(targetObjId)
	if( targetObjId == nil ) then
		SetCurrentTarget(nil,true)		
		return
	end
	
	local newTarget = GameObj(tonumber(targetObjId))
	if not(newTarget:IsValid()) then
		return
	end
	
	SetCurrentTarget(newTarget,true)	

	if( mInCombatState ) then
		BeginCombat()
	end
end


-- Stat Calculations
--Actors
mCombatMusicPlaying = false
function SetInCombat(inCombat, force)
	if inCombat ~= true then inCombat = false end
	-- we use a local variable to be able to see changes to the state immediately
	if(IsDead(this)) then 
		inCombat = false
	end
	if( mInCombatState ~= inCombat or force ) then
		mInCombatState = inCombat
		this:SendMessage("CombatStatusUpdate", inCombat)
		this:SetSharedObjectProperty("CombatMode", inCombat)
			
		if(mInCombatState == true) then
			--if (this:IsPlayer() and IsCombatMap()) then
			--	this:PlayMusic("Combat")
			--	mCombatMusicPlaying = true
			--end
			this:SendMessage("BreakInvisEffect", "Combat")
			if ( this:GetSharedObjectProperty("IsSneaking") ) then
				this:SendMessage("EndSneak")
			end
			if(_Weapon.RightHand.Object ~= nil) then
				PlayWeaponSound(this,"Draw")
			end
		
			if( this:GetObjVar("IsHarvesting") ) then		
				-- this is a messy hack since the tool itself does the gathering right now
				if( _Weapon.RightHand.Class == "Tool" ) then
					_Weapon.RightHand.Object:SendMessage("CancelHarvesting",this)
				end
				this:SendMessage("CancelHarvesting",this)
			end
		else
			mWeaponDrawn = false
			if ( _Weapon.RightHand.IsRanged ) then
				this:PlayAnimation("end_draw_bow")
			end
			--if (mCombatMusicPlaying) then
			--	this:StopMusic()
			--	mCombatMusicPlaying = false
			--end

			if(_Weapon.RightHand.Object ~= nil) then
				PlayWeaponSound(this,"Sheathe")
			end
		end

		--UpdateAttackView()
	end
end

--Enter attack range of right hand weapon
RegisterEventHandler(EventType.EnterView, "PrimaryAttackRange",
	function(obj)
		if not( mInCombatState ) then return end
		if( obj ~= mCurrentTarget ) then return end

		if not( this:HasTimer("SWING_TIMER_RightHand") ) then
			if( IsDead(this) ) then return end
			PerformWeaponAttack(mCurrentTarget, "RightHand")
		end
	end)


--Leave attack range of right hand weapon
RegisterEventHandler(EventType.LeaveView, "PrimaryAttackRange", function(obj)
		HandleLeavePrimaryAttackRange(obj)
	end)


--Enter attack range of left hand weapon
RegisterEventHandler(EventType.EnterView, "SecondaryAttackRange", function()
		if not( mInCombatState ) then return end
		if( obj ~= mCurrentTarget ) then return end
		if not( this:HasTimer("SWING_TIMER_LeftHand") ) then
			if( IsDead(this) ) then return end
			PerformWeaponAttack(mCurrentTarget, "LeftHand")
		end
	end)

--Right Hand Swing Timer
RegisterEventHandler(EventType.Timer, "SWING_TIMER_RightHand",
	function()
		PerformWeaponAttack(mCurrentTarget, "RightHand")
	end)


--Left Hand Swing Timer for Dual wield support
RegisterEventHandler(EventType.Timer, "SWING_TIMER_LeftHand",
	function()
		PerformWeaponAttack(mCurrentTarget, "LeftHand")
	end)

---------------------------
-----------------------
--LEGACY
function HandleMagicalAttackRequest(spellName,spTarg,spellSource,doNotRetarget,damagePvP)
	--DebugMessage(isNotAOE, " is AOE")
	if (spTarg == spellSource and not ServerSettings.Combat.NoSelfDamageOnAOE and (not isNotAOE) and not damagePvP) then return end
	--D*ebugMessage("[INFO]Spell Request: " ..tostring(spellName) .. " Target: " .. spTarg:GetName().. " Source:"..tostring(spellSource) .. " NoRetarget:" .. tostring(doNotRetarget))
	--DebugMessage("damagePvP is "..tostring(damagePvP))
	PerformMagicalAttack(spellName, spTarg, spellSource, doNotRetarget,damagePvP)
end

function HandleAutotargetToggleRequest(newState)	
	if(newState == nil) then
		newState = not(this:GetObjVar("AutotargetEnabled"))
	end

	if(newState == "on" or newState == true) then
		this:SystemMessage("Autotargetting enabled.")
		this:SetObjVar("AutotargetEnabled",true)
	else
		this:SystemMessage("Autotargetting disabled.")
		this:DelObjVar("AutotargetEnabled")
	end
end

-- This has been re-branded to Karma Protection -KH 2/4/18
function HandlePvPToggleRequest(newState)
	-- on means you can now perform negative actions
	if(newState == "on" or newState == true) then
		this:DelObjVar("PvPDisabled")
		this:SystemMessage("Karma Protection Disabled.", "info")		
	else
		this:SetObjVar("PvPDisabled", true)
		this:SystemMessage("Karma Protection Enabled.", "info")
	end
end

RegisterEventHandler(EventType.Message, "ResetSwingTimer", function (delay, hand)
	ResetSwingTimer(delay or 0, hand)
end)

function ScheduleEndCombatTimer(checkVal)
	local delaySecs = ServerSettings.Combat.NPCEndCombatPulse
	if(this:IsPlayer()) then
		delaySecs = ServerSettings.Combat.PlayerEndCombatPulse
	end
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(delaySecs),"CheckEndCombat", checkVal)
end

function ClearTarget()
	SetCurrentTarget(nil)
	if ( this:IsPlayer() ) then
		this:SendClientMessage("ChangeTarget", nil)
	end
end


RegisterEventHandler(EventType.Timer,"CheckEndCombat",
function (checkVal)
	if (mCurrentTarget == nil or not mCurrentTarget:IsValid()) or (mCurrentTarget:IsMobile() and IsDead(mCurrentTarget)) or (this:DistanceFrom(mCurrentTarget) > COMBAT_RANGE) then
		if(checkVal == nil) then checkVal = 0 end
		checkVal = checkVal + 1
		if(checkVal > 5) then 
			this:SendMessage("EndCombatMessage", "CheckEndCombat") 
		else
			ScheduleEndCombatTimer(checkVal)
		end
	else
		ScheduleEndCombatTimer(checkVal)
	end
end)

--Movement Handler
RegisterEventHandler(EventType.StartMoving, "", 
	function()
		if ( this:HasTimer("DrawSpeedTimer") ) then
			--this:RemoveTimer("DrawSpeedTimer")
		end
		mIsMoving = true
		--mWeaponDrawn = false

		if(InCombat(this)) then
			if( _Weapon.RightHand.IsRanged ) then
				--this:PlayAnimation("end_draw_bow")
			end
		end
	end)

RegisterEventHandler(EventType.Timer,"DrawSpeedTimer",
	function ( ... )
		-- make sure we still need to draw the weapon
		if(InCombat(this) and not(mWeaponDrawn) and _Weapon.RightHand.DrawSpeed) then
			mWeaponDrawn = true
			PerformWeaponAttack(mCurrentTarget,"RightHand")
		end
	end)

RegisterEventHandler(EventType.StopMoving, "", 
	function ()
		mIsMoving = false

		if ( InCombat(this) ) then
			-- Attempt to draw weapon if our swing timer is up
			if _Weapon.RightHand.DrawSpeed and not(this:HasTimer(GetSwingTimerName())) then
				PerformWeaponAttack(mCurrentTarget,"RightHand")
			end

			if ( mCurrentTarget ~= nil ) then
				LookAt(this, mCurrentTarget)
			end
		end
	end)
---
RegisterEventHandler(EventType.Message, "DelayNextSwing",
	function (args)
		local delay = args.delay or 1
		local timer = args.timer or "All"
		DelaySwingTimer(delay, "All")
		end)

RegisterEventHandler(EventType.Message,"EndCombatMessage",function (reason)
	--if(reason ~= nil) then DebugMessage(this:GetName() .. " Ended Combat: " .. reason) end
	SetInCombat(false)
end)

RegisterEventHandler(EventType.Message, "ForceCombat", function (reason)
	--if(reason ~= nil) then DebugMessage(this:GetName() .. " Ended Combat: " .. reason) end
	if ( mInCombatState ~= true ) then
		SetInCombat(true)
		BeginCombat()
	end
end)


RegisterEventHandler(EventType.ItemEquipped, "", 
	function(item)
		if ( item ~= nil ) then
			local slot = GetEquipSlot(item)
			if ( IsPlayerCharacter(this) ) then

				CancelCastPrestigeAbility(this)

				if ( slot == "RightHand" or slot == "LeftHand" ) then
					UpdatePlayerWeaponAbilities(this)
				end

				UpdateEquipmentSkillBonuses(this,item)
				
				if ( this:HasTimer("SpellPrimeTimer") and GetEquipmentClass(item) == "WeaponClass" ) then
					CancelSpellCast()
					DoFizzle(this)
				end
			end
			if ( slot == "RightHand" or slot == "LeftHand" ) then
				-- update reference to our weapons
				UpdateWeaponCache(slot, item)
			end
		end
	end)

RegisterEventHandler(EventType.ItemUnequipped, "", 
	function(item)
		if ( item ~= nil ) then
			local slot = GetEquipSlot(item)
			if ( IsPlayerCharacter(this) ) then

				CancelCastPrestigeAbility(this)

				UpdateEquipmentSkillBonuses(this,item)
				
				if ( slot == "RightHand" or slot == "LeftHand" ) then
					-- weapon was unequipped, clear queued abilities.
					ClearQueuedWeaponAbility()
					-- reset swing timer
					ResetSwingTimer(0.5, slot)
					UpdatePlayerWeaponAbilities(this)
				end
			end
			if ( slot == "RightHand" or slot == "LeftHand" ) then
				-- update reference to our weapons
				UpdateWeaponCache(slot, false)
			end
		end
	end)

RegisterEventHandler(EventType.ClientUserCommand, "targetObject", HandleScriptCommandTargetObject)
RegisterEventHandler(EventType.ClientUserCommand, "toggleCombat", HandleScriptCommandToggleCombat)
RegisterEventHandler(EventType.ClientUserCommand, "autotarget", HandleAutotargetToggleRequest)
RegisterEventHandler(EventType.Message, "ClearTarget", ClearTarget)
RegisterEventHandler(EventType.Message, "AttackTarget", HandleAttackTarget)
RegisterEventHandler(EventType.ClientUserCommand, "pvp", HandlePvPToggleRequest)
RegisterEventHandler(EventType.Message,"RequestMagicalAttack" , HandleMagicalAttackRequest)

function HandleWeaponDamageReceived(damager, isCrit, srcWeapon, type, slot, isReflected)
	local damageInfo = {
		Attacker = damager,
		IsCrit = isCrit,
		Source = srcWeapon,
		Type = type,
		Slot = slot,
		IsReflected = isReflected,
	}
	ApplyDamageToTarget(this, damageInfo)
end

function HandleTypeDamageReceived(damager, damage, isCrit, type, slot, isReflected)
	local damageInfo = {
		Attacker = damager,
		Damage = damage,
		Type = type,
		IsCrit = isCrit,
		Slot = slot,
		IsReflected = isReflected,
	}
	ApplyDamageToTarget(this, damageInfo)
end

function HandleMagicDamageReceived(damager, damage, isCrit, slot, isReflected)
	HandleTypeDamageReceived(damager, damage, isCrit, "MAGIC", slot, isReflected)
end

function HandleTrueDamageReceived(damager, damage, isCrit, slot, isReflected)
	local damageInfo = {
		Attacker = damager,
		Damage = damage,
		Type = "TrueDamage",
		IsCrit = isCrit,
		Slot = slot,
		IsReflected = isReflected,
	}
	ApplyDamageToTarget(this, damageInfo)
end

RegisterEventHandler(EventType.Message, "ProcessWeaponDamage", HandleWeaponDamageReceived)
RegisterEventHandler(EventType.Message, "ProcessTypeDamage", HandleTypeDamageReceived)
RegisterEventHandler(EventType.Message, "ProcessMagicDamage", HandleMagicDamageReceived)
RegisterEventHandler(EventType.Message, "ProcessTrueDamage", HandleTrueDamageReceived)

-------------INITIALIZERS
--- cache some info on our weapons since they get used a lot.
UpdateWeaponCache("LeftHand")
UpdateWeaponCache("RightHand")

SetInCombat(false, true)
this:DelObjVar("CurrentTarget")


RegisterEventHandler(EventType.Message, "QueueWeaponAbility",
	function(ability)
		if(mQueuedWeaponAbility) then
			if ( this:IsPlayer() ) then
				-- anytime mQueuedWeaponAbility is set, we clear it when it's called. This works like a toggle.
				this:SendClientMessage("SetActionActivated",{"CombatAbility",mQueuedWeaponAbility.ActionId,false})
			end
			-- 'activated' one that was already active. Clear it and stop here
			if ( mQueuedWeaponAbility.ActionId == ability.ActionId ) then
				mQueuedWeaponAbility = nil
				return
			end
		end
		mQueuedWeaponAbility = ability
		if ( this:IsPlayer() ) then
			this:SendClientMessage("SetActionActivated",{"CombatAbility",mQueuedWeaponAbility.ActionId,true})
			this:SystemMessage("You will attempt "..mQueuedWeaponAbility.DisplayName.." on your next swing.","info")
			if( mInCombatState == false ) then
				BeginCombat()
			end
		end
	end)

-- Global functions inside helpers.
RegisterEventHandler(EventType.ClientUserCommand, "wa", function(primarySecondary)
	PlayerUseWeaponAbility(this, primarySecondary)
end)
RegisterEventHandler(EventType.ClientUserCommand, "pa", function(position)
	PerformPrestigeAbility(this, mCurrentTarget, position)
end)