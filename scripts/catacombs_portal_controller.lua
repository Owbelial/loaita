require 'base_ai_settings'
require 'base_ai_state_machine'

local bonesForRitual = 10

AI.Settings.ShouldSleep = false

PropPermObjects = {
	PortalAnimation = PermanentObj(56253),
	BonePile = PermanentObj(56135)
}

portalObj = nil

local bonesCollected = 0

function UpdateBones()
	local percentComplete = bonesCollected / bonesForRitual 
	if(percentComplete >= 1.0) then
		PropPermObjects.BonePile:SetVisualState("Large")
	elseif(percentComplete > 0.5) then
		PropPermObjects.BonePile:SetVisualState("Medium")
	elseif(bonesCollected > 0) then
		PropPermObjects.BonePile:SetVisualState("Default")
	else
		PropPermObjects.BonePile:SetVisualState("Hidden")
	end

	if(bonesCollected >= bonesForRitual) then
		AI.StateMachine.ChangeState("WaitingForTokens")
	end
end

AI.StateMachine.AllStates = {	
	Inactive = {		
		OnEnterState = function(self)
			if(portalObj ~= nil and portalObj:IsValid()) then
				portalObj:Destroy()
				portalObj = nil
			end
			PropPermObjects.BonePile:SetVisualState("Hidden")
			PropPermObjects.PortalAnimation:SetVisualState("Hidden")

			this:DelObjVar("CatacombsPortalActive")

			AddView("Bones",SearchObjVar("ResourceType","Bones",1),1)
			RegisterEventHandler(EventType.EnterView,"Bones",
				function(targetObj)
					bonesCollected = bonesCollected + GetStackCount(targetObj)
					targetObj:Destroy()
					UpdateBones()
				end)
		end,

		OnExitState = function(self)
			UnregisterEventHandler("catacombs_portal_controller",EventType.EnterView,"Bones")
			UnregisterEventHandler("catacombs_portal_controller",EventType.LeaveView,"Bones")
			DelView("Bones")
		end,
	},

	WaitingForTokens = {
		GetPulseFrequencyMS = function() return 2500 end,

		OnEnterState = function(self)			
			local nearbyPlayers = GetViewObjects("QuestView")
			for index, player in pairs(nearbyPlayers) do
				player:SendMessage("AdvanceQuest","CatacombsReDiscoveryQuest","Sacrifice")
			end
					
			PropPermObjects.PortalAnimation:SetVisualState("Default")			

			self:AiPulse()
		end,

		AiPulse = function(self)	
			if(bonesCollected < bonesForRitual) then
				AI.StateMachine.ChangeState("Inactive")
				return
			end

			local contemptObj = nil
			local deceptionObj = nil
			local pestilenceObj = nil

			local objsOnAltar = FindObjects(SearchRange(this:GetLoc(),5))
			for i,obj in pairs(objsOnAltar) do 
				if(obj:GetCreationTemplateId() == "deception_skull") then
					contemptObj = obj
				elseif(obj:GetCreationTemplateId() == "ruin_skull") then
					deceptionObj = obj
				elseif(obj:GetCreationTemplateId() == "contempt_skull") then
					pestilenceObj = obj
				end
			end

			if(contemptObj and deceptionObj and pestilenceObj) then
				AI.StateMachine.ChangeState("PortalActive")
				contemptObj:Destroy()					
				deceptionObj:Destroy()				
				pestilenceObj:Destroy()
			end
		end,

		OnExitState = function(self,newState)			
			this:StopEffect("RadiationAuraEffect")
		end
	},

	PortalActive = {
		GetPulseFrequencyMS = function() return 4 * 60 * 60 * 1000 end,

		OnEnterState = function(self)			
			local nearbyPlayers = GetViewObjects("QuestView")
			for index, player in pairs(nearbyPlayers) do
				player:SendMessage("AdvanceQuest","CatacombsReDiscoveryQuest","EnterPortal")
			end
			this:PlayEffect("DeathwaveEffect")		
			this:PlayEffect("RedCoreImpactWaveEffect")		
			CreateTempObj("portal_red",this:GetLoc(),"portal")
			this:SetObjVar("CatacombsPortalActive",true)
			RegisterSingleEventHandler(EventType.CreatedObject,"portal",
				function(success,objRef)
					portalObj = objRef
					local catacombsHubLoc = MapLocations.Catacombs.Hub
					
					objRef:SetObjVar("Destination",catacombsHubLoc)
					objRef:SetObjVar("RegionAddress","Catacombs")
					objRef:SetName("Mysterious Portal")
					--objRef:AddModule("decay")
					--objRef:SetObjVar("DecayTime",20)
					objRef:SetHue("FF0000")

					AddView("ActivePortal",SearchSingleObject(objRef),1000)
					RegisterEventHandler(EventType.LeaveView,"ActivePortal",
						function()
							AI.StateMachine.ChangeState("Inactive")
						end)
				end)			
		end,

		AiPulse = function(self)
			AI.StateMachine.ChangeState("Inactive")
		end
	}
}

RegisterEventHandler(EventType.EnterView,"QuestView",
function (player)
	if (not player:IsPlayer()) then return end
	if (AI.StateMachine.CurState == "PortalActive") then
		player:SendMessage("AdvanceQuest","CatacombsReDiscoveryQuest","EnterPortal")
		DebugMessage("Yes it's working.")
	end
	if (AI.StateMachine.CurState == "WaitingForSacrifice") then
		player:SendMessage("AdvanceQuest","CatacombsReDiscoveryQuest","Sacrifice")
		DebugMessage("Yes it's working here.")
	end
end)

AI.StateMachine.Init("Inactive")
AddView("QuestView",SearchPlayerInRange(25,true))