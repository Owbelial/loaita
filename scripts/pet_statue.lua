function UsePetStatue(user)	
	local useLoc = this:TopmostContainer()
	if useLoc ~= user then return end
	local pet = this:GetObjVar("PetTarget")
	if(pet:TopmostContainer() ~= user) then 
		return 
	end
	if not(pet:IsValid()) then return end
	pet:SendMessage("UnStatuify")

end

RegisterEventHandler(EventType.Message, "UseObject", 
	function (user,usedType)
		if(usedType ~= "Summon Pet") then return end
		if GetRegionAddress() == "TwoTowers" then
			user:SystemMessage("You cant summon pets here.","info")
			return
	end		
		--DebugMessage("Using bed")
		if (user == nil) then
			return
		end

		if not( user:IsValid() ) then
			return
		end	
		if (user:GetSharedObjectProperty("CombatMode")) then
			user:SystemMessage("You cannot do that while in combat.","info")
			return
		end
		if(this:HasTimer("PetSummonTimeOut")) then
			user:SystemMessage("That is not ready yet..", "info")
			return
		end
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "PetSummonTimeOut")
		UsePetStatue(user)
	end)

RegisterSingleEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function()
		AddUseCase(this,"Summon Pet",true)
	end)