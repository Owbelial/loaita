require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_plating")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_plating", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Plating") then return end
		AlterEquipmentBonusStat(this, "BonusPiercingResist", 1)
		AlterEquipmentBonusStat(this, "BonusAbsorption", 1)
			this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)