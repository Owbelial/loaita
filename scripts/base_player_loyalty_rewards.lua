AwardTypes =
{
	HalloweenEvent2015 = { AwardValue = 100, OneTime = true, DisplayStr = "participating in the halloween event." }
}

FocusHours = 
{
	PA3Launch = { AwardValue = 100, OneTime = true, Start="12/12/2015 23:00:00", End="12/13/2015 1:00:00", DisplayStr = "participating in Pre-Alpha 3 Launch Weekend." },
	Focus121615 = { AwardValue = 50, OneTime = true, Start="12/16/2015 20:00:00", End="12/16/2015 22:00:00", DisplayStr = "participating in focus hours playtest." },
}

Rewards = 
{
	{ template="unicorn_horn_helm", pointCost=100},
	{ template="flower_hat_helm", pointCost=100},
	{ template="cat_ears_helm", pointCost=500},
	{ template="devil_horns_helm", pointCost=1000},
	{ template="halo_helm", pointCost=2500},
	{ template="starry_mage_helm", pointCost=5000},
}

function GetLifetimeLoyaltyPointCount()
	local currentPointsProp = this:GetAccountProp("LoyaltyRewards-LifetimePoints") 

	if(currentPointsProp ~= nil) then
		return tonumber(currentPointsProp)
	end

	return 0
end

function GetSpentPointsCount()
	local spentPointsProp = this:GetAccountProp("LoyaltyRewards-PointsSpent")

	if(spentPointsProp ~= nil) then
		return tonumber(spentPointsProp)
	end

	return 0
end

function GetCurrentLoyaltyPointCount()	
	return math.max(0,(GetLifetimeLoyaltyPointCount() - GetSpentPointsCount()))
end

function SpendLoyaltyPoints(amount)
	local spentPoints = GetSpentPointsCount()

	if not(this:SetAccountProp("LoyaltyRewards-PointsSpent",tostring(spentPoints + amount))) then
		return false
	end

	this:SystemMessage("You have spent "..amount.." loyalty reward points. You have ".. GetCurrentLoyaltyPointCount() .. " remaining.")

	return true
end

function ShowLoyaltyRewardInfo()
	if not(IsOfficialShard()) then
		return
	end

	-- DAB LOYALTY DISABLED
	do return end

	local loyaltyPoints = GetLifetimeLoyaltyPointCount()
	if(loyaltyPoints > 0) then
		this:SystemMessage("You have earned a total of "..loyaltyPoints.."[$1706]"..GetCurrentLoyaltyPointCount().." remaining to spend on unique items. Use the /loyalty command to redeem them.")
	else
		this:SystemMessage("[$1707]")
	end
end

function GrantAward(awardType,awardData)
	if not(IsOfficialShard()) then
		return
	end	
	if not(CanSetAccountProp()) then
		DebugMessage("ERROR: Attempted to grant loyalty points but login server was unavailable. UserId: "..GetAttachedUserId()..", Award: "..awardType..", Info: "..DumpTable(awardData))
	end

	if(awardData.OneTime) then
		local playerAwardStr = this:GetAccountProp("LoyaltyRewards-Earned") or ""
		local playerAwards = StringSplit(playerAwardStr,",")
		if(IsInTableArray(playerAwards,awardType)) then
			return
		end

		if(#playerAwards > 0) then
			playerAwardStr = playerAwardStr .. ","
		end
		playerAwardStr = playerAwardStr .. awardType

		this:SetAccountProp("LoyaltyRewards-Earned",playerAwardStr)
	end

	if(awardData.AwardValue == nil or awardData.AwardValue <= 0) then
		return
	end
	
	
	currentPoints = GetLifetimeLoyaltyPointCount() + awardData.AwardValue
	this:SetAccountProp("LoyaltyRewards-LifetimePoints",tostring(currentPoints))
	this:SystemMessage("You have been awarded "..awardData.AwardValue.." account loyalty points for "..awardData.DisplayStr,"event")
end

RegisterEventHandler(EventType.Message,"AwardLoyaltyPoints",
	function(awardType,amount)
		local awardData = AwardTypes[awardType]
		if(awardData == nil) then
			awardData = { AwardValue = amount, OneTime = false, DisplayStr = awardType }
		end

		if(awardData.amount ~= nil) then
			GrantAward(awardType,awardData)
		end
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ()
		for awardType,awardData in pairs(FocusHours) do
			--if(DateTime.Now >= DateTime.Parse(awardData.Start) and DateTime.Now < DateTime.Parse(awardData.End)) then								
			--	CallFunctionDelayed(TimeSpan.FromSeconds(5),
			--		function()
			--			GrantAward(awardType,awardData)
			--		end)
			--end
		end
	end)

-- REWARD WINDOW

loyaltyRewardWindowOpen = false
selectedReward = nil

function ShowRewardWindow()
	do return end

	--if not(IsOfficialShard()) then
	--	this:SystemMessage("[$1708]")
	--	return
	--end

	--if not(CanSetAccountProp()) then
	--	this:SystemMessage("[$1709]")
	--	if(loyaltyRewardWindowOpen) then
	--		this:CloseDynamicWindow("LoyaltyWindow")
	--		loyaltyRewardWindowOpen = false
	--	end
	--	return
	--end

	local dynWindow = DynamicWindow("LoyaltyWindow","Loyalty Rewards",280,320,-150,-250,"","Center")
	local loyaltyPoints = GetCurrentLoyaltyPointCount()

	dynWindow:AddLabel(130,10,"Select Loyalty Reward",280,0,18,"center")
	dynWindow:AddLabel(130,30,"Remaining Points: "..loyaltyPoints,280,0,18,"center")

	for i,rewardEntry in pairs(Rewards) do
		local buttonState = ""
		if(rewardEntry.pointCost > loyaltyPoints) then
			buttonState = "disabled"
		elseif(selectedReward ~= nil and selectedReward.template == rewardEntry.template) then
			buttonState = "pressed"
		end
		local objectName = GetTemplateObjectName(rewardEntry.template)
		dynWindow:AddButton(30, 35 + i*26, "Reward|"..i, StripColorFromString(objectName).." ("..rewardEntry.pointCost..")", 200, 26,"","",false,"List",buttonState)
	end
	
	local redeemState = (selectedReward == nil and "disabled") or ""	
	dynWindow:AddButton(20, 230, "DoReward", "Redeem", 220, 0,"","",true,"",redeemState)

	this:OpenDynamicWindow(dynWindow)

	loyaltyRewardWindowOpen = true
end

function ToggleLoyaltyWindow()
	if(loyaltyRewardWindowOpen) then
		this:CloseDynamicWindow("LoyaltyWindow")
		loyaltyRewardWindowOpen = false
	else
		ShowRewardWindow()
	end
end

RegisterEventHandler(EventType.DynamicWindowResponse,"LoyaltyWindow",
	function (user,buttonId)
		if(buttonId == "DoReward") then		
			if not(CanSetAccountProp()) then
				this:SystemMessage("[$1710]")
				return
			end
	
			if(selectedReward ~= nil) then				
				local loyaltyPoints = GetCurrentLoyaltyPointCount()
				if(selectedReward.pointCost <= loyaltyPoints) then
					CreateObjInBackpack(this,selectedReward.template,"loyalty_reward_created",selectedReward.pointCost)
				end
			end
			loyaltyRewardWindowOpen = false
		elseif(buttonId:match("Reward")) then
			local rewardIndex = buttonId:sub(8)
			selectedReward = Rewards[tonumber(rewardIndex)]
			ShowRewardWindow(user)
		else
			loyaltyRewardWindowOpen = false
		end		
	end)

RegisterEventHandler(EventType.CreatedObject,"loyalty_reward_created",
	function(success,objRef,pointCost)
		if(success) then
			if(SpendLoyaltyPoints(pointCost)) then
				selectedReward = nil
				this:SystemMessage("You have received a "..objRef:GetName(),"event")			
				objRef:SetObjVar("LoyaltyReward",true)
				objRef:SetObjVar("LoyaltyRedeemer",this)
			else
				objRef:Destroy()
				this:SystemMessage("[$1711]")
				return
			end
		end
	end)