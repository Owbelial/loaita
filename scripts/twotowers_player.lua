function GetController()
	return FindObjectWithTag("MapController")
end

function CleanUp()
	DebugMessage("twotowers_player CleanUp")
	this:DelObjVar("OverrideDeath")	
	this:DelObjVar("OverrideQuestWindow")
	--this:SetObjVar("IsDead", false)		
	this:SendMessage("RemoveInvisEffect","twotowers_spectator")

	if(this:HasModule("twotowers_player")) then
		this:DelModule("twotowers_player")
		this:DelObjVar("OverrideDeath")	
		this:DelObjVar("OverrideQuestWindow")
		this:SetObjVar("Karma",this:GetObjVar("LastKarma"))	
		this:DelObjVar("LastKarma")
		this:DelObjVar("TwoTowersKills")
		this:DelObjVar("TwoTowersDeaths")
				--this:SetObjVar("IsDead", false)		
		this:SendMessage("RemoveInvisEffect","twotowers_spectator")
		end
end

function OnLoad()	
	DebugMessage("twotowers_player OnLoad")
	this:SetObjVar("OverrideDeath",true)
	this:SetObjVar("OverrideQuestWindow",true)
	if(not(this:HasModule("twotowers_player"))) then
		this:AddModule("twotowers_player")
		this:SetObjVar("OverrideDeath",true)
		this:SetObjVar("OverrideQuestWindow",true)
		this:SetObjVar("LastKarma",this:GetObjVar("Karma"))	
		this:SetObjVar("TwoTowersKills",0)
		this:SetObjVar("TwoTowersDeaths",0)
	end		
	--this:SendMessage("AddInvisEffect","twotowers_spectator")
end

RegisterEventHandler(EventType.Message,"twotowers_spectator",
	function ( ... )
		DebugMessage("twotowers_player twotowers_spectator")
		--this:SetObjVar("IsDead", true)		
		this:SendMessage("AddInvisEffect","twotowers_spectator")

	end)

RegisterEventHandler(EventType.Message,"twotowers_player",
	function ( ... )
	DebugMessage("twotowers_player twotowers_player")
	SetCurHealth(this,GetMaxHealth(this))
	SetCurMana(this,GetMaxMana(this))
	SetCurStamina(this,GetMaxStamina(this))
		--this:DelObjVar("IsDead")
			local bindRegionAddress = "TwoTowers"
            local popuprandom = math.random(1,10)
			local bindLoc = nil

			if popuprandom == 1 then
			bindLoc = Loc(-59.76, 0.00, -11.65)
			elseif popuprandom == 2 then
			bindLoc = Loc(-52.09, 0.00, -1.93)
			elseif popuprandom == 3 then
			bindLoc = Loc(-39.97, 0.00, 4.93)
			elseif popuprandom == 4 then
			bindLoc = Loc(14.00, 0.00, 14.72)
			elseif popuprandom == 5 then
			bindLoc = Loc(39.57, 0.00, -29.14)
			elseif popuprandom == 6 then
			bindLoc = Loc(30.06, 0.00, -89.44)
			elseif popuprandom == 7 then
			bindLoc = Loc(-33.53, 0.00, -99.19)
			elseif popuprandom == 8 then
			bindLoc = Loc(-76.23, 0.00, -76.56)
			elseif popuprandom == 9 then
			bindLoc = Loc(-38.39, 0.00, -82.20)
			elseif popuprandom == 10 then
			bindLoc = Loc(21.95, 0.00, -30.16)
			end

	TeleportUser(this, this, bindLoc, bindRegionAddress)

	this:SendMessage("RemoveInvisEffect","twotowers_spectator")
	end)

RegisterEventHandler(EventType.Message,"HasDiedMessage",
	function (killer)
		GetController():SendMessage("PlayerKilled",this,killer)
	end)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function ( ... )
		DebugMessage("twotowers_player ModuleAttached")

		--if(IsDead(this)) then
		--	this:SendMessage("Resurrect",100,this,true)
		--end

		OnLoad()
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		DebugMessage("twotowers_player LoadedFromBackup")

		if(GetWorldName() ~= "TwoTowers") then
			CleanUp()
		else 
			OnLoad()
		end
	end)

RegisterEventHandler(EventType.ClientUserCommand,"dfgdfgdfg",
	function()
		GetController():SendMessage("ToggleScoreWindow",this)
	end)

if(IsImmortal(this)) then
	RegisterEventHandler(EventType.ClientUserCommand,"start",
		function(gameType)
			GetController():SendMessage("StartMatch",gameType)
		end)
end


RegisterEventHandler(EventType.ClientUserCommand,"exitarena",
	function()

			bindLoc = GetPlayerSpawnPosition(this)
			local bindRegionAddress = GetRegionAddress()
			local spawnPosEntry = this:GetObjVar("SpawnPosition")
			if GetRegionAddress() ~= bindRegionAddress then 
			print(GetRegionAddress())
			return 
			end

			-- send them home
				if(this:HasModule("twotowers_player")) then
				this:DelModule("twotowers_player")
				this:DelObjVar("OverrideDeath")	
				this:DelObjVar("OverrideQuestWindow")
				this:SetObjVar("Karma",this:GetObjVar("LastKarma"))	
				this:DelObjVar("LastKarma")
				this:DelObjVar("TwoTowersKills")
				this:DelObjVar("TwoTowersDeaths")
				--this:DelObjVar("Invulnerable")
				--this:SetObjVar("IsDead", false)		
				this:SendMessage("RemoveInvisEffect","twotowers_spectator")
				end
			TeleportUser(this, this, bindLoc, bindRegionAddress, this:GetFacing())
	end)


RegisterEventHandler(EventType.ClientUserCommand,"enterarena",
	function()
			
			if ( GetRemainingActivePetSlots(this) < 5 ) then
			this:SystemMessage("U have to dismiss your pets.", "info")
			return
			end
			--bindLoc = GetPlayerSpawnPosition(this)
			local bindRegionAddress = "TwoTowers"
            local popuprandom = math.random(1,10)
			local bindLoc = nil

			if popuprandom == 1 then
			bindLoc = Loc(-59.76, 0.00, -11.65)
			elseif popuprandom == 2 then
			bindLoc = Loc(-52.09, 0.00, -1.93)
			elseif popuprandom == 3 then
			bindLoc = Loc(-39.97, 0.00, 4.93)
			elseif popuprandom == 4 then
			bindLoc = Loc(14.00, 0.00, 14.72)
			elseif popuprandom == 5 then
			bindLoc = Loc(39.57, 0.00, -29.14)
			elseif popuprandom == 6 then
			bindLoc = Loc(30.06, 0.00, -89.44)
			elseif popuprandom == 7 then
			bindLoc = Loc(-33.53, 0.00, -99.19)
			elseif popuprandom == 8 then
			bindLoc = Loc(-76.23, 0.00, -76.56)
			elseif popuprandom == 9 then
			bindLoc = Loc(-38.39, 0.00, -82.20)
			elseif popuprandom == 10 then
			bindLoc = Loc(21.95, 0.00, -30.16)
			end

			if GetRegionAddress() == bindRegionAddress then 
			--print(bindLoc)
			return 
			end


			--if ( spawnPosEntry ~= nil ) then
			--	bindLoc = spawnPosEntry.Loc
			--	bindRegionAddress = spawnPosEntry.Region
			--end
			-- send them home
			if(not(this:HasModule("twotowers_player"))) then
				this:AddModule("twotowers_player")
					this:SetObjVar("OverrideDeath",true)
					this:SetObjVar("OverrideQuestWindow",true)
					this:SetObjVar("LastKarma",this:GetObjVar("Karma"))	
					this:SetObjVar("TwoTowersKills",0)
					this:SetObjVar("TwoTowersDeaths",0)
					--this:SetObjVar("Invulnerable",true)
					this:SetObjVar("LastKarma",this:GetObjVar("Karma"))	
			end
			TeleportUser(this, this, bindLoc, bindRegionAddress)
	end)

