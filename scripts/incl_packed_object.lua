require 'incl_container'

function PackObject(user,objRef)
	if(objRef == nil or not(objRef:IsValid())) then return end	

	local templateId = objRef:GetCreationTemplateId()

	-- temporary to allow support for existing data (can be removed post-wipe)
	if ( templateId == "treasurechest_keyed" ) then templateId = "chest_secure" end

	CreatePackedObjectInBackpack(user,templateId)
end

function RegisterCreateHandler(handle)
	RegisterSingleEventHandler(EventType.CreatedObject,handle,
		function(success,objRef,packed_template)
			--DebugMessage("CreatePackedObject "..tostring(templateId).." "..tostring(packed_template).." "..tostring(success))
			if(success) then
				objRef:SetObjVar("UnpackedTemplate",packed_template)

				local templateData = GetTemplateData(packed_template)
				if(templateData ~= nil) then
					objRef:SetName(StripColorFromString(templateData.Name).." (Packed)")

					if(templateData.LuaModules ~= nil and templateData.LuaModules.create_key ~= nil) then
						objRef:SetObjVar("ShouldCreateKey",true)
					end
				end
			end
		end)
end

function CreatePackedObjectInCarrySlot(user,templateId,handle)
	if (handle == nil) then handle = "packed_object_created" end
	local carriedObject = user:CarriedObject()
	if (carriedObject ~= nil) then
		this:SystemMessage("You are already carrying something.")
		return
	end
	RegisterCreateHandler(handle)
	CreateObjInCarrySlot(user,"packed_object",handle,templateId)
end

function CreatePackedObjectInBackpack(user,templateId,handle)
	if (handle == nil) then handle = "packed_object_created" end
	
	RegisterCreateHandler(handle)
	CreateObjInBackpackOrAtLocation(user, "packed_object", handle, templateId)	
end

function CreatePackedObjectAtLoc(templateId,spawnLoc,handle)
	if (handle == nil) then handle = "packed_object_created" end

	RegisterCreateHandler(handle)
	CreateObj("packed_object",spawnLoc,handle,templateId)
end