-- this script will apply random bonuses to a piece of equipment


--BBTODO
-- Add Random Spell Bonuses
mBonusWeight = 0
mQuality = 10 
mOverrideBonusLocation = false
mBonusDictName = nil
mBonusDict = {}
mIgnoreWeight = false
--D*ebugMessage("LOADIN")
function ApplyPresetBonuses(bonuses)
	mIgnoreWeight = true
		for bonusId, bonusData in pairs(bonuses) do		
		bonusValue = GetBonusValue(bonusData)		

		if( bonusValue ~= nil ) then
			--D*ebugMessage("Adding preset bonus: ".. bonusId .. " value: ".. bonusValue)
			if(mOverrideBonusLocation) then
				mBonusDict[bonusId] = bonusValue
			else
				this:SetObjVar(bonusId, bonusValue)
			end	
		end
	end
	--D*ebugMessage(mBonusDictName)
	--D*ebugDict(mBonusDict)
	if(mOverrideBonusLocation) then this:SetObjVar(mBonusDictName, mBonusDict) end
	mIgnoreWeight = false
end

function ApplyPresetSpellBonuses(bonuses)
	mIgnoreWeight = true
		local spellBonusDict = {}
		local bonusAdd = false
		for spellId, spBonusData in pairs(bonuses) do
			spellTab = {}
			spellAdd = false
			for bonusId, bonusData in pairs(spBonusData) do		
				bonusValue = GetBonusValue(bonusData)		

				if( bonusValue ~= nil ) then
					--D*ebugMessage("Adding preset bonus: ".. bonusId .. " value: ".. bonusValue)
					spellTab[bonusId] = bonusValue
						bonusAdd = true
						spellAdd = true
				end
			end
			if(spellAdd) then
				spellBonusDict[spellId]= spellTab
			end
			spellAdd = false

		end
	--D*ebugMessage(mBonusDictName)
	--D*ebugDict(mBonusDict)
	if(bonusAdd) then this:SetObjVar("SpellBonusesDict", spellBonusDict) end
	mIgnoreWeight = false
end

function ApplyRandomBonuses(availableBonuses, numBonuses)
	bonusKeys = getTableKeys(availableBonuses)
	if (bonusKeys == nil or #bonusKeys == 0) then return end
	for i=1,numBonuses do
		if(#bonusKeys > 0) then
			local bonusIndex = math.random(#bonusKeys)
			local bonusId = bonusKeys[bonusIndex]
			local bonusValue = GetBonusValue(availableBonuses[bonusId])
			if( bonusValue ~= nil ) then
					--D*ebugMessage("Adding random bonus: ".. bonusId .. " value: ".. bonusValue)
				if(mOverrideBonusLocation) then
					mBonusDict[bonusId] = bonusValue
				else
					this:SetObjVar(bonusId, bonusValue)
				end	
				table.remove(bonusKeys, bonusIndex)
			end
		end
	end
	--D*ebugMessage(mBonusDictName)
	--D*ebugDict(mBonusDict)
	if(mOverrideBonusLocation) then this:SetObjVar(mBonusDictName, mBonusDict) end
	--D*ebugMessage("WEIGHT: " .. tostring(mBonusWeight))
end

function ApplyRandomSpellBonuses(availableBonuses, numBonuses)
	bonusKeys = getTableKeys(availableBonuses)
	if (bonusKeys == nil or #bonusKeys == 0) then return end
	for i=1,numBonuses do
		local bonusIndex = math.random(#bonusKeys)
		local bonusId = bonusKeys[bonusIndex]
		local bonusValue = GetBonusValue(availableBonuses[bonusId])
		if( bonusValue ~= nil ) then
				--D*ebugMessage("Adding random bonus: ".. bonusId .. " value: ".. bonusValue)
			if(mOverrideBonusLocation) then
				mBonusDict[bonusId] = bonusValue
			else
				this:SetObjVar(bonusId, bonusValue)
			end	
			table.remove(bonusKeys, bonusIndex)
		end
	end
	--D*ebugMessage(mBonusDictName)
	--D*ebugDict(mBonusDict)
	if(mOverrideBonusLocation) then this:SetObjVar(mBonusDictName, mBonusDict) end
	--D*ebugMessage("WEIGHT: " .. tostring(mBonusWeight))
end

function HandleLoaded()	
	-- check for preset bonuses
	--D*ebugMessage("PRESETS")
	if(initializer.Quality ~= nil) then
			mQuality = initializer.Quality or 10
		end
	
	if(initializer.BonusDict ~= nil) then
		mOverrideBonusLocation = true
		mBonusDictName = initializer.BonusDict
	end
	if( initializer.Bonuses ~= nil ) then
		--D*ebugMessage("[apply_bonuses] Applying preset bonuses")
		ApplyPresetBonuses(initializer.Bonuses)
	end
	if( initializer.AvailableBonuses ~= nil ) then		
		local numBonuses = initializer.NumBonuses or GetNumBonuses()
		--D*ebugMessage("[apply_bonuses] Applying random bonuses "..tostring(numBonuses))
		ApplyRandomBonuses(initializer.AvailableBonuses, numBonuses)
	end
	if ( initializer.SetSpellBonuses ~= nil) then
		ApplyPresetSpellBonuses(initializer.SetSpellBonuses)
	end
	if ( initializer.RandomSpellBonuses ~= nil) then
		ApplyRandomSpellBonuses(initializer.RandomSpellBonuses)
	end
	ColorItemName(this)
	this:SendMessage("UpdateTooltip")		
	this:DelModule("apply_bonuses")
end

function GetNumBonuses()
	local numBonusesFactor = math.random()
	if(numBonusesFactor > 0.9 ) then
		return 3
	elseif( numBonusesFactor > 0.6 ) then
		return 2
	else
		return 1
	end
end

function GetBonusValue(bonusData)	
	if( bonusData.value ~= nil ) then
		return bonusData.value
	elseif( bonusData.min ~= nil and bonusData.max ~= nil ) then
		local bonusFactor = 1
		if( bonusData.precision ~= nil ) then
			bonusFactor = math.pow(10,bonusData.precision)
		end
		--D*ebugTable(bonusData)
		local bonusMin = bonusData.min * bonusFactor
		local bonusMax = bonusData.max * bonusFactor
		local bonusRange = bonusMax - bonusMin
		local roll = math.random()
		local bonusValue =  math.floor((roll * bonusRange) + bonusMin) / bonusFactor
		local bonusWeight = math.floor(roll * 100) / 100
		local maxBonusWeight = mQuality
		if(mQuality < 0) then
			maxBonusWeight = math.sqrt(mQuality * mQuality)
		end
		if(bonusValue == 0) then return nil end
		if((bonusWeight + mBonusWeight) > maxBonusWeight) and not(mIgnoreWeight) then
			return nil
		end
		mBonusWeight = mBonusWeight + bonusWeight
		return bonusValue
	end
	return nil
end

RegisterSingleEventHandler(EventType.ModuleAttached, "apply_bonuses", HandleLoaded)