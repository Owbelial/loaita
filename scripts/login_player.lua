require 'incl_starting_professions'
require 'base_mobile_advanced'

require 'incl_player_names'

--module that attaches that opens the character customization window.
mInitializer = initializer

SelectedBodyType = 1
SelectedHairType = 1
SelectedHairColorType = 1
GenderBodyTable = CharacterCustomization.BodyTypesMale
GenderHairTable = CharacterCustomization.HairTypesMale
FullBody = false
UniverseCount = 0
ValidUniverses = nil

-- tells us if this user is here for character creation (false if just selecting starting city)
function IsCreate()
	return not(this:HasObjVar("playerInitialized"))
end

function GoState(stateName)
	--DebugMessage("GoState: "..stateName)
	this:SetObjVar("CharCreateState",stateName)
	if(stateName == "Appearance") then	
		curGender = "Male"
		if(IsFemale(this)) then
			curGender = "Female"
		end	
		OpenApppearanceWindow()
	elseif(stateName == "Gear") then
		curGear = nil
		OpenStartingGearWindow()
	elseif(stateName == "Universe") then
		if not(ServerSettings.CharacterCreation.ShouldSelectUniverse) then
			OpenSelectCityWindow()
		else
			ValidUniverses = GetUniversesWithMap(ServerSettings.CharacterCreation.StartingMap)
			if(#ValidUniverses == 1) then				
				this:SetObjVar("CharCreateUniverse",ValidUniverses[1])
				GoState("City")
			elseif(#ValidUniverses > 1) then
				curUniverse = nil
				OpenSelectUniverseWindow()
			else
				ClientDialog.Show{
				    TargetUser = this,
				    DialogId = "Error",
				    TitleStr = "Error",
				    Button1Str = "Ok",
				    DescStr = "No available starting location. Please try again later.",
				    ResponseFunc = function (user, buttonId)
				    	this:KickUser("Starting location not found",false)
					end
				}
			end
		end
	elseif(stateName == "City") then
		OpenSelectCityWindow()
	end
end

function GoBack()
	local curState = this:GetObjVar("CharCreateState")
	-- if not in character creation just log out
	if not(curState) then
		this:KickUser("Logged Out",false)
	elseif(curState == "Appearance") then
		this:KickUser("Logged Out",false)
	elseif(curState == "Gear") then
		-- clear out gear
		ChangeToTemplate("playertemplate_blank",{KeepAppearance = true, LoadAbilities = false, SetStats = false, SetName = false, BuildHotbar = false, LoadLoot=false})
		
		GoState("Appearance")
	elseif(curState == "Universe") then		
		if(IsCreate()) then
			GoState("Gear")
		end
	elseif(curState == "City") then
		this:CloseDynamicWindow("CityWindow")
		if(ValidUniverses and #ValidUniverses > 1) then
			GoState("Universe")
		else
			GoState("Gear")
		end
	end
end

function EnterWorld(curCity)
	local universeName = this:GetObjVar("CharCreateUniverse")
	local destRegionAddress = ServerSettings.CharacterCreation.StartingMap
	if(universeName) then
		destRegionAddress = universeName .. "." .. destRegionAddress
	end

	local cityInfo = ServerSettings.CharacterCreation.StartingLocations[curCity]
	if(cityInfo.Subregion) then
		destRegionAddress = destRegionAddress .. "." ..cityInfo.Subregion
	end

	local curGear = this:GetObjVar("CharCreateGear")

	if( not(IsClusterRegionOnline(destRegionAddress)) ) then 
		ClientDialog.Show{
				    TargetUser = this,
				    DialogId = "Error",
				    TitleStr = "Error",
				    Button1Str = "Ok",
				    DescStr = "There was an error during character creation. Try again later. If this issue persists, contact an admin.",
				    ResponseFunc = function (user, buttonId)
				    	this:KickUser("Login region not found",false)
					end
				}
	end		
	
	if(curGear) then
		local gearInfo = CharacterCustomization.StartingGear[curGear]
		this:SetObjVar("StartingTemplate",gearInfo.Template)
	end

	this:DelObjVar("CharCreateState")
	this:DelObjVar("CharCreateGear")
	this:DelObjVar("CharCreateUniverse")

	TeleportUser(this,this,MapLocations[ServerSettings.CharacterCreation.StartingMap][cityInfo.MapLocation],destRegionAddress)
end

function UpdateBody()
	local currentEquipped = this:GetEquippedObject("BodyPartHead")
	--DebugMessage("currentEquipped is "..tostring(currentEquipped))
	if (currentEquipped ~= nil) then
		currentEquipped:Destroy()
	end
	--create the new body type
	if (GenderBodyTable[SelectedBodyType].Template ~= nil) then
		CreateEquippedObj(GenderBodyTable[SelectedBodyType].Template, this, "created_body_type")--set the equipped object to the template in the index
	end
end

function UpdateHair()
	local currentEquipped = this:GetEquippedObject("BodyPartHair")
	if (currentEquipped ~= nil) then
		currentEquipped:Destroy()
	end
	--create the new hair type
	if (GenderHairTable[SelectedHairType].Template ~= nil) then
		CreateEquippedObj(GenderHairTable[SelectedHairType].Template, this, "created_hair")--set the equipped object to the template in the index
	end
end

function ChangeGender(newGender)
	SelectedBodyType = 1
	SelectedHairType = 1
	SelectedHairColorType = 1

	if(newGender == "Male") then
		this:SetAppearanceFromTemplate("playertemplate_male")
		GenderBodyTable = CharacterCustomization.BodyTypesMale
		GenderHairTable = CharacterCustomization.HairTypesMale
	else
		this:SetAppearanceFromTemplate("playertemplate_female")
		GenderBodyTable = CharacterCustomization.BodyTypesFemale
		GenderHairTable = CharacterCustomization.HairTypesFemale
	end

	-- reset body and hair
	UpdateBody()
	UpdateHair()
end

curGender = "Male"
function OpenApppearanceWindow()	
	if (GenderHairTable == nil or GenderBodyTable == nil) then
		DebugMessage("[custom_char_window|charCustomWindow] ERROR: Player has unsupported gender/body type.")
		return
	end

	local dynWindow = DynamicWindow("CharCreateWindow","",320,300,200,-150,"Transparent","Left")

	dynWindow:AddLabel(155,10,"Change Appearance",323,0,26,"center")

	local maleState = ""
	local femaleState = ""
	if(curGender == "Male") then
		maleState = "pressed"
	else
		femaleState = "pressed"
	end

	dynWindow:AddButton(60, 40, "Gender|Male", "", 73, 85, "Male", "", false,"MaleButton",maleState)
	dynWindow:AddButton(170, 40, "Gender|Female", "", 73, 85, "Female", "", false,"FemaleButton",femaleState)
	--body type tab
	dynWindow:AddImage(25,126+6,"DropHeaderBackground",250,55,"Sliced")
	dynWindow:AddLabel(155,130+9,"[F3F781]Body[-]",323,0,23,"center")
	dynWindow:AddButton(35,155-2,"BodyLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,155+9,GenderBodyTable[SelectedBodyType].Name,323,0,18,"center")
	dynWindow:AddButton(255,155-2,"BodyRight","",0,0,"","",false,"Next")
	--hair tab
	dynWindow:AddImage(25,181+6,"DropHeaderBackground",250,55,"Sliced")
	dynWindow:AddLabel(155,185+9,"[F3F781]Hair[-]",323,0,23,"center")
	dynWindow:AddButton(35,210-2,"HairLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,210+9,GenderHairTable[SelectedHairType].Name,323,0,18,"center")
	dynWindow:AddButton(255,210-2,"HairRight","",0,0,"","",false,"Next")
	--hair color
	dynWindow:AddImage(25,236+6,"DropHeaderBackground",250,55,"Sliced")
	dynWindow:AddLabel(155,240+9,"[F3F781]Hair Color[-]",323,0,23,"center")
	dynWindow:AddButton(35,265-2,"HairColorLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,265+9,CharacterCustomization.HairColorTypes[SelectedHairColorType].Name,323,0,18,"center")
	dynWindow:AddButton(255,265-2,"HairColorRight","",0,0,"","",false,"Next")

	dynWindow:AddButton(88, 410, "ChangeAppearance", "Next", 122, 32, "", "", true,"")
	dynWindow:AddButton(88, 452, "Back", "Back", 122, 32, "", "", true,"")
			
	this:OpenDynamicWindow(dynWindow)
end

curGear = nil
function OpenStartingGearWindow()
	local dynWindow = DynamicWindow("CharCreateWindow","",320,300,200,-150,"Transparent","Left")

	dynWindow:AddLabel(155,10,"Change Starting Gear",323,0,26,"center")

	local curY = 52
	for i,gearInfo in pairs(CharacterCustomization.StartingGear) do
		local state = ""
		if(curGear == i) then
			state = "pressed"
		end
		dynWindow:AddButton(25,curY,"Gear|"..tostring(i),gearInfo.Name,250,22,"","",false,"List",state)
		curY = curY + 23
	end

	local nextState = ""
	if not(curGear) then
		nextState = "disabled"
	end

	dynWindow:AddButton(88, 410, "ChangeGear", "Next", 122, 32, "", "", true,"",nextState)
	dynWindow:AddButton(88, 452, "Back", "Back", 122, 32, "", "", true,"")

	this:OpenDynamicWindow(dynWindow)
end

curUniverse = nil
function OpenSelectUniverseWindow()	
	local dynWindow = DynamicWindow("CharCreateWindow","",320,300,200,-150,"Transparent","Left")

	dynWindow:AddLabel(155,10,"Choose Universe",323,0,26,"center")

	local curY = 52
	for i,universeName in pairs(ValidUniverses) do
		local state = ""
		if(curUniverse == universeName) then
			state = "pressed"
		end
		dynWindow:AddButton(25,curY,"Universe|"..universeName,universeName,250,22,"","",false,"List",state)
		curY = curY + 23
	end

	local nextState = ""
	if not(curUniverse) then
		nextState = "disabled"
	end

	local backState = ""
	if not(IsCreate()) then
		backState = "disabled"
	end

	dynWindow:AddButton(88, 410, "ChangeUniverse", "Next", 122, 32, "", "", true,"",nextState)
	dynWindow:AddButton(88, 452, "Back", "Back", 122, 32, "", "", true,"",backState)

	this:OpenDynamicWindow(dynWindow)
end

curCity = nil
function OpenSelectCityWindow()
	local dynWindow = DynamicWindow("CharCreateWindow","",320,300,200,-150,"Transparent","Left")

	dynWindow:AddLabel(155,10,"Choose Starting Town",323,0,26,"center")
	
	local curY = 52
	for i,cityInfo in pairs(ServerSettings.CharacterCreation.StartingLocations) do
		local state = ""
		if(curCity == i) then
			state = "pressed"
		end
		dynWindow:AddButton(25,curY,"City|"..tostring(i),cityInfo.Name,250,22,cityInfo.Description,"",false,"List",state)
		curY = curY + 23
	end

	local nextState = ""
	if not(curCity) then
		nextState = "disabled"
	end

	local backState = ""
	if not(IsCreate()) then
		backState = "disabled"
	end

	dynWindow:AddButton(88, 410, "ChangeCity", "Enter World", 122, 32, "", "", true,"",nextState)
	dynWindow:AddButton(88, 452, "Back", "Back", 122, 32, "", "", true,"",backState)

	this:OpenDynamicWindow(dynWindow)
	
	local dynWindow = DynamicWindow("CityWindow","",1024,1024,-1224,-512,"Transparent","Right")

	local mapName = ServerSettings.CharacterCreation.StartingMap
	local mapIcons = {}

	for i,cityInfo in pairs(ServerSettings.CharacterCreation.StartingLocations) do
		local icon = "location_town"
		if(curCity == i) then
			icon = "location_town_selected"
		end
		
		table.insert(mapIcons,
			{Icon=icon, Id=cityInfo.Name, Location=MapLocations[mapName][cityInfo.MapLocation], Tooltip=cityInfo.Name.."\n\n"..cityInfo.Description, Width=94, Height=94})
	end

	dynWindow:AddMap(0,0,1024,1024,mapName,false,mapIcons)
	dynWindow:AddButton(451, 980, "ChangeCity", "Enter World", 122, 32, "", "", true,"",nextState)

	this:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "CharCreateWindow",
	function (user,buttonId)

		--DebugMessage("SelectedBodyType is "..tostring(SelectedBodyType))
		--DebugMessage("SelectedHairType is "..tostring(SelectedHairType))
		--DebugMessage("SelectedHairColorType is "..tostring(SelectedHairColorType))		

		if (GenderHairTable == nil or GenderBodyTable == nil) then
			DebugMessage("[custom_char_window|charCustomWindow] ERROR: Player has unsupported gender/body type.")
			return
		end

		if (buttonId == "FullBody") then
			FullBody = not FullBody
			OpenApppearanceWindow()
		end
		if (buttonId == "BodyLeft") then
			SelectedBodyType = SelectedBodyType - 1--deincrement by one
			--change the body type
			if (SelectedBodyType > #GenderBodyTable) then SelectedBodyType = 1 end--if over the length set to 1
			if (SelectedBodyType < 1) then SelectedBodyType = #GenderBodyTable end--if less than 1 then set to length
			UpdateBody()
			--this:PlayEffect("TeleportToEffect")
			--reopen the character window
			OpenApppearanceWindow()
		elseif (buttonId == "BodyRight") then
			SelectedBodyType = SelectedBodyType + 1--increment by one
			--change the body type
			if (SelectedBodyType > #GenderBodyTable) then SelectedBodyType = 1 end--if over the length set to 1
			if (SelectedBodyType < 1) then SelectedBodyType = #GenderBodyTable end--if less than 1 then set to length
			UpdateBody()
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenApppearanceWindow()
		elseif (buttonId == "HairLeft") then
			SelectedHairType = SelectedHairType - 1--deincrement by one
			--change the hair type
			if (SelectedHairType > #GenderHairTable) then SelectedHairType = 1 end--if over the length set to 1
			if (SelectedHairType < 1) then SelectedHairType = #GenderHairTable end--if less than 1 then set to length
			UpdateHair()
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenApppearanceWindow()
		elseif (buttonId == "HairRight") then
			SelectedHairType = SelectedHairType + 1--increment by one
			--change the hair type
			if (SelectedHairType > #GenderHairTable) then SelectedHairType = 1 end--if over the length set to 1
			if (SelectedHairType < 1) then SelectedHairType = #GenderHairTable end--if less than 1 then set to length
			UpdateHair()
			--this:PlayEffect("TeleportToEffect")
			OpenApppearanceWindow()
			--reopen the character window
		elseif (buttonId == "HairColorLeft") then
			SelectedHairColorType = SelectedHairColorType - 1--deincrement by one
			--change the hair color
			if (SelectedHairColorType > #CharacterCustomization.HairColorTypes) then SelectedHairColorType = 1 end--if over the length set to 1
			if (SelectedHairColorType < 1) then SelectedHairColorType = #CharacterCustomization.HairColorTypes end--if less than 1 then set to length

			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if(currentEquipped) then
				currentEquipped:SetHue(CharacterCustomization.HairColorTypes[SelectedHairColorType].Color)

				--reopen the character window
				--this:PlayEffect("TeleportToEffect")
				OpenApppearanceWindow()
			end
		elseif (buttonId == "HairColorRight") then
			SelectedHairColorType = SelectedHairColorType + 1--increment by one
			--change the hair color
			if (SelectedHairColorType > #CharacterCustomization.HairColorTypes) then SelectedHairColorType = 1 end--if over the length set to 1
			if (SelectedHairColorType < 1) then SelectedHairColorType = #CharacterCustomization.HairColorTypes end--if less than 1 then set to length

			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if(currentEquipped) then
				currentEquipped:SetHue(CharacterCustomization.HairColorTypes[SelectedHairColorType].Color)

				--reopen the character window
				--this:PlayEffect("TeleportToEffect")
				OpenApppearanceWindow()
			end
		elseif (buttonId == "ChangeAppearance") then
			GoState("Gear")
		elseif(buttonId == "ChangeGear") then
			if(curGear) then
				this:SetObjVar("CharCreateGear",curGear)
				GoState("Universe")
			end
		elseif(buttonId == "ChangeUniverse") then
			if(curUniverse) then 				
				this:SetObjVar("CharCreateUniverse",curUniverse)
				GoState("City")
			end
		elseif(buttonId == "ChangeCity") then
			if(curCity) then 
				EnterWorld(curCity)
			end
		elseif(buttonId == "Back") then
			GoBack()
		else
			local result = StringSplit(buttonId,"|")
    		local action = result[1]
    		local arg = result[2]
    		if(action == "Gender") then
    			if(curGender ~= arg) then
	    			curGender = arg 
	    			ChangeGender(arg)
    				OpenApppearanceWindow()
    			end
    		elseif(action == "Gear") then
    			local gearIndex = tonumber(arg)
    			if(gearIndex ~= curGear) then
	    			local gearInfo = CharacterCustomization.StartingGear[gearIndex]
    				ChangeToTemplate(gearInfo.Template,{KeepAppearance = true, LoadAbilities = false, SetStats = false, SetName = false, BuildHotbar = false, IgnoreBodyParts=true, LoadLoot=false})
    				curGear = gearIndex
    				OpenStartingGearWindow()
    			end
    		elseif(action == "Universe") then
    			local universeName = arg
    			if(universeName ~= nil) then
    				curUniverse = universeName
    				OpenSelectUniverseWindow()
				end
    		elseif(action == "City") then
    			local cityIndex = tonumber(arg)
    			if(cityIndex ~= curCity) then
	    			curCity = cityIndex
    				OpenSelectCityWindow()    				
    			end
    		end
		end
	end)

RegisterEventHandler(EventType.DynamicWindowResponse, "CityWindow",
	function (user,buttonId)
		if(buttonId ~= nil and buttonId ~= "") then
			for i,cityInfo in pairs(ServerSettings.CharacterCreation.StartingLocations) do
				if(cityInfo.Name == buttonId) then
					curCity = i
					OpenSelectCityWindow()
					return
				end
			end
		elseif(buttonId == "ChangeCity") then
			if(curCity) then 
				EnterWorld(curCity)
			end
		end
	end)

RegisterEventHandler(EventType.CreatedObject,"created_hair",
	function(success,objRef)
		objRef:SetHue(CharacterCustomization.HairColorTypes[SelectedHairColorType].Color)--set the equipped object to the template in the index
	end)

function LoginInit()
	this:SetObjectTag("AttachedUser")
	-- this prevents the server from sending updates from other players in the login region
	this:SetUpdateRange(0)
end

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function ( ... )
		LoginInit()

		GoState("Appearance")
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		LoginInit()
				
		if not(IsCreate()) then
			-- region is not running give them a choice of new starting region
			ClientDialog.Show{
				    TargetUser = this,
				    DialogId = "Error",
				    TitleStr = "Error",
				    Button1Str = "Ok",
				    DescStr = "The region your character logged out in is not online. Please select a starting town.",
				    ResponseFunc = function (user, buttonId)				    	
					end
				}
			GoState("Universe")
		else
			local createState = this:GetObjVar("CharCreateState")
			--DebugMessage("CREATE STATE: "..tostring(createState))
			if not(createState) then
				DebugMessage("ERROR: Player entered world without createState.")
				ChangeToTemplate("playertemplate_blank",{KeepAppearance = true, LoadAbilities = false, SetStats = false, SetName = false, BuildHotbar = false, LoadLoot=false})
				GoState("Appearance")
			elseif(createState == "Appearance") then
				GoState("Appearance")
			elseif(createState == "Gear") then
				GoState("Gear")
			elseif(createState == "Universe" or createState == "City") then
				GoState("Universe")
			end
		end
	end)

RegisterEventHandler(EventType.UserLogout,"", 
	function ( ... )
		this:CompleteLogout()
	end)