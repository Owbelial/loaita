-- These stats are reset and recalculated on login/logout 
-- start with all stats dirty
statsToRecalculate = {
	HealthRegen = true, 
	ManaRegen = true, 
	StaminaRegen = true, 	
	Strength = true,
	Agility = true,
	Intelligence = true,
	Constitution = true,
	Wisdom = true,
	Will = true,
	Accuracy = true,
	Evasion = true,
	Attack = true,
	Power = true,
	Force = true,
	Defense = true,
	AttackSpeed = true,
	CritChance = true,
	CritChanceReduction = true,
	MoveSpeed = true,
	MountMoveSpeed = true,
}

if(IsPlayerCharacter(this)) then
	statsToRecalculate.MaxVitality = true
	statsToRecalculate.VitalityRegen = true
end

function RecalculateHealthRegen()
	-- checks to see if disabled before calculating regen
	if(this:HasObjVar("NoRegen") or IsDead(this)) then
		this:SetObjVar("HealthRegen",0)
		this:SetStatRegenRate("Health",0)
	else
		this:SetStatRegenRate("Health", (ServerSettings.Stats.BaseHealthRegenRate + GetMobileMod(MobileMod.HealthRegenPlus)) *GetMobileMod(MobileMod.HealthRegenTimes, 1) )
	end

	--D*ebugMessage("[base_mobilestats::RecalculateHealthRegen] For "..this:GetName()..": "..healthRegen)
end

function RecalculateManaRegen()

	local manaRegen = this:GetObjVar("ManaRegen")
	if not( manaRegen ) then
		if ( IsPlayerCharacter(this) ) then
			
			local channeling = GetSkillLevel(this, "ChannelingSkill")
			local skillMod = 1
			if ( channeling >= 100 ) then
				skillMod = skillMod + 0.1
			end

			-- get the lowest armor mana regen mod from equipped armor
			local armorModifier = 1 -- start at the max mod possible
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					local classRegen = GetArmorClassManaRegenModifier(item, slot)
					if ( classRegen < armorModifier ) then armorModifier = classRegen end
				end
			end

			--DebugMessage("ArmorManaRegenMod: "..armorModifier)
		
			-- double the modifier for active focusing
			if ( HasMobileEffect(this, "Focus") ) then
				armorModifier = armorModifier * 2
			end
			
			manaRegen = (skillMod * (channeling*4/400+GetInt(this)/400) * armorModifier) -- da *3 a *4

			--[[ TODO: Re-enabled per item mana regen bonus
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					manaRegen = manaRegen + GetArmorTypeManaRegenModifier(item, slot)
				end
			end
			]]
			--DebugMessage("MANA REGEN RATE: "..manaRegen)
		else
			manaRegen = ServerSettings.Stats.DefaultMobManaRegen
		end
	end

	--DebugMessage("manaRegen: "..manaRegen)

	this:SetStatRegenRate("Mana", (ServerSettings.Stats.BaseManaRegenRate + manaRegen + GetMobileMod(MobileMod.ManaRegenPlus)) * GetMobileMod(MobileMod.ManaRegenTimes, 1) )
end

function RecalculateStaminaRegen()

	local staminaRegen = this:GetObjVar("StaminaRegen")
	if not( staminaRegen ) then
		if ( this:IsPlayer() ) then
			staminaRegen = 0
			-- players regenerate stamina dufferently dependant on the armor worn
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					staminaRegen = staminaRegen + GetArmorStaRegenModifier(item, slot)
				end
			end
		else
			staminaRegen = ServerSettings.Stats.DefaultMobStamRegen
		end
	end
	
	this:SetStatRegenRate("Stamina", (ServerSettings.Stats.BaseStaminaRegenRate + staminaRegen + GetMobileMod(MobileMod.StaminaRegenPlus)) * GetMobileMod(MobileMod.StaminaRegenTimes, 1) )
end

function RecalculateVitalityRegen()
	if ( this:HasObjVar("NoRegen") or IsDead(this) ) then
		this:SetObjVar("VitalityRegen",0)
		this:SetStatRegenRate("Vitality",0)
	else
		this:SetStatRegenRate("Vitality", (ServerSettings.Stats.BaseVitalityRegenRate + GetMobileMod(MobileMod.VitalityRegenPlus)) *GetMobileMod(MobileMod.VitalityRegenTimes, 1))
	end
end

function RecalculateMaxHealth()
	local isPlayer = IsPlayerCharacter(this)
	local baseHealth = this:GetObjVar("BaseHealth")
	if(baseHealth == nil) then
		if(isPlayer) then
			baseHealth = ServerSettings.Stats.PlayerBaseHealth
		else
			baseHealth = ServerSettings.Stats.NPCBaseHealth
		end
	end
	
	local conPlus = 0
	if (isPlayer) then
		conPlus = ServerSettings.Stats.ConHpBonusFunc(GetCon(this))
	end

	local maxHealth = (baseHealth + conPlus + GetMobileMod(MobileMod.MaxHealthPlus)) * GetMobileMod(MobileMod.MaxHealthTimes, 1)
	--this:NpcSpeech("MaxHP: "..tostring(maxHealth).." : ("..tostring(baseHealth).."*"..tostring(GetMobileMod(MobileMod.MaxHealthTimes, 1))..")")
	this:SetStatMaxValue("Health", math.floor(maxHealth))
end

function RecalculateMaxMana()
	this:SetStatMaxValue("Mana", math.floor(((GetInt(this)*2.5) + GetMobileMod(MobileMod.MaxManaPlus)) *GetMobileMod(MobileMod.MaxManaTimes, 1)) )
end

function RecalculateMaxStamina()
	this:SetStatMaxValue("Stamina", math.floor(((ServerSettings.Stats.BaseStamina * ServerSettings.Stats.AgiModifierFunc(GetAgi(this))) + GetMobileMod(MobileMod.MaxStaminaPlus)) *GetMobileMod(MobileMod.MaxStaminaTimes, 1)) )
end

function RecalculateMaxVitality()
	this:SetStatMaxValue("Vitality", math.floor((ServerSettings.Stats.BaseVitality + GetMobileMod(MobileMod.MaxVitalityPlus)) *GetMobileMod(MobileMod.MaxVitalityTimes, 1)) )
end

function RecalculateDerivedStr()
	this:SetStatValue("Str", math.floor(( GetBaseStr(this) + GetMobileMod(MobileMod.StrengthPlus) ) *GetMobileMod(MobileMod.StrengthTimes, 1)) )

	RecalculateAttack()

	--D*ebugMessage("[base_statmod::RecalculateDerivedStr] For "..this:GetName())
end

function RecalculateDerivedAgi()
	this:SetStatValue("Agi", math.floor(( GetBaseAgi(this) + GetMobileMod(MobileMod.AgilityPlus) ) *GetMobileMod(MobileMod.AgilityTimes, 1)) )

	RecalculateAccuracy()
	RecalculateEvasion()
	RecalculateAttackSpeed()
	RecalculateCritChance()
	RecalculateMaxStamina()
	RecalculateMoveSpeed()

	--D*ebugMessage("[base_statmod::RecalculateDerivedAgi] For "..this:GetName())
end

function RecalculateDerivedInt()
	this:SetStatValue("Int", math.floor(( GetBaseInt(this) + GetMobileMod(MobileMod.IntelligencePlus) ) *GetMobileMod(MobileMod.IntelligenceTimes, 1)) )

	RecalculateMaxMana()
	RecalculatePower()
	RecalculateForce()

	--D*ebugMessage("[base_statmod::RecalculateDerivedInt] For "..this:GetName())
end

function RecalculateDerivedCon()
	this:SetStatValue("Con", math.floor(( GetBaseCon(this) + GetMobileMod(MobileMod.ConstitutionPlus) ) *GetMobileMod(MobileMod.ConstitutionTimes, 1)) )
	RecalculateMaxHealth()
	--D*ebugMessage("[base_statmod::RecalculateDerivedCon] For "..this:GetName())
end

--KH TODO: This works as a type of magic resistance.
function RecalculateDerivedWis()
	this:SetStatValue("Wis", math.floor(( GetBaseWis(this) + GetMobileMod(MobileMod.WisdomPlus) ) *GetMobileMod(MobileMod.WisdomTimes, 1)) )
end

--KH TODO: This works as a type of crowd control resistance.
function RecalculateDerivedWill()
	this:SetStatValue("Will", math.floor(( GetBaseWill(this) + GetMobileMod(MobileMod.WillPlus) ) *GetMobileMod(MobileMod.WillTimes, 1)) )
end

function RecalculateAccuracy()
	local isPlayer = IsPlayerCharacter(this)
	local weaponClass = "Fist"
	if(_Weapon and _Weapon.RightHand) then
		weaponClass = _Weapon.RightHand.Class
	end

	local thisWeaponSkill = 0
	if(EquipmentStats.BaseWeaponClass[weaponClass]) then
		thisWeaponSkill = EquipmentStats.BaseWeaponClass[weaponClass].WeaponSkill
	else
		DebugMessage("ERROR: Invalid weapon class: "..tostring(weaponClass))
	end

	local baseAccuracy = ServerSettings.Stats.AgiHitModifierFunc(GetAgi(this))
	local weaponAccuracy = EquipmentStats.BaseWeaponClass[weaponClass].Accuracy
	local weaponAccuracyBonus = 0
	if ( _Weapon and _Weapon.RightHand.Object ~= nil ) then
		weaponAccuracyBonus = _Weapon.RightHand.Object:GetObjVar("AccuracyBonus") or 0
	end

	local skillAccuracyBonus = GetSkillLevel(this,thisWeaponSkill) * ServerSettings.Stats.WeaponSkillAccuracyBonus

	local accuracy = (baseAccuracy + weaponAccuracy + weaponAccuracyBonus + skillAccuracyBonus + GetMobileMod(MobileMod.AccuracyPlus)) *GetMobileMod(MobileMod.AccuracyTimes, 1)

	--DebugMessage("[ACCURACY]["..this:GetName().."]",tostring(accuracy),tostring(baseAccuracy),tostring(weaponAccuracy),tostring(weaponAccuracyBonus),tostring(skillAccuracyBonus),tostring(GetMobileMod(MobileMod.AccuracyTimes, 1)),tostring(GetMobileMod(MobileMod.AccuracyPlus)))
	this:SetStatValue("Accuracy", math.floor(accuracy))
end

function RecalculateEvasion()
	local evasionPlusMod = GetMobileMod(MobileMod.EvasionPlus)
	local evasionTimesMod = GetMobileMod(MobileMod.EvasionTimes, 1)
	local isPlayer = IsPlayerCharacter(this)

	local weaponClass = "Fist"
	if(_Weapon and _Weapon.RightHand) then
		weaponClass = _Weapon.RightHand.Class
	end

	local thisWeaponSkill = EquipmentStats.BaseWeaponClass[weaponClass].WeaponSkill

	local baseEvasion = ServerSettings.Stats.AgiHitModifierFunc(GetAgi(this))

	-- We dont call the GetArmorProficiencyType helper function to avoid iterating through the armor twice
	local evasionArmorBonus = 0
	local canGetLightArmorBonus = isPlayer
	for i,slot in pairs(ARMORSLOTS) do
		local armorClass = nil

		local item = this:GetEquippedObject(slot)		
		if(item) then
			evasionArmorBonus = evasionArmorBonus + (item:GetObjVar("EvasionBonus") or 0)
			if(canGetLightArmorBonus) then
				armorClass = GetArmorClass(item)
			end
		end

		if(armorClass ~= "Light") then
			canGetLightArmorBonus = false
		end
	end

	local lightArmorEvasionBonus = 0
	if(isPlayer and canGetLightArmorBonus) then
		lightArmorEvasionBonus = GetSkillLevel(this,"LightArmorSkill") * ServerSettings.Stats.LightArmorEvasionBonus
	end

	local skillEvasionBonus = GetSkillLevel(this,thisWeaponSkill) * ServerSettings.Stats.WeaponSkillEvasionBonus

	local shieldEvasionPenalty = 0
	local thisShieldClass = nil
	if(_Weapon and _Weapon.LeftHand.Object) then
		thisShieldClass = GetShieldType(_Weapon.LeftHand.Object)
	end

	if(thisShieldClass ~= nil) then
		shieldEvasionPenalty = ServerSettings.Stats.ShieldEvasionPenalty
	end

    local evasion = (baseEvasion+evasionArmorBonus+skillEvasionBonus-shieldEvasionPenalty+lightArmorEvasionBonus+evasionPlusMod) *evasionTimesMod
   	
	--DebugMessage("[EVASION]["..this:GetName().."]",tostring(evasion),tostring(baseEvasion),tostring(evasionArmorBonus),tostring(skillEvasionBonus),tostring(shieldEvasionPenalty),tostring(evasionTimesMod),tostring(evasionPlusMod))
   	this:SetStatValue("Evasion",math.floor(evasion))
end

function RecalculateAttack()
	local isPlayer = IsPlayerCharacter(this)

	local attack = this:GetObjVar("Attack")
	local damageBonus = 0
	if not(attack) then
		if(isPlayer) then
			attack = EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Attack or 0
			if EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Attack > 3 then
				attack = attack + 5
			end
			damageBonus = GetMagicItemDamageBonus(_Weapon.RightHand.Object)
		else
			attack = ServerSettings.Stats.DefaultMobAttack
		end
	end

	local strModifier = ServerSettings.Stats.StrModifierFunc(GetStr(this))

	local damageSkillBonus = (GetSkillLevel(this, "MeleeSkill") or 0) / 12

	local scaledAttack = ((attack * strModifier  + damageSkillBonus) + damageBonus + GetMobileMod(MobileMod.AttackPlus)) *GetMobileMod(MobileMod.AttackTimes, 1)
	--DebugMessage("ATTACK",tostring(scaledAttack))
	this:SetStatValue("Attack",math.floor(scaledAttack))
end

-- power is magic cast damage modifier
function RecalculatePower()
	local power = this:GetObjVar("Power") -- for mobs or overrides on players
	local powerBonus = 0
	if not( power ) then
		if ( IsPlayerCharacter(this) ) then
			power = EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Power or 0
			--powerBonus = GetMagicItemDamageBonus(_Weapon.RightHand.Object)
		else
			power = ServerSettings.Stats.DefaultMobPower
		end
	end
	local channelingBonus = (GetSkillLevel(this, "ChannelingSkill") or 0) / 6 -- da 5 a 8
	local scaledPower = ( power + powerBonus + channelingBonus + GetMobileMod(MobileMod.PowerPlus)) * (GetMobileMod(MobileMod.PowerTimes, 1) * ServerSettings.Stats.IntMod(GetInt(this)))
	this:SetStatValue("Power", math.floor(scaledPower))

	--this:NpcSpeech("power: "..power)
	--this:NpcSpeech("powerBonus: "..powerBonus)
	--this:NpcSpeech("channelingBonus: "..channelingBonus)
	--this:NpcSpeech("intmod: "..ServerSettings.Stats.IntMod(GetInt(this)))
end

-- force is magic cast beneficial plus
function RecalculateForce()
	local force = this:GetObjVar("Force") -- for mobs or overrides on players
	local forceBonus = 0
	if not( force ) then
		if ( IsPlayerCharacter(this) ) then
			force = EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Force or 0
			--forceBonus = GetMagicItemDamageBonus(_Weapon.RightHand.Object) --TODO for when we implement weapon bonuses
		else
			force = ServerSettings.Stats.DefaultMobForce
		end
	end

	local restorationBonus = (GetSkillLevel(this, "RestorationSkill") or 0) / 5
	local scaledForce = (force + forceBonus + restorationBonus + GetMobileMod(MobileMod.ForcePlus)) * (GetMobileMod(MobileMod.ForceTimes, 1) + ServerSettings.Stats.IntMod(GetInt(this)))
	this:SetStatValue("Force", math.floor(scaledForce))
end

function RecalculateDefense()
	local defensePlusMod = GetMobileMod(MobileMod.DefensePlus)	
	local defenseTimesMod = GetMobileMod(MobileMod.DefenseTimes, 1)
	local isPlayer = IsPlayerCharacter(this)

	local thisArmorRating = 0
	local thisArmorBonus = 0
	local canGetHeavyArmorBonus = false

	if(isPlayer) then
		canGetHeavyArmorBonus = true		
		for i,slot in pairs(ARMORSLOTS) do
			local armorType = "Natural"
			local armorClass = nil
			local item = this:GetEquippedObject(slot)
			if(item) then
				armorType = GetArmorType(item)
				armorClass = GetArmorClassFromType(armorType)

				thisArmorBonus = thisArmorBonus + GetMagicItemArmorBonus(item)							
			end

			if(armorClass ~= "Heavy") then
				canGetHeavyArmorBonus = false
			end

			thisArmorRating = thisArmorRating + EquipmentStats.BaseArmorStats[armorType][slot].ArmorRating
		end		
	else
		-- DAB COMBAT CHANGES: MAKE DEFAULT MOB ARMOR CONFIGURABLE
		thisArmorRating = this:GetObjVar("Armor") or ServerSettings.Stats.DefaultMobArmorRating
	end

	local heavyArmorBonus = 0
	if(canGetHeavyArmorBonus) then
		heavyArmorBonus = GetSkillLevel(this,"HeavyArmorSkill") * ServerSettings.Stats.HeavyArmorDefenseBonus
	end
 
	--Defense = (4+Armor Def. * Buff 1)+Buff 2+Armor Bonus
	local defense = (4 + thisArmorRating + thisArmorBonus + heavyArmorBonus + defensePlusMod) *defenseTimesMod

	this:SetStatValue("Defense",math.floor(defense))
end

function RecalculateAttackSpeed()
	local isPlayer = IsPlayerCharacter(this)

	local speed = this:GetObjVar("AttackSpeed")
	local speedMod = 1
	local bowSpeedModifier = nil
	if not(speed) then
		if(isPlayer) then
			speed = GetWeaponClassStat(_Weapon.RightHand.Object, "Speed")
			bowSpeedModifier = GetWeaponTypeStat(_Weapon.RightHand.Object,"BowSpeedModifier")
			speedMod = GetMagicItemSpeedModifier(_Weapon.RightHand.Object)
		else
			-- DAB COMBAT CHANGES: MAKE THIS CONFIGURABLE
			speed = ServerSettings.Stats.DefaultMobWeaponSpeed
    	end
    end

	local lightArmorAttackSpeedMultiplier = 1
	if(isPlayer) then
		if(GetArmorProficiencyType(this) == "Light") then
			lightArmorAttackSpeedMultiplier = 1 + (GetSkillLevel(this,"LightArmorSkill") * ServerSettings.Stats.LightArmorAttackSpeedMultiplier)
		end
	end

	local baseAttackSpeed = ServerSettings.Stats.AgiModifierFunc(GetAgi(this)) * speed * speedMod * lightArmorAttackSpeedMultiplier
	
	local attacksPerSecond = 1
	
	-- weapons with a bow speed modifier have a different speed calculation
	if(bowSpeedModifier) then
		attacksPerSecond = (baseAttackSpeed/1000) * (1500/bowSpeedModifier)
	else
		attacksPerSecond = (baseAttackSpeed/500)
	end
	--DebugMessage("bowSpeedModifier",tostring(attacksPerSecond),tostring(baseAttackSpeed),tostring(bowSpeedModifier))

	--DebugMessage("GetSwingSpeed",tostring(1/(baseAttackSpeed/500)),tostring(ServerSettings.Stats.AgiModifierFunc(GetAgi(this))),tostring(speed),tostring(GetMagicItemSpeedModifier(weapon)),tostring(lightArmorAttackSpeedMultiplier))
	local attackSpeedSecs = 1/attacksPerSecond

	this:SetStatValue("AttackSpeed",math.round(attackSpeedSecs,2))
end

function RecalculateCritChance()
	local critChancePlusMod = GetMobileMod(MobileMod.CritChancePlus)
	local critChanceTimesMod = GetMobileMod(MobileMod.CritChanceTimes, 1)
	local isPlayer = IsPlayerCharacter(this)

	local weaponClass = "Fist"
	if(_Weapon and _Weapon.RightHand) then
		weaponClass = _Weapon.RightHand.Class
	end

	local lightArmorCritChanceBonus = 0
	if(isPlayer) then
		if(GetArmorProficiencyType(this) == "Light") then
			lightArmorCritChanceBonus = GetSkillLevel(this,"LightArmorSkill") * ServerSettings.Stats.LightArmorCritChanceBonus
		end
	end
	local agiModifier = ServerSettings.Stats.AgiModifierFunc(GetAgi(this))

	local magicItemCritChanceBonus = 0
	if(_Weapon and _Weapon.RightHand.Object) then
		magicItemCritChanceBonus = GetMagicItemCritChanceBonus(_Weapon.RightHand.Object)
	end

	local critChance = ((agiModifier * EquipmentStats.BaseWeaponClass[weaponClass].Critical / 10) + magicItemCritChanceBonus + lightArmorCritChanceBonus + critChancePlusMod)* critChanceTimesMod
	--DebugMessage("CritChance",tostring(critChance),tostring(agiModifier),tostring(EquipmentStats.BaseWeaponClass[weaponClass].Critical),tostring(critChanceTimesMod),tostring(critChancePlusMod),tostring(magicItemCritChanceBonus),tostring(lightArmorCritChanceBonus))

	this:SetStatValue("CritChance",math.round(critChance,1))
end

function RecalculateCritChanceReduction()
	local isPlayer = IsPlayerCharacter(this)

	local critChanceReduction = 0	
	if(isPlayer) then
		local armorProfType = GetArmorProficiencyType(this)		
		if(armorProfType == "Heavy") then
			critChanceReduction = GetSkillLevel(this,"HeavyArmorSkill") * ServerSettings.Stats.HeavyArmorCritChanceReduction
		end
	end

	this:SetStatValue("CritChanceReduction",critChanceReduction)
end

function RecalculateMoveSpeed()

    if ( IsMounted(this) ) then
    	local mountSpeed = ServerSettings.Stats.MountMoveSpeed or ServerSettings.Stats.BaseMoveSpeed
      	this:SetBaseMoveSpeed( ( ( mountSpeed + GetMobileMod(MobileMod.MountMoveSpeedPlus) ) ) * GetMobileMod(MobileMod.MountMoveSpeedTimes, 1) )
    else
		if not( IsPlayerCharacter(this) ) then
			return
		end

		-- determine if player is wearing any heavy armor
		local anyHeavy = (
			HasMobileEffect(this, "HeavyArmorDebuff")
			or IsWearingHeavyArmor(this, _Weapon.LeftHand.ShieldType ~= nil) -- wearing any heavy armor?
		)

		local agi = ServerSettings.Stats.HeavyArmorMoveSpeedAgi
		if not( anyHeavy ) then
			-- when not wearing any heavy armor, we use their real agi to calculate the speed boost.
			agi = GetAgi(this)
		end
		
		local agilityMod = ServerSettings.Stats.AgiModifierFunc(agi)

    	this:SetBaseMoveSpeed( ( ( ServerSettings.Stats.BaseMoveSpeed + GetMobileMod(MobileMod.MoveSpeedPlus) ) * agilityMod ) * GetMobileMod(MobileMod.MoveSpeedTimes, 1) )
	end

end

recalcFuncs = 
{
	{Name="Strength", Func=RecalculateDerivedStr},
	{Name="Intelligence", Func=RecalculateDerivedInt},
	{Name="Agility", Func=RecalculateDerivedAgi},
	{Name="Constitution", Func=RecalculateDerivedCon},
	{Name="Wisdom", Func=RecalculateDerivedWis},
	{Name="Will", Func=RecalculateDerivedWill},
	{Name="HealthRegen", Func=RecalculateHealthRegen},
	{Name="ManaRegen", Func=RecalculateManaRegen},
	{Name="StaminaRegen", Func=RecalculateStaminaRegen},
	{Name="VitalityRegen", Func=RecalculateVitalityRegen},
	{Name="Accuracy", Func=RecalculateAccuracy},
	{Name="Evasion", Func=RecalculateEvasion},
	{Name="Attack", Func=RecalculateAttack},
	{Name="Power", Func=RecalculatePower},
	{Name="Force", Func=RecalculateForce},
	{Name="Defense", Func=RecalculateDefense},
	{Name="AttackSpeed", Func=RecalculateAttackSpeed},
	{Name="CritChance", Func=RecalculateCritChance},
	{Name="CritChanceReduction", Func=RecalculateCritChanceReduction},
	{Name="MoveSpeed", Func=RecalculateMoveSpeed},
	{Name="MountMoveSpeed", Func=RecalculateMoveSpeed},
	{Name="MaxHealth", Func=RecalculateMaxHealth},
	{Name="MaxVitality", Func=RecalculateMaxVitality},
}

-- this is the delayed function intended to reduced duplicate calls to recalc functions
function DoRecalculateStats()
	-- these functions must be called in order
	-- as some derived stats depend on others
	for key,value in pairs(recalcFuncs) do
		--DebugMessage("CHECKING",tostring(value.Name),tostring(statsToRecalculate[value.Name]))
		if( statsToRecalculate[value.Name] == true ) then			
			value.Func()
		end
	end

	--this:SendMessage("UpdateCharacterWindow")

	statsToRecalculate = {}
end

-- this is the message handler that marks derived stats as dirty to be recalculated
function MarkStatsDirty(recalculateStatsDict)
	--DebugMessage("DIRTY",DumpTable(recalculateStatsDict))
	for key, value in pairs(recalculateStatsDict) do		
		statsToRecalculate[key] = true
	end

	if( this:HasTimer("DoRecalculateStats") ) then
		this:RemoveTimer("DoRecalculateStats")
	end

	-- schedule client update
	-- 100ms buffer to reduce spam
	this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(100),"DoRecalculateStats")
end

-- Recalculate on load
this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(100),"DoRecalculateStats")

RegisterEventHandler(EventType.Timer, "DoRecalculateStats", DoRecalculateStats)
RegisterEventHandler(EventType.Message, "RecalculateStats", MarkStatsDirty)

-- This is to be included by mobiles for the ability to add and remove stat modifiers

-- this defines the derived stats that need to be recalculated when this skill changes
skillGainEffects = {
	MagicAffinitySkill = { ManaRegen = true },
	LightArmorSkill = { Evasion = true, CritChance = true, AttackSpeed = true },
	HeavyArmorSkill = { Defense = true, CritChance = true },	
	SlashingSkill = { Evasion = true, Accuracy = true },
	PiercingSkill = { Evasion = true, Accuracy = true },
	BashingSkill = { Evasion = true, Accuracy = true },
	ArcherySkill = { Evasion = true, Accuracy = true },
	LancingSkill = { Evasion = true, Accuracy = true },
	ChannelingSkill = { Power = true },
	RestorationSkill = { Force = true },
}

-- Args
--     statModName: the name of the stat to modify (right now we old support regen modifiers)
--     statModIdentifier: the identifier of this particular mod (so it can be removed later)
--     statModType: the type of modifier (bonus or multiplier)
--     statModValue: the actual modifier value
function HandleAddStatMod(statModName,statModIdentifier,statModType,statModValue,statModTime)
	LuaDebugCallStack("[DEPRECIATED] AddStatMod has been replaced with SetMobileMod.")
end

-- Args
--     statModName: the name of the stat to remove (right now we old support regen modifiers)
--     statModIdentifier: the identifier the mod to remove
function HandleRemoveStatMod(statModName, statModIdentifier)
	LuaDebugCallStack("[DEPRECIATED] AddStatMod has been replaced with SetMobileMod.")
end

function StatsHandleEquipmentChanged(item)
	local itemSlot = GetEquipSlot(item)

	local checkHeavyArmor = false
	local dirtyTable = {}

	if( itemSlot == "RightHand" ) then
		dirtyTable = {Accuracy=true,Attack=true,AttackSpeed=true,CritChance=true,Force=true,Power=true}
	elseif( itemSlot == "LeftHand" and GetShieldType(item) ) then
		--checkHeavyArmor = true
		dirtyTable.Evasion = true
	elseif( IsArmorSlot(itemSlot) ) then
		dirtyTable = {Evasion=true, Defense=true, ManaRegen=true, StaminaRegen=true, AttackSpeed=true}
		local armorClass = GetArmorClass(item)
		if(armorClass == "Heavy") then
			checkHeavyArmor = true
			dirtyTable.CritChanceReduction = true
		elseif(armorClass == "Light") then
			dirtyTable.CritChance = true
		end
	end

	if ( checkHeavyArmor ) then
		if ( IsWearingHeavyArmor(this, _Weapon.LeftHand.ShieldType ~= nil) ) then
			-- always clear the duration debuff when equipping a heavy item
			if ( HasMobileEffect(this, "HeavyArmorDebuff") ) then
				this:SendMessage("EndHeavyArmorDebuffEffect")
			else
				dirtyTable.MoveSpeed = true
			end
		else
			if not( HasMobileEffect(this, "HeavyArmorDebuff") ) then
				StartMobileEffect(this, "HeavyArmorDebuff", nil, {})
			end
		end
	end

	MarkStatsDirty(dirtyTable)
end

function StatsHandleSkillChanged(skillName)
	if(skillGainEffects[skillName]) then
		MarkStatsDirty(skillGainEffects[skillName])
	end
end

RegisterEventHandler(EventType.Message, "AddStatMod", HandleAddStatMod)
RegisterEventHandler(EventType.Message, "RemoveStatMod", HandleRemoveStatMod)

RegisterEventHandler(EventType.Message, "OnSkillLevelChanged", StatsHandleSkillChanged)