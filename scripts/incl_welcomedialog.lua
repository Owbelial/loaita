function ShowWelcomeDialog(showAnyway)
    -- GODS KNOW THIS STUFF!
    showAnyway = showAnyway or false
   -- if(this:IsGod() and not showAnyway) then
    --    return
   -- end
    
    local welcomeWindow = DynamicWindow("Welcome","Benvenuti su LoA Italia",570,600)
    welcomeWindow:AddLabel(170,7,"Modifiche/Fix 28/02/2018",510,420,20,"",true)

    welcomeWindow:AddLabel(20,40,"Cortesemente leggere le News sul Sito per essere aggiornati sui Fix e Novita'.",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,60,"-Inserita icona di cooldown delle pozioni",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,80,"-Aggiunta seconda barra delle spell-comandi custom",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,100,"-Corretto il buff dell abilita di prestigio Destruction",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,120,"-Rimosso il kill degli animali pacifici da altri animali npc pacifici",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,140,"-Migliorati i comandi e le reazioni dei pet con i comandi vocali",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,160,"-Corretto un errore di visualizzazione col comando hunger",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,180,"-Ora alla creazione del pg si mantiene il tratto corporeo",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,200,"-Fixate le guardie, ora attaccano gli aggressori",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,220,"-Corretto il craft delle plant armor",510,420,20,"",true)
   -- welcomeWindow:AddLabel(20,240,"Altri fix sono in elaborazione.",510,420,20,"",true)
    welcomeWindow:AddLabel(20,260,"Lo Staff",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,280,"-Inseriti nuovi loot casuali nei mostri",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,300,"-Eseguite varie pulizie di codice e fix minori al gioco",510,420,20,"",true)
   -- welcomeWindow:AddLabel(20,320,"-Corretti alcuni messaggi di gioco prima non presenti",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,340,"-Coretta l'abilita delle armi a due mani FollowTrought",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,360,"-Fixato il non poter calcolare gli oggetti impilati per la vendita",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,380,"Loot:Gold,Armi,risorse.",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,400,"Stato: CONCLUSA. ",510,420,20,"",true)
    --welcomeWindow:AddLabel(20,420,"Lo Staff",510,420,20,"",true)

    welcomeWindow:AddButton(20,460,"GameGuide","Apri il Sito di LoA Italia",510,0,"","",false)
    welcomeWindow:AddButton(20,490,"","Chiudi",510,0,"","",true)

    this:OpenDynamicWindow(welcomeWindow,this)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"Welcome", 
    function (user,buttonId)
        if(buttonId == "GameGuide") then
           this:SendClientMessage("OpenURL","http://legendsofaria.info")
        end
    end)

RegisterEventHandler(EventType.ClientUserCommand,"help", 
    function (args)
        if(args == nil) then
            ShowWelcomeDialog(true)
        end
    end)

--RegisterEventHandler(EventType.LoadedFromBackup,"", 
  -- function (user,buttonId)
 --     ShowWelcomeDialog()
 -- end)

--RegisterEventHandler(EventType.Message,"ShowWelcomeDialog",function ( ... )
--    ShowWelcomeDialog(true)
--end)


function MostraSalvataggioPg(showAnyway)

    showAnyway = showAnyway or false

    
    local salvapgWindow = DynamicWindow("SalvataggioPg","Pannello Salvataggio Stat/Skill PG",570,600)
    salvapgWindow:AddLabel(170,7,"Informazioni sul salvataggio",510,420,20,"",true)

    salvapgWindow:AddLabel(20,40,"Legends of Aria Italia non intende fare wipe di NESSUN TIPO, ma essendo il gioco in pieno sviluppo, e tutt'ora in pre beta, per evitare wipe forzati dagli sviluppatori o wipe dati dallo stravolgimento del gioco permettiamo il salvataggio delle skill e stat dei vostri pg, in modo tale che se per qualsiasi motivo occorrera' fare un wipe potrete riavere le stat e le skill allenate. Una volta salvato potrete controllare dal nostro sito web il token dei vostri personaggi.",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,60,"-Inserita icona di cooldown delle pozioni",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,80,"-Aggiunta seconda barra delle spell-comandi custom",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,100,"-Corretto il buff dell abilita di prestigio Destruction",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,120,"-Rimosso il kill degli animali pacifici da altri animali npc pacifici",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,140,"-Migliorati i comandi e le reazioni dei pet con i comandi vocali",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,160,"-Corretto un errore di visualizzazione col comando hunger",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,180,"-Ora alla creazione del pg si mantiene il tratto corporeo",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,200,"-Fixate le guardie, ora attaccano gli aggressori",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,220,"-Corretto il craft delle plant armor",510,420,20,"",true)
    salvapgWindow:AddLabel(20,340,"I vostri pg sono Importanti e verranno tutelati il piu' possibile.",510,420,20,"",true)
    salvapgWindow:AddLabel(20,360,"Lo Staff di LoA Italia",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,280,"-Inseriti nuovi loot casuali nei mostri",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,300,"-Eseguite varie pulizie di codice e fix minori al gioco",510,420,20,"",true)
   -- salvapgWindow:AddLabel(20,320,"-Corretti alcuni messaggi di gioco prima non presenti",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,340,"-Coretta l'abilita delle armi a due mani FollowTrought",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,360,"-Fixato il non poter calcolare gli oggetti impilati per la vendita",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,380,"Loot:Gold,Armi,risorse.",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,400,"Stato: CONCLUSA. ",510,420,20,"",true)
    --salvapgWindow:AddLabel(20,420,"Lo Staff",510,420,20,"",true)

    salvapgWindow:AddButton(20,460,"SalvaPg","Salva il Personaggio",510,0,"","",true)
    salvapgWindow:AddButton(20,490,"","Chiudi",510,0,"","",true)

    this:OpenDynamicWindow(salvapgWindow,this)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"SalvataggioPg", 
    function (user,buttonId)
        if(buttonId == "SalvaPg") then

		if ( user:HasTimer("RecentSalvataggio") ) then
			user:SystemMessage("Hai effettutato il salvatagio poco tempo fa, non serve farlo spesso.", "info")
			--return
		end
--C:\inetpub\wwwroot\players
            local file = io.open("C:/inetpub/wwwroot/players/"..(user:GetAttachedUserId()).."/"..(user:GetName())..".txt", "w")
            if file == NULL then
                os.execute( "mkdir C:\\inetpub\\wwwroot\\players\\"..(user:GetAttachedUserId()).."" )
                local file = io.open("C:/inetpub/wwwroot/players/"..(user:GetAttachedUserId()).."/"..(user:GetName())..".txt", "w")
                    file:write((string.format("User ID:%s,", (string.format(user:GetAttachedUserId())))), "\n")
                    file:write((string.format("ID di Gioco:%s,", (string.format(user.Id)))), "\n")
                    file:write((string.format("Minuti di Gioco:%s,", (string.format(user:GetObjVar("PlayMinutes") or 0)))), "\n")
              	    file:write((string.format("Nome:%s,", (user:GetName()))), "\n") 
                    file:write((string.format("Str:%s,", (GetStr(user)))), "\n") 
                    file:write((string.format("Agi:%s,", (GetAgi(user)))), "\n")
                    file:write((string.format("Int:%s,", (GetInt(user)))), "\n") 
                    file:write((string.format("Cos:%s,", (GetCon(user)))), "\n") 
                    file:write((string.format("Wis:%s,", (GetWis(user)))), "\n") 
                    file:write((string.format("Will:%s,", (GetWill(user)))), "\n") 
	                for i,skillInfo in pairs(GetAllSkills(user)) do
		                if GetSkillLevel(user,skillInfo.Name) > 0 then
			                file:write((GetSkillDisplayName(skillInfo.Name)..":"..GetSkillLevel(user,skillInfo.Name)).."," ,"\n")
                        end
            	    end
                print("Nuovo Profilo Creato")
                file:close()
                user:ScheduleTimerDelay(TimeSpan.FromMinutes(5), "RecentSalvataggio")
                user:SystemMessage("Hai salvato il personaggio correttamente!", "info")
            else
                file:write((string.format("User ID:%s,", (string.format(user:GetAttachedUserId())))), "\n")
                file:write((string.format("ID di Gioco:%s,", (string.format(user.Id)))), "\n")
                file:write((string.format("Minuti di Gioco:%s,", (string.format(user:GetObjVar("PlayMinutes") or 0)))), "\n")
              	file:write((string.format("Nome:%s,", (user:GetName()))), "\n") 
                file:write((string.format("Str:%s,", (GetStr(user)))), "\n") 
                file:write((string.format("Agi:%s,", (GetAgi(user)))), "\n")
                file:write((string.format("Int:%s,", (GetInt(user)))), "\n") 
                file:write((string.format("Cos:%s,", (GetCon(user)))), "\n") 
                file:write((string.format("Wis:%s,", (GetWis(user)))), "\n") 
                file:write((string.format("Will:%s,", (GetWill(user)))), "\n") 
	            for i,skillInfo in pairs(GetAllSkills(user)) do
		            if GetSkillLevel(user,skillInfo.Name) > 0 then
			            file:write((GetSkillDisplayName(skillInfo.Name)..":"..GetSkillLevel(user,skillInfo.Name)).."," ,"\n")
                    end
            	end
                print("Giocatore Aggiunto/Aggiornato")
                file:close()
                user:ScheduleTimerDelay(TimeSpan.FromMinutes(5), "RecentSalvataggio")
                user:SystemMessage("Hai salvato il personaggio correttamente!", "info")
            end
        end
    end)

RegisterEventHandler(EventType.ClientUserCommand,"salvapg", 
    function (args)
        if(args == nil) then
            MostraSalvataggioPg(true)
        end
    end)





--configurazione
flarghezza = 1800
faltezza = 600
ftitolo = "LoA Italia Market" 

fnumerooggetti = 10

--oggetti
oggetto1 = {}
oggetto1.descrizione = "Unique random Ritual abilities"
oggetto1.oggetto = "Hunting Scimitar"
oggetto1.valore = 300
oggetto1.oggettotemplate = "hunting_scimitar"

oggetto2 = {}
oggetto2.descrizione = "Unique random Ritual abilities"
oggetto2.oggetto = "Torment Hammer"
oggetto2.valore = 300
oggetto2.oggettotemplate = "torment_hammer"

oggetto3 = {}
oggetto3.descrizione = "Unique random Ritual abilities"
oggetto3.oggetto = "Sword of Destiny"
oggetto3.valore = 300
oggetto3.oggettotemplate = "sword_of_destiny"

oggetto4 = {}
oggetto4.descrizione = "Unique random Ritual abilities"
oggetto4.oggetto = "Bonecrusher"
oggetto4.valore = 300
oggetto4.oggettotemplate = "bonecrusher"

oggetto5 = {}
oggetto5.descrizione = "Unique random Ritual abilities"
oggetto5.oggetto = "Royal Halberd"
oggetto5.valore = 300
oggetto5.oggettotemplate = "royal_halberd"

oggetto6 = {}
oggetto6.descrizione = "Unique random Ritual abilities"
oggetto6.oggetto = "Axe of the Silence"
oggetto6.valore = 300
oggetto6.oggettotemplate = "axe_of_the_silence"

oggetto7 = {}
oggetto7.descrizione = "Unique random Ritual abilities"
oggetto7.oggetto = "Nexus Bow"
oggetto7.valore = 300
oggetto7.oggettotemplate = "nexus_bow"

oggetto8 = {}
oggetto8.descrizione = "Unique random Ritual abilities"
oggetto8.oggetto = "Staff of Nova"
oggetto8.valore = 300
oggetto8.oggettotemplate = "staff_of_nova"

oggetto9 = {}
oggetto9.descrizione = "Best in Slot"
oggetto9.oggetto = "Blue Drake Minion"
oggetto9.valore = 2000
oggetto9.oggettotemplate = "minion_dragon_statue_blue"

oggetto10 = {}
oggetto10.descrizione = "Best in Slot"
oggetto10.oggetto = "Red Drake Minion"
oggetto10.valore = 2000
oggetto10.oggettotemplate = "minion_dragon_statue"

--------------------------------

spesa = 0
nomeoggetto = ""

function ShowLoaMarket(showAnyway)
    showAnyway = showAnyway or false
    
    if (fnumerooggetti <= 0) then return
    else
        local loamarket = DynamicWindow("ShopLoaMarket",ftitolo,flarghezza,faltezza)
        local riga = 0
        local rigab = 0
        local desc = ""
        local obj = ""
        local val = 0
        local templ = ""
        local idbottone = ""

        loamarket:AddLabel(200,5,"Description",350,420,40,"",true)
        loamarket:AddLabel(500,5,"Name",450,420,40,"",true)
        loamarket:AddLabel(750,5,"Points",510,420,40,"",true)
        --(this:HasObjVar("LoAItaliaPoints"))
        

            for i=1,fnumerooggetti do
                if (i == 1) then 
                    desc = oggetto1.descrizione
                    obj = oggetto1.oggetto
                    val = oggetto1.valore
                    idbottone = "bottone1"
                    templ = oggetto1.oggettotemplate                                        
                elseif (i == 2) then 
                    desc = oggetto2.descrizione
                    obj = oggetto2.oggetto
                    val = oggetto2.valore
                    idbottone = "bottone2"
                    templ = oggetto2.oggettotemplate
                elseif (i == 3) then 
                    desc = oggetto3.descrizione
                    obj = oggetto3.oggetto
                    val = oggetto3.valore
                    idbottone = "bottone3"
                    templ = oggetto3.oggettotemplate
                elseif (i == 4) then 
                    desc = oggetto4.descrizione
                    obj = oggetto4.oggetto
                    val = oggetto4.valore
                    idbottone = "bottone4"
                    templ = oggetto4.oggettotemplate
                elseif (i == 5) then 
                    desc = oggetto5.descrizione
                    obj = oggetto5.oggetto
                    val = oggetto5.valore
                    idbottone = "bottone5"
                    templ = oggetto5.oggettotemplate
                elseif (i == 6) then 
                    desc = oggetto6.descrizione
                    obj = oggetto6.oggetto
                    val = oggetto6.valore
                    idbottone = "bottone6"
                    templ = oggetto6.oggettotemplate
                elseif (i == 7) then 
                    desc = oggetto7.descrizione
                    obj = oggetto7.oggetto
                    val = oggetto7.valore
                    idbottone = "bottone7"
                    templ = oggetto7.oggettotemplate
                elseif (i == 8) then 
                    desc = oggetto8.descrizione
                    obj = oggetto8.oggetto
                    val = oggetto8.valore
                    idbottone = "bottone8"
                    templ = oggetto8.oggettotemplate
                elseif (i == 9) then 
                    desc = oggetto9.descrizione
                    obj = oggetto9.oggetto
                    val = oggetto9.valore
                    idbottone = "bottone9"
                    templ = oggetto9.oggettotemplate
                elseif (i == 10) then 
                    desc = oggetto10.descrizione
                    obj = oggetto10.oggetto
                    val = oggetto10.valore
                    idbottone = "bottone10"
                    templ = oggetto10.oggettotemplate
                end
                                        
                    riga = riga + 50
                    rigab = riga - 5
                    loamarket:AddLabel(200,riga,desc,510,420,20,"",true)
                    loamarket:AddLabel(500,riga,obj,510,420,20,"",true)
                    loamarket:AddImage(10,riga - 20,tostring(GetTemplateIconId(templ)),64,64,"Object")
                    loamarket:AddLabel(750,riga,tostring(val),510,420,20,"",true)
                    if val > this:GetObjVar("LoAItaliaPoints") then 
                        loamarket:AddButton(850,rigab,idbottone,"[A11F12]Buy[-]",110,0,"","",true)
                    else
                        loamarket:AddButton(850,rigab,idbottone,"[006000]Buy[-]",110,0,"","",true)
                    end

                    i = i + 1
            end
        this:OpenDynamicWindow(loamarket,this)
    end
end


----------------------------------------------------------



RegisterEventHandler(EventType.DynamicWindowResponse,"ShopLoaMarket", 
function (user,buttonId)

    local puntirichiesti = 0
    local PointFaction = user:GetObjVar("LoAItaliaPoints")

       --oggetto1
        if(buttonId == "bottone1") then
          puntirichiesti = oggetto1.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.") 
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto1.oggetto
                    spesa = oggetto1.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
			        CreateObjInContainer(oggetto1.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)
                    print(objRef)
                    --objRef:SetItemTooltip()
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.") 
                end
          end

          --oggetto2
        if(buttonId == "bottone2") then
          puntirichiesti = oggetto2.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                    else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto2.oggetto
                    spesa = oggetto2.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto2.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare) 
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")    
                end
          end
      --oggetto3
        if(buttonId == "bottone3") then
          puntirichiesti = oggetto3.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto3.oggetto
                    spesa = oggetto3.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto3.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")    
                end
          end
            --oggetto4
        if(buttonId == "bottone4") then
          puntirichiesti = oggetto4.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto4.oggetto
                    spesa = oggetto4.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto4.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)   
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")  
                end
          end
            --oggetto5
        if(buttonId == "bottone5") then
          puntirichiesti = oggetto5.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto5.oggetto
                    spesa = oggetto5.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto5.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)   
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")   
                end
          end
      --oggetto6
        if(buttonId == "bottone6") then
          puntirichiesti = oggetto6.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto6.oggetto
                    spesa = oggetto6.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto6.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare) 
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")   
                end
          end
            --oggetto7
        if(buttonId == "bottone7") then
          puntirichiesti = oggetto7.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto7.oggetto
                    spesa = oggetto7.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto7.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)    
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")  
                end
          end
            --oggetto8
        if(buttonId == "bottone8") then
          puntirichiesti = oggetto8.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto8.oggetto
                    spesa = oggetto8.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto8.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)  
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")   
                end
          end

                      --oggetto9
        if(buttonId == "bottone9") then
          puntirichiesti = oggetto9.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto9.oggetto
                    spesa = oggetto9.valore
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    CreateObjInContainer(oggetto9.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)  
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")   
                end
          end
    
                --oggetto10
        if(buttonId == "bottone10") then
                        puntirichiesti = oggetto10.valore
                if (puntirichiesti > PointFaction) then user:SystemMessage("You dont have enough points.")  
                else
                    local bankapg = user:GetEquippedObject("Bank")
                    local ammontare = 1
                    nomeoggetto = oggetto10.oggetto
                    spesa = oggetto10.valore
                    CreateObjInContainer(oggetto10.oggettotemplate, bankapg, GetRandomDropPosition(bankapg), "creaPremio", ammontare)  
                    user:SetObjVar("LoAItaliaPoints",PointFaction - puntirichiesti)
                    user:SystemMessage("We sent the "..nomeoggetto.." into your bankbox.")   
                end
          end
    
    
    end)

RegisterEventHandler(EventType.ClientUserCommand,"loamarket", 
    function (args)
        if(args == nil) then
            if not this:HasObjVar("LoAItaliaPoints") then
		        this:SetObjVar("LoAItaliaPoints",0)
	        end
            ShowLoaMarket(true)
            this:SystemMessage("You have "..tostring(this:GetObjVar("LoAItaliaPoints")).." LoA Italia points. ")    
        end
    end)

RegisterEventHandler(EventType.CreatedObject, "creaPremio", 
	function (success,objref,ammontare,hue)		

		if (hue ~= nil) then
			objref:SetHue(hue)
		end
		if (ammontare>1) then
			RequestSetStack(objref,ammontare)
		end

		SetItemTooltip(objref)
	end)