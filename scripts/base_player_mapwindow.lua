mapWindowOpen = false
curMap = nil
maxCustomMarkers = 10

isZoomed = true
canZoom = (GetWorldName() == "NewCelador")

waypointIcons = { "marker_circle1", "marker_circle2", "marker_diamond1", "marker_diamond2", "marker_diamond3", "marker_diamond4", "marker_diamond5", "location_corpse", "location_home","pentagram_marker" }

-- this function can change the map name based on conditions in the map
-- specifically: the current catacombs map is based on the dungeon configuration
customMapFunctions = {
	Catacombs = function (isZoomed)
		-- DAB TODO: make it possible to get catacombs map
		return nil
	end,

	NewCelador = function (isZoomed)
		if (isZoomed) then
			local subregionName = GetSubregionName()
			if(subregionName:match("SewerDungeon")) then
				return nil
			elseif(subregionName) then
				return subregionName
			end
		end

		return "NewCelador"
	end,

	Ruin = function()
		if(IsImmortal(this)) then
			return "CatacombsConfig1"
		end
	end,

	Deception = function ()
		if(IsImmortal(this)) then
			return "CatacombsConfig3"
		end
	end,

	Contempt = function()
		if(IsImmortal(this)) then
			return "CatacombsConfig4"
		end
	end,
}

catacombsPortal = nil

customMapMarkerFunctions = {
	NewCelador = function(mapIcons)
		if not(catacombsPortal) then
			catacombsPortal = FindObject(SearchTemplate("catacombs_portal_controller"))
		end
		
		if(catacombsPortal) then
			if(catacombsPortal:HasObjVar("CatacombsPortalActive")) then
				table.insert(mapIcons,{Icon="pentagram_marker", Location=catacombsPortal:GetLoc(), Tooltip="The portal to Catacombs has been opened."})
			else
				table.insert(mapIcons,{Icon="marker_diamond1",Location=catacombsPortal:GetLoc(),Tooltip="To Catacombs. (Inactive)"})
			end
		end
	end,	
}

function GetCurrentMapName()
	local mapName = GetWorldName()
	if(customMapFunctions[mapName] ~= nil) then
		return customMapFunctions[mapName](isZoomed)
	end
end

function GetMapMarkers()
	local mapIcons = {}
	local mapName = GetWorldName()	

	local userHouse = GetUserHouse(this)
	if(userHouse ~= nil and userHouse:IsValid()) then
		table.insert(mapIcons,{Icon="location_home", Location=userHouse:GetLoc(), Tooltip="Home"})
	end

	local groupId = GetGroupId(this)
	if ( groupId ~= nil ) then
		local members = GetGroupVar(groupId, "Members")
		if ( members ~= nil ) then
			for i=1,#members do
				local member = members[i]
				if ( member ~= this and member:IsValid() ) then
					local name = StripColorFromString(member:GetName())
					table.insert(mapIcons,{Icon="marker_diamond2",Tooltip=name,Location=member:GetLoc(),MapVisibility=false})
				end
			end
		end
	end
	
	if (IsDead(this)) then		
		--Add res shrines if you end up dead.		
		local shrines = FindObjectsWithTagInRange("Shrine",this:GetLoc(),400)
		if (shrines ~= nil) then
			for i, shrine in pairs(shrines) do
				table.insert(mapIcons,{Icon="marker_diamond5", Location=shrine:GetLoc(), Tooltip="Resurrection Shrine"})
			end
		end
	end

	local lastDeathRegion = this:GetObjVar("LastDeathRegion")
	--DebugMessage("corpseRegion is "..tostring(this:GetObjVar("corpseRegion")))
	if(lastDeathRegion == GetRegionAddress()) then
		--DebugMessage("corpseObject is "..tostring(this:GetObjVar("CorpseObject")))
		local lastCorpse = this:GetObjVar("CorpseObject")
		if (lastCorpse ~= nil and lastCorpse:IsValid()) then
			local deathLocation = lastCorpse:GetLoc()
			table.insert(mapIcons,{Icon="location_corpse", Location=deathLocation, Tooltip="Corpse"})
		end
	end

	local mapMarkers = this:GetObjVar("MapMarkers")
	--DebugMessage(DumpTable(mapMarkers))	
	if( mapMarkers ~= nil) then
		for i,markerEntry in pairs(mapMarkers) do
			--DebugMessage(mapMarkers.Tooltip)
			local isValidRegion = true
			if(markerEntry.RegionAddress ~= nil and markerEntry.RegionAddress ~= GetRegionAddress()) then
				isValidRegion = false
				--DebugMessage(1)
			end
			if(markerEntry.Map ~= nil and markerEntry.Map ~= mapName) then
				isValidRegion = false
				--DebugMessage(2)
			end
			--DebugMessage("isValidRegion is "..tostring(isValidRegion))
			if(isValidRegion) then
				--DebugMessage("Location is ")
				if(markerEntry.ObjTemplate ~= nil and markerEntry.ObjTemplate ~= "") then
					-- DAB DEBUG: NEED TO OPTIMIZE THIS
					local matchingObjs
					if(markerEntry.Region ~= nil) then
						matchingObjs = FindObjects(SearchMulti{SearchTemplate(markerEntry.ObjTemplate),SearchRegion(markerEntry.Region)})
					else
						matchingObjs = FindObjects(SearchTemplate(markerEntry.ObjTemplate))
					end

					for i,matchingObj in pairs(matchingObjs) do 
						table.insert(mapIcons,{Icon=markerEntry.Icon, Location=matchingObj:GetLoc(), Tooltip = markerEntry.Tooltip})
					end
				else
					--DebugMessage(markerEntry.Loc)
					local loc = markerEntry.Location
					--DebugMessage("loc is "..tostring(loc))
					if(markerEntry.Obj ~= nil and markerEntry.Obj:IsValid()) then
						loc = markerEntry.Obj:GetLoc()
					end
					if(loc ~= nil) then						
						table.insert(mapIcons,{Icon=markerEntry.Icon, Location=loc, Tooltip = markerEntry.Tooltip})

						if(markerEntry.RemoveDistance and this:GetLoc():Distance(loc) < markerEntry.RemoveDistance) then
							table.remove(mapMarkers,i)
							this:SetObjVar("MapMarkers",mapMarkers)
						end
					end
				end				
			end
		end
	end

	if(customMapMarkerFunctions[mapName] ~= nil) then
		customMapMarkerFunctions[mapName](mapIcons)
	end	
	return mapIcons
end

function GetUserWaypoints()
	local worldName = GetWorldName()
	local subregionName = GetSubregionName()

	local customWaypoints = this:GetObjVar("UserWaypoints")
	if(customWaypoints and customWaypoints[worldName]) then
		if not(canZoom) or isZoomed then		
			local waypointTable = customWaypoints[worldName]
			if(subregionName and subregionName ~= "") then
				if(customWaypoints[worldName][subregionName]) then
					waypointTable = customWaypoints[worldName][subregionName]
				else
					waypointTable = nil
				end
			end

			if(waypointTable) then
				return waypointTable			
			end		
		elseif(subregionName and subregionName ~= "") then
			waypointTable = {}
			for i, subregionWaypoints in pairs(customWaypoints[worldName]) do
				for i, subregionWaypoint in pairs(subregionWaypoints) do
					table.insert(waypointTable,subregionWaypoint)
				end
			end
			return waypointTable
		end
	end
end

function UpdateUserWaypoint(waypointId,waypointInfo)
	local worldName = GetWorldName()
	local subregionName = GetSubregionName()

	local customWaypoints = this:GetObjVar("UserWaypoints") or {}
	-- get world table
	if not(customWaypoints[worldName]) then
		customWaypoints[worldName] = {}
	end	
	local waypointTable = customWaypoints[worldName]

	-- get subregion table (if necesary)
	if(subregionName and subregionName ~= "") then
		if not(customWaypoints[worldName][subregionName]) then
			customWaypoints[worldName][subregionName] = {}
		end
		waypointTable = customWaypoints[worldName][subregionName]
	end

	-- update waypoint
	if not(waypointId) then
		if(not(IsImmortal(this)) and #waypointTable > 10) then
			this:SystemMessage("You have reached the maximum limit of waypoints for this map. (10)")
			return
		end
		table.insert(waypointTable,waypointInfo)
	elseif not(waypointInfo) then
		table.remove(waypointTable,waypointId)
	else
		waypointTable[waypointId] = waypointInfo
	end
	this:SetObjVar("UserWaypoints",customWaypoints)
end

function ShowMapWindow(mapName)	
	mapName = mapName or curMap

	local dynWindow = DynamicWindow("MapWindow",mapName,924,924,-462,-462,"Transparent","Center")	

	local mapIcons = GetUserWaypoints()
	
	if(mapIcons) then
		for i,mapInfo in pairs(mapIcons) do
			mapInfo.Id = "Marker|"..i
		end
	end

	dynWindow:AddButton(920,22,"","",46,28,"","",true,"ScrollClose")
	
	dynWindow:AddMap(0,0,924,924,mapName,true,mapIcons)


	if(canZoom) then
		local disabledState = ""
		if not(isZoomed) then
			disabledState = "disabled"
		end
		dynWindow:AddButton(442,875,"ZoomOut","",24,24,"","",false,"Minus",disabledState)

		disabledState = ""
		if (isZoomed) then
			disabledState = "disabled"
		end
		dynWindow:AddButton(468,875,"ZoomIn","",24,24,"","",false,"Plus",disabledState)
	end

	this:OpenDynamicWindow(dynWindow)
	mapWindowOpen = true
	curMap = mapName
end

function HasMap(mapName)
	if(IsImmortal(this)) then
		return true
	end

	if not(ServerSettings.Misc.MustLearnMaps) then
		return true
	end

	local availableMaps = this:GetObjVar("AvailableMaps") or {}
	return availableMaps[mapName] ~= nil
end

RegisterEventHandler(EventType.ClientUserCommand,"map",
	function ()		
		local mapName = GetCurrentMapName()

		if(not(mapName) or not(HasMap(mapName))) then
			this:SystemMessage("You don't have a map of this area.")
		elseif(mapWindowOpen) then
			this:CloseDynamicWindow("MapWindow")
			mapWindowOpen = false
		else
			ShowMapWindow(mapName)
		end
	end)

curWaypointId = nil
curWaypointIcon = 1
curWaypointLoc = nil
function OpenEditWaypointWindow(name,loc)	
	loc = loc or curWaypointLoc

	local newWindow = DynamicWindow("EditWaypoint","Edit Waypoint",370,220)

	newWindow:AddLabel(20,8,"Name (limit 20 characters)",0,0,18)
	newWindow:AddTextField(10,30,328,20,"entry",name)
    newWindow:AddButton(264, 130, "Save", "Save")  

    local curX = 0
    local curY = 0
    for i,iconName in pairs(waypointIcons) do
    	local state = ""
    	if(curWaypointIcon == i) then
    		state = "pressed"
    	end
    	newWindow:AddButton(curX+15,curY+60,"WaypointIcon|"..i,"",40,40	,"","",false,"",state)
    	newWindow:AddImage(curX+17,curY+62,iconName,34,34)
    	curX = curX + 40
    	if(i == 8) then
    		curX = 0
    		curY = curY + 40
    	end
    end

    curWaypointLoc = loc
    this:OpenDynamicWindow(newWindow)    
end

RegisterEventHandler(EventType.DynamicWindowResponse,"EditWaypoint",
	function (user,buttonId,fieldData)
		if(buttonId == "Save") then
			local waypointName = fieldData.entry
			if (string.len(waypointName) <= 1) then
		 		this:SystemMessage("The waypoint name must be longer than 1 character.","info")
		 		OpenEditWaypointWindow(fieldData.entry)
		 		return
		 	end

			if (string.len(waypointName) > 20) then
		 		this:SystemMessage("The waypoint name must be less than 20 characters.","info")
		 		OpenEditWaypointWindow(fieldData.entry)
		 		return
		 	end

		 	if(#waypointName:gsub("[%a%d ]","") ~= 0) then
			    this:SystemMessage("The waypoint can only contain alphanumeric characters and spaces.","info")
			    OpenEditWaypointWindow(fieldData.entry)
		 		return
			end

			UpdateUserWaypoint(curWaypointId,{Tooltip=waypointName,Location=curWaypointLoc,Icon=waypointIcons[curWaypointIcon]})			
			ShowMapWindow()
		else
			local result = StringSplit(buttonId,"|")
			local action = result[1]
			local arg = result[2]
			if(action == "WaypointIcon") then
				curWaypointIcon = tonumber(arg)
				OpenEditWaypointWindow(fieldData.entry)
			end
		end
	end)

RegisterEventHandler(EventType.DynamicWindowResponse,"MapWindow",
	function (user,buttonId,fieldData)
		
		if(buttonId == "MapClick") then
			local args = StringSplit(fieldData.Location,",")
			local clickLoc = Loc(tonumber(args[1]),0,tonumber(args[2]))

			local options = {"Add Waypoint"}
			if(IsImmortal(this)) then
				table.insert(options,"Goto")
				table.insert(options,"Portal")
			end

			this:OpenCustomContextMenu("MapClickOptions","Action",options)
   			RegisterSingleEventHandler(EventType.ContextMenuResponse,"MapClickOptions",
   				function(user,optionStr)
   					if(optionStr == "Add Waypoint") then
   						curWaypointId = nil
   						OpenEditWaypointWindow("New Waypoint",clickLoc)
   					elseif(IsImmortal(this)) then
					   	if ( optionStr == "Goto" ) then
							this:SetWorldPosition(clickLoc)
						elseif ( optionStr == "Portal" ) then
							OpenTwoWayPortal(this:GetLoc(), clickLoc)
						end
					end
   				end)	
		elseif(buttonId == "ZoomOut" and canZoom) then
			isZoomed = false
			local mapName = GetCurrentMapName()
			ShowMapWindow(mapName)
		elseif(buttonId == "ZoomIn" and canZoom) then
			isZoomed = true
			local mapName = GetCurrentMapName()
			ShowMapWindow(mapName)		
		else
			local result = StringSplit(buttonId,"|")
			local action = result[1]
			local arg = result[2]
			if(action == "Marker") then
				local options = {"Edit Waypoint", "Remove Waypoint"}
				if(IsImmortal(this)) then
					table.insert(options,"Goto")
				end

				local mapIcons = GetUserWaypoints()
				local mapIconId = tonumber(arg)
				if(mapIcons[mapIconId]) then
					this:OpenCustomContextMenu("MarkerClickOptions","Action",options)
					RegisterSingleEventHandler(EventType.ContextMenuResponse,"MarkerClickOptions",
		   				function(user,optionStr)
		   					if(optionStr == "Edit Waypoint") then
		   						curWaypointId = mapIconId
		   						OpenEditWaypointWindow(mapIcons[mapIconId].Tooltip,mapIcons[mapIconId].Location)
		   					elseif(optionStr == "Remove Waypoint") then
		   						curWaypointId = mapIconId
		   						UpdateUserWaypoint(curWaypointId,nil)
		   						ShowMapWindow()
		   					elseif(optionStr == "Goto" and IsImmortal(this)) then
								this:SetWorldPosition(mapIcons[mapIconId].Location)
							end
		   				end)
				end
			end
		end

		if(not(buttonId) or buttonId == "") then
			mapWindowOpen = false
		end
	end)

RegisterEventHandler(EventType.Message,"OpenMapWindow",
	function (mapName)
		if(mapName == nil) then mapName = GetWorldName() end

		--ShowMapWindow(mapName)	
		this:SystemMessage("You don't have a map of this area.")
	end)

RegisterEventHandler(EventType.Timer,"UpdateMapMarkers",
	function ()
		local icons = GetMapMarkers()
		--DebugMessage("Yep")		
		--DebugMessage("-------------------------------------------")
		--DebugMessage(DumpTable(icons))
		--DebugMessage("Length of icons is "..tostring(#icons))
		
		--DebugMessage("SENDING")
		this:SendClientMessage("UpdateMapMarkers",icons)
		
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(4 + math.random()),"UpdateMapMarkers")
	end)

this:ScheduleTimerDelay(TimeSpan.FromSeconds(1+math.random()),"UpdateMapMarkers")