
function OnTrialLoad()
	local pledgeLevel = tonumber(this:GetAccountProp("ProductLevel") or "0")
	if(false) then --pledgeLevel > 0) then
		this:DelModule("trial_player")	
	end

	local expireDate = this:GetAccountProp("ExpireDate")
	if(expireDate and expireDate ~= "") then
        date = Convert.ToDateTime(expireDate)
        timeRemaining = (date - DateTime.Now)
        if(timeRemaining.Days < 3) then
        	messageStr = timeRemaining.Days .. " day(s)"
        	if(timeRemaining.Days < 1) then
        		messageStr = timeRemaining.Hours .. " hour(s)"
        	end
        	ClientDialog.Show{
			    TargetUser = this,
			    DialogId = "PledgeWarning",
			    TitleStr = "Warning",
			    DescStr = "Your trial account is expiring in "..messageStr.."[$2651]",
			    Button1Str = "Pledge Now",
			    ResponseFunc = function( user, buttonId )
						if(buttonId == 0) then
							this:SendClientMessage("OpenURL","http://shardsonline.com/pledge")
						end
					end
			}
        end
	end
end

RegisterEventHandler(EventType.DynamicWindowResponse,"PledgeUI",
	function (user,buttonId)
		if(buttonId == "Pledge") then
			this:SendClientMessage("OpenURL","http://shardsonline.com/pledge")
		end
	end)

CallFunctionDelayed(TimeSpan.FromSeconds(1),OnTrialLoad)