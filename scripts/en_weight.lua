require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_weight")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_weight", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Weight") then return end
		AlterEquipmentBonusStat(this, "BonusPenetration", 1)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)