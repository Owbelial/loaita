
TEXT_PER_LINE = 55
MAX_RETURNS = 22
MAX_TEXT_PER_PAGE = TEXT_PER_LINE*MAX_RETURNS
MAX_WORD_LENGTH = 11
WRITING_ANIMATION = "fishreel"
--[[function GetPageLength(text,offset)
	--DebugMessage("Yep")
	--LuaDebugCallStack("offset = "..tostring(offset))

	local subString = string.sub(text,offset,MAX_TEXT_PER_PAGE)
	local lengthOfString = string.len(subString)
	--local charCount = 0
	--local returnCount = 0
	--local lastReturn = 0
	local i = 1
	local line = 0
	--local testString = ""
	while i < lengthOfString or i < MAX_TEXT_PER_PAGE do
		local char = string.sub(text,i,i)
		if (i%TEXT_PER_LINE == 0 )then
			line = line + 1
		end
		if (char == "\n") then
			line = line + 1
		end
		if (line >= MAX_RETURNS) then
			return i
		end
		i = i + 1
		--[[
		testString = testString .. char
		charCount = charCount + 1
		lastReturn = lastReturn + 1
		if (char == "\n") then
			--DebugMessage("Return found: "..tostring(i))
			i = i + TEXT_PER_LINE - lastReturn
			lastReturn = 0
			--ebugMessage(i)
			returnCount = returnCount + 1
			--DebugMessage("returnCount is "..tostring(returnCount))
		end
		if (lastReturn > TEXT_PER_LINE) then
			lastReturn = 0
		end
		--DebugMessage(char)
		--DebugMessage("NOW:")
		--DebugMessage(i,i+TEXT_PER_LINE,MAX_TEXT_PER_PAGE)
		if (char == " " and (i) / TEXT_PER_LINE > MAX_RETURNS ) then
			--if (char ~= "\n") then 
			--	charCount = charCount + (MAX_TEXT_PER_PAGE-i)
			--end
			DebugMessage("correct string = "..testString)
			return charCount
		end
		
	end
	return MAX_TEXT_PER_PAGE
end]]--

--[[function CalcPageOffsets(text)
	local textLength = string.len(text)
	local MAX_PAGES = 500 --yeah go ahead and write a novel
	local indexes = {}
	local count = 1
	local quit = false
	local i = 1
	--DebugMessage("Starting page dump")
	while i < MAX_PAGES and not quit do
		--DebugMessage("Next Iteration")
		indexes[i] = count
		count = GetPageLength(text,count) + count
		--DebugMessage("=====================")
		--DebugMessage(indexes[i],count,i)
		i = i + 1
		--DebugMessage("count is "..tostring(count).."pageLength is "..tostring(GetPageLength(text,count)).." textLength is "..tostring(textLength))
		if (count + GetPageLength(text,count) > textLength) then
			indexes[i] = count
			quit = true
		end
	end
	this:SetObjVar("PageIndexes",indexes)
	return indexes
end--]]
--function GetLastPageLength(text,offset)
--	local subString = string.sub(text,math.max(0,offset-MAX_TEXT_PER_PAGE),offset)
--	local lengthOfString = string.len(subString)
--	local charCount = 0
--	for i=lengthOfString,1,-1 do
--		local char = string.sub(text,i,i)
--		charCount = charCount + 1
--		if (char == "\n") then
--			i = i - 50
--		end
--		if (i <= 0) then
--			DebugMessage(charCount)
--			return charCount
--		end
--	end
--	return MAX_TEXT_PER_PAGE
--end
if (initializer ~= nil) then
	--DebugMessage(initializer.Text)
	this:SetObjVar("ScrollContents",initializer.Text)
end

--[[function GetCurrentPageText(user,page)
	DebugMessage("YES IT'S RELOADING")
	local text = this:GetObjVar("ScrollContents")
	if (text == nil) then return end
	local length = string.len(text)
	local cursor = this:GetObjVar("PageIndexes")[page]
	local pageEnd = this:GetObjVar("PageIndexes")[page+1]
	if (cursor == nil) then
		LuaDebugCallStack("CURSOR IS NIL")
		cursor = 1
	end
	--DebugMessage("A")
	return string.sub(text,cursor,(pageEnd) or length)
end--]]

function GetCurrentPageText(user,lines,lineNumb)
	local resultText = ""
	local textColor = this:GetObjVar("TextColor") or "[000000]"
	for i=lineNumb,#lines,1 do
		if (lines[i] == "\n") then
			resultText = resultText.."\n"
		elseif ( lines[i] ~= nil ) then
			resultText = resultText ..lines[i]
		end
		if (i >= lineNumb + MAX_RETURNS) then
			break
		end
	end
	return textColor ..resultText.."[-]"
end

function CalcLines(text)
	local lines = {}
	local lengthOfString = string.len(text)
	local i = 1
	local charCount = 1
	local line = ""
	while i < lengthOfString or i < MAX_TEXT_PER_PAGE do
		local char = string.sub(text,i,i)
		line = line .. char
		if (charCount > TEXT_PER_LINE or char == "\n" or ((char == " " or char == "-"  or char == "," or char == ".") and charCount + MAX_WORD_LENGTH > TEXT_PER_LINE)) then
			if (char ~= "\n") then
				line = line .."\n"
			end
			--DebugMessage(line)
			table.insert(lines,line)
			charCount = 0
			line = ""
		end
		charCount = charCount + 1
		i = i + 1
	end
	local char = string.sub(text,i,i)
	line = line .. char
	table.insert(lines,line)
	return lines
end

function ShowContents(user,line)
	local window = DynamicWindow("NoteDialog",StripColorFromString(this:GetName()),545,751,-300,-300,"Transparent","Center")
	window:AddButton(498,28,"","",46,28,"","",true,"ScrollClose")
	local text = this:GetObjVar("ScrollContents") or ""
	local indexes = CalcLines(text)
	text = GetCurrentPageText(user,indexes,line)
	window:AddImage(-60,-60,"ScrollParchmentUIWindow",0,0)
	window:AddLabel(60,80,text,430,515,20,"left",false,false,"Kingthings_18")
	if (line > 1) then
		window:AddButton(10 ,585,"Decrease","",0,0,"","",false,"OrnateLeft")
	end
	if (line + MAX_RETURNS < #indexes) then
		window:AddButton(420,585,"Increase","",0,0,"","",false,"OrnateRight")
	end
	if (this:HasObjVar("MapMarker")) then
		user:SystemMessage("[$1946]")
		AddMapMarker(user,{Icon="marker_diamond1", Location=this:GetObjVar("MapMarker"), Map=this:GetObjVar("Region"), Tooltip=this:GetName().."'s' Location"},"NoteMapMarker"..this.Id)
	end
	user:OpenDynamicWindow(window,this)	
end

RegisterEventHandler(EventType.DynamicWindowResponse, "NoteDialog",
	function (user,buttonId)
		--DebugMessage("Returning")
		local text = this:GetObjVar("ScrollContents")
		if (text == nil) then return end
		local indexes = CalcLines(text)
		line = user:GetObjVar("Line") or 1
		if (buttonId == "Increase") then
			line = line + MAX_RETURNS + 1
			user:SetObjVar("Line",line)
			ShowContents(user,line)
			return
		elseif (buttonId == "Decrease") then
			line = line - MAX_RETURNS - 1
			user:SetObjVar("Line",line)
			ShowContents(user,line)
			return
		end
	end)


RegisterEventHandler(EventType.Message, "UseObject",
	function (user,usedType)
		--DebugMessage("Start here, usedType is "..tostring(usedType))
		if(usedType ~= "Use" and (usedType ~= "Examine" and usedType ~= "Write" and usedType ~= "Add Map Marker")) then return end
		if (usedType == "Write") then
			if (this:TopmostContainer() ~= user) then
				user:SystemMessage("[$1947]")
				return
			end
			--DebugMessage("Attempt to open the dialog.")
			OpenWriteDialog(user)
			return
		end
		if (usedType == "Add Map Marker") then
			if (this:TopmostContainer() ~= user) then
				user:SystemMessage("[$1948]")
				return
			end
			this:SetObjVar("MapMarker",user:GetLoc())
			this:SetObjVar("Region",GetWorldName())
			user:SystemMessage("[$1949]")
			return
		end

		if this:HasObjVar("AnotherLanguage") then 
			user:SystemMessage("[$1950]")
			return
		end
		user:SetObjVar("Line",1)
		ShowContents(user,1)
	end)

function OpenWriteDialog(user)
	local newWindow = DynamicWindow("WriteNoteWindow","Write Note",400,620,200,200,"Default","TopLeft")

	local currentContents = this:GetObjVar("ScrollContents")

	local x = 30

	newWindow:AddTextField(x, 15, 320,140+140+150+150, "WritingContents", currentContents)
	
	newWindow:AddButton(x, 526, "Write", "Write", 150, 0, "", "", false)
	newWindow:AddButton(x+170, 526, "Cancel", "Cancel", 150, 0, "", "", true)
	--newWindow:AddButton(x, y, "CancelImprovement", "Cancel", 200, 0, "Stop Attempting to Improve the item.", "", false)
	user:OpenDynamicWindow(newWindow,this)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"WriteNoteWindow",function (user,buttonId,contents)
	if (user == nil or not user:IsValid()) then
		return
	end
	if (buttonId == "Cancel") then
		return
	end
	if (buttonId == "Write") then
		local badWord = GetBadWord(contents.WritingContents)
		if (ServerSettings.Misc.EnforceBadWordFilter and badWord ~= nil) then
			user:SystemMessage("[$1951]"..badWord)
			return
		end
		local writeTime = math.min(5000,math.max(1000,contents.WritingContents:len()*4 + 1))
		--DebugMessage("Write time is "..tostring(writeTime))
		user:SystemMessage("You write the note.")
		user:CloseDynamicWindow("WriteNoteWindow")
		user:PlayAnimation(WRITING_ANIMATION)	
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(writeTime),"PreventExamine")
		SetMobileModExpire(user, "Disable", "Writing", true, TimeSpan.FromMilliseconds(writeTime))
		ProgressBar.Show
		{
			Label="Writing",
			Duration=(writeTime/1000),
			TargetUser = user,
			PresetLocation="AboveHotbar",
			CanCancel = false,
		}
		CallFunctionDelayed(TimeSpan.FromMilliseconds(writeTime),function()
			user:PlayAnimation("idle")
			this:SystemMessage("You finish writing the note.")
			this:SendMessage("WriteNote",contents.WritingContents)
		end)
	end
end)


RegisterEventHandler(EventType.Message, "DecipherReadObject", 
	function (user)
        --DebugMessage("Message received in tablet")
		ShowContents(user,1)
	end)


RegisterEventHandler(EventType.Message,"WriteNote",
	function(contents)
		this:SetObjVar("ScrollContents",contents)
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "note", 
    function ()
         SetTooltipEntry(this,"idol", "Perhaps you should inspect this item further.")
         if (this:HasObjVar("Rewriteable")) then
         	AddUseCase(this,"Write",false)
         	AddUseCase(this,"Add Map Marker",false)
         end
         AddUseCase(this,"Examine",true)
    end)