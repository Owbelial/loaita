
RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),function(...)
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(15),"CleanUp")
	--this:ScheduleTimerDelay(TimeSpan.FromSeconds(DURATION), "SizePotionTimer")
	this:SystemMessage("What do you wish to carve up?")

    this:RequestClientTargetGameObj(this, "SelectFoundersKnifeItem")
    RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"SelectFoundersKnifeItem",HandleClientResponse)
end)

function HandleClientResponse(target,user)

	if (not ValidateUse(target,user)) then
		CleanUp()
	elseif (not target:HasObjVar("FoundersKnifeItem")) then
        user:SystemMessage("There's nothing to harvest from that.")
        return
    else
    	this:PlayAnimation("carve")
    	ProgressBar.Show{
            Label="Harvesting",
            Duration=2,
            TargetUser=this
        }
    	FaceObject(this,target)
        CallFunctionDelayed(TimeSpan.FromSeconds(2),function() 
			CreateObjInBackpack(this,target:GetObjVar("FoundersKnifeItem"))
			user:SystemMessage("You harvested something.")
			target:DelObjVar("FoundersKnifeItem")
            CleanUp()
		end)
	end
end

function ValidateUse(target,user)
    if( user == nil or not(user:IsValid()) ) then
        return false
    end
    if( target == nil or not(target:IsValid()) ) then
        return false
    end
    local topmostTarget = target:TopmostContainer() or target
    local hasItem = HasItem(user,"item_founders_knife")

    if (hasItem == nil) then
    	user:SystemMessage("Where did the founder's knife go anyway?")
    	return false
    end

    if (topmostTarget:DistanceFrom(user) > OBJECT_INTERACTION_RANGE) then
    	user:SystemMessage("You need to be near the target.")
    	return false
    end

    if (not target:IsMobile()) then
        user:SystemMessage("You're just going to blunt the knife doing that.")
        return false
    end

    if (not IsDead(target)) then
        user:SystemMessage("It's still alive!")
        return false
    end

    return true
end

function CleanUp()
	--this:SystemMessage("The shrinking effect has worn off.")
	this:DelModule(GetCurrentModule())
end

RegisterEventHandler(EventType.Message, "HasDiedMessage", 
	function()
		CleanUp()
	end)

RegisterEventHandler(EventType.Timer,"CleanUp",function ( ... )
	CleanUp()
end)