require 'incl_player_names'
--module that attaches that opens the character customization window.
mInitializer = initializer

SelectedBodyType = 1
SelectedHairType = 1
SelectedHairColorType = 1
FullBody = false

function OpenCharacterWindow()
	if (IsMale(this)) then
		GenderBodyTable = CharacterCustomization.BodyTypesMale
		GenderHairTable = CharacterCustomization.HairTypesMale
	end
	if (IsFemale(this)) then
		GenderBodyTable = CharacterCustomization.BodyTypesFemale
		GenderHairTable = CharacterCustomization.HairTypesFemale
	end

	if (GenderHairTable == nil or GenderBodyTable == nil) then
		DebugMessage("[custom_char_window|charCustomWindow] ERROR: Player has unsupported gender/body type.")
		return
	end

	local dynWindow = DynamicWindow("charCustomWindow","Appearance",320,600,-160,-290,"Default","Center")

	if (FullBody == false) then
		dynWindow:AddImage(27-5,25-5+13,"SkillTracker_BG",255,255,"Sliced")
		dynWindow:AddPortrait(27,25+13,245,245,this,"head",false)		
		dynWindow:AddButton(61, 0, "FullBody", "Zoom Out", 180, 20, "Switch to seeing the full body.", "", false,"")
	else
		dynWindow:AddPortrait(60,20,180,270,this,"full",false)	
		dynWindow:AddButton(61, 0, "FullBody", "Zoom In", 180, 20, "Switch to zoom in on your head.", "", false,"")
	end
	dynWindow:AddImage(8,302,"BasicWindow_Panel",284,224,"Sliced")
	--body type tab
	dynWindow:AddImage(25,306+6,"SkillTracker_BG",250,55,"Sliced")
	dynWindow:AddLabel(155,310+10,"[F3F781]Body[-]",323,0,23,"center")
	dynWindow:AddButton(35,335-2,"BodyLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,335+10,GenderBodyTable[SelectedBodyType].Name,323,0,18,"center")
	dynWindow:AddButton(255,335-2,"BodyRight","",0,0,"","",false,"Next")
	--hair tab
	dynWindow:AddImage(25,361+6,"SkillTracker_BG",250,55,"Sliced")
	dynWindow:AddLabel(155,365+10,"[F3F781]Hair[-]",323,0,23,"center")
	dynWindow:AddButton(35,390-2,"HairLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,390+10,GenderHairTable[SelectedHairType].Name,323,0,18,"center")
	dynWindow:AddButton(255,390-2,"HairRight","",0,0,"","",false,"Next")
	--hair color
	dynWindow:AddImage(25,416+6,"SkillTracker_BG",250,55,"Sliced")
	dynWindow:AddLabel(155,420+10,"[F3F781]Hair Color[-]",323,0,23,"center")
	dynWindow:AddButton(35,445-2,"HairColorLeft","",0,0,"","",false,"Previous")
	dynWindow:AddLabel(155,445+10,CharacterCustomization.HairColorTypes[SelectedHairColorType].Name,323,0,18,"center")
	dynWindow:AddButton(255,445-2,"HairColorRight","",0,0,"","",false,"Next")

	dynWindow:AddButton(61, 485, "ChangeApperance", "Accept", 180, 30, "Change the apperance of your character.", "", true,"")
			
	this:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "charCustomWindow",
	function (user,buttonId)

		--DebugMessage("SelectedBodyType is "..tostring(SelectedBodyType))
		--DebugMessage("SelectedHairType is "..tostring(SelectedHairType))
		--DebugMessage("SelectedHairColorType is "..tostring(SelectedHairColorType))

		if (IsMale(this)) then
			GenderBodyTable = CharacterCustomization.BodyTypesMale
			GenderHairTable = CharacterCustomization.HairTypesMale
		end
		if (IsFemale(this)) then
			GenderBodyTable = CharacterCustomization.BodyTypesFemale
			GenderHairTable = CharacterCustomization.HairTypesFemale
		end

		if (GenderHairTable == nil or GenderBodyTable == nil) then
			DebugMessage("[custom_char_window|charCustomWindow] ERROR: Player has unsupported gender/body type.")
			return
		end

		if (buttonId == "FullBody") then
			FullBody = not FullBody
			OpenCharacterWindow()
		end
		if (buttonId == "BodyLeft") then
			SelectedBodyType = SelectedBodyType - 1--deincrement by one
			--change the body type
			if (SelectedBodyType > #GenderBodyTable) then SelectedBodyType = 1 end--if over the length set to 1
			if (SelectedBodyType < 1) then SelectedBodyType = #GenderBodyTable end--if less than 1 then set to length
			local currentEquipped = this:GetEquippedObject("BodyPartHead")
			--DebugMessage("currentEquipped is "..tostring(currentEquipped))
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			--create the new body type
			if (GenderBodyTable[SelectedBodyType].Template ~= nil) then
				CreateEquippedObj(GenderBodyTable[SelectedBodyType].Template, this, "created_body_type")--set the equipped object to the template in the index
			end
			--this:PlayEffect("TeleportToEffect")
			--reopen the character window
			OpenCharacterWindow()
		elseif (buttonId == "BodyRight") then
			SelectedBodyType = SelectedBodyType + 1--increment by one
			--change the body type
			if (SelectedBodyType > #GenderBodyTable) then SelectedBodyType = 1 end--if over the length set to 1
			if (SelectedBodyType < 1) then SelectedBodyType = #GenderBodyTable end--if less than 1 then set to length
			local currentEquipped = this:GetEquippedObject("BodyPartHead")
			--DebugMessage("currentEquipped is "..tostring(currentEquipped))
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			--create the new body type
			if (GenderBodyTable[SelectedBodyType].Template ~= nil) then
				CreateEquippedObj(GenderBodyTable[SelectedBodyType].Template, this, "created_body_type")--set the equipped object to the template in the index
			end
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenCharacterWindow()
		elseif (buttonId == "HairLeft") then
			SelectedHairType = SelectedHairType - 1--deincrement by one
			--change the hair type
			if (SelectedHairType > #GenderHairTable) then SelectedHairType = 1 end--if over the length set to 1
			if (SelectedHairType < 1) then SelectedHairType = #GenderHairTable end--if less than 1 then set to length
			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			--create the new hair type
			if (GenderHairTable[SelectedHairType].Template ~= nil) then
				CreateEquippedObj(GenderHairTable[SelectedHairType].Template, this, "created_hair")--set the equipped object to the template in the index
			end
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenCharacterWindow()
		elseif (buttonId == "HairRight") then
			SelectedHairType = SelectedHairType + 1--increment by one
			--change the hair type
			if (SelectedHairType > #GenderHairTable) then SelectedHairType = 1 end--if over the length set to 1
			if (SelectedHairType < 1) then SelectedHairType = #GenderHairTable end--if less than 1 then set to length
			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			--create the new hair type
			if (GenderHairTable[SelectedHairType].Template ~= nil) then
				CreateEquippedObj(GenderHairTable[SelectedHairType].Template, this, "created_hair")--set the equipped object to the template in the index
			end
			--this:PlayEffect("TeleportToEffect")
			OpenCharacterWindow()
			--reopen the character window
		elseif (buttonId == "HairColorLeft") then
			SelectedHairColorType = SelectedHairColorType - 1--deincrement by one
			--change the hair color
			if (SelectedHairColorType > #CharacterCustomization.HairColorTypes) then SelectedHairColorType = 1 end--if over the length set to 1
			if (SelectedHairColorType < 1) then SelectedHairColorType = #CharacterCustomization.HairColorTypes end--if less than 1 then set to length

			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			
			if (GenderHairTable[SelectedHairType].Template ~= nil) then
				CreateEquippedObj(GenderHairTable[SelectedHairType].Template, this, "created_hair")--set the equipped object to the template in the index
			end
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenCharacterWindow()
		elseif (buttonId == "HairColorRight") then
			SelectedHairColorType = SelectedHairColorType + 1--increment by one
			--change the hair color
			if (SelectedHairColorType > #CharacterCustomization.HairColorTypes) then SelectedHairColorType = 1 end--if over the length set to 1
			if (SelectedHairColorType < 1) then SelectedHairColorType = #CharacterCustomization.HairColorTypes end--if less than 1 then set to length

			local currentEquipped = this:GetEquippedObject("BodyPartHair")
			if (currentEquipped ~= nil) then
				currentEquipped:Destroy()
			end
			
			if (GenderHairTable[SelectedHairType].Template ~= nil) then
				CreateEquippedObj(GenderHairTable[SelectedHairType].Template, this, "created_hair")--set the equipped object to the template in the index
			end
			--reopen the character window
			--this:PlayEffect("TeleportToEffect")
			OpenCharacterWindow()
		elseif (buttonId == "ChangeApperance" or buttonId == "") then
			--just close the window
			this:CloseDynamicWindow("charCustomWindow")
			this:DelModule("custom_char_window")
			this:SendMessage("ClosedCustomCharWindow")
		end
	end)

RegisterEventHandler(EventType.ModuleAttached,"custom_char_window",
function()	
	OpenCharacterWindow()
end)

RegisterEventHandler(EventType.StartMoving, "" , 
	function ()
		this:CloseDynamicWindow("charCustomWindow")
		this:DelModule(GetCurrentModule())
		this:SendMessage("ClosedCustomCharWindow")
	end)

RegisterEventHandler(EventType.CreatedObject,"created_hair",
	function(user)
		local currentEquipped = this:GetEquippedObject("BodyPartHair")
		currentEquipped:SetHue(CharacterCustomization.HairColorTypes[SelectedHairColorType].Color)--set the equipped object to the template in the index
	end)