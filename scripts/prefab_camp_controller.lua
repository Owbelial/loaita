function ScheduleMobCheckTimer()
	--DebugMessage("---ScheduleMobCheckTimer")
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(10 + math.random()),"MobCheckTimer")
end

function HasMobs()
	local prefabObjects = this:GetObjVar("PrefabObjects")
	if(prefabObjects) then
		for i,prefabObject in pairs(prefabObjects) do
			if(prefabObject and prefabObject:IsValid() and prefabObject:IsMobile()) then
				--DebugMessage("---HasMobs true")
				return true
			end
		end
	end
	--DebugMessage("---HasMobs false")
end

function MobCheck()
	--DebugMessage("---MobCheck")
	if(HasMobs()) then
		ScheduleMobCheckTimer()
	else
		--DebugMessage("---No mobs fire decay")
		-- no mobs left in camp, trigger decay
		this:FireTimer("MobDecayTimer")
	end
end
RegisterEventHandler(EventType.Timer,"MobCheckTimer",function () MobCheck() end)

function CampSetup(noDecay)
	local prefabObjects = this:GetObjVar("PrefabObjects")
	if(prefabObjects) then
		for i,prefabObj in pairs(prefabObjects) do
			prefabObj:Destroy()
		end
	end

	campName = this:GetObjVar("PrefabName")
	if(campName) then
		if not(noDecay) then
			if(this:HasModule("spawn_decay")) then
				this:SendMessage("RefreshSpawnDecay")
			else
			    this:AddModule("spawn_decay")
			end
		end
    	CreatePrefab(campName,this:GetLoc(),Loc(0,0,0),"prefab_object_spawned",this)
    end
end

RegisterEventHandler(EventType.Message,"Reset",
	function (noDecay)
		CampSetup(noDecay ~= nil)
	end)

RegisterEventHandler(EventType.CreatedObject,"prefab_object_spawned",
    function (success,objRef,prefabController)
        if(success) then
        	if(objRef:IsMobile()) then
        		ScheduleMobCheckTimer()
        	end
            AddToListObjVar(prefabController,"PrefabObjects",objRef)
        end
    end)

RegisterEventHandler(EventType.Message,"Destroy",
	function ( ... )
		local prefabObjects = this:GetObjVar("PrefabObjects")
		if(prefabObjects) then
			for i,prefabObj in pairs(prefabObjects) do
				prefabObj:Destroy()
			end
		end

		this:Destroy()
	end)
