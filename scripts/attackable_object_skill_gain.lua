--[[
	Set the combat skill on the attackable object, 
		when players hit the object these set skills will be used to determine the gains.
]]

RegisterEventHandler(EventType.ModuleAttached,"attackable_object_skill_gain",
	function ( ... )

		-- make it look attackable
		AddUseCase(this,"Attack",true)

		if ( initializer ~= nil and initializer.SkillLevel ~= nil ) then
			this:SetObjVar("SkillDictionary", {
				BashingSkill = {
					SkillLevel = initializer.SkillLevel
				}
			})
		end
	end)