function GetPvPController()
	local controllerSearch = FindObjects(SearchModule("vs_pvp_controller"))
	if( #controllerSearch > 0 ) then
		local controller = controllerSearch[1]
		return controller
	end
end

function GetPvPGameState()
	local controller = GetPvPController()
	if( controller ~= nil ) then
		return controller:GetObjVar("GameState")
	end

	return "Prematch"
end

function AddPoints(team,points)
	local controller = GetPvPController()
	if( controller ~= nil ) then
		controller:SendMessage("addpoints",team,points)
	end
end

function AddLoyalty(playerObj,team,amount)
	local loyaltyVal = playerObj:GetObjVar(team.."Loyalty") or 0
  	playerObj:SetObjVar(team.."Loyalty",loyaltyVal + amount)
  	
  	local lifetimeLoyalty = playerObj:GetObjVar("LifetimePvPLoyalty") or 0
  	playerObj:SetObjVar("LifetimePvPLoyalty",lifetimeLoyalty + amount)
end