require 'incl_PvPEvent'

function HandleDeath(killer)
	if not( killer:IsPlayer() ) then return end

	local killerTeam = killer:GetObjVar("MobileTeamType")
	local myTeam = this:GetObjVar("MobileTeamType")
	if( killerTeam ~= nil and myTeam ~= nil and killerTeam ~= myTeam ) then
		local args = {myTeam,this:GetName()}
		CreateObjInBackpack(killer,"player_head","player_head",args)
		killer:SystemMessage("You cut off the head of your fallen foe.")

		AddPoints(killerTeam,5)
	end	
end

function HandleHeadCreated(success,headObj,args)
	if not(success) then return end

	local headTeam = args[1]
	local headName = args[2]

	headObj:SetName(StripColorFromString(headName).."'s bloody head")
	headObj:SetObjVar("MobileTeamType",headTeam)
end

RegisterEventHandler(EventType.Message, "HasDiedMessage", HandleDeath)
RegisterSingleEventHandler(EventType.ModuleAttached, "incl_PvPPlayer", HandleModuleLoaded)
RegisterEventHandler(EventType.CreatedObject, "player_head", HandleHeadCreated)
RegisterSingleEventHandler(EventType.LoadedFromBackup, "", HandleLoadedFromBackup)