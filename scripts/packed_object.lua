require 'incl_keyhelpers'

function ValidateUse(user)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if (IsDead(user)) then
		return false
	end

	return true
end

userObj = nil

function RequestPlacementLoc(user)
	local unpackedTemplate = this:GetObjVar("UnpackedTemplate")	
	if( unpackedTemplate == nil ) then
		return
	end

	user:SystemMessage("Where do you wish to place this object?")
	user:RequestClientTargetLocPreview(this, "targetLoc",unpackedTemplate,Loc(0,0,0))
end

RegisterEventHandler(EventType.Message, "UseObject", 
	function(user,usedType)
		if(usedType ~= "Unpack" and usedType ~="Use") then return end
		
		if not(ValidateUse(user) ) then
			return
		end

		RequestPlacementLoc(user)

		userObj = user
	end)

RegisterEventHandler(EventType.ClientTargetLocResponse, "targetLoc", 
	function(success, targetLoc)
		-- validate target
		if( not(success)) then
			return
		end

		local unpackedTemplate = this:GetObjVar("UnpackedTemplate")	
		if( unpackedTemplate ~= nil ) then
			if (not GetContainingHouseForLoc(targetLoc)) then			
				userObj:SystemMessage("You can only unpack objects at a housing plot.","event")
				RequestPlacementLoc(userObj)
				return
			end
			if not(IsPassable(targetLoc)) then				
				userObj:SystemMessage("[$2380]","event")
				RequestPlacementLoc(userObj)
				return
			end

			userObj:PlayAnimation("kneel")		

			CreateObj(unpackedTemplate,targetLoc,"unpackedCreate")
		end
	end)

RegisterEventHandler(EventType.CreatedObject, "key", 
	function(success,objRef,user)
		this:Destroy()
	end)

RegisterEventHandler(EventType.CreatedObject, "unpackedCreate", 
	function(success,objRef)
		if( success ) then
			user = userObj
			objRef:SetObjVar("PackedTemplate", this:GetCreationTemplateId())
			objRef:AddModule("repackable_object")
			local houseControlObj = GetContainingHouseForObj(objRef)

			if(houseControlObj ~= nil and IsHouseOwner(userObj,houseControlObj)) then
				houseControlObj:SendMessage("ObjectPlaced",objRef,userObj)
			else
				--local decayTime = this:GetObjVar("UnpackedDecay")
				--if( decayTime ~= nil ) then
				objRef:SetObjVar("DecayTime",decayTime)
				objRef:AddModule("decay")
				--end
			end

			if (objRef:HasObjVar("ShouldCreateKey")) then
				-- if this container is being placed in a house
				if ( houseControlObj ~= nil ) then
					-- assign the houses key to work with this chest
					local houseLockUniqueId = houseControlObj:GetObjVar("HouseLockUniqueId")
					if ( houseLockUniqueId ~= nil ) then
						objRef:SetObjVar("lockUniqueId", houseLockUniqueId)
						-- lock the chest
						objRef:SendMessage("Lock")

						user:SystemMessage("[$2381]")
					else
						user:SystemMessage("[$2382]")
					end
				else
					-- not being placed in a house
					this:AddModule("create_key")
				end
			end
			
			this:Destroy()
		end
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function ()
         SetTooltipEntry(this,"packed", "Can be unpacked and placed in the world.")
         AddUseCase(this,"Unpack",true,"HasObject")
    end)