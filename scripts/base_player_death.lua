-- Completely skip this custom death stuff if the mob is possessed
if(IsPossessed(this)) then
	return
end

mBackpack = nil

function OnDeathStart()
	if(this:HasObjVar("OverrideDeath")) then
		return 
	end

	-- make IsDead() checks pass for this mobile
	this:SetObjVar("IsDead", true)
	-- gray out screen
	this:PlayLocalEffect(this,"GrayScreenEffect")
	this:PlayMusic("Death")
	this:SetCloak(true)

	this:SystemMessage("[D70000]You have died![-]","event")
	this:SystemMessage("[$1681]")
	this:SetMobileFrozen(true,true)
	CallFunctionDelayed(TimeSpan.FromSeconds(2),function ( ... )
		this:SetMobileFrozen(false,false)
	end)

	this:PlayAnimation("roar")

	-- create their corpse
	local corpseTemplate = GetTemplateData("player_corpse")

	if not(ServerSettings.PlayerInteractions.FullItemDropOnDeath) or IsInitiate(this) then
		corpseTemplate.ObjectVariables.NoDisrobe = true
	end

	-- sorta messy
	corpseTemplate.ClientId = this:GetIconId()
	corpseTemplate.ScaleModifier = this:GetScale().X
	corpseTemplate.Hue = this:GetHue()

	-- put proper ownership text to the name for the corpse.
	local name = this:GetName()
	if ( string.sub(name, -1, -1) == "s" ) then
		name = name .. "'"
	else
		name = name .. "'s"
	end
	corpseTemplate.Name = ColorizePlayerName(this, name .. " Corpse")
	CreateCustomObj("player_corpse", corpseTemplate, this:GetLoc(), "created_corpse")
end

RegisterEventHandler(EventType.CreatedObject, "created_corpse", function (success, objRef)
		if (success) then
			-- set a refrence to the player this corpse belongs to, allows resurrecting the player by targeting their corpse.
			objRef:SetObjVar("PlayerObject", this)
			-- visa versa.
			this:SetObjVar("CorpseObject", objRef)
			this:SetObjVar("LastDeathRegion",GetRegionAddress())													-- takeEquipment			

			objRef:SetMobileFrozen(true,true)
			objRef:PlayAnimation("die")

			objRef:SetObjVar("BackpackOwner", this)
			SetKarma(objRef, GetKarma(this) or 0)
			local allegiance = GetAllegianceId(this)
			if ( allegiance ~= nil ) then
				objRef:SetObjVar("Allegiance", allegiance)
			end
			-- freeze the current conflict table on the corpse
			FreezeConflictTable(this, objRef)
			-- clear the conflict table
        	SetAllRelationsGuardIgnore(this)
			
			-- set copy as corpse
			objRef:SetSharedObjectProperty("IsDead", true)
			objRef:SendMessage("CheckSpawns",this)
			-- set what happens when you interact in the default way (left click for example)
			objRef:SetSharedObjectProperty("DefaultInteraction", "Open Pack")

			
			local shouldHueEquipment = not(ServerSettings.PlayerInteractions.FullItemDropOnDeath) or IsInitiate(this)
			TurnPlayerIntoGhost(shouldHueEquipment)

			if not(IsInitiate(this)) then
				if (ServerSettings.PlayerInteractions.FullItemDropOnDeath) then
					for i,equipObj in pairs(this:GetAllEquippedObjects()) do
						local slot = GetEquipSlot(equipObj)
						-- can't do the backpack this way because we need the createdobject event for it
						if ( slot ~= "TempPack" and slot ~= "Backpack" and slot ~= "Bank" and not(slot:match("BodyPart")) and not(equipObj:HasModule("blessed")) ) then
							local theloc = GetRandomDropPosition(objRef)
							objRef:EquipObjectWithLoc(equipObj,theloc)						
						end
					end

					local backpack = this:GetEquippedObject("Backpack")
					if (backpack ~= nil) then
						local backpackObjects = backpack:GetContainedObjects()
						for index,object in pairs(backpackObjects) do
							if not( object:HasModule("blessed") ) then				  		
						  		local theloc = GetRandomDropPosition(objRef)
						 		object:MoveToContainer(objRef,theloc)
						 	end
						end
					end
				else
					CreateEquippedObj("backpack",objRef,"backpack_corpse_creation")
				end
			end
		end		
	end)

RegisterEventHandler(EventType.CreatedObject,"backpack_corpse_creation",function (success, objRef )
	--move all my stuff to the corpse container
	local backpack = this:GetEquippedObject("Backpack")
	local corpseBackpack = objRef
	if ( corpseBackpack and backpack ~= nil) then
		local backpackObjects = backpack:GetContainedObjects()
		for index,object in pairs(backpackObjects) do
		  	local theloc = object:GetLoc()
		 	object:MoveToContainer(corpseBackpack,theloc)
		end
	end
end)

-- corpse should only be passed when the resurrector is resurrecting a corpse vs resurrecting a ghost.
function OnDeathEnd( resurrector, corpse, force )
	this:CloseDynamicWindow("ResurrectDialog")
	
	this:SetMobileFrozen(true,true)
	if ( corpse ~= nil ) then
		-- the player was resurrected from their corpse, so..

		-- let's validate the corpse.
		if ( corpse ~= this:GetObjVar("CorpseObject") ) then
			-- corpses don't match
			return
		end

		if not( corpse:HasObjVar("PlayerObject") ) then
			if ( IsPlayerCharacter(resurrect) ) then
				resurrector:SystemMessage("They have already been resurrected.", "info")
			end
			return
		end

		if ( force ) then
			ResurrectCorpse(corpse)
			return
		end

		-- ask them if they want to be resurrected at their corpse
		ClientDialog.Show{
		    TargetUser = this,
		    DialogId = "ResurrectDialog",
		    TitleStr = "You are being resurrected.",
		    DescStr = "[$1683]",
		    Button1Str = "Yes",
		    Button2Str = "No",
		    ResponseFunc = function (user, buttonId)
				buttonId = tonumber(buttonId)
				this:SetMobileFrozen(false, false)
				if( buttonId == 0) then
					ResurrectCorpse(corpse)
				end
			end
		}

	else
		-- player was resurrected from their ghost.
		this:PlayEffect("ResurrectEffect", 1.5)

		-- remove PlayerObject from their corpse (if existant) preventing a resurrect on a corpse after they have been resurrected from their ghost.
		local oldCorpse = this:GetObjVar("CorpseObject")
		if ( oldCorpse ~= nil and oldCorpse:IsValid()) then
			oldCorpse:DelObjVar("PlayerObject")
			-- remove 'appearance' items if not full loot
			if not ( ServerSettings.PlayerInteractions.FullItemDropOnDeath ) then
			    local leftHand = oldCorpse:GetEquippedObject("LeftHand")
			    local rightHand = oldCorpse:GetEquippedObject("RightHand")
			    local chest = oldCorpse:GetEquippedObject("Chest")
			    local legs = oldCorpse:GetEquippedObject("Legs")
			    local head = oldCorpse:GetEquippedObject("Head")
			    local body = oldCorpse:GetEquippedObject("BodyPartHead")
			    local hair = oldCorpse:GetEquippedObject("BodyPartHair")

			  	if ( leftHand ~= nil ) then leftHand:Destroy() end
			  	if ( rightHand ~= nil ) then rightHand:Destroy() end
			  	if ( chest ~= nil ) then chest:Destroy() end
			  	if ( legs ~= nil ) then legs:Destroy() end
			  	if ( head ~= nil ) then head:Destroy() end
			  	if ( body ~= nil) then body:Destroy() end
			  	if ( hair ~= nil) then hair:Destroy() end
			
				-- turn the corpse into a skeleton
				oldCorpse:SetAppearanceFromTemplate("skeleton")
				oldCorpse:DelObjVar("NoSkele")
			end
		end
		EndDeath()
	end
end

function ResurrectCorpse(corpse)
	-- cloak the player
	this:SetCloak(true)

	-- play resurrect effect
	corpse:PlayEffect("ResurrectEffect")

	-- make corpse stand up
	corpse:SetSharedObjectProperty("IsDead", false)
	--give them their health.
	SetCurHealth(this,GetMaxHealth(this))
	-- move the player to the location of their corpse.
	this:SetWorldPosition(corpse:GetLoc())

	-- move equipment back if it was taken
	if ( ServerSettings.PlayerInteractions.FullItemDropOnDeath ) then

	    local leftHand = corpse:GetEquippedObject("LeftHand")
	    local rightHand = corpse:GetEquippedObject("RightHand")
	    local chest = corpse:GetEquippedObject("Chest")
	    local legs = corpse:GetEquippedObject("Legs")
	    local head = corpse:GetEquippedObject("Head")

	  	if ( leftHand ~= nil ) then this:EquipObject(leftHand) end
	  	if ( rightHand ~= nil ) then this:EquipObject(rightHand) end
	  	if ( chest ~= nil ) then this:EquipObject(chest) end
	  	if ( legs ~= nil ) then this:EquipObject(legs) end
	  	if ( head ~= nil ) then this:EquipObject(head) end
	end
	
	local backpack = this:GetEquippedObject("Backpack")
	if ( backpack == nil ) then
		-- TODO
		-- no backpack, should we stop it? I mean all the items are going to be destoyed..
	end
	-- move the rest of the corpses contents back into their backpack
	if ( backpack ~= nil ) then
		local corpseObjects = corpse:GetContainedObjects()
		for index,object in pairs(corpseObjects) do
			local slot = GetEquipSlot(object)
			if ( slot == nil or ( slot ~= "TempPack" and slot ~= "Bank" and not slot:match("BodyPart")) ) then
				object:MoveToContainer(backpack,object:GetLoc())
			end
		end
	end

	CallFunctionDelayed(TimeSpan.FromMilliseconds(400), function()
		--destroy the corpse
		corpse:Destroy()
		-- uncloak player
		this:SetCloak(false)

		EndDeath()
	end)
end

function EndDeath()
	this:StopEffect("GrayScreenEffect")
	
	-- stop looking like a ghost
	ChangePlayerBackFromGhost()

	-- stop being treated like a ghost
	this:DelObjVar("IsDead")
	
	this:SendMessage("SetFullLevelPct",50)

	SetMobileMod(this, "HealthRegenPlus", "Death", nil)
	SetMobileMod(this, "ManaRegenPlus","Death", nil)
	SetMobileMod(this, "StaminaRegenPlus","Death", nil)
	SetMobileMod(this, "VitalityRegenPlus","Death", nil)

	this:SetMobileFrozen(false, false)

	if ( ServerSettings.Vitality.AdjustOnDeathEnd and not IsInitiate(this) and GetCurVitality(this) > 0 ) then
        AdjustCurVitality(this, ServerSettings.Vitality.AdjustOnDeathEnd)
	end
end

GHOST_HUE = "0xC100FFFF"
function TurnPlayerIntoGhost(shouldHueEquipment)
	-- force them out of combat
	this:SendMessage("EndCombatMessage")
	-- make them look like a ghost
	local hueTable = {}
	hueTable.SelfHue = this:GetHue()
	this:SetHue(GHOST_HUE)

	if (this:GetEquippedObject("BodyPartHair") ~= nil) then
		--DebugMessage("Yessir")
		hueTable.HairHue = this:GetEquippedObject("BodyPartHair"):GetHue()
		this:GetEquippedObject("BodyPartHair"):SetObjVar("OldHue",hueTable.HairHue)
		this:GetEquippedObject("BodyPartHair"):SetHue(GHOST_HUE)
	end

	if(shouldHueEquipment) then
		if (this:GetEquippedObject("Chest") ~= nil) then
			hueTable.ChestHue = this:GetEquippedObject("Chest"):GetHue()
			this:GetEquippedObject("Chest"):SetObjVar("OldHue",hueTable.ChestHue)
			this:GetEquippedObject("Chest"):SetHue(GHOST_HUE)
		end
		if (this:GetEquippedObject("Legs") ~= nil) then
			hueTable.LegsHue = this:GetEquippedObject("Legs"):GetHue()
			this:GetEquippedObject("Legs"):SetObjVar("OldHue",hueTable.LegsHue)
			this:GetEquippedObject("Legs"):SetHue(GHOST_HUE)
		end
		if (this:GetEquippedObject("Head") ~= nil) then
			hueTable.HeadHue = this:GetEquippedObject("Head"):GetHue()
			this:GetEquippedObject("Head"):SetObjVar("OldHue",hueTable.HeadHue)
			this:GetEquippedObject("Head"):SetHue(GHOST_HUE)
		end		
		if (this:GetEquippedObject("RightHand") ~= nil) then
			--DebugMessage("Yessir")
			hueTable.RightHand = this:GetEquippedObject("RightHand"):GetHue()
			this:GetEquippedObject("RightHand"):SetObjVar("OldHue",hueTable.RightHand)
			this:GetEquippedObject("RightHand"):SetHue(GHOST_HUE)
		end
		if (this:GetEquippedObject("LeftHand") ~= nil) then
			--DebugMessage("Yessir")
			hueTable.LeftHand = this:GetEquippedObject("LeftHand"):GetHue()
			this:GetEquippedObject("LeftHand"):SetObjVar("OldHue",hueTable.LeftHand)
			this:GetEquippedObject("LeftHand"):SetHue(GHOST_HUE)
		end
	end

	this:SetObjVar("OldHues",hueTable)
	this:SetObjVar("IsGhost",true)
	this:SetCloak(true)
end

function ChangePlayerBackFromGhost()
	local hueTable = this:GetObjVar("OldHues")
	if (hueTable ~= nil) then
		if (this:GetEquippedObject("Chest") ~= nil and hueTable.ChestHue) then
			this:GetEquippedObject("Chest"):SetHue(hueTable.ChestHue)
		end
		if (this:GetEquippedObject("Legs") ~= nil and hueTable.LegsHue) then
			this:GetEquippedObject("Legs"):SetHue(hueTable.LegsHue)
		end
		if (this:GetEquippedObject("Head") ~= nil and hueTable.HeadHue) then
			this:GetEquippedObject("Head"):SetHue(hueTable.HeadHue)
		end
		if (this:GetEquippedObject("BodyPartHair") ~= nil and hueTable.HairHue) then
			this:GetEquippedObject("BodyPartHair"):SetHue(hueTable.HairHue)
		end
		if (this:GetEquippedObject("LeftHand") ~= nil and hueTable.LeftHand) then
			this:GetEquippedObject("LeftHand"):SetHue(hueTable.LeftHand)
		end
		if (this:GetEquippedObject("RightHand") ~= nil and hueTable.RightHand) then
			this:GetEquippedObject("RightHand"):SetHue(hueTable.RightHand)
		end
		this:SetHue(hueTable.SelfHue)
	else
		DebugMessage("[base_player_death] ERROR: Could not load player hue table on death! Resetting hues!")
		if (this:GetEquippedObject("Chest") ~= nil) then
			this:GetEquippedObject("Chest"):SetHue(this:GetEquippedObject("Chest"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		if (this:GetEquippedObject("Legs") ~= nil) then
			this:GetEquippedObject("Legs"):SetHue(this:GetEquippedObject("Legs"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		if (this:GetEquippedObject("Head") ~= nil) then
			this:GetEquippedObject("Head"):SetHue(this:GetEquippedObject("Head"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		if (this:GetEquippedObject("BodyPartHair") ~= nil) then
			this:GetEquippedObject("BodyPartHair"):SetHue(this:GetEquippedObject("BodyPartHair"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		if (this:GetEquippedObject("LeftHand") ~= nil) then
			this:GetEquippedObject("LeftHand"):SetHue(this:GetEquippedObject("LeftHand"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		if (this:GetEquippedObject("RightHand") ~= nil) then
			this:GetEquippedObject("RightHand"):SetHue(this:GetEquippedObject("RightHand"):GetObjVar("OldHue") or "0xFFFFFFFF")
		end
		this:SetHue("0xFFFFFFFF")
	end
	this:DelObjVar("OldHues")
	this:DelObjVar("IsGhost")
	this:SetCloak(false)
end

baseMobileDeath = DoMobileDeath
function DoMobileDeath(damager)

	--controllo twotowers
	if(this:HasObjVar("OverrideDeath")) then
		SetCurHealth(this,GetMaxHealth(this))
		SetCurMana(this,GetMaxMana(this))
		SetCurStamina(this,GetMaxStamina(this))
		this:PlayEffect("HealEffect")

			local bindRegionAddress = "TwoTowers"
            local popuprandom = math.random(1,10)
			local bindLoc = nil

			if popuprandom == 1 then
			bindLoc = Loc(-59.76, 0.00, -11.65)
			elseif popuprandom == 2 then
			bindLoc = Loc(-52.09, 0.00, -1.93)
			elseif popuprandom == 3 then
			bindLoc = Loc(-39.97, 0.00, 4.93)
			elseif popuprandom == 4 then
			bindLoc = Loc(14.00, 0.00, 14.72)
			elseif popuprandom == 5 then
			bindLoc = Loc(39.57, 0.00, -29.14)
			elseif popuprandom == 6 then
			bindLoc = Loc(30.06, 0.00, -89.44)
			elseif popuprandom == 7 then
			bindLoc = Loc(-33.53, 0.00, -99.19)
			elseif popuprandom == 8 then
			bindLoc = Loc(-76.23, 0.00, -76.56)
			elseif popuprandom == 9 then
			bindLoc = Loc(-38.39, 0.00, -82.20)
			elseif popuprandom == 10 then
			bindLoc = Loc(21.95, 0.00, -30.16)
			end

			TeleportUser(this, this, bindLoc, bindRegionAddress)

    		--local bohId = uuid()

			local vittimapuntid = this:GetObjVar("TwoTowersDeaths")
			local vittimapuntik	= this:GetObjVar("TwoTowersKills")

			local killerpuntid 	= damager:GetObjVar("TwoTowersDeaths")
			local killerpuntik	= damager:GetObjVar("TwoTowersKills")

			damager:SetObjVar("TwoTowersKills",killerpuntik + 1)
			this:SetObjVar("TwoTowersDeaths",vittimapuntid + 1)

			--local vittimakda = truediv(vittimapuntik / vittimapuntid)
			--local killerkda = truediv(killerpuntik ,killerpuntid)

			--if killerkda >= GlobalVarRead("PrimoPostoTW") then 
			--SetGlobalVar("PrimoPostoTW", bohId, killerkda)
			--SetGlobalVar("PrimoPostoTWNome",bohId, damager:GetName())
			--elseif killerkda >= GlobalVarRead("SecondoPostoTW") then 
			--SetGlobalVar("SecondoPostoTW",bohId, killerkda)
			--SetGlobalVar("SecondoPostoTWNome",bohId, damager:GetName())
			--elseif killerkda >= GlobalVarRead("TerzoPostoTW") then 
			--SetGlobalVar("TerzoPostoTW",bohId, killerkda)
			--SetGlobalVar("TerzoPostoTWNome", bohId,damager:GetName())
			--end

			--if vittimakda >= GlobalVarRead("PrimoPostoTW") then 
			--SetGlobalVar("PrimoPostoTW",bohId, vittimakda)
			--SetGlobalVar("PrimoPostoTWNome",bohId, this:GetName())
			--elseif vittimakda >= GlobalVarRead("SecondoPostoTW") then 
			--SetGlobalVar("SecondoPostoTW",bohId, vittimakda)
			--SetGlobalVar("SecondoPostoTWNome",bohId, this:GetName())
			--elseif vittimakda >= GlobalVarRead("TerzoPostoTW") then 
			--SetGlobalVar("TerzoPostoTW",bohId,vittimakda)
			--SetGlobalVar("TerzoPostoTWNome", bohId,this:GetName())
			--end

			this:SystemMessage("KD Ratio:"..this:GetObjVar("TwoTowersKills").."/"..this:GetObjVar("TwoTowersDeaths").."", "info")
			damager:SystemMessage("KD Ratio:"..damager:GetObjVar("TwoTowersKills").."/"..damager:GetObjVar("TwoTowersDeaths").."", "info")
		return 
	end
--controllo twotowers
	baseMobileDeath(damager)

	mBackpack = this:GetEquippedObject("Backpack")

	-- close all open container windows in the players backpack
	if (mBackpack ~= nil) then
		CloseContainerRecursive(this,mBackpack)
	end

	-- move the carried object back to the backpack
	local carriedObject = this:CarriedObject()
	if(carriedObject ~= nil) then
		local randomLoc = GetRandomDropPosition(mBackpack)
		if not(TryPutObjectInContainer(carriedObject, mBackpack, randomLoc)) then
			carriedObject:SetWorldPosition(this:GetLoc())
		end
	end
	
	OnDeathStart()
end

RegisterEventHandler(EventType.Message, "PlayerResurrect", 
	function ( resurrector, corpse, force )
		OnDeathEnd( resurrector, corpse, force )
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )		
		if(IsDead(this)) then
			if(this:HasObjVar("OverrideDeath")) then
				return 
			end
						
			CallFunctionDelayed(TimeSpan.FromSeconds(0.5),
			function ( ... )
				this:PlayLocalEffect(this,"GrayScreenEffect")
			end)				
		end
	end)
