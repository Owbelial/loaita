require 'incl_player_titles'

RegisterEventHandler(EventType.ModuleAttached,"autofix",
	function ()		
		-- Remove bugged magical guides
		if not(this:HasModule("npe_player")) then
			for i,ownedObj in pairs(this:GetOwnedObjects()) do 
				if(ownedObj:GetCreationTemplateId() == "npe_magical_guide") then
					ownedObj:Destroy()
				end
			end
		end

		--DebugMessage("YES IT IS WORKING MUAHAAHAHAHAHAH")
		--Remove the salvage skill
		local mySkillsTable = this:GetObjVar("SkillDictionary") or {}
		local newSkillsTable = {}
		for keys,j in pairs(mySkillsTable) do
			--DebugMessage("Keys is "..tostring(keys))
			if (SkillData.AllSkills[keys] ~= nil) then
				--DebugMessage("Addings")
				newSkillsTable[keys] = j
			end
			--DebugMessage("NEXT")
		end
		this:SetObjVar("SkillDictionary",newSkillsTable)

		local questTable = this:GetObjVar("QuestTable") or {}
		local newQuestTable = {}
		for i,j in pairs(questTable) do
			local mRemove = false
			if (j.QuestName == "BlacksmithIntroQuest") then
				mRemove = true
				this:DelObjVar("Intro|Samogh the Blacksmith")
			end
			if (j.QuestName == "TailorIntroQuest") then
				mRemove = true
				this:DelObjVar("Intro|Robert the Tailor")
			end
			if (j.QuestName == "WoodsmithIntroQuest") then
				mRemove = true
				this:DelObjVar("Intro|John the Carpenter")
    			this:DelObjVar("Intro|Issac the Carpenter")
			end
			if (not mRemove) then
				table.insert(newQuestTable,j)
			end
		end
		this:SetObjVar("questTable",newQuestTable)
	
		PlayerTitles.Remove(this,"GuildTitle")
		
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"AutofixDetach")

		if not(this:HasObjVar("CB1Fix")) then
			this:SetObjVar("CB1Fix",true)

			SetWill(this,10)
			SetWis(this,10)
			SetCon(this,10)

			SetKarma(this, ServerSettings.Karma.NewPlayerKarma)

			this:SetStatValue("PrestigeXP",0)

			local hotbarActions = this:GetObjVar("HotbarActions")
			if(hotbarActions) then
				for slotIndex,action in pairs(hotbarActions) do
					this:SendMessage("RemoveUserActionFromSlot",action.Slot)
				end
			end

			for i,equippedObj in pairs(this:GetAllEquippedObjects()) do
				FixCB1Item(equippedObj)
			end
		end

		-- DAB TODO: Re-enable this when we find out whats wrong with SetAccountProp
		--TryAwardFamiliar()

	end)

function FixCB1Item(equippedObj)
	SetItemTooltip(equippedObj)

	if(equippedObj:IsContainer()) then
		for i,contObj in pairs(equippedObj:GetContainedObjects()) do
			FixCB1Item(contObj)
		end
	end
end

function TryAwardFamiliar()
	if not(IsOfficialShard()) then
		return
	end	

	local isKSBacker = this:GetAccountProp("KickstarterBacker") ~= nil
	if(isKSBacker) then
		isRedeemed = this:GetAccountProp("KickstarterFamiliarRedeemed") ~= nil		
		if not(isRedeemed) then
			if not(CanSetAccountProp()) then
				DebugMessage("ERROR: Attempted to grant familiar but login server was unavailable. UserId: "..GetAttachedUserId()..", Award: "..awardType..", Info: "..DumpTable(awardData))
			end
			this:SetAccountProp("KickstarterFamiliarRedeemed","True")
			CreateObjInBackpack(this,"baby_dragon_statue")
			this:SystemMessage("A baby dragon familiar statue has been created in your backpack for your service.")
			this:SystemMessage("A baby dragon familiar statue has been created in your backpack for your service.","event")
		end
	end
end

RegisterEventHandler(EventType.Timer,"AutofixDetach",
	function()
		this:DelModule("autofix")
	end)