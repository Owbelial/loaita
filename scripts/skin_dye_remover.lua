
function DyeRemoverDialogReponse(user,buttonId)	
	buttonId = tonumber(buttonId)
	target = this:GetObjVar("DyeTarget")

	if( target == nil or not target:IsValid()) then
		return
	end

	if (buttonId == 0 and ValidateUse(user,target)) then	
		target:SetHue("0xFFFFFF")
		user:SystemMessage("You have successfully undyed yourself.")
		this:Destroy()	
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "dyeSkin",
	function(target,user)

		if (not ValidateUse(user,target)) then return end

		this:SetObjVar("DyeTarget",target)

		ClientDialog.Show{
			TargetUser = user,
			DialogId = "DyeClothing",
			TitleStr = "Dye Clothing",
			DescStr = "Do you wish to remove the skin dye on yourself?",
			ResponseFunc = DyeRemoverDialogReponse
		}
	end
)

function ValidateUse(user,target)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( target == nil or not target:IsValid()) then
		return false
	end

	if( this:TopmostContainer() ~= user ) then
		user:SystemMessage("[$2597]")
		return false
	end

	local isDyeable = (user == target)
	if (not isDyeable) then
		user:SystemMessage("You can only dye yourself with this dye.")
		return false
	end

	return true
end

RegisterEventHandler(EventType.Message, "UseObject", 
    function(user,usedType)
    	if(usedType ~= "Use") then return end

		this:SetObjVar("DyeTarget",target)

		newHue = colorcode
		--DebugMessage("RequestClientTargetGameObj")
		user:RequestClientTargetGameObj(this, "dyeSkin")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function ()
    	SetTooltipEntry(this,"dye", "Used to remove dye from your skin.")
    	AddUseCase(this,"Apply",true,"HasObject")
	end)