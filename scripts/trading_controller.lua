

TRADE_TIMEOUT = 30

mTraderA = this
mTraderB = nil

mPlayerA_Accept = false
mPlayerB_Accept = false

mTradingPouchA = nil
mTradingPouchB = nil

function CanTradeWith(trader,traderWith)
	if (trader == nil) then
		return false
	end
	if (traderWith == nil) then
		return false
	end
	if (not trader:IsValid()) then
		traderWith:SystemMessage("The person you were trading with dissapeared!")
		return false
	end
	if (not traderWith:IsValid()) then
		trader:SystemMessage("The person you were trading with dissapeared")
		return false
	end
	if (IsDead(trader)) then
		trader:SystemMessage("The dead don't trade with the living.")
		return false
	end
	if (IsDead(traderWith)) then
		trader:SystemMessage("The dead don't trade with the living.")
		return false
	end
	if (not traderWith:IsPlayer()) then
		trader:SystemMessage("[$2641]")
		return false
	end
	if (not trader:IsPlayer()) then
		traderWith:SystemMessage("[$2642]")
		return false
	end
	if (trader:DistanceFrom(traderWith) > OBJECT_INTERACTION_RANGE) then
		trader:SystemMessage("You strayed too far away to trade.")
		return false
	end
	
	--success
	return true
end

function MoveItemsBack()
	mTraderA = mTraderA or this
	mTraderB = mTraderB
	if (mTraderA ~= nil) then
		for i,item in pairs(mTradingPouchA:GetContainedObjects()) do
			local backpackObj = mTraderA:GetEquippedObject("Backpack")
  			local randomLoc = GetRandomDropPosition(backpackObj)
  			item:MoveToContainer(backpackObj,randomLoc)
		end
	end
	if (mTraderB ~= nil) then
		for i,item in pairs(mTradingPouchB:GetContainedObjects()) do
			local backpackObj = mTraderB:GetEquippedObject("Backpack")
  			local randomLoc = GetRandomDropPosition(backpackObj)
  			item:MoveToContainer(backpackObj,randomLoc)
  		end
	end
end

function InterruptTrading()
	if (mTraderB ~= nil) then
		mTraderB:SystemMessage("The trade was cancelled.")
	end
	if (mTraderA ~= nil) then
		mTraderA:SystemMessage("The trade was cancelled.")
	end
	MoveItemsBack()
	CleanUp(true)
end

function CompleteTrade()
	--switch the items
	mTraderA = mTraderA or this
	mTraderB = mTraderB
	if (mTraderA ~= nil and mTraderA:IsValid()) and (mTraderB ~= nil and mTraderB:IsValid()) then
		mTraderA:SystemMessage("You complete the trade with "..mTraderB:GetName())
		mTraderA:SystemMessage("You complete the trade with "..mTraderB:GetName(),"event")
		mTraderB:SystemMessage("You complete the trade with "..mTraderA:GetName())
		mTraderB:SystemMessage("You complete the trade with "..mTraderA:GetName(),"event")
		--move the items from eachother's trading pouches.
		for i,item in pairs(mTradingPouchA:GetContainedObjects()) do
			local backpackObj = mTraderB:GetEquippedObject("Backpack")
  			local randomLoc = GetRandomDropPosition(backpackObj)
  			item:MoveToContainer(backpackObj,randomLoc)
		end
		for i,item in pairs(mTradingPouchB:GetContainedObjects()) do
			local backpackObj = mTraderA:GetEquippedObject("Backpack")
  			local randomLoc = GetRandomDropPosition(backpackObj)
  			item:MoveToContainer(backpackObj,randomLoc)
  		end
	else
		InterruptTrading()
	end
	CleanUp()
end

function CleanUp(redundant)
	if (not redundant) then
		MoveItemsBack()
	end
	
	if(mTradingPouchA:IsValid()) then
		if(mTradingPouchA:GetContainedObjects() == 0) then
			mTradingPouchA:Destroy()
		else
			DebugMessage("Attempt to cleanup trade with items still in trade container")
		end
	end
	if(mTradingPouchB:IsValid()) then
		if(mTradingPouchA:GetContainedObjects() == 0) then
			mTradingPouchB:Destroy()
		else
			DebugMessage("Attempt to cleanup trade with items still in trade container")
		end
	end

	if (mTraderA ~= nil and mTraderA:IsValid()) then
		mTraderA:CloseDynamicWindow("TradingWindow")
	end
	if (mTraderB ~= nil and mTraderB:IsValid()) then
		mTraderB:CloseDynamicWindow("TradingWindow")
	end
	this:DelModule("trading_controller")
end

function ShowTradingWindow(user)
	--DebugMessage("user is "..tostring(user:GetName()))
	local dynWindow = DynamicWindow("TradingWindow","Trade",485,200,-240,-100,"Transparent","Center")
	local isTraderA = (user == mTraderA)	
	local canAccept = false
	dynWindow:AddImage(0,0,"TradeWindow_Background",485,200,"Sliced")
	dynWindow:AddImage(485-245+10,-10+2,"TradeWindow_Arrow_TheirOffer",210,20,"Sliced")
	dynWindow:AddImage(45,190-2,"TradeWindow_Arrow_MyOffer",210,20,"Sliced")
	dynWindow:AddContainerScene(-115,-45,125,125,mTradingPouchA)
	dynWindow:AddContainerScene( 360,-45,125,125,mTradingPouchB)
	dynWindow:AddButton(190-7,132,"Cancel","Cancel",115,35,"Cancel the trade.",nil,false,"Default",result)
	local result = "default"
	if (user ~= mTraderA) then
		result = "disabled"
		if (not CanAddWeightToContainer(user:GetEquippedObject("Backpack"),GetContentsWeight(mTradingPouchA))) then
			dynWindow:AddLabel(240,100,"[D70000]Their offer is too heavy to accept.[-]",300,25,20,"center")
			mPlayerB_Accept = false
		end
		dynWindow:AddLabel(150-43+50,65,"[646496]Their Offer[-]",120,25,25,"center")
	else
		dynWindow:AddLabel(150-43+50,65,"[646496]My Offer[-]",120,25,25,"center")
	end
	dynWindow:AddButton(-35-5-10,161+8+20,"AcceptA","Accept",120,35,"Accept the trade",nil,false,"Invisible",result)
	dynWindow:AddImage(-35-5-15,161+8+20,"TradeWindow_AcceptedCheckBox",130,35,"Sliced")
	dynWindow:AddImage(100+45,80-30,"SectionDividerBar",200,0,"Sliced")
	dynWindow:AddImage(100+45,120,"InventoryDivider",200,0,"Sliced")
	result = "default"
	if (user ~= mTraderB) then
		result = "disabled"
		dynWindow:AddLabel(260+14+50,65,"[646496]Their Offer[-]",120,25,25,"center")
		if (not CanAddWeightToContainer(user:GetEquippedObject("Backpack"),GetContentsWeight(mTradingPouchB))) then
			dynWindow:AddLabel(240,100,"[D70000]Their offer is too heavy to accept.[-]",300,25,20,"center")
			mPlayerA_Accept = false
		end
	else
		dynWindow:AddLabel(260+14+50,65,"[646496]My Offer[-]",120,25,25,"center")
	end
	dynWindow:AddLabel(260+14+50-100+15,65-30-10,"Trade",120,25,25,"center")
	dynWindow:AddImage(485-35-8-5-15,161+8+20,"TradeWindow_AcceptedCheckBox",130,35,"Sliced")
	dynWindow:AddLabel(-35-5+10+20,161+8+20+7,"Accepted:",100,25,20,"center")
	dynWindow:AddLabel(485-35-8-5+10+10-7+20,161+8+20+7,"Accepted:",100,25,20,"center")
	dynWindow:AddButton(485-35-8-5-10,161+8+20,"AcceptB","Accept",120,35,"Accept the trade",nil,false,"Invisible",result)
	if (mPlayerA_Accept) then
		dynWindow:AddImage(-35-5+57+12+15,161+8+20+7,"Checkmark",24,24,"Sliced")
	end
	if (mPlayerB_Accept) then
		dynWindow:AddImage(485-35-8-5+57+12+15,161+8+20+7,"Checkmark",24,24,"Sliced")
	end
	user:OpenDynamicWindow(dynWindow,this)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"TradingWindow",
function (user,buttonId)
	--DebugMessage("BUTTON INPUT ----------------------------------------------")
	--DebugMessage("user is "..tostring(user))
	--DebugMessage("ButtonId is "..tostring(buttonId))	
	--Check to see if the user is valid
	--DebugMessage(1)
	if (user == nil or not user:IsValid()) then 
		--DebugMessage(2)
		CleanUp()
		return
	end
	local isTraderA = (user == mTraderA)
	--If they closed the window
	--DebugMessage(3)
	if (buttonId == "" or buttonId == nil) then
		--DebugMessage(4)
		CleanUp()
		return
	end
	if (not CanTradeWith(mTraderA,mTraderB)) then
		InterruptTrading()
		return
	end
	--if we try to do someone else's accept button do nothing.
	--DebugMessage(5)
	if (isTraderA and "AcceptB" == buttonId) then
		--DebugMessage(6)
		return
	elseif (not isTraderA) and "AcceptA" == buttonId then
		--DebugMessage(7)
		return 
	end
	--DebugMessage(8)
	--if we hit an except button that's ours
	if (buttonId == "AcceptA" or buttonId == "AcceptB") then
		--DebugMessage(9)
		if (isTraderA) then
			--DebugMessage(10)
			mPlayerA_Accept = not mPlayerA_Accept
		else	
			--DebugMessage(11)
			mPlayerB_Accept = not mPlayerB_Accept
		end
	end
	--If we cancel then clean up.
	if (buttonId == "Cancel") then
		--DebugMessage(12)
		InterruptTrading()
		return
	end
	--If we both accept then complete the trade
	--DebugMessage(13)
	if (mPlayerA_Accept and mPlayerB_Accept) then
		--DebugMessage(14)
		if (not CanAddWeightToContainer(mTraderA:GetEquippedObject("Backpack"),GetContentsWeight(mTradingPouchB))) then
			return
		end
		if (not CanAddWeightToContainer(mTraderB:GetEquippedObject("Backpack"),GetContentsWeight(mTradingPouchA))) then
			return
		end
		CompleteTrade()
		return
	end
	--DebugMessage(15)
	--Update the trading windows.
	ShowTradingWindow(mTraderA)
	ShowTradingWindow(mTraderB)
end)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),function ( ... )	
	if(initializer ~= nil) then
		InitiateTrade(initializer.TradeTarget)
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(TRADE_TIMEOUT),"CancelTradingTimer")
	else
		CleanUp()
	end	
end)

pendingTradeCreates = 0

function InitiateTrade(target)
	if (CanTradeWith(this,target)) then
	this:SystemMessage("You invite "..tostring(target:GetName()).." to trade.") 
		ClientDialog.Show{
			TargetUser = target,
			DialogId = "TradeInvite"..target.Id,
		    TitleStr = "Trading",
		    DescStr = this:GetName().." invites you to trade.",
		    Button1Str = "Accept",
		    Button2Str = "Decline",
		    ResponseObj= from,
		    ResponseFunc= function(user,buttonId)
				local buttonId = tonumber(buttonId)
				--DebugMessage("Guild: Handle Group Invite Response");
				if (user == nil) then 
					CleanUp()
					return
				end

				if (buttonId == nil) then 
					CleanUp()
					return
				end

				-- Handles the invite command of the dynamic window
				if (buttonId == 0) then
					--DebugMessage("Bam")
					mTraderA = this
					mTraderB = user
										
					pendingTradeCreates = 2
					CreateEquippedObj("backpack", user, "created_trading_pouch",user)
					CreateEquippedObj("backpack", this, "created_trading_pouch",this)					

					local searchDistanceFromSelf = SearchSingleObject(user,SearchObjectInRange(OBJECT_INTERACTION_RANGE))
					AddView("CheckInTradeProximity",searchDistanceFromSelf,1.0)
					mTraderA:SystemMessage(mTraderB:GetName().." accepts your trade request.")
					mTraderB:SystemMessage("You agree to trade with "..mTraderA:GetName())
					RegisterSingleEventHandler(EventType.LeaveView,"CheckInTradeProximity",
						function()
							CleanUp()
						end)

					this:RemoveTimer("CancelTradingTimer")
					return
				end
				this:SystemMessage(user:GetName().." does not wish to trade at this time.")
				user:SystemMessage("You decline to trade with "..this:GetName())
				--DebugMessage("Bam2")
				CleanUp()
			end
		}
	else
		CleanUp()
	end
end

function CancelAccept()
	mPlayerA_Accept = false
	mPlayerB_Accept = false
end

RegisterEventHandler(EventType.EnterView,"CheckBagsForNewItemsA",
	function()
		CancelAccept()
		ShowTradingWindow(mTraderA)
		ShowTradingWindow(mTraderB)
	end)
RegisterEventHandler(EventType.LeaveView,"CheckBagsForNewItemsA",
	function()
		CancelAccept()
		ShowTradingWindow(mTraderA)
		ShowTradingWindow(mTraderB)
	end)
RegisterEventHandler(EventType.EnterView,"CheckBagsForNewItemsB",
	function()
		CancelAccept()
		ShowTradingWindow(mTraderA)
		ShowTradingWindow(mTraderB)
	end)
RegisterEventHandler(EventType.LeaveView,"CheckBagsForNewItemsB",
	function()
		CancelAccept()
		ShowTradingWindow(mTraderA)
		ShowTradingWindow(mTraderB)
	end)
RegisterEventHandler(EventType.CreatedObject,"created_trading_pouch",function (success,objRef,user)
	--DebugMessage(2.5)
	if (success) then
		objRef:SetObjVar("TradingPouch",true)
		--DebugMessage(tostring(mTraderA).." is mTraderA")
		--DebugMessage(tostring(user).." is user")
		if (user == mTraderA) then
			--DebugMessage("mTradingPouchA")
			mTradingPouchA = objRef
		else
			--DebugMessage("mTradingPouchB")
			mTradingPouchB = objRef
		end
		--DebugMessage("A:",mTradingPouchA,mTradingPouchB)
		--DebugMessage(3)
		pendingTradeCreates = pendingTradeCreates - 1
		if(pendingTradeCreates == 0) then	
			-----------------NOTE: MUST BE DONE BEFORE ENABLING------------------------------------------
			--DFB TODO: SearchContainer is deprecated! We need to attach the trading_controller_callback module 
			--to the pouch and change the views to message handlers!!!!
			--local searcherB = SearchContainer(mTradingPouchA)
			--local searcherA = SearchContainer(mTradingPouchA)
			--AddView("CheckBagsForNewItemsA",searcherB,0.1)	
			--AddView("CheckBagsForNewItemsB",searcherA,0.1)							
			-----------------NOTE: MUST BE DONE BEFORE ENABLING------------------------------------------
			ShowTradingWindow(mTraderA)
			ShowTradingWindow(mTraderB)	
		end
	else
		DebugMessage("[trading_controller] ERROR: Failed to create trading pouch for "..user:GetName())
		CleanUp()
	end
end)

RegisterEventHandler(EventType.Timer,"CancelTradingTimer",function ( ... )
	CleanUp()
end)

RegisterEventHandler(EventType.LoadedFromBackup,"",function ( ... )
	CleanUp()
end)

RegisterEventHandler(EventType.UserLogout,"",function ( ... )
	CleanUp()
end)