require 'incl_container'
require 'incl_resource_source'


USAGE_DISTANCE = 5
mSalvageStation = nil
DUR_FACTOR = 50
mSkMod = 1
mResTab = {}
function startSalvageProcess(salvageStation)
	if not IsValid(salvageStation) then 
		CleanUp()
		return
	end
	mySalvCont = mSalvageStation:TopmostContainer() or mSalvageStation
	local salvageLoc = mySalvCont:GetLoc()
	if(this:GetLoc():Distance(salvageLoc) > USAGE_DISTANCE ) then
		--DebugMessage("Too Far to Salvage")
		this:SystemMessage("[FA0C0C]You are too far to use that![-]")

		return
	end
	if not(this:HasLineOfSightToObj(mySalvCont,ServerSettings.Combat.LOSEyeLevel)) then 
		this:SystemMessage("[FA0C0C]You cannot see that![-]")
		return 
	end
end

function DoSalvage(target,user)
	--DebugMessage(target:GetName(),user:GetName(), " are the things you're trying to salvage.")
	if(mSalvageStation == nil) then
		--DebugMessage("Invalid Salvage Station")
		this:SystemMessage("[FA0C0C]Invalid Salvage Station Used[-]")
		return
	end

	
	local mySalvCont = mSalvageStation:TopmostContainer() or mSalvageStation
	local salvageLoc = mySalvCont:GetLoc()
	if(this:GetLoc():Distance(salvageLoc) > 5 ) then
		--DebugMessage("Too Far to Salvage")
		this:SystemMessage("[FA0C0C]You are too far to use that![-]")
		return
	end
	if not(this:HasLineOfSightToObj(mySalvCont,ServerSettings.Combat.LOSEyeLevel)) then 
		this:SystemMessage("[FA0C0C]You cannot see that![-]")
		return 
	end

	if(not mSelectedItem) then
		this:SystemMessage("[FA0C0C]As you go to salvage it you realize the item no longer exists") 
		LuaDebugCallStack("ERROR: DoSalvage called with nil mSelectedItem")
		CleanUp()
		return
	end

	createSuccess = false
	mSelectSalvage = false
	local salvageSource = mSelectedItem:GetObjVar("BaseItemObjVar") or mSelectedItem:GetObjVar("UnpackedTemplate") or mSelectedItem:GetCreationTemplateId()

	if not salvageSource then
		this:SystemMessage("[FA0C0C]You cannot salvage that![-]")
		--DebugMessage("Invalid Salvagement No Objvar")
		CleanUp()
		return 
	end
	if not (GetSkillRequiredForTemplate(salvageSource) == mSkill) then
		this:SystemMessage("[FA0C0C]You can't salvage that here![-]")
		--DebugMessage("Invalid Salvagement No Objvar")
		CleanUp()
		return 
	end

	local sourceRecipe = GetRecipeForBaseTemplate(salvageSource)
	if(sourceRecipe == nil) then
		this:SystemMessage("[FA0C0C] You have no idea how to salvage that![-]")
		--DebugMessage("Invalid Salvagement:No Salvage table match")
		CleanUp()
		return 
	end
	local mRTemp = sourceRecipe
	local curDur = GetDurabilityValue(target) or 1
	local durModifier = curDur / (target:GetObjVar("InitalMaxDur") or GetMaxDurabilityValue(target) or 1)
	
	--DebugMessage("Salvage Added")
	local salvageSk = GetSkillLevel(this,mSkill)
	mSkMod = math.min(1,((math.max(3,salvageSk) * 1.15)) / 125)
	local backpackObj = this:GetEquippedObject("Backpack")
	-- TODO: Verify creation success and refund money on failure
	local createdStuff = {}
	if( backpackObj ~= nil ) then			
		local quality = GetQualityString(target)
		local stackCount = GetStackCount(target)
		--DebugMessage("QUALITY ",tostring(quality),"Stackcount",stackCount)
		local resourceTable = GetQualityResourceTable(sourceRecipe.Resources,quality)
		--DebugMessage(DumpTable(resourceTable))
		for resourceName,recipeAmount in pairs (resourceTable) do
			local salvageValue = 0
			local salvageResource = nil
			if(ResourceData.ResourceInfo[resourceName] ~= nil) then
				salvageValue = ResourceData.ResourceInfo[resourceName].SalvageValue or 1
				salvageResource = ResourceData.ResourceInfo[resourceName].SalvageResource or resourceName
			end

			if(salvageValue > 0) then				
				local salvageAmountFinal = math.max(1, math.floor(salvageValue * recipeAmount * durModifier * mSkMod)) * stackCount
				--DebugMessage("SalvageData",salvageAmountFinal,stackCount,salvageValue,recipeAmount,durModifier,mSkMod)
				local iTemp = ResourceData.ResourceInfo[salvageResource].Template
				if not(iTemp == nil) then
					if( TryAddToStack(salvageResource, backpackObj, salvageAmountFinal) ) then			
						createSuccess = true
						--DebugMessage("Adding to Existing: " ..i.. " # " .. salvageAmountFinal)
						this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(750), "SalvageCleanupTimer")
					else
						-- if it didnt stack, create in container
						CreateObjInBackpackOrAtLocation(this,iTemp,"salvage_resource_created",salvageAmountFinal)						
						createSuccess = true
						this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(750), "SalvageCleanupTimer")
						--DebugMessage("attemtped to create quantity: " .. salvageAmountFinal)				
					end		
				end
			end
		end
	end
	if(createSuccess == true) then
		this:SystemMessage("[$2449]" .. target:GetName() .. ".[-]")
		this:SystemMessage("[FA0C0C] " .. target:GetName() .. " has been destroyed.[-]")
		this:SystemMessage("[$2450]" .. target:GetName() .. ".[-]","event")
		this:SystemMessage("[FA0C0C] " .. target:GetName() .. " has been destroyed.[-]","event")
		--this:SendMessage("RequestSkillGainCheck", mSkill)
		--TryCreateRecipe(target)
		target:Destroy()		
		mSelectedItem = nil
		ShowSalvageWindow()
	else
		this:SystemMessage("You are unable to salvage anything of value from " .. target:GetName() .. ".")
		CleanUp()
	end
end	

function GetResourceTemplateId(resource)
	if(resource == nil) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId got nil")
		return
	elseif not(ResourceData.ResourceInfo[resource]) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId no resource info for "..tostring(resource))
		return
	end

	return ResourceData.ResourceInfo[resource].Template
end

function ShowSalvageWindow(user)
	--800x480
	local mainWindow = DynamicWindow("SalvageWindow","Salvage",800,480,-410,-280,"","Center")

	--Left side section
	mainWindow:AddImage(5,25,"ThinFrameBackgroundInfo",770-11,395,"Sliced")
	mainWindow:AddImage(6,0,"ArrowFrameBackground1",245,420,"Sliced")
	mainWindow:AddImage(19,10,"ThinFrameBackgroundInfo",221,400,"Sliced")
	mainWindow:AddImage(70-15+10,94-15,"SkillsSelectionFrame_Default",100+30,100+30,"Sliced")
	mainWindow:AddImage(20,18,"DropHeaderBackground",219,22,"Sliced")
	mainWindow:AddImage(20,10,"InventoryWindow_NameplateDesign",219,30,"Sliced")
	mainWindow:AddImage(70+10,94,"ObjectPictureFrame",100,100,"Sliced")
	
	mainWindow:AddImage(45-8,265,"SectionDividerBar",185,7,"Sliced")
	mainWindow:AddLabel(130,19,"Place Object Into Slot",220,20,18,"center")

	--Main section
	mainWindow:AddImage(253,25,"InventoryWindow_NameplateDesign",510,30,"Sliced")
	mainWindow:AddImage(253,25+8,"DropHeaderBackground",510,30,"Sliced")
	mainWindow:AddLabel(510,37,"Materials Recieved From Salvaging",510,20,20,"center")
	mainWindow:AddImage(266,84,"ThinFrameBackgroundInfo",480,275,"Sliced")
	mainWindow:AddImage(266,360,"ThinFrameBackgroundInfo",480,60,"Sliced")
	mainWindow:AddImage(245,175,"ArrowFrameBackground2")

	mainWindow:AddButton(70+10,94,"SalvageCarriedObject","",100,100,"Drop an object into this slot to continue.","",false,"Invisible")

	local showNotEnoughResourceText = false
	local salvageEnabled = "disabled"
	if (mSelectedItem ~= nil) then
		mainWindow:AddImage(70+26,94+26,tostring(GetTemplateIconId(mSelectedItem:GetCreationTemplateId())),64,64,"Object",mSelectedItem:GetHue())
		mainWindow:AddLabel(135,228,StripColorFromString(mSelectedItem:GetName()),220,20,22,"center")
		salvageEnabled = "normal"

		--I'm allowed to use this function here because there's no base items with multiple recipes
		--If this should change then this function should change.
		local itemTemplate = mSelectedItem:GetObjVar("BaseItemObjVar") or mSelectedItem:GetObjVar("UnpackedTemplate") or mSelectedItem:GetCreationTemplateId()
		--rest is just more crazy loading code
		local quality = GetQualityString(mSelectedItem)
		local recipeTable = GetRecipeForBaseTemplate(itemTemplate)
		if (recipeTable == nil) then
			this:SystemMessage("You can't salvage that.")
			CleanUp()
			return
		end
		--local repairChance = GetRecipeChancePercentage(mSelectedItem)
		local resourceTable = GetQualityResourceTable(recipeTable.Resources,quality)
		local scrollWindow = ScrollWindow(270,90,460,265,45)

		for resource,amount in pairs(resourceTable) do 
			local resourceDisplayName = GetResourceDisplayName(resource)
			local element = ScrollElement()
			local resourceTemplate = GetResourceTemplateId(resource)
			local salvageValue = 0
			local salvageResource = nil
			local curDur = GetDurabilityValue(mSelectedItem) or 1
			local durModifier = curDur / (mSelectedItem:GetObjVar("InitalMaxDur") or GetMaxDurabilityValue(mSelectedItem) or 1)
			local salvageSk = GetSkillLevel(this,mSkill)
			local mSkMod = math.min(1,((math.max(3,salvageSk) * 1.15)) / 125)
			if(ResourceData.ResourceInfo[resource] ~= nil) then
				salvageValue = ResourceData.ResourceInfo[resource].SalvageValue or 1
				salvageResource = ResourceData.ResourceInfo[resource].SalvageResource or resource
				resourceTemplate = ResourceData.ResourceInfo[salvageResource].Template
				resourceDisplayName = GetResourceDisplayName(salvageResource)
			end
			local stackCount = GetStackCount(mSelectedItem)

			local result = math.max(1, math.floor(salvageValue * amount * durModifier * mSkMod)) * stackCount

			element:AddImage(28,6,"ThinFrameBackgroundInfo",400,27,"Sliced")
			element:AddImage(25,0,"CraftingNoItemsFrame",38,38)
			element:AddImage(25+2,0+2,tostring(GetTemplateIconId(resourceTemplate)),38,38,"Object")
			element:AddLabel(70,9,tostring(result).." " ..resourceDisplayName,400,0,20)
			scrollWindow:Add(element)	

		end
		mainWindow:AddScrollWindow(scrollWindow)
		--mainWindow:AddLabel(510,365,"There is a "..repairChance.."% chance to gain a "..recipeTable.DisplayName.." recipe.",450,50,16,"center")
		mainWindow:AddLabel(510,365,"Upon salvaging this item, the item will be destroyed and you will redeem some resources used in it's crafting.",450,50,16,"center")
	else
		mainWindow:AddLabel(510,365,"[$2451]",450,50,16,"center",false,false,"PermianSlabSerif_16_Dynamic")
	end
	mainWindow:AddButton(80,295,"SalvageItem","Salvage",100,30,"Salvage the current item.","",false,"Default",salvageEnabled)
	this:OpenDynamicWindow(mainWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"SalvageWindow",function (user,buttonId)
	if (buttonId) == "SalvageCarriedObject" then
		 item = user:CarriedObject()
		-- move the carried object back to the backpack
		if(item ~= nil) then
			--DebugMessage("Can repair item is "..tostring(CanRepairItem(user,item)))
			if (CanSalvageItem(user,item)) then
				mSelectedItem = item
				local backpackObj = user:GetEquippedObject("Backpack")
				if (backpackObj ~= nil) then
					local randomLoc = GetRandomDropPosition(backpackObj)
					if not(TryPutObjectInContainer(mSelectedItem, backpackObj, randomLoc)) then
						mSelectedItem:SetWorldPosition(this:GetLoc())
					end
				end
			end
		elseif (mSelectedItem ~= nil) then
			TryPutObjectInContainer(mSelectedItem, user, Loc(0,0,0))
			mSelectedItem = nil
		end
	end
	if (buttonId == "SalvageItem") then
		if (user == this) then
			user:CloseDynamicWindow("SalvageWindow")
			if(mSalvageStation ~= nil) then
				local toolAnim = mSalvageStation:GetObjVar("ToolAnimation")
				if(toolAnim ~= nil) then
					this:PlayAnimation(toolAnim)
				end
				local toolSound = mSalvageStation:GetObjVar("ToolSound")
				if(toolSound ~= nil) then
					this:PlayObjectSound(toolSound,false,BASE_CRAFTING_DURATION)
				end
			end
			this:SendMessage("EndCombatMessage")
			FaceObject(this,mSalvageStation)
			SetMobileModExpire(this, "Freeze", "Crafting", true, TimeSpan.FromSeconds(BASE_CRAFTING_DURATION))
			ProgressBar.Show
			{
				Label="Salvaging",
				Duration=BASE_CRAFTING_DURATION,
				PresetLocation="AboveHotbar",
				CanCancel = true,
				TargetUser = this,
			}
			CallFunctionDelayed(TimeSpan.FromSeconds(BASE_CRAFTING_DURATION),function()
				this:PlayAnimation("idle")
				DoSalvage(mSelectedItem,user)
			end)
		else
			DoSalvage(mSelectedItem,user)
		end
	elseif (buttonId ~= "") then
		ShowSalvageWindow(user)
	end
end)

function CanSalvageItem(user, item)
	if(item:TopmostContainer() ~= user) then 
		user:SystemMessage("[$2452]")
		--DebugMessage("Invalid Salvagement ItemLoc")
		return false
	end

	if(IsDead(user)) then
		user:SystemMessage("[$2453]")
		--DebugMessage("Invalid Salvagement DeadUser")
		return false
	end
	if(item:IsMobile()) then
		if not (IsDead(item)) then
			user:SystemMessage("[$2454]")
			--DebugMessage("Invalid Mobile Salvagement")
		else
			user:SystemMessage("[FA0C0C] You should use a knife for that.[-]")
			--DebugMessage("Invalid Salvagement Dead Target")
		end
		return false
	end
	local itemTemplate = item:GetObjVar("BaseItemObjVar") or item:GetObjVar("UnpackedTemplate") or item:GetCreationTemplateId()
	local recipeTable = GetRecipeForBaseTemplate(itemTemplate)
	if (recipeTable == nil) then
		this:SystemMessage("You can't salvage that.")
		return false
	end
	if (recipeTable.CanSalvage == false) then
		this:SystemMessage("You can't salvage that.")
		return false
	end
	if not (GetSkillRequiredForTemplate(itemTemplate) == mSkill) then
		this:SystemMessage("[FA0C0C] You can't salvage that here![-]")
		--DebugMessage("Invalid Salvagement No Objvar")
		return false
	end

	return true

end

function TryCreateRecipe(item)
	--DebugMessage("TEMP::" ..tostring(tempInfo))
	local roll = math.random(0,100)
	local template = item:GetObjVar("BaseItemObjVar") or item:GetObjVar("UnpackedTemplate") or item:GetCreationTemplateId()
	local recipe = GetRecipeForBaseTemplate(template)
	local recipeChance = GetRecipeChancePercentage(item)
	--DebugMessage("Roll is ",roll,"recipe chance is ",recipeChance)
	if(roll < recipeChance) then
		CreateObjInBackpack(this,"recipe", "salvage_recipe_created",GetRecipeNameFromBaseTemplate(template),recipe.DisplayName)
	end
end

function GetRecipeChancePercentage(item)	
	local cSkill = mSkill
	local template = item:GetObjVar("BaseItemObjVar") or item:GetObjVar("UnpackedTemplate") or item:GetCreationTemplateId()
	local recipe = GetRecipeForBaseTemplate(template)
	--DebugMessage("Recipe is "..tostring(recipe))
	local myCSkLev = GetSkillLevel(this,cSkill)
	local minSkill = recipe.MinLevelToCraft
	if (myCSkLev > minSkill) then
		return math.min(100, 12 * GetSkillPctPotency(myCSkLev))		
	else
		return 0
	end
	--DebugMessage("cSkill is "..tostring(cSkill))
	--DebugMessage(myCSkLev.." is the skill level")
end


function CleanUp()
	--LuaDebugCallStack("Where")
	this:PlayAnimation("idle")
	--if(this:HasTimer("SalvageCleanupTimer")) then this:RemoveTimer("SalvageCleanupTimer") end
	SetMobileMod(this, "Disable", "Crafting", nil)
	if(this:HasTimer("SalvageTimer")) then this:RemoveTimer("SalvageTimer") end
	ProgressBar.Cancel("Salvaging",this)
	this:CloseDynamicWindow("SalvageWindow")
	this:DelModule("salvage_controller")
end

RegisterEventHandler(EventType.CreatedObject,"salvage_resource_created", 
	function(success, objRef, amount)
		--DebugMessage("Created Object")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "SalvageCleanupTimer")
		if(success == true) then
			local resName = objRef:GetObjVar("ResourceType")
			if(resName == nil) then return end
			--DebugMessage("ResName: " .. tostring(resName))
			local myNum = mResTab[resName]
			--DebugMessage("myNum:" .. myNum)
			if(amount < 2) then return end
		local backpackObj = this:GetEquippedObject("Backpack")
			RequestSetStack(objRef,amount)
			--objRef:SetObjVar("stackCount", myNum)
		end
	end)

--RegisterEventHandler(EventType.Timer, "SalvageCleanupTimer", 
	--function()
	--	CleanUp()
	--end)

RegisterEventHandler(EventType.Message, "RESTART_SALVAGE_PROCESS", 
	function(user,station,skill)
		mSalvageStation = station
		mSkill = skill
		if (skill == "AlchemySkill") then
			this:SystemMessage("Salvaging disabled for Alchemy at this time.")
			CleanUp()
			return
		end
		mSelectedItem = nil
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(30), "SalvageCleanupTimer")
		user:SystemMessage("What do you wish to salvage?")
		mResTab = {}
		ShowSalvageWindow(user)
		--user:RequestClientTargetGameObj(this, "SalvageTarget")

	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "SalvageTarget", 
	function(target,user)
		if(target == nil) then
			CleanUp()
			return
		end
		--DebugMessage(user:GetName() .. " trying to salvage " .. target:GetName())
		if(CanSalvageItem(user, target)) then
			--DebugMessage("Salvage Approved")
			DoSalvage( target,user)
				--CleanUp()
		else
			CleanUp()
		end
	end)

RegisterEventHandler(EventType.StartMoving, "" , function ( ... )
	--DebugMessage("What the hell")
	CleanUp()
end)
RegisterEventHandler(EventType.DynamicWindowResponse,"ProgressBarSalvaging",CleanUp)

RegisterEventHandler(EventType.CreatedObject, "salvage_recipe_created", 
	function(success,recipe,recipeIndex,recipeName)
		if (success) then
			if (recipeIndex == nil) then 
				DebugMessage("[incl_npc_tasks] ERROR: Tried to create a nil recipe!") 
				return 
			end
			recipe:SetObjVar("Recipe",recipeIndex)
			recipe:SetObjVar("RecipeDisplayName",recipeName)
			recipeName = "[FF0000]Recipe for "..recipeName.."[-]"
			recipe:SetName(recipeName)
			DebugMessage("System message should be sent.")
			this:SystemMessage("[F7F700]You have obtained a new recipe:"..tostring(recipeName).. ".[-]","event")
			this:SystemMessage("[F7F700]You have obtained a new recipe:"..tostring(recipeName).. ".[-]")
		end
	end
)