require 'incl_combatai'
require 'incl_regions'
require 'ai_default_animal'

-- Combat AI settings
AI.Settings.AggroChanceAnimals = 0 --1 out of this number chance to attack other animals
AI.Settings.FleeUnlessAngry = false --only fight if the mob is angry
AI.Settings.AggroRange = 5.0 --Range to start aggroing
AI.Settings.CanFlee = false --Determines if this mob should ever run away
AI.Settings.ChargeSpeed = 3.0 --Speed mob charges at target
AI.Settings.ChaseRange = 10.0 --distance from self to chase target
AI.Settings.CanTaunt = false --Determines if this mob should taunt the target
AI.Settings.CanHowl = false --Determines if this mob should stop and howl
AI.Settings.CanSpeak = false --Determines if this mob speaks
AI.Settings.CanCast = false --Determines if this mob should cast or not
AI.Settings.CanUseCombatAbilities = false
AI.Settings.ChanceToNotAttackOnAlert = 0 --1 out of this number chance to attack when alerted
AI.Settings.FleeDistance = 10.0 --Distance to run away when fleeing
AI.Settings.InjuredPercent = 1.0 --Percentage of health to run away
AI.Settings.FleeSpeed = 1 --speed to run away
AI.Settings.FleeUnlessInCombat = false --if this setting is checked true then it will always flee if in combat.
AI.Settings.FleeChance = 0 --Chance to run away
AI.Settings.Leash = false --Determines if this mob should ever leash
AI.Settings.LeashDistance = 40 --Max distance from spawn this mob should ever go
AI.Settings.CanWander = true --Determines if this mob wanders or not
AI.Settings.WanderChance = 5 -- Chance to wander
AI.Settings.ScaleToAge = false
AI.Settings.StationedLeash = false --if true, mob will go back to it's spawn position on idle
		--Following variables are unused if the previous one is set to true
AI.Settings.ScaleRange = 0 --Maximum range to simulate scale randomization. 
		--Following variables are overridden if RandomizeScale or ScaleToAge is true
AI.Settings.CanSeeBehind = true--DFB TODO: If true, the mob will go into alert even if you sneak up behind them.
AI.Settings.ShouldAggro = false

-- external inputs