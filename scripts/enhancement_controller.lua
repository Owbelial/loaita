require 'base_enhancement'
require 'incl_resource_source'

local ENHANCEMENT_DURATION = 5
local mName = nil
local mItem = nil
local mAvail = {}
local mEdata = {}
local mTool = nil

function CleanUp()
	this:PlayAnimation("idle")
	mName = nil
	mItem = nil
	SetMobileMod(this, "Disable", "Crafting", nil)
	this:CloseDynamicWindow("EnhancementWindow")
	ProgressBar.Cancel("Enhancing",this)
	this:DelModule("enhancement_controller")
end


function AttemptToEnhanceItem(eItem, eName)
	if(eName == nil) then
		this:SystemMessage("[FA0CAA]Enhancement Cancelled.[-]")
		CleanUp()
	end
	if(eItem == nil) or not eItem:IsValid() then 
		this:SystemMessage("[F7CC0A]Invalid target for enhancement.[-]")
		CleanUp()
	end
 	if not (HasSkillForEnhancement(this, eItem, eName)) then
 		this:SystemMessage("[$1803]")
	 	CleanUp()
	 	return
 	end

	 if not(HasResourcesForEnhancement(this,eName)) then
	 	this:SystemMessage("Not enough resources for this enhancement.")
	 	CleanUp()
	 	return
 	end
 	if not(CanAddEnhancementToItem(eItem, eName)) then
		this:SystemMessage("Can't enhance this any more.")
	 	CleanUp()
	 	return
 	end
  	if not(CanAddEnhancementEffect(eItem, eName)) then
  		this:SystemMessage("That weapon too strong to be enhanced any further.")
	 	CleanUp()
	 	return
 	end
 	--D*ebugMessage("EnhancingItem")
  	if(ConsumeResourcesForEnhancement(this,eName)) then
  		mItem = eItem
  		mName = eName
  		--D*ebugMessage("Enhancing...")
  	else
  		--D*ebugMessage("Enhancment Failed Consume Init")
  		CleanUp()
  	end
end


RegisterEventHandler(EventType.Message, "InitiateCrafting",
	function(tool, craftType)
		if(craftType ~= "Enhancement") then
			CleanUp()
			return
		end
		mTool = tool
		UseEnhancmentTool(this)
		end)

function UseEnhancmentTool(user)
	ShowEnhancementWindow(user)
	 --user:RequestClientTargetGameObj(user, "EnhancementTarget")
end
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "EnhancementTarget",
function (targItem)
	mEdata = {}
	if(targItem == nil) then
		this:SystemMessage("[F7CC0A]Invalid Target for Enhancement[-]")
		CleanUp()
		return
	end
	if not(targItem:IsValid()) then 
		this:SystemMessage("[F7CC0A]Invalid Target for Enhancement[-]")
		CleanUp()
		return
	end
	if(targItem:IsMobile()) then 
		this:SystemMessage("[F7CC0A]Invalid Target for Enhancement[-]")
		CleanUp()
		return
	end
	if(targItem:TopmostContainer() ~= this) then
		this:SystemMessage("[ffcc0a] You do not have access to that![-]")
		CleanUp()
		return
	end
	mItem = targItem
	ShowEnhancementWindow(this,targItem, nil)
	end)

function GetResourceTemplateId(resource)
	if(resource == nil) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId got nil")
		return
	elseif not(ResourceData.ResourceInfo[resource]) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId no resource info for "..tostring(resource))
		return
	end

	return ResourceData.ResourceInfo[resource].Template
end

function BuildEnhancmentString(enhancement)
	local string = ""
	local enhancementData = AllEnhancementData[enhancement]
	for bonusName,bonusData in pairs(enhancementData.EnhancementBonuses) do
		if (bonusName == "BonusMinDamage") then
			string = string .. "+"..bonusData.ValuePerInstance.." to min damage.\n"
		elseif (bonusName == "BonusMaxDamage") then
			string = string .. "+"..bonusData.ValuePerInstance.." to max damage.\n"
		elseif (bonusName == "BonusSpeedOffset") then
			string = string .. "-"..bonusData.ValuePerInstance.." seconds to attack speed delay.\n"
		elseif (bonusName == "BonusFinalDamage") then
			string = string .. "+"..bonusData.ValuePerInstance.." to max and min damage.\n"
		elseif (bonusName == "BonusParryChance") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to parry chance.\n"
		elseif (bonusName == "BonusCritChance") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to crit chance.\n"
		elseif (bonusName == "BonusRangeOffset") then
			string = string .. "+"..bonusData.ValuePerInstance.." to attack range.\n"
		elseif (bonusName == "BonusPenetration") then
			string = string .. "+"..bonusData.ValuePerInstance.." to armor penetration.\n"
		elseif (bonusName == "BonusDurability") then
			string = string .. "+"..bonusData.ValuePerInstance.." to durability.\n"
		elseif (bonusName == "BonusRechargeTime") then
			string = string .. "+"..bonusData.ValuePerInstance.." to recharge time.\n"
		elseif (bonusName == "BonusNockTime") then
			string = string .. "+"..bonusData.ValuePerInstance.." to nock time.\n"
		elseif (bonusName == "BonusDrawTime") then
			string = string .. "-"..bonusData.ValuePerInstance.." seconds to draw time.\n"
		elseif (bonusName == "BonusAbsorption") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to damage absorbtion.\n"
		elseif (bonusName == "BonusEvasionModifier") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to dodge chance.\n"
		elseif (bonusName == "BonusPiercingResist") then
			string = string .. "+"..bonusData.ValuePerInstance.." to piercing resistance.\n"
		elseif (bonusName == "BonusSlashingResist") then
			string = string .. "+"..bonusData.ValuePerInstance.." to slashing resistance.\n"
		elseif (bonusName == "BonusBashingResist") then
			string = string .. "+"..bonusData.ValuePerInstance.." to bashing resistance.\n"
		elseif (bonusName == "BonusManaBarrier") then
			string = string .. "-"..bonusData.ValuePerInstance.." to mana regen.\n"
		elseif (bonusName == "BonusStaminaModifier") then
			string = string .. "+"..bonusData.ValuePerInstance.." to stamina cost.\n"
		elseif (bonusName == "BonusSwingSpeedModifier") then
			string = string .. "+"..bonusData.ValuePerInstance.." seconds to swing speed.\n"
		elseif (bonusName == "BonusDurability") then
			string = string .. "+"..bonusData.ValuePerInstance.." to bonus durability.\n"
		elseif (bonusName == "BonusMinBlockChance" ) then
			string = string .. "+"..bonusData.ValuePerInstance.."% minimum block chance.\n"
		elseif (bonusName == "BonusIncreasedBlockChance") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to max block chance.\n"
		elseif (bonusName == "BonusBlockCooldown") then
			string = string .. "+"..bonusData.ValuePerInstance.."% to max damage.\n"
		elseif (bonusName == "BonusBlockDuration") then
			string = string .. "+"..bonusData.ValuePerInstance.." to block chance duration.\n"
		elseif (bonusName == "SpellConduit") then
			string = string .. "Removes debuff on casting spells with a weapon.\n"
		elseif (bonusName == "BonusBleedDamage") then
			string = string.. "Adds +"..bonusData.ValuePerInstance.." bleed damage."
		else
			--Do nothing, it's not in here
			--string = string .. "+"..bonusData.ValuePerInstance.." to unspecified bonus.\n"
		end
	end
	return "[A1ADCC]"..string.."[-]"
end

function ShowEnhancementWindow(enhancer)
	--800x480
	local mainWindow = DynamicWindow("EnhancementWindow","Enhancement",800,480,-410,-280,"","Center")

	--Left side section
	mainWindow:AddImage(5,25,"ThinFrameBackgroundInfo",770-11,395,"Sliced")
	mainWindow:AddImage(6,0,"ArrowFrameBackground1",245,420,"Sliced")
	mainWindow:AddImage(19,10,"ThinFrameBackgroundInfo",221,400,"Sliced")
	mainWindow:AddImage(70-15+10,94-15,"SkillsSelectionFrame_Default",100+30,100+30,"Sliced")
	mainWindow:AddImage(20,18,"DropHeaderBackground",219,22,"Sliced")
	mainWindow:AddImage(20,10,"InventoryWindow_NameplateDesign",219,30,"Sliced")
	mainWindow:AddImage(70+10,94,"ObjectPictureFrame",100,100,"Sliced")
	
	mainWindow:AddImage(45-8,265,"SectionDividerBar",185,7,"Sliced")
	mainWindow:AddLabel(134,19,"Place Object Into Slot",220,20,18,"center")

	--Main section
	mainWindow:AddImage(253,25,"InventoryWindow_NameplateDesign",510,30,"Sliced")
	mainWindow:AddImage(253,25+8,"DropHeaderBackground",510,30,"Sliced")
	mainWindow:AddLabel(510,40,"Available Enhancements for Items",510,20,20,"center")

	mainWindow:AddButton(70+10,94,"EnhanceCarriedObject","",100,100,"Drop an object into this slot to continue.","",false,"Invisible")
	local showNotEnoughResourceText = false
	local enhanceEnabled = "disabled"
	local eAvail = GetAvailableEnhancementsForItem(mEnhancementItem)
	--DebugMessage("mEnhancementItem is "..tostring(mEnhancementItem))
	if (mEnhancementItem ~= nil) then
		mainWindow:AddImage(266,84,"ThinFrameBackgroundInfo",480+12,320,"Sliced")
		mainWindow:AddImage(450,84,"ThinFrameBackgroundInfo",307,225,"Sliced")
		mainWindow:AddImage(450,311,"ThinFrameBackgroundInfo",307,93,"Sliced")
		mainWindow:AddImage(435,110,"SectionDividerBar",295,7,"Sliced")
		mainWindow:AddImage(266,84,"ThinFrameBackgroundInfo",186,320,"Sliced")
		--mainWindow:AddImage(266,360,"ThinFrameBackgroundInfo",480,60,"Sliced")
		mainWindow:AddImage(245,175,"ArrowFrameBackground2")
	--DebugMessage("mEnhancementItem is "..tostring(mEnhancementItem))
		--DebugMessage("Getting here")
		mainWindow:AddImage(70+26,94+26,tostring(GetTemplateIconId(mEnhancementItem:GetCreationTemplateId())),64,64,"Object",mEnhancementItem:GetHue())
		mainWindow:AddLabel(135,228,StripColorFromString(mEnhancementItem:GetName()),220,20,22,"center")
		enhanceEnabled = "normal"

		--I'm allowed to use this function here because there's no base items with multiple recipes
		--If this should change then this function should change.
		local itemTemplate = mEnhancementItem:GetObjVar("BaseItemObjVar") or mEnhancementItem:GetObjVar("UnpackedTemplate") or mEnhancementItem:GetCreationTemplateId()
		--rest is just more crazy loading code
		local recipeTable = GetRecipeForBaseTemplate(itemTemplate)
		if (recipeTable == nil) then
			this:SystemMessage("You can't enhance that.")
			CleanUp()
			return
		end
		if (mSelectedEnhancement == nil) then
			mSelectedEnhancement = "Weight"
		end
		--Enhancement main section
		local scrollWindow = ScrollWindow(265,90,200-29,310,32)
		local enhancements = {}
		for i,j in pairs(AllEnhancementData) do
			j.Key = i --Probablly bad but it's not hurting anything
			table.insert(enhancements,j)
		end
		table.sort(enhancements,function(a,b)  --a.<b.
			local firstSkill = nil
			local secondSkill = nil
			for i,j in pairs(a.SkillRequired) do
				if (firstSkill == nil or firstSkill < j) then
					firstSkill = j
				end
			end
			for i,j in pairs(b.SkillRequired) do
				if (secondSkill == nil or secondSkill < j) then
					secondSkill = j
				end
			end
			return firstSkill < secondSkill
		end)
		for i,enhancementData in pairs(enhancements) do 
			local enhancement = enhancementData.Key
			local element = ScrollElement()
			local selected = "default"
			if (mSelectedEnhancement == enhancement) then selected = "pressed" end
			local canEnhance = true
			if (CanAddEnhancementToItem(mEnhancementItem, enhancement)) then
				element:AddButton(5,0,"ChangeEnhancement|"..tostring(enhancement),"",155,30,"Select this enhancement","",false,"SectionButton",selected)
				element:AddLabel(20,5,enhancementData.EnhancementDisplayName,155,30,18,"",false,false)
			else
				canEnhance = false
				element:AddButton(5,0,"ChangeEnhancement|"..tostring(enhancement),"",155,30,"[$1804]","",false,"SectionButton",selected)
				element:AddLabel(20,5,"[999999]"..enhancementData.EnhancementDisplayName.."[-]",155,30,18,"",false,false)
			end
			if (mSelectedEnhancement == enhancement) then
				local resources = GetEnhancementData(enhancement, "ResourcesRequired")
				mainWindow:AddLabel(460,89,enhancementData.EnhancementDisplayName,307,30,22)
				mainWindow:AddImage(266,84,"ThinFrameBackgroundInfo",186,320,"Sliced")
				local skillRequired = enhancementData.SkillRequired
				local skillString = "[D70000]"
				if (canEnhance == true) then
					for skill,amount in pairs(skillRequired) do
						if (GetSkillLevel(this,skill) >= amount) then
							skillString = ""
						end
						mainWindow:AddLabel(460,89+25,skillString.."Minimum "..GetSkillDisplayName(skill).." Skill: "..tostring(amount),307,30,18)
					end
				else
					mainWindow:AddLabel(460,89+25,skillString.."Cannot add enhancement to this item. ",307,30,18)
				end
				mainWindow:AddLabel(460,89+65,enhancementData.EnhancementDisplayEffect,295,80,18)
				mainWindow:AddLabel(460,89+155,BuildEnhancmentString(enhancement),295,464,18)

				--TODO: Write enhancement window code
				--DebugMessage(DumpTable(resources))--nasty nasty loading code
				local resourceTable = enhancementData.ResourcesRequired
				local resourceSectionSize = CountTable(resourceTable)
				local SIZE_PER_RESOURCE = 52
				local resourceStartLocation = 590 - 8 - ((resourceSectionSize*SIZE_PER_RESOURCE)/2)+10
				local position = resourceStartLocation
				local count = 0
				for i,j in pairs(resourceTable) do
					--DebugMessage(DumpTable(recipeTable).." da fuc")
					local myResources = this:GetEquippedObject("Backpack"):CountResourcesInContainer(i)
					local resourcesRequired = j
					local resultString =  myResources.. " / " .. resourcesRequired
					--get the resources required
					--count the resources the player has
					local resourceTemplate = GetResourceTemplateId(i)
					local tooltipString = "You need " ..tostring(resourcesRequired - myResources).." more "..GetResourceDisplayName(i)
					if (myResources >= resourcesRequired) then
						tooltipString = "You'll need to use "..tostring(resourcesRequired).. " "..GetResourceDisplayName(i).." enhance the item. You have "..tostring(myResources).."."
					else
						enhanceEnabled = "disabled"
						showNotEnoughResourceText = true
					end
					--add the image highlighted if you have the resource
					local resourceHue = "FFFFFF"
					if (HasResources(resourceTable, this)) then
						mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,320,"CraftingItemsFrame",48,48)
					else
						mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,320,"CraftingNoItemsFrame",48,48)
						resourceHue = "777777"
					end
					--invisible button that does a tooltip
					mainWindow:AddButton(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,320,"","",48,48,tooltipString,nil,false,"Invisible")
					--add the icon
					mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+26,320,tostring(GetTemplateIconId(resourceTemplate)),40,40,"Object",resourceHue)
					--display it red if they don't have it
					if (myResources < resourcesRequired) then
						resultString = "[D70000]" .. resultString .. "[-]"
						showNotEnoughResourceText = true
					end
					mainWindow:AddLabel(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+45,365,resultString,50,20,16,"center",false,false,"PermianSlabSerif_16_Dynamic")
					count = count + 1
				end
			end
			scrollWindow:Add(element)	
		end
		mainWindow:AddScrollWindow(scrollWindow)
		mainWindow:AddButton(80,295,"EnhanceItem|"..mSelectedEnhancement,"Enhance",100,30,"Enhance the current item.","",false,"Default",enhanceEnabled)
	else
		mainWindow:AddLabel(510,365,"[$1805]",450,50,16,"center")
	end
	this:OpenDynamicWindow(mainWindow) 	
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(30), "EnhancementMenuTimer")
end


function HandleEnhancementRequest(user, id)
	if(user ~= this) then
		return
	end
	if(id == nil or id == "") then 
		CleanUp()
		return
	end
	local result = StringSplit(id,"|")
	local action = result[1]
	local arg = result[2]
	if (id == "EnhanceCarriedObject") then
		local item = user:CarriedObject()
		if(item ~= nil) then
			local itemTemplate = item:GetObjVar("BaseItemObjVar") or item:GetObjVar("UnpackedTemplate") or item:GetCreationTemplateId()
			--DebugMessage("A: ",mTool ~= nil ,"B: ",item ~= nil," And the C to the E is ", GetSkillRequiredForTemplate(itemTemplate) == mTool:GetObjVar("ToolSkill"))
			if (mTool ~= nil and item ~= nil and GetSkillRequiredForTemplate(itemTemplate) == mTool:GetObjVar("ToolSkill")) then
			--DebugMessage("Can repair item is "..tostring(CanRepairItem(user,item)))
				--DebugMessage("item is "..tostring(item))
				mEnhancementItem = item
				local backpackObj = user:GetEquippedObject("Backpack")
				if (backpackObj ~= nil) then
					local randomLoc = GetRandomDropPosition(backpackObj)
					if not(TryPutObjectInContainer(mEnhancementItem, backpackObj, randomLoc)) then
						mEnhancementItem:SetWorldPosition(this:GetLoc())
					end
				end
			else
				user:SystemMessage("[D70000]You can't enhance that item here.[-]")
			end
		elseif (mEnhancementItem ~= nil) then
			TryPutObjectInContainer(mEnhancementItem, user, Loc(0,0,0))
			mEnhancementItem = nil
		end
		ShowEnhancementWindow(user)
	end
	--D*ebugMessage("ID: " ..id )
	if (action == "ChangeEnhancement") then
		mSelectedEnhancement = arg
		ShowEnhancementWindow(user)
		return
	end
	--D*ebugMessage("ENHANCE CHOICE: " ..tostring(mEdata[tonumber(id)]))
	if (action == "EnhanceItem" and mEnhancementItem ~= nil and arg ~= nil) then
		AttemptToEnhanceItem(mEnhancementItem, arg)	
		this:CloseDynamicWindow("EnhancementWindow")
	elseif (action == "EnhanceItem") then
		user:SystemMessage("[$1806]")
	end
	
end
function HandleConsumeResourceResponse(success,transactionId)	
	--D*ebugMessage("Consume REsponse: " .. transactionId .. " :: " ..tostring(success))
	if(transactionId ~= "enhancement_controller") then
		this:SystemMessage("Enhancement Interrupted")
		CleanUp()
		return
	end
	if(success == false) then
		this:SystemMessage("Enhancement Resource Availability Error")
		CleanUp()
		return
	end
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(ENHANCEMENT_DURATION), "EnhancementTimer", {["Enhancement"] = mName, ["Item"] = mItem})	mItem = nil 
	mName = nil
	this:SendMessage("EndCombatMessage")
	if(mTool ~= nil) then
		local toolAnim = mTool:GetObjVar("ToolAnimation")
		if(toolAnim ~= nil) then
			this:PlayAnimation(toolAnim)
		end

		local toolSound = mTool:GetObjVar("ToolSound")
		if(toolSound ~= nil) then
			this:PlayObjectSound(toolSound,false,BASE_CRAFTING_DURATION)
		end
	end
	FaceObject(this,mTool)
	SetMobileModExpire(this, "Freeze", "Crafting", true, TimeSpan.FromSeconds(ENHANCEMENT_DURATION))
	ProgressBar.Show
		{
			Label="Enhancing",
			Duration=ENHANCEMENT_DURATION,
			PresetLocation="AboveHotbar",
			CanCancel = true,
			TargetUser = this,
		}
	
end

RegisterEventHandler(EventType.Timer,"EnhancementTimer",
	function(args)
		if(args.Item == nil) or (args.Enhancement == nil) then 
			--D*ebugMessage("Invalid Enhancement Timer data")
			CleanUp()
		end
		--D*ebugMessage("Requesting Add Enhancement")
		--D*ebugTable(args)
		RequestAddEnhancement(this, args.Item, args.Enhancement)
		end)
RegisterEventHandler(EventType.Message, "EnhanceActivityComlete",
	function(result)
		ShowEnhancementWindow(this)
		end)

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", HandleConsumeResourceResponse)
RegisterEventHandler(EventType.DynamicWindowResponse,"ProgressBarEnhancing",CleanUp)
RegisterEventHandler(EventType.StartMoving,"",CleanUp)
RegisterEventHandler(EventType.DynamicWindowResponse, "EnhancementWindow", HandleEnhancementRequest)