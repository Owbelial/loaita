require 'incl_enhancement'


RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_hone")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_hone", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Hone") then return end
		AlterEquipmentBonusStat(this, "BonusFinalDamage", 1)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)