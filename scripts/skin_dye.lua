
function DyeDialogResponse(user,buttonId)
	buttonId = tonumber(buttonId)
	target = this:GetObjVar("DyeTarget")
	
	if( target == nil or not target:IsValid()) then
		return
	end

	if (buttonId == 0 and ValidateUse(user,target)) then	
    	local colorcode = this:GetObjVar("DyeHue")
		target:SetHue(CombineHues(colorcode,target:GetHue()))
		user:SystemMessage("[FA0C0C] You have successfully dyed yourself.")
		this:Destroy()	
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "dyeSkin",
	function(target,user)
		if (not ValidateUse(user,target)) then return end

		this:SetObjVar("DyeTarget",target)

		ClientDialog.Show{
			TargetUser = user,
			DialogId = "DyeClothing",
			TitleStr = "Skin Dye",
			DescStr = "Do you wish to dye yourself?",
			ResponseFunc = DyeDialogResponse
		}
	end)

function CombineHues(hue1,hue2)
	local hue1red = tonumber(string.sub(hue1,1,2),16)
	local hue1green = tonumber(string.sub(hue1,3,4),16)
	local hue1blue = tonumber(string.sub(hue1,5,6),16)
	local hue2red = tonumber(string.sub(hue2,1,2),16)
	local hue2green = tonumber(string.sub(hue2,3,4),16)
	local hue2blue = tonumber(string.sub(hue2,5,6),16)
	
	local newred =  math.min(255, (hue1red + hue2red) / 2)
	local newgreen =math.min(255, (hue1green + hue2green) / 2)
	local newblue = math.min(255, (hue1blue + hue2blue) / 2)	
	
	return "0xFF"..string.upper(string.format("%02x%02x%02x",newred,newgreen,newblue))
end

function ValidateUse(user,target)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( target == nil or not target:IsValid()) then
		return false
	end

	if( this:TopmostContainer() ~= user ) then
		user:SystemMessage("[$2596]")
		return false
	end

	if not(this:HasObjVar("DyeHue")) then 
		user:SystemMessage("[F7CC0A] Invalid Dye")
		return false
	end

	local isDyeable = (user == target)
	if (not isDyeable) then
		user:SystemMessage("You can only dye yourself with this dye.")
		return false
	end

	return true
end

RegisterEventHandler(EventType.Message, "UseObject", 
    function(user,usedType)
    	if(usedType ~= "Use" and usedType ~= "Apply") then return end
    	
    	local colorcode = this:GetObjVar("DyeHue")
		if( colorcode == nil ) then
			return
		end

		this:SetObjVar("DyeTarget",target)

		user:RequestClientTargetGameObj(this, "dyeSkin")
	end)

if(not this:HasObjVar("DyeHue")) then
	this:SetObjVar("DyeHue",this:GetHue())
end

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function ()
    	SetTooltipEntry(this,"dye", "Used to alter the color of your skin.")
    	AddUseCase(this,"Apply",true,"HasObject")
	end)