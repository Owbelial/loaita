require 'incl_resource_source'

USAGE_DISTANCE = 5
mRepairStation = nil

-- percentage of original resources required to repair something from 1 durability to full
RepairResourceMod = 0.5
MIN_DUR_LOSS = 1
MAX_DUR_LOSS = 8

function GetResourcesRequired(repairItem,target)
	local repairTarget = repairItem:GetObjVar("BaseItemObjVar") or repairItem:GetCreationTemplateId()	

	local targetRecipe = GetRecipeForBaseTemplate(repairTarget)
	--DebugMessage(repairTarget.." is repair target")
	if(targetRecipe == nil) then
		if (target ~= this) then
			QuickDialogMessage(this,target,"[$2436]")
			CleanUp()
		end
		target:SystemMessage("[FA0C0C] You have no idea how to repair that![-]")
		--DebugMessage("Invalid Repair: No recipe")
		CleanUp()
		return 
	end

	local quality = GetQualityString(repairItem)
	local resourceTable = GetQualityResourceTable(targetRecipe.Resources,quality)
	if(resourceTable == nil) then
		if (target ~= this) then
			QuickDialogMessage(this,target,"[$2437]")
			CleanUp()
		end
		target:SystemMessage("[FA0C0C] You have no idea how to repair that![-]")
		--DebugMessage("Invalid Repair: No resource table in recipe")
		CleanUp()
		return 
	end
	--DebugMessage(DumpTable(resourceTable))

	local curDur = math.max(0,GetDurabilityValue(repairItem))
	local maxDur = GetMaxDurabilityValue(repairItem)
	local durabilityRatio = 1 - (curDur / (maxDur + 1))

	local resources = {}
	for resourceName,recipeAmount in pairs (resourceTable) do
		resources[resourceName] = math.ceil(recipeAmount * RepairResourceMod * durabilityRatio)
	end

	return resources
end

function GetCoinRepairValue(item)
	local itemBaseTemp = item:GetObjVar("BaseItemObjVar") or item:GetCreationTemplateId()
	if(itemBaseTemp ~= nil) then
		local dur = math.max(1,item:GetObjVar("Durability"))
		local maxDur = item:GetObjVar("MaxDurability")
		local durRatio = 1
		if (dur ~= nil and maxDur ~= 0 ) then
			durRatio = maxDur/dur
		end

		local sourceRecipe = GetRecipeForBaseTemplate(itemBaseTemp)
		if(sourceRecipe ~= nil) then
			local canUseResourceMethod = true
			baseRet = 0
			local quality = GetQualityString(item)			
			local resourceTable = GetQualityResourceTable(sourceRecipe.Resources,quality)
			if(resourceTable == nil) then
				DebugMessage("WARNING: Recipe based item has invalid resource table "..tostring(quality),tostring(sourceRecipe.DisplayName),item:GetCreationTemplateId(),itemBaseTemp)
			end

			for resourceName,recipeAmount in pairs (resourceTable) do
				if(ResourceData.ResourceInfo[resourceName] ~= nil) then
					local templateName = ResourceData.ResourceInfo[resourceName].Template
					if(CustomItemValues[templateName] ~= nil) then
						baseRet = baseRet + (CustomItemValues[templateName] * recipeAmount)
					else
						-- if one or more of the resources do not have a value specified we can not use this method
						canUseResourceMethod = false
						DebugMessage("WARNING: Recipe based item contains a resource which has no CustomItemValue "..tostring(resourceType),templateName)
					end
				else
					canUseResourceMethod = false
					DebugMessage("WARNING: Recipe based item contains a resource which has no Resource entry in ResourceData.ResourceInfo table. "..resourceName,sourceRecipe.DisplayName)
				end
			end

			if(canUseResourceMethod) then
				--DebugMessage("Price evaluation from resources "..baseRet,itemBaseTemp,quality)
				return (baseRet * durRatio) / 3
			end
		end		
	end
	return GetItemValue(item,"coins")
end

BASE_REPAIR_CHANCE = 5
MIN_REPAIR_THRESHOLD = 45
function GetRepairUnscathedChance(item,target,skill)
	local repairSk = GetSkillLevel(target,skill)
	local repairCap = GetSkillCap(target,skill)
	local skillRatio = repairSk / (repairCap + 1)
	local curDur = math.max(0,GetDurabilityValue(item))
	local maxDur = GetMaxDurabilityValue(item)
	local durabilityRatio = curDur / (maxDur + 1)
	local maxDurRatio = (MIN_REPAIR_THRESHOLD/(maxDur + 1))
	if (maxDur <= MAX_DUR_LOSS or maxDur == 0) then return 0 end
	return math.floor(skillRatio * BASE_REPAIR_CHANCE * ( durabilityRatio * maxDurRatio) )
end

function GetRepairChance(item,user,skill)
	return 100 - (GetRepairUnscathedChance(item,user,skill) + GetBreakChance(item,user,skill))
end

function GetResourceTemplateId(resource)
	if(resource == nil) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId got nil")
		return
	elseif not(ResourceData.ResourceInfo[resource]) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId no resource info for "..tostring(resource))
		return
	end
	
	return ResourceData.ResourceInfo[resource].Template
end

function ShowRepairWindow(user)
	--800x480
	local mainWindow = DynamicWindow("RepairWindow","Repair",800,480,-410,-280,"","Center")

	--Left side section
	mainWindow:AddImage(5,25,"ThinFrameBackgroundInfo",770-11,395,"Sliced")
	mainWindow:AddImage(6,0,"ArrowFrameBackground1",245,420,"Sliced")
	mainWindow:AddImage(19,10,"ThinFrameBackgroundInfo",221,400,"Sliced")
	mainWindow:AddImage(70-15+10,94-15,"SkillsSelectionFrame_Default",100+30,100+30,"Sliced")
	mainWindow:AddImage(20,18,"DropHeaderBackground",219,22,"Sliced")
	mainWindow:AddImage(20,10,"InventoryWindow_NameplateDesign",219,30,"Sliced")
	mainWindow:AddImage(70+10,94,"ObjectPictureFrame",100,100,"Sliced")
	
	mainWindow:AddImage(45-8,265,"SectionDividerBar",185,7,"Sliced")
	mainWindow:AddLabel(134,19,"Place Object Into Slot",220,20,18,"center")

	--Main section
	mainWindow:AddImage(253,25,"InventoryWindow_NameplateDesign",510,30,"Sliced")
	mainWindow:AddImage(253,25+8,"DropHeaderBackground",510,30,"Sliced")
	mainWindow:AddLabel(510,40,"Materials Needed To Repair Item",510,20,20,"center")
	mainWindow:AddImage(266,84,"ThinFrameBackgroundInfo",480,275,"Sliced")
	mainWindow:AddImage(266,360,"ThinFrameBackgroundInfo",480,60,"Sliced")
	mainWindow:AddImage(245,175,"ArrowFrameBackground2")

	mainWindow:AddButton(70+10,94,"RepairCarriedObject","",100,100,"Drop an object into this slot to continue.","",false,"Invisible")

	local showNotEnoughResourceText = false
	local repairEnabled = "disabled"
	if (mSelectedItem ~= nil) then
		mainWindow:AddImage(70+26,94+26,tostring(GetTemplateIconId(mSelectedItem:GetCreationTemplateId())),64,64,"Object",mSelectedItem:GetHue())
		mainWindow:AddLabel(135,228,StripColorFromString(mSelectedItem:GetName()),220,20,22,"center")
		repairEnabled = "normal"

		local string = ""
		local itemSkill = GetSkillRequiredForTemplate(mSelectedItem:GetObjVar("BaseItemObjVar") or mSelectedItem:GetCreationTemplateId())
		repairChance = GetRepairUnscathedChance(mSelectedItem,this,itemSkill)
		repairMinorDamageChance = GetRepairChance(mSelectedItem,this,itemSkill)
		breakChance = GetBreakChance(mSelectedItem,this,itemSkill)	
		if(repairChance > 0) then
			string = string .."[33FFBB] Repair Without Damaging: "..ConvertPercentToString(repairChance).."[-]"
		end
		if(repairMinorDamageChance > 0) then
			string = string .."\n[33EEBB] Repair With Minor Damage: "..ConvertPercentToString(repairMinorDamageChance).."[-]"
		end
		if(breakChance > 0) then
			string = string .."\n[FA0C0C] Break Item: "..ConvertPercentToString(breakChance).."[-]\n"
		end
		mainWindow:AddLabel(510,365,string,450,50,16,"center")

		local SIZE_PER_RESOURCE = 50
		--set the starting position to be the size/2
		--nasty nasty loading code
		local quality = GetQualityString(mSelectedItem)
		local repairTarget = mSelectedItem:GetObjVar("BaseItemObjVar") or mSelectedItem:GetCreationTemplateId()
		local targetRecipe = GetRecipeForBaseTemplate(repairTarget)
		local resourceTable = GetQualityResourceTable(targetRecipe.Resources,quality)
		local resourceSectionSize = CountTable(resourceTable)
		local resourceStartLocation = 490 - 8 - ((resourceSectionSize*SIZE_PER_RESOURCE)/2)+10
		local position = resourceStartLocation
		local count = 0
		for i,j in pairs(resourceTable) do
			local recipe = GetRecipeNameFromBaseTemplate(repairTarget)
			local recipeTable = GetRecipeTableFromSkill(mSkill)[recipe]
			--DebugMessage(DumpTable(recipeTable).." da fuc")
			local myResources = user:GetEquippedObject("Backpack"):CountResourcesInContainer(i)
			local resourcesRequired = j
			local resultString =  myResources.. " / " .. resourcesRequired
			--get the resources required
			--count the resources the player has
			local resourceTemplate = GetResourceTemplateId(i)
			local tooltipString = "You need " ..tostring(resourcesRequired - myResources).." more "..GetResourceDisplayName(i)
			if (myResources >= resourcesRequired) then
				tooltipString = "You'll need to use "..tostring(resourcesRequired).. " "..GetResourceDisplayName(i).." to fix this. You have "..tostring(myResources).."."
			else
				repairEnabled = "disabled"
				showNotEnoughResourceText = true
			end
			--add the image highlighted if you have the resource
			local resourceHue = "FFFFFF"
			if (HasResources(recipeTable.Resources, this ,quality)) then
				mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,205,"CraftingItemsFrame",38,38)
			else
				mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,205,"CraftingNoItemsFrame",38,38)
				resourceHue = "777777"
			end
			--invisible button that does a tooltip
			mainWindow:AddButton(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,205,"","",38,38,tooltipString,nil,false,"Invisible")
			--add the icon
			mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+26,205,tostring(GetTemplateIconId(resourceTemplate)),28,28,"Object",resourceHue)
			--display it red if they don't have it
			if (myResources < resourcesRequired) then
				resultString = "[D70000]" .. resultString .. "[-]"
				showNotEnoughResourceText = true
			end
			--add the label
			mainWindow:AddLabel(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+42,245,resultString,50,20,16,"center",false,false,"PermianSlabSerif_16_Dynamic")
			count = count + 1
		end
	else
		mainWindow:AddLabel(510,365,"[$2438]",450,50,16,"center")
	end
	if (showNotEnoughResourceText) then
		mainWindow:AddLabel(510,185,"[D70000]Not enough resources required.[-]",200,20,18,"center",false,false,"PermianSlabSerif_16_Dynamic")
	end
	mainWindow:AddButton(80,295,"RepairItem","Repair",100,30,"Repair the current item.","",false,"Default",repairEnabled)
	user:OpenDynamicWindow(mainWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"RepairWindow",function (user,buttonId)
	if (buttonId) == "RepairCarriedObject" then
		 item = user:CarriedObject()
		-- move the carried object back to the backpack
		if(item ~= nil) then
			--DebugMessage("Can repair item is "..tostring(CanRepairItem(user,item)))
			if (CanRepairItem(user,item)) then
				mSelectedItem = item
				local backpackObj = user:GetEquippedObject("Backpack")
				if (backpackObj ~= nil) then
					local randomLoc = GetRandomDropPosition(backpackObj)
					if not(TryPutObjectInContainer(mSelectedItem, backpackObj, randomLoc)) then
						mSelectedItem:SetWorldPosition(this:GetLoc())
					end
				end
			end
		elseif (mSelectedItem ~= nil) then
			TryPutObjectInContainer(mSelectedItem, user, Loc(0,0,0))
			mSelectedItem = nil
		end
	end
	if (buttonId == "RepairItem") then
		ThrowUpRepairDialog(user,mSelectedItem)
	end
	if (buttonId ~= "") then
		ShowRepairWindow(user)
	end
end)

BASE_DURABILITY_THRESHOLD = 50
function GetDurabilityLossAmount(item,target,skill)
	local curDur = math.max(0,GetDurabilityValue(item))
	local maxDur = GetMaxDurabilityValue(item)
	baseLoss = math.random(MIN_DUR_LOSS,MAX_DUR_LOSS)
	if (maxDur - curDur < baseLoss) then
		baseLoss = math.max(1,maxDur - curDur - 2)
	end
	local repairSk = GetSkillLevel(target,skill)
	local repairCap = GetSkillCap(target,skill)
	local durMod = (((1/(curDur/maxDur))/2) + ((1/(repairSk/repairCap))/2))
	--DebugMessage("baseLoss is "..tostring(baseLoss))
	--DebugMessage("durMod is " ..tostring(durMod))
	--DebugMessage("RESULT = "..tostring(math.floor(math.min(math.max(baseLoss*durMod,MIN_DUR_LOSS),MAX_DUR_LOSS) + 0.5)))
	return math.floor(math.min(math.max(baseLoss*durMod,MIN_DUR_LOSS),MAX_DUR_LOSS) + 0.5)
end

BASE_BREAK_CHANCE = 5
MIN_BREAK_THRESHOLD = 120
function GetBreakChance(item,target,skill)
	--the bane of MMO players everywhere.
	--DebugMessage(target:GetName())
	local repairSk = GetSkillLevel(target,skill)
	local repairCap = GetSkillCap(target,skill)
	--DebugMessage(repairSk,repairCap)
	local skillRatio = repairCap / (repairSk + 1)
	-- DAB HACK: temporary bonus to durability lowers break chance
	local curDur = math.max(0,GetDurabilityValue(item)) + 10
	local maxDur = GetMaxDurabilityValue(item)
	local durabilityRatio = maxDur / (curDur + 1)
	local maxDurRatio = (maxDur/MIN_BREAK_THRESHOLD)
	if (maxDur <= MAX_DUR_LOSS or maxDur == 0) then return 100 end
	--DebugMessage("GetBreakChance",skillRatio,BASE_BREAK_CHANCE,durabilityRatio,maxDurRatio,math.floor(skillRatio * BASE_BREAK_CHANCE * ( durabilityRatio * maxDurRatio)))
	return math.floor(skillRatio * BASE_BREAK_CHANCE * ( durabilityRatio * maxDurRatio) )
end


function BuildImprovmentClientDialogString(item,user)
	local string = ""
	local itemSkill = GetSkillRequiredForTemplate(item:GetObjVar("BaseItemObjVar") or item:GetCreationTemplateId())
	repairChance = GetRepairUnscathedChance(item,this,itemSkill)
	repairMinorDamageChance = GetRepairChance(item,this,itemSkill)
	breakChance = GetBreakChance(item,this,itemSkill)	

	if(repairChance > 0) then
		string = string .."[33FFBB] Repair Without Damaging: "..ConvertPercentToString(repairChance).."[-]"
	end
	if(repairMinorDamageChance > 0) then
		string = string .."\n[33EEBB] Repair With Minor Damage: "..ConvertPercentToString(repairMinorDamageChance).."[-]"
	end
	if(breakChance > 0) then
		string = string .."\n[FA0C0C] Break Item: "..ConvertPercentToString(breakChance).."[-]\n"
	end

	local resources = GetResourcesRequired(item,user)
	--DebugMessage(DumpTable(resources))
	if (user == this and not (IsTableEmpty(resources))) then
		string = string .."\nRequires: "
		for resourceName,amount in pairs(resources) do
			local displayName = ResourceData.ResourceInfo[resourceName].DisplayName or resourceName			
			string = string .. " " .. tostring(amount) .. " " .. tostring(displayName) .. ","
		end
		string = StripTrailingComma(string)
	elseif (user ~= this) then
		string = string .. "\nRequires "..math.floor(tostring(GetCoinRepairValue(item))).." copper."
	end
	return string
end

function ThrowUpRepairDialog(user,item)
	local string = BuildImprovmentClientDialogString(item,user)
	if (user == this) then
		string = "[$2439]"
	end
	ClientDialog.Show{
		TargetUser = user,
		DialogId = "RepairItem",
		TitleStr = "Repair Item?",
		DescStr = string,
		Button1Str = "Confirm",
		Button2Str = "Cancel",	
		ResponseObj = this,	
		ResponseFunc = function (user,buttonId)
			if (buttonId == 0) then
				if (user == this) then
					user:CloseDynamicWindow("RepairWindow")
					if(mRepairStation ~= nil) then
						local toolAnim = mRepairStation:GetObjVar("ToolAnimation")
						if(toolAnim ~= nil) then
							this:PlayAnimation(toolAnim)
						end

						local toolSound = mRepairStation:GetObjVar("ToolSound")
						if(toolSound ~= nil) then
							this:PlayObjectSound(toolSound,false,BASE_CRAFTING_DURATION)
						end
					end
					this:SendMessage("EndCombatMessage")
					FaceObject(this,mRepairStation)
					SetMobileModExpire(this, "Disable", "Crafting", true, TimeSpan.FromSeconds(BASE_CRAFTING_DURATION))
					ProgressBar.Show
					{
						Label="Repairing",
						Duration=BASE_CRAFTING_DURATION,
						PresetLocation="AboveHotbar",
						CanCancel = true,
						TargetUser = this,
					}
					CallFunctionDelayed(TimeSpan.FromSeconds(BASE_CRAFTING_DURATION),function()
						this:PlayAnimation("idle")
						DoRepair(mItem,user)
					end)
				else
					DoRepair(mItem,user)
				end
			else
				CleanUp()
			end
		end
	}
	mItem = item
	--DoRepair(target, user)
	--this:SendClientMessage("StartProgressBar",{"Improving",BASE_CRAFTING_DURATION})
end

function ConsumeCoins(coinValue,user)
	RequestConsumeResource(user,"coins",math.floor(coinValue))
end

function DoRepair(target,user)
	if(mRepairStation == nil and user == this) then
		--DebugMessage("Invalid Repair Station")
		user:SystemMessage("[FA0C0C] Invalid Repair Station Used[-]")
		return
	end
	
	local myRepairContainer = this
	local repairLoc = this:GetLoc()
	if (mRepairStation ~= nil) then
		repairLoc = mRepairStation:GetLoc()
		myRepairLoc = mRepairStation:TopmostContainer() or mRepairStation
	end
	if(user:GetLoc():Distance(repairLoc) > 5 ) then
		if (not (user == this)) then
			QuickDialogMessage(this,user,"Come back here and I'll try to fix it for you!")
			return
		end
		user:SystemMessage("[FA0C0C] You are too far to use that![-]")
		return
	end
	if not(user:HasLineOfSightToObj(myRepairContainer,ServerSettings.Combat.LOSEyeLevel)) then 
		user:SystemMessage("[FA0C0C] You cannot see that![-]")
		return 
	end

	createSuccess = false
	mSelectRepair = false
	local repairSource = nil
	local itemSkill = GetSkillRequiredForTemplate(target:GetObjVar("BaseItemObjVar") or target:GetCreationTemplateId())
	repairSource = target:GetObjVar("BaseItemObjVar") or target:GetCreationTemplateId()

	if ((user == this)) then 
		if not (itemSkill == mSkill ) then
			user:SystemMessage("[FA0C0C] You can't repair that here![-]")
			--DebugMessage("Invalid Repair No Objvar")
			CleanUp()
			return 
		end
	end
	
	local resources = GetResourcesRequired(target,user)
	local coinValue = GetCoinRepairValue(target)
	if user == this and not HasResources(resources,user) then
		--[[if (not (user == this)) then
			QuickDialogMessage(this,user,"[$2440]")
			CleanUp()
			return
		end--]]
		user:SystemMessage("[$2441]")
		--DebugMessage("Invalid Repair No Objvar")
		CleanUp()
		return 
	elseif (user ~= this) then
		if (CountCoins(user) < coinValue) then
			QuickDialogMessage(this,user,"I'm sorry but I'll only repair your item for "..tostring(math.floor(GetCoinRepairValue(target))).." copper.")
			CleanUp()
			return
		end
	end

	local roll = math.random(1,100) 
	local repairChance = GetRepairUnscathedChance(target,this,itemSkill)
	local repairMinorDamageChance = GetRepairChance(target,this,itemSkill)
	local breakChance = GetBreakChance(target,this,itemSkill)

	if (roll < breakChance) then
		result = "FAIL"
	elseif (roll < breakChance + repairMinorDamageChance) then
		result = "REPAIR_IMPERFECT"
	else
		result = "REPAIR"
	end
	local subject = "You"
	if (not (user == this)) then
		subject = this:GetName()
	end

	local oldDurability = GetMaxDurabilityValue(target)
	if (result == "FAIL") then
		--cue angry players
		if (not (user == this)) then
			--cue really angry players
			QuickDialogMessage(this,user,"...Oh man. Uh, I think I broke it... Sorry.")
		end
		user:SystemMessage("[FA0C0C] "..subject.." destroyed " .. target:GetName() .. " while attempting to repair it![-]")
		user:SystemMessage("[FA0C0C] "..subject.." destroyed " .. target:GetName() .. " while attempting to repair it![-]","event")
		target:Destroy()
		CleanUp()
	elseif (result == "REPAIR_IMPERFECT") then
		local repairDamage = GetDurabilityLossAmount(target,user,itemSkill)
		if (repairDamage < 4) then
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with slight damage.[-]")
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with slight damage.[-]","event")
		elseif (repairDamage < 6) then
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with some minor damage.[-]")
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with some minor damage.[-]","event")
		else
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with some scuffs.[-]")
			user:SystemMessage("[57FA0C] "..subject.." repaired " .. target:GetName() .. " with some scuffs.[-]","event")
		end
		-- DURABILITY CHANGE: These functions are obsolute and need to be replaced to make this work!
		--SetMaxDurabilityValue(target, oldDurability-repairDamage)
		--SetDurabilityValue(target,oldDurability - repairDamage)
		--target:SetObjVar("Durability", oldDurability - repairDamage)
		--UpdateDurabilityTooltip(target,oldDurability-repairDamage,oldDurability-repairDamage)
		if (not (user == this)) then
			QuickDialogMessage(this,user,"Alright, I fixed it. Should be useable.")
		else
			user:SendMessage("RequestSkillGainCheck", mSkill)
		end
		if (user ~= this) then 
			ConsumeCoins(coinValue,user)
		else
			ConsumeResources(resources,user)
		end
	else
		--local repairDamage = GetDurabilityLossAmount(target)
		--if (repairDamage < 4) then
		user:SystemMessage("[FAFA0C] "..subject.." repaired " .. target:GetName() .. " without damaging it![-]")
		user:SystemMessage("[FAFA0C] "..subject.." repaired " .. target:GetName() .. " without damaging it![-]","event")
		--end
		if (not (user == this)) then
			QuickDialogMessage(this,user,"Alright, I fixed it. Should be as good as new.")
		end
		-- DURABILITY CHANGE: These functions are obsolute and need to be replaced to make this work!
		--SetDurabilityValue(target,oldDurability)
		--target:SetObjVar("Durability", oldDurability - repairDamage)
		--SetMaxDurabilityValue(target, oldDurability-repairDamage)
		--UpdateDurabilityTooltip(target,oldDurability-repairDamage,oldDurability-repairDamage)
		if (user == this) then
			user:SendMessage("RequestSkillGainCheck", mSkill)
		end

		if (user ~= this) then 
			ConsumeCoins(coinValue,user)
		else
			ConsumeResources(resources,user)
		end
	end
	CleanUp()
end	

function CanRepairItem(user, item)
	--lol
	if(item:IsMobile()) then
		if not (IsDead(item)) then
			user:SystemMessage("[$2442]")
			--DebugMessage("Invalid Mobile Repair")
		else
			user:SystemMessage("[FA0C0C] You can't fix death.[-]")
			--DebugMessage("Invalid Repair Dead Target")
		end
		return false
	end
	local durValue = GetDurabilityValue(item)
	if (durValue == nil) then 
		user:SystemMessage("[FA0C0C] This item cannot be repaired.[-]")
		return false
	end
	local curDur = math.max(0,durValue)
	local maxDur = GetMaxDurabilityValue(item)
	local repairSk = GetSkillLevel(this,mSkill)

	local subject = "You"
	if (not (user == this)) then
		subject = this:GetName()
	end

	if (curDur == nil or maxDur == nil) then
		user:SystemMessage("[FA0C0C] This item cannot be repaired.[-]")
		return false
	end

	if(item:TopmostContainer() ~= user) then 
		user:SystemMessage("[$2443]")
		--DebugMessage("Invalid Repair ItemLoc")
		return false
	end
	local itemSkill = GetSkillRequiredForTemplate(item:GetObjVar("BaseItemObjVar") or item:GetCreationTemplateId())
	if (not (user == this)) then
		if (GetSkillLevel(this,itemSkill) == 0) then
			QuickDialogMessage(this,user,"[$2444]")
			return false
		end
	else
		if (mSkill ~= itemSkill) then
			user:SystemMessage("[FA0C0C] You can't repair that here![-]")
			return false
		end
	end
	if (GetBreakChance(item,this,itemSkill) > 100) then
		if (not (user == this)) then
			QuickDialogMessage(this,user,"[$2445]")
		end
		local skillDisplayName = GetSkillDisplayName(mSkill)
		user:SystemMessage("[FA0C0C] "..subject.." lack(s) the "..skillDisplayName.." skill to repair this item.[-]")
		return false
	end
	if (GetMaxDurabilityValue(item) == GetDurabilityValue(item)) then
		if (not (user == this)) then
			QuickDialogMessage(this,user,"[$2446]")
		end
		user:SystemMessage("[FAFA0C]This item is not in need of repair.[-]")
		return false
	end
	if (GetMaxDurabilityValue(item) < GetDurabilityValue(item)) then
		DebugMessage("ERROR: Repair item is bugged max dur < cur dur ",tostring(item),tostring(GetMaxDurabilityValue(item)),tostring(GetDurabilityValue(item)))
		return false
	end
	if(IsDead(user)) then
		user:SystemMessage("[$2447]")
		--DebugMessage("Invalid Repair DeadUser")
		return false
	end

	return true

end

function startRepairProcess(repairLoc,user)

	local repairLoc = repairLoc
	if(user:GetLoc():Distance(repairLoc) > USAGE_DISTANCE ) then
		--DebugMessage("Too Far to Repair")
		user:SystemMessage("[FA0C0C] You are too far to use that![-]")

		return
	end
	if not(user:HasLineOfSightToLoc(repairLoc,ServerSettings.Combat.LOSEyeLevel)) then 
		user:SystemMessage("[FA0C0C] You cannot see that![-]")
		return 
	end
end

--[[--RegisterEventHandler(EventType.Timer, "RepairCleanupTimer", 
	function()
		CleanUp()
	end)]]--

RegisterEventHandler(EventType.StartMoving, "", 
	function()
		CleanUp()
	end)

RegisterEventHandler(EventType.Message, "RESTART_REPAIR_PROCESS", 
	function(user,station,skill)
		mRepairStation = station
		mSkill = skill
		mUser = user
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(30), "RepairCleanupTimer")
		if (not (user == this)) then
			this:NpcSpeech("What would you like me to repair?")
			user:RequestClientTargetGameObj(this, "RepairTarget")
		else
			user:SystemMessage("What do you wish to repair?")
			ShowRepairWindow(user)
		end
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "RepairTarget", 
	function(target,user)
		if(target == nil) then
			CleanUp()
			return
		end
		if(CanRepairItem(user, target)) then
			ThrowUpRepairDialog(user,target)
		else
			CleanUp()
		end
	end)

function CleanUp()
	this:PlayAnimation("idle")
	if (this:HasTimer("RepairCleanupTimer")) then this:RemoveTimer("RepairCleanupTimer") end
	SetMobileMod(this, "Disable", "Crafting", nil)
	ProgressBar.Cancel("Repairing",mUser)
	this:DelModule("repair_controller")
	this:CloseDynamicWindow("RepairWindow")
end
RegisterEventHandler(EventType.DynamicWindowResponse,"ProgressBarRepairing",CleanUp)