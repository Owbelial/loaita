require 'incl_container'

mCotton = nil
mResourceType = nil
mBackpack = nil
mTool = nil
mFinalCloth = 0
mClothRatio = 20

function TryWeave(cotton)
	mCotton = cotton
	mResourceType = mCotton:GetObjVar("ResourceType")
	mBackpack = this:GetEquippedObject("Backpack")
	mTool = GetNearbyTool()

	if not( VerifyWeave() ) then
		CleanUp()
		return
	end

	WeaveCotton()
end

function VerifyWeave()

	if ( this:HasTimer("PreventWeavingSpamTimer") ) then
		this:SystemMessage("You must wait a little between weaving.")
		return false
	end

	if ( mBackpack == nil ) then
		this:SystemMessage("You need to have a backpack to weave cotton.")
		return false
	end

	if not( mCotton:IsContainedBy(mBackpack) ) then
		this:SystemMessage("The cotton must be in your backpack to weave it.")
		return false
	end

	if ( mTool == nil ) then
		this:SystemMessage("You must be near a loom to weave cotton.")
		return false
	end

	return true
end

function WeaveCotton()
	this:PlayObjectSound("Fabrication", false, 2)
	FaceObject(this,mTool)

	local stackCount = GetStackCount(mCotton)

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(6), "PreventWeavingSpamTimer")
	mFinalCloth = stackCount * mClothRatio
	ConsumeResources(stackCount)
end

function GetNearbyTool()
	return FindObject(SearchMulti(
    {
        SearchRange(this:GetLoc(), 3),
        SearchTemplate("tool_loom")
    }))
end

function ConsumeResources(amount)
	RequestConsumeResource(this,mResourceType, amount, "WeaveConsumeResource", this)
end

function CreateCloth()
	this:SystemMessage("You weave the cotton into "..mFinalCloth.." cloth.")

	local added, addtostackreason = TryAddToStack("Fabric", mBackpack, mFinalCloth)
	if ( added ) then
		CleanUp()
		return
	end

	local createId = "weave_cotton_"..uuid()
	local canCreateInBag, reason = CreateObjInBackpackOrAtLocation(this, "resource_bolt_of_fabric", createId, mFinalCloth)
	RegisterSingleEventHandler(EventType.CreatedObject, createId,
		function(success, objRef, amount)
			SetItemTooltip(objRef)
			if success and amount > 1 then
				RequestSetStack(objRef,amount)
			end
			CleanUp()
		end)
end

function CleanUp()
	this:DelModule(GetCurrentModule())
end

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", 
	function (success, transactionId, user)
		if ( transactionId == "WeaveConsumeResource" ) then
			if ( success ) then
				CreateCloth()
			end
		end
	end)

RegisterEventHandler(EventType.Message, "WeaveCotton", function(cotton)
	TryWeave(cotton)
end)