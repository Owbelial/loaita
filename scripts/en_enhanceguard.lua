require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_enhanceguard")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_enhanceguard", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)
		
		if(enName ~= EnhanceGuard) then return end
		AlterEquipmentBonusStat(this, "BonusParryChance", 1)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)