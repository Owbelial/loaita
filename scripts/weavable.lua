
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	AddUseCase(this,"Weave", true, "HasObject")

end)

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
	if( usedType == "Weave" ) then
		user:AddModule("weaving")
		user:SendMessage("WeaveCotton", this)
	end
end)