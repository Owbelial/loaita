require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_reinforcement")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_reinforcement", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Reinforcement") then return end
		AlterEquipmentBonusStat(this, "BonusSlashingResist", 1)
		AlterEquipmentBonusStat(this, "BonusAbsorption", 3)
			this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)