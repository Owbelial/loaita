TemplateDefines = {
	-- DAB COMBAT CHANGES: UPDATE OR REMOVE THESE DEFINES

	LootTable = 
	{
		-- All new players will get this gear
		NewbiePlayer = 
		{
		    -- Starting items
			NumCoins = 50,
			NumItems = 5,
	    	LootItems = {
    			{ Template = "torch", Unique = true },
    			{ Template = "tool_hunting_knife", Unique = true },
    			{ Template = "item_bread", Unique = true },
    			{ Template = "bandage", Unique = true, StackCount = 5 },
    			{ Template = "tinderbox", Unique = true, StackCount = 5 },
    		},
		},

		Humanoid = 
		{
			NumItemsMin = 0,
			NumItemsMax = 3,
	    	LootItems = {
    			{ Weight = 25, Template = "torch", Unique = true },
    			{ Weight = 25, Template = "tool_hunting_knife", Unique = true },
    			{ Weight = 25, Template = "item_apple", Unique = true },
    			{ Weight = 25, Template = "item_bread", Unique = true },
    			{ Weight = 25, Template = "item_ale", Unique = true },
    			{ Weight = 5, Template = "potion_lheal", Unique = true },
    			{ Weight = 5, Template = "potion_cure", Unique = true },
    			{ Weight = 10, Template = "clothing_bandana_helm", Unique = true },
    			{ Weight = 10, Template = "clothing_tattered_legs", Unique = true },
    			{ Weight = 10, Template = "clothing_tattered_shirt_chest", Unique = true },
    			{ Weight = 10, Template = "tool_cookingpot", Unique = true },
    			{ Weight = 10, Template = "tool_hatchet", Unique = true },
    			{ Weight = 10, Template = "tool_mining_pick", Unique = true },
    			{ Weight = 10, Template = "tool_fishing_rod", Unique = true },
    		},
		},

		SpiderPoor = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
    			{ Weight = 90, Template = "resource_strand", StackCountMin = 1, StackCountMax = 2, Unique = true },
    			{ Weight = 10, Template = "resource_thread", Unique = true },
    		},
		},

		Spider = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
    			{ Weight = 90, Template = "resource_strand", StackCountMin = 1, StackCountMax = 2, Unique = true },
    			{ Weight = 10, Template = "resource_thread", Unique = true },
    			{ Weight = 50, Template = "animalparts_spider_silk", Unique = true, StackCountMin = 1, StackCountMax = 2 },
    		},
		},

		SpiderRich = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
    			{ Weight = 90, Template = "resource_fine_thread", Unique = true },
    			{ Weight = 10, Template = "resource_fibers", Unique = true },
    			{ Weight = 50, Template = "animalparts_spider_silk", Unique = true, StackCountMin = 1, StackCountMax = 2 },
				{ Weight = 5, Template = "animalparts_spider_silk_golden", Unique = true, StackCountMin = 1, StackCountMax = 2 },

    		},
		},


		SkeletonPoor = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
	    		{ Weight = 25, Template = "animalparts_bones_cursed", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    			{ Weight = 25, Template = "animalparts_blood", Unique = true },
    		},
		},

		Skeleton = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Weight = 5, Template = "animalparts_bones_cursed", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    			{ Weight = 25, Template = "animalparts_blood", Unique = true },
    		},
		},

		SkeletonRich = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Weight = 5, Template = "animalparts_bones_cursed", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    			{ Weight = 25, Template = "animalparts_blood", Unique = true },
    		},
		},

		SpectralPoor = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_spectral", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		Spectral = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_spectral", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		SpectralRich = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_spectral", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		MiasmaPoor = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_miasma", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		Miasma = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Chance = 20, Template = "animalparts_miasma", Unique = true },
    		},
		},

		MiasmaRich = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Chance = 20, Template = "animalparts_miasma", Unique = true },
    		},
		},

		DMiasmaPoor = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		DMiasma = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Chance = 5, Template = "animalparts_miasma_deathly", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		DMiasmaRich = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,
	    	LootItems = {
	    		{ Chance = 5, Template = "animalparts_miasma_deathly", Unique = true },
	    		{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		ZombiePoor = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_marrow", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		Zombie = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_marrow", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		ZombieRich = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_bone_marrow", Unique = true },
    			{ Weight = 25, Template = "animalparts_human_skull", Unique = true },
    		},
		},

		ScrollsLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 5, Template = "lscroll_heal", Unique = true },
				{ Chance = 5, Template = "lscroll_cure", Unique = true },
				{ Chance = 5, Template = "lscroll_poison", Unique = true },
				{ Chance = 5, Template = "lscroll_ruin", Unique = true },
				{ Chance = 5, Template = "lscroll_defence", Unique = true },

			},
		},		
		ScrollsMed = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.8, Template = "lscroll_greater_heal", Unique = true },
				{ Chance = 0.8, Template = "lscroll_lightning", Unique = true },
				{ Chance = 0.8, Template = "lscroll_bombardment", Unique = true },
				{ Chance = 0.8, Template = "lscroll_electricbolt", Unique = true },
				{ Chance = 0.8, Template = "lscroll_attack", Unique = true },
			},
		},
		ScrollsHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.8, Template = "lscroll_resurrect", Unique = true },
				{ Chance = 0.8, Template = "lscroll_earthquake", Unique = true },
				{ Chance = 0.8, Template = "lscroll_meteor", Unique = true },
				{ Chance = 0.8, Template = "lscroll_portal", Unique = true },

			},
		},
		Apprentice = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "loa_sword_trainingsword", Unique = true },
				{ Chance = 0.5, Template = "loa_dagger_letter", Unique = true },
				{ Chance = 0.5, Template = "loa_2hsword_longaxe", Unique = true },
				{ Chance = 0.5, Template = "loa_mace_club", Unique = true },
				{ Chance = 0.5, Template = "loa_2hmace_quarterstaff", Unique = true },
				{ Chance = 0.5, Template = "loa_pole_warfork", Unique = true },
				{ Chance = 0.5, Template = "low_bow_shortbow", Unique = true },
				{ Chance = 0.5, Template = "loa_shield_buckler", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_brigandine_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_brigandine_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_brigandine_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_light_padded_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_light_padded_chest", Unique = true },
				{ Chance = 0.5, Template = "loa_light_padded_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_apprentice_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_apprentice_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_apprentice_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_staff_staff", Unique = true },
				{ Chance = 5, Template = "potion_heal", Unique = true },

				{ Chance = 5, Template = "essenza_magicainf", Unique = true },
				{ Chance = 5, Template = "sangue_normale", Unique = true },

			},
		},
		RookieGearLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "loa_sword_axe", Unique = true },
				{ Chance = 0.5, Template = "loa_dagger_dagger", Unique = true },
				{ Chance = 0.5, Template = "loa_2hsword_greataxe", Unique = true },
				{ Chance = 0.5, Template = "loa_mace_spikedclub", Unique = true },
				{ Chance = 0.5, Template = "loa_2hmace_sledgehammer", Unique = true },
				{ Chance = 0.5, Template = "loa_pole_gladius", Unique = true },
				{ Chance = 0.5, Template = "low_bow_longbow", Unique = true },
				{ Chance = 0.5, Template = "loa_shield_smallshield", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_chain_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_chain_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_chain_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_light_leather_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_light_leather_chest", Unique = true },
				{ Chance = 0.5, Template = "loa_light_leather_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_linen_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_linen_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_linen_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_staff_tribal", Unique = true },
				{ Chance = 5, Template = "potion_heal", Unique = true },

				{ Chance = 5, Template = "essenza_magicainf", Unique = true },
				{ Chance = 5, Template = "sangue_normale", Unique = true },

				{ Chance = 4, Template = "essenza_magica", Unique = true },
				{ Chance = 4, Template = "sangue_undead", Unique = true },

			},
		},
		RookieGearHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 1, Template = "loa_sword_rapier", Unique = true },
				{ Chance = 1, Template = "loa_dagger_dagger", Unique = true },
				{ Chance = 1, Template = "loa_2hsword_greataxe", Unique = true },
				{ Chance = 1, Template = "loa_mace_spikedclub", Unique = true },
				{ Chance = 1, Template = "loa_2hmace_sledgehammer", Unique = true },
				{ Chance = 1, Template = "loa_pole_gladius", Unique = true },
				{ Chance = 1, Template = "low_bow_longbow", Unique = true },
				{ Chance = 1, Template = "loa_shield_smallshield", Unique = true },
				{ Chance = 1, Template = "loa_heavy_chain_helm", Unique = true },
				{ Chance = 1, Template = "loa_heavy_chain_tunic", Unique = true },
				{ Chance = 1, Template = "loa_heavy_chain_leggings", Unique = true },
				{ Chance = 1, Template = "loa_light_leather_helm", Unique = true },
				{ Chance = 1, Template = "loa_light_leather_chest", Unique = true },
				{ Chance = 1, Template = "loa_light_leather_leggings", Unique = true },
				{ Chance = 1, Template = "loa_robe_linen_leggings", Unique = true },
				{ Chance = 1, Template = "loa_robe_linen_helm", Unique = true },
				{ Chance = 1, Template = "loa_robe_linen_tunic", Unique = true },
				{ Chance = 1, Template = "loa_staff_tribal", Unique = true },

				{ Chance = 5, Template = "essenza_magicainf", Unique = true },
				{ Chance = 5, Template = "sangue_normale", Unique = true },

				{ Chance = 4, Template = "essenza_magica", Unique = true },
				{ Chance = 4, Template = "sangue_undead", Unique = true },

				{ Chance = 3, Template = "essenza_magicabrill", Unique = true },
				{ Chance = 3, Template = "sangue_gorgone", Unique = true },

			},
		},
		JourneymanGearLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.25, Template = "loa_sword_knights", Unique = true },
				{ Chance = 0.25, Template = "loa_dagger_kryss", Unique = true },
				{ Chance = 0.25, Template = "loa_mace_ironhammer", Unique = true },
				{ Chance = 0.25, Template = "loa_2hmace_dwarvenhammer", Unique = true },
				{ Chance = 0.25, Template = "loa_pole_warspear", Unique = true },
				{ Chance = 0.25, Template = "low_bow_eldeirbow", Unique = true },
				{ Chance = 0.25, Template = "loa_shield_maraudershield", Unique = true },
				{ Chance = 0.25, Template = "loa_heavy_scale_helm", Unique = true },
				{ Chance = 0.25, Template = "loa_heavy_scale_tunic", Unique = true },
				{ Chance = 0.25, Template = "loa_heavy_scale_leggings", Unique = true },
				{ Chance = 0.25, Template = "loa_light_bone_helm", Unique = true },
				{ Chance = 0.25, Template = "loa_light_bone_chest", Unique = true },
				{ Chance = 0.25, Template = "loa_light_bone_leggings", Unique = true },
				{ Chance = 0.25, Template = "loa_robe_plant_leggings", Unique = true },
				{ Chance = 0.25, Template = "loa_robe_plant_helm", Unique = true },
				{ Chance = 0.25, Template = "loa_robe_plant_tunic", Unique = true },
				{ Chance = 0.25, Template = "loa_staff_iron", Unique = true },

				{ Chance = 5, Template = "essenza_magicainf", Unique = true },
				{ Chance = 5, Template = "sangue_normale", Unique = true },

				{ Chance = 4, Template = "essenza_magica", Unique = true },
				{ Chance = 4, Template = "sangue_undead", Unique = true },

				{ Chance = 3, Template = "essenza_magicabrill", Unique = true },
				{ Chance = 3, Template = "sangue_gorgone", Unique = true },

				{ Chance = 1, Template = "essenza_magicamerav", Unique = true },
				{ Chance = 1, Template = "sangue_drago", Unique = true },
				
			},
		},
		JourneymanGearHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "loa_sword_knights", Unique = true },
				{ Chance = 0.5, Template = "loa_dagger_kryss", Unique = true },
				{ Chance = 0.5, Template = "loa_mace_ironhammer", Unique = true },
				{ Chance = 0.5, Template = "loa_2hmace_dwarvenhammer", Unique = true },
				{ Chance = 0.5, Template = "loa_pole_warspear", Unique = true },
				{ Chance = 0.5, Template = "low_bow_eldeirbow", Unique = true },
				{ Chance = 0.5, Template = "loa_shield_maraudershield", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_scale_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_scale_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_heavy_scale_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_light_bone_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_light_bone_chest", Unique = true },
				{ Chance = 0.5, Template = "loa_light_bone_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_plant_leggings", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_plant_helm", Unique = true },
				{ Chance = 0.5, Template = "loa_robe_plant_tunic", Unique = true },
				{ Chance = 0.5, Template = "loa_staff_iron", Unique = true },

				{ Chance = 5, Template = "essenza_magicainf", Unique = true },
				{ Chance = 5, Template = "sangue_normale", Unique = true },

				{ Chance = 4, Template = "essenza_magica", Unique = true },
				{ Chance = 4, Template = "sangue_undead", Unique = true },

				{ Chance = 3, Template = "essenza_magicabrill", Unique = true },
				{ Chance = 3, Template = "sangue_gorgone", Unique = true },

				{ Chance = 2, Template = "essenza_magicamerav", Unique = true },
				{ Chance = 2, Template = "sangue_drago", Unique = true },
				
				{ Chance = 0.5, Template = "essenza_magicadiv", Unique = true },
				{ Chance = 0.5, Template = "sangue_demoniaco", Unique = true },
			},
		},
		ExpertGearLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.25, Template = "loa_sword_falchion", Unique = true },
				{ Chance = 0.25, Template = "loa_mace_dwarvenmace", Unique = true },
				{ Chance = 0.25, Template = "loa_dagger_lich", Unique = true },
				{ Chance = 0.25, Template = "loa_pole_waraxe", Unique = true },
				{ Chance = 0.25, Template = "loa_shield_dwarvenshield", Unique = true },
				{ Chance = 0.25, Template = "loa_staff_sun", Unique = true },
				{ Chance = 0.25, Template = "loa_2hsword_peacekeeper", Unique = true },
				{ Chance = 0.25, Template = "loa_2hmace_dwarvenhammer", Unique = true },
				{ Chance = 0.25, Template = "loa_bow_savagebow", Unique = true },
			},
		},
		ExpertGearHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "loa_sword_falchion", Unique = true },
				{ Chance = 0.5, Template = "loa_mace_dwarvenmace", Unique = true },
				{ Chance = 0.5, Template = "loa_dagger_lich", Unique = true },
				{ Chance = 0.5, Template = "loa_pole_waraxe", Unique = true },
				{ Chance = 0.5, Template = "loa_shield_dwarvenshield", Unique = true },
				{ Chance = 0.5, Template = "loa_staff_sun", Unique = true },
				{ Chance = 0.5, Template = "loa_2hsword_peacekeeper", Unique = true },
				{ Chance = 0.5, Template = "loa_2hmace_dwarvenhammer", Unique = true },
				{ Chance = 0.5, Template = "loa_bow_savagebow", Unique = true },
			},
		},
		MasterGearLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.1, Template = "loa_sword_elven", Unique = true },
				{ Chance = 0.1, Template = "loa_mace_handoftethys", Unique = true },
				{ Chance = 0.1, Template = "loa_dagger_demonfang", Unique = true },
				{ Chance = 0.1, Template = "loa_pole_runedhalberd", Unique = true },
				{ Chance = 0.1, Template = "loa_shield_fortress", Unique = true },
				{ Chance = 0.1, Template = "loa_staff_magi", Unique = true },
				{ Chance = 0.1, Template = "loa_2hsword_justice", Unique = true },
				{ Chance = 0.1, Template = "loa_2hmace_destruction", Unique = true },
				{ Chance = 0.1, Template = "loa_bow_bonebow", Unique = true },
			},
		},
		MasterGearHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "loa_sword_elven", Unique = true },
				{ Chance = 0.5, Template = "loa_mace_handoftethys", Unique = true },
				{ Chance = 0.5, Template = "loa_dagger_demonfang", Unique = true },
				{ Chance = 0.5, Template = "loa_pole_runedhalberd", Unique = true },
				{ Chance = 0.5, Template = "loa_shield_fortress", Unique = true },
				{ Chance = 0.5, Template = "loa_staff_magi", Unique = true },
				{ Chance = 0.5, Template = "loa_2hsword_justice", Unique = true },
				{ Chance = 0.5, Template = "loa_2hmace_destruction", Unique = true },
				{ Chance = 0.5, Template = "loa_bow_bonebow", Unique = true },
			},
		},

		Cerberus = 
		{
			NumItemsMin = 1,
			NumItemsMax = 3,	

			LootItems = 
			{ 
				{ Weight = 80, Template = "loa_dagger_darksword", Unique = true },
				{ Weight = 80, Template = "loa_mace_hota", Unique = true },
				{ Weight = 80, Template = "loa_sword_valor", Unique = true },
				{ Weight = 80, Template = "loa_pole_celeste", Unique = true },
				{ Weight = 80, Template = "loa_shield_dragonguard", Unique = true },
				{ Weight = 80, Template = "loa_staff_dead", Unique = true },
				{ Weight = 80, Template = "loa_2hsword_justice", Unique = true },
				{ Weight = 80, Template = "loa_2hmace_destruction", Unique = true },
				{ Weight = 80, Template = "loa_bow_bonebow", Unique = true },

			},
		},
		
		Death = 
		{
			NumItemsMin = 3,
			NumItemsMax = 10,	

			LootItems = 
			{ 
				{ Weight = 50, Template = "furniture_throne_reaper", Unique = true },
				{ Weight = 50, Template = "teleporter_catacombs", Unique = true },
				{ Weight = 80, Template = "loa_dagger_darksword", Unique = true },
				{ Weight = 80, Template = "loa_mace_hota", Unique = true },
				{ Weight = 80, Template = "loa_sword_valor", Unique = true },
				{ Weight = 80, Template = "loa_pole_celeste", Unique = true },
				{ Weight = 80, Template = "loa_shield_dragonguard", Unique = true },
				{ Weight = 80, Template = "loa_staff_dead", Unique = true },
				{ Weight = 80, Template = "loa_2hsword_justice", Unique = true },
				{ Weight = 80, Template = "loa_2hmace_destruction", Unique = true },
				{ Weight = 80, Template = "loa_bow_bonebow", Unique = true },

			},
		},

		DeathResources = 
		{
			NumItemsMin = 3,
			NumItemsMax = 5,

	    	LootItems = {
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "animalparts_miasma_deathly", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "resource_fibers", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "resource_fibers", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "resource_fibers", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "resource_fibers", StackCountMin = 5, StackCountMax = 10, Unique = true },
	    		{ Weight = 10, Template = "resource_fibers", StackCountMin = 5, StackCountMax = 10, Unique = true },
    		},
		},

		ClothScrapsLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "resource_clothscraps", Unique = true, StackCountMin = 1, StackCountMax = 2, },
			},
		},
		ClothScrapsHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "resource_clothscraps", Unique = true, StackCountMin = 3, StackCountMax = 5, },
			},
		},
		IronLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 20, Template = "resource_iron", Unique = true, StackCountMin = 1, StackCountMax = 2, },
			},
		},
		IronHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 20, Template = "resource_iron", Unique = true, StackCountMin = 2, StackCountMax = 4, },
			},
		},

		ScrollsBoss = 
		{
			NumItems = 1,	

			LootItems = 
			{
				{ Chance = 2, Template = "lscroll_electricbolt", Unique = true },
				{ Chance = 2, Template = "lscroll_walloffire", Unique = true },
				{ Chance = 2, Template = "lscroll_teleport", Unique = true },
				{ Chance = 2, Template = "lscroll_resurrect", Unique = true },
				{ Chance = 2, Template = "lscroll_cloak", Unique = true },
				{ Chance = 2, Template = "lscroll_attack", Unique = true },
				{ Chance = 1, Template = "lscroll_bombardment", Unique = true },
				{ Chance = 1, Template = "lscroll_meteor", Unique = true },
				{ Chance = 1, Template = "lscroll_earthquake", Unique = true },
				{ Chance = 1, Template = "lscroll_portal", Unique = true },
			},
		},
		CultistLootEasy = 
		{
			NumItemsMin = 0,
			NumItemsMax = 1,
			LootItems =
			{ 
				{ Weight = 20, Template = "tool_hunting_knife", Unique = true },
				{ Weight = 20, Template = "potion_lheal" },
				{ Weight = 5, Template  = "cultist_note_a" },
				{ Weight = 5, Template  = "cultist_note_b" },
				{ Weight = 5, Template  = "cultist_note_c" },
				{ Weight = 5, Template  = "cultist_note_d" },
				{ Weight = 5, Template  = "cultist_note_e" },
				{ Weight = 5, Template  = "cultist_note_f" },
				{ Weight = 5, Template  = "ancient_map" },
				{ Weight = 1, Template  = "cultist_scripture_a" },
				{ Weight = 1, Template  = "cultist_scripture_b" },
				{ Weight = 1, Template  = "cultist_scripture_c" },
				{ Weight = 1, Template  = "cultist_scripture_d" },
			},
		},
		CultistLootMedium = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,
			LootItems =
			{ 
				{ Weight = 20, Template = "tool_hunting_knife", Unique = true },
				{ Weight = 20, Template = "potion_lheal" },
				{ Weight = 5, Template  = "cultist_note_a" },
				{ Weight = 5, Template  = "cultist_note_b" },
				{ Weight = 5, Template  = "cultist_note_c" },
				{ Weight = 5, Template  = "cultist_note_d" },
				{ Weight = 5, Template  = "cultist_note_e" },
				{ Weight = 5, Template  = "cultist_note_f" },
				{ Weight = 5, Template  = "ancient_map" },
				{ Weight = 1, Template  = "cultist_scripture_a" },
				{ Weight = 1, Template  = "cultist_scripture_b" },
				{ Weight = 1, Template  = "cultist_scripture_c" },
				{ Weight = 1, Template  = "cultist_scripture_d" },
				{ Weight = 1, Template  = "cultist_scripture_e" },
				{ Weight = 1, Template  = "cultist_scripture_f" },
				{ Weight = 1, Template  = "cultist_scripture_g" },
				{ Weight = 1, Template  = "cultist_scripture_h" },
			},
		},
		CultistLootHard = 
		{
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems =
			{ 
				{ Weight = 20, Template = "tool_hunting_knife", Unique = true },
				{ Weight = 20, Template = "potion_lheal" },
				{ Weight = 5, Template  = "cultist_note_a" },
				{ Weight = 5, Template  = "cultist_note_b" },
				{ Weight = 5, Template  = "cultist_note_c" },
				{ Weight = 5, Template  = "cultist_note_d" },
				{ Weight = 5, Template  = "cultist_note_e" },
				{ Weight = 5, Template  = "cultist_note_f" },
				{ Weight = 5, Template  = "ancient_map" },
				{ Weight = 3, Template  = "cultist_scripture_a" },
				{ Weight = 3, Template  = "cultist_scripture_b" },
				{ Weight = 3, Template  = "cultist_scripture_c" },
				{ Weight = 3, Template  = "cultist_scripture_d" },
				{ Weight = 3, Template  = "cultist_scripture_e" },
				{ Weight = 3, Template  = "cultist_scripture_f" },
				{ Weight = 3, Template  = "cultist_scripture_g" },
				{ Weight = 3, Template  = "cultist_scripture_h" },
				{ Weight = 3, Template  = "cultist_scripture_i" },
				{ Weight = 3, Template  = "cultist_scripture_j" },
			},
		},
		MagePoor = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 1, StackCountMax = 3,},
			},
		},
		MageRich = 
		{
			NumItemsMin = 2,
			NumItemsMax = 3,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 2, StackCountMax = 5,},
			},
		},
		ArcherPoor = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "arrow", Unique = true, StackCountMin = 1, StackCountMax = 10,},
			},
		},
		ArcherRich = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Weight = 50, Template = "arrow", Unique = true, StackCountMin = 10, StackCountMax = 30,},
			},
		},
		MageBoss = 
		{
			NumItems = 3,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 50, StackCountMax = 100,},
			},
		},
		WarriorPoor = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "bandage", Unique = true, StackCountMin = 1, StackCountMax = 2, },
			},
		},
		WarriorRich = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "bandage", Unique = true, StackCountMin = 1, StackCountMax = 5,},
			},
		},
		One = 
		{
			NumCoinsMin = 8,
			NumCoinsMax = 12,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.5, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Two = 
		{
			NumCoinsMin = 8,
			NumCoinsMax = 13,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.5, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Three = 
		{
			NumCoinsMin = 13,
			NumCoinsMax = 20,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.5, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Four = 
		{
			NumCoinsMin = 19,
			NumCoinsMax = 29,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.5, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.5, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Five = 
		{
			NumCoinsMin = 30,
			NumCoinsMax = 42,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Six = 
		{
			NumCoinsMin = 41,
			NumCoinsMax = 58,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Seven = 
		{
			NumCoinsMin = 51,
			NumCoinsMax = 72,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Eight = 
		{
			NumCoinsMin = 67,
			NumCoinsMax = 91,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Nine = 
		{
			NumCoinsMin = 80,
			NumCoinsMax = 108,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Ten = 
		{
			NumCoinsMin = 96,
			NumCoinsMax = 130,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Eleven = 
		{
			NumCoinsMin = 109,
			NumCoinsMax = 147,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Twelve = 
		{
			NumCoinsMin = 112,
			NumCoinsMax = 154,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_1", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_1", Unique = true },
			},
		},
		Thirteen = 
		{
			NumCoinsMin = 127,
			NumCoinsMax = 180,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		Fourteen = 
		{
			NumCoinsMin = 128,
			NumCoinsMax = 189,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		Fifteen = 
		{
			NumCoinsMin = 131,
			NumCoinsMax = 199,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		Sixteen = 
		{
			NumCoinsMin = 132,
			NumCoinsMax = 206,
			NumItems = 1,
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},	
		},
		Seventeen = 
		{
			NumCoinsMin = 132,
			NumCoinsMax = 212,
			NumItems = 1,
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},	
		},
		Eighteen = 
		{
			NumCoinsMin = 133,
			NumCoinsMax = 220,
			NumItems = 1,
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},	
		},
		Nineteen = 
		{
			NumCoinsMin = 135,
			NumCoinsMax = 232,
			NumItems = 1,
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},	
		},
		Twenty = 
		{
			NumCoinsMin = 142,
			NumCoinsMax = 248,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		TwentyOne = 
		{
			NumCoinsMin = 145,
			NumCoinsMax = 260,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		TwentyTwo = 
		{
			NumCoinsMin = 150,
			NumCoinsMax = 280,
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 0.1, Template = "prestige_fieldmage_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_fieldmage_silence_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_shieldbash_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_knight_vanguard_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_stasis_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_scout_snipe_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_backstab_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_rogue_dart_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellshield_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_sorcerer_spellchamber_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_stunstrike_2", Unique = true },
				{ Chance = 0.1, Template = "prestige_gladiator_hamstring_2", Unique = true },
			},
		},
		Poor = 
		{
			NumCoinsMin = 1,
			NumCoinsMax = 25,
		},

		ContemptMob =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 1, Template = "contempt_skull", Unique = true },
			}
		},
		RuinMob =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 1, Template = "ruin_skull", Unique = true },
			}
		},
		DeceptionMob =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 1, Template = "deception_skull", Unique = true },
			}
		},
	},

	GridSpawns = 
	{
		UpperPlains = {
			Easy = { 
				{ Count = 25, TemplateId =  "wolf" }, 
				{ Count = 25, TemplateId =  "black_bear" }, 
				{ Count = 30, TemplateId =  "hind" }, 
				{ Count = 20, TemplateId = "chicken" }, 
				{ Count = 10, TemplateId = "bay_horse" }, 
			},
			Medium = {
				{ Count = 20, TemplateId =  "wolf" }, 
				{ Count = 15, TemplateId =  "black_bear" }, 
				{ Count = 20, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "chicken" }, 
				{ Count = 10, TemplateId = "bay_horse" }, 
				{ Count = 20, TemplateId = "brown_bear" }, 
				{ Count = 15, TemplateId = "wolf_grey" }, 				
			},
			Hard = {
				{ Count = 5, TemplateId =  "wolf" }, 
				{ Count = 5, TemplateId =  "black_bear" }, 
				{ Count = 5, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "chicken" }, 
				{ Count = 10, TemplateId = "bay_horse" }, 
				{ Count = 10, TemplateId = "brown_bear" }, 
				{ Count = 25, TemplateId = "wolf_grey" },
				{ Count = 20, TemplateId = "grizzly_bear" },
				{ Count = 20, TemplateId = "great_hart" },				
			},
		},
		SouthernHills = {
			Easy = { 
				{ Count = 25, TemplateId =  "coyote" }, 
				{ Count = 25, TemplateId =  "black_bear" }, 
				{ Count = 30, TemplateId =  "hind" }, 
				{ Count = 20, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 

			},
			Medium = {
				{ Count = 20, TemplateId =  "coyote" }, 
				{ Count = 15, TemplateId =  "black_bear" }, 
				{ Count = 20, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 
				{ Count = 20, TemplateId = "brown_bear" }, 
				{ Count = 15, TemplateId = "wolf_black" }, 
			},
			Hard = {
				{ Count = 5, TemplateId =  "coyote" }, 
				{ Count = 5, TemplateId =  "black_bear" }, 
				{ Count = 5, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 
				{ Count = 10, TemplateId = "brown_bear" }, 
				{ Count = 25, TemplateId = "wolf_black" },
				{ Count = 20, TemplateId = "grizzly_bear" },
				{ Count = 20, TemplateId = "great_hart" },
			},
		},
		BarrenLands = {
			Easy = { 
				{ Count = 30, TemplateId =  "snake_sand" }, 
				{ Count = 30, TemplateId =  "beetle" }, 
				{ Count = 5, TemplateId =  "wolf_desert" },
			},
		},
		BlackForest = {
			Easy = { 
				{ Count = 30, TemplateId =  "bat" }, 
				{ Count = 30, TemplateId =  "spider" }, 
				{ Count = 5, TemplateId =  "spider_large" },
			},
		},
	},

	MaleHeads = { "head_male02","head_male03","head_male04","head_male05" },
	FemaleHeads = { "head_female01","head_female02","head_female03","head_female04","head_female05","head_female06", },

	MaleWaywardHeads = {{"head_male04","FF9933"} },
	FemaleWaywardHeads = { {"head_female04","FF9933"}, },

	FoundersDancersMaleHeads = { "head_male02","head_male03","head_male04","head_male05","head_male02","head_male03","head_male04","head_male05","head_male02","head_male03","head_male04","head_male05",
	{"head_male02"},{"head_male03"},{"head_male04"},{"head_male05",},},

	FoundersDancersFemaleHeads = { "head_female01","head_female02","head_female03","head_female04","head_female05","head_female06", "head_female01","head_female02","head_female03","head_female04","head_female05","head_female06",
	{"head_female01"},{"head_female02"},{"head_female03",},{"head_female04"},{"head_female05"},{"head_female01",},},

	MaleWaywardHair = {
		{"hair_male_messy","0xFF191919"},
		{"hair_male_buzzcut","0xFF191919"},
		{"","0xFF191919"},
		{"","0xFF191919"},
	},
	FemaleWaywardHair = {
		{"hair_female_shaggy","0xFF191919"},
	},

	MaleHairSlave = 
	{
	{"hair_male_buzzcut","0xFF554838"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFFA7856A"},
	{"hair_male_buzzcut","0xFFB55239"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFFB7A69E"},
	{"hair_male_buzzcut","0xFFB9B184"},

	{"hair_male","0xFF554838"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFA7856A"},
	{"hair_male","0xFFB55239"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFB7A69E"},
	{"hair_male","0xFFB9B184"},

	{"","0xFF554838"},
	{"","0xFFA56B46"},
	{"","0xFFA7856A"},
	{"","0xFFB55239"},
	{"","0xFFA56B46"},
	{"","0xFFB7A69E"},
	{"","0xFFB9B184"},
	},

	MaleHairBeggar = 
	{
	{"hair_male_messy","0xFF554838"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFFA7856A"},
	{"hair_male_messy","0xFFB55239"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFFB7A69E"},
	{"hair_male_messy","0xFFB9B184"},

	{"hair_male","0xFF554838"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFA7856A"},
	{"hair_male","0xFFB55239"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFB7A69E"},
	{"hair_male","0xFFB9B184"},

	{"hair_male_windswept","0xFF554838"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFFA7856A"},
	{"hair_male_windswept","0xFFB55239"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFFB7A69E"},
	{"hair_male_windswept","0xFFB9B184"},
	
	{"","0xFF554838"},
	{"","0xFFA56B46"},
	{"","0xFFA7856A"},
	{"","0xFFB55239"},
	{"","0xFFA56B46"},
	{"","0xFFB7A69E"},
	{"","0xFFB9B184"},
	},

	MaleHairVillage = 
	{
	{"hair_male","0xFF554838"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFA7856A"},
	{"hair_male","0xFFB55239"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFB7A69E"},
	{"hair_male","0xFFB9B184"},

	{"hair_male_bangs","0xFF554838"},
	{"hair_male_bangs","0xFFA56B46"},
	{"hair_male_bangs","0xFFA7856A"},
	{"hair_male_bangs","0xFFB55239"},
	{"hair_male_bangs","0xFFA56B46"},
	{"hair_male_bangs","0xFFB7A69E"},
	{"hair_male_bangs","0xFFB9B184"},

	{"hair_male_buzzcut","0xFF554838"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFFA7856A"},
	{"hair_male_buzzcut","0xFFB55239"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFFB7A69E"},
	{"hair_male_buzzcut","0xFFB9B184"},

	{"hair_male_messy","0xFF554838"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFFA7856A"},
	{"hair_male_messy","0xFFB55239"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFFB7A69E"},
	{"hair_male_messy","0xFFB9B184"},

	{"hair_male_nobleman","0xFF554838"},
	{"hair_male_nobleman","0xFFA56B46"},
	{"hair_male_nobleman","0xFFA7856A"},
	{"hair_male_nobleman","0xFFB55239"},
	{"hair_male_nobleman","0xFFA56B46"},
	{"hair_male_nobleman","0xFFB7A69E"},
	{"hair_male_nobleman","0xFFB9B184"},

	{"hair_male_rougish","0xFF554838"},
	{"hair_male_rougish","0xFFA56B46"},
	{"hair_male_rougish","0xFFA7856A"},
	{"hair_male_rougish","0xFFB55239"},
	{"hair_male_rougish","0xFFA56B46"},
	{"hair_male_rougish","0xFFB7A69E"},
	{"hair_male_rougish","0xFFB9B184"},

	{"hair_male_sideundercut","0xFF554838"},
	{"hair_male_sideundercut","0xFFA56B46"},
	{"hair_male_sideundercut","0xFFA7856A"},
	{"hair_male_sideundercut","0xFFB55239"},
	{"hair_male_sideundercut","0xFFA56B46"},
	{"hair_male_sideundercut","0xFFB7A69E"},
	{"hair_male_sideundercut","0xFFB9B184"},
	
	{"hair_male_undercut","0xFF554838"},
	{"hair_male_undercut","0xFFA56B46"},
	{"hair_male_undercut","0xFFA7856A"},
	{"hair_male_undercut","0xFFB55239"},
	{"hair_male_undercut","0xFFA56B46"},
	{"hair_male_undercut","0xFFB7A69E"},
	{"hair_male_undercut","0xFFB9B184"},
	
	{"hair_male_windswept","0xFF554838"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFFA7856A"},
	{"hair_male_windswept","0xFFB55239"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFFB7A69E"},
	{"hair_male_windswept","0xFFB9B184"},

	{"","0xFF554838"},
	{"","0xFFA56B46"},
	{"","0xFFA7856A"},
	{"","0xFFB55239"},
	{"","0xFFA56B46"},
	{"","0xFFB7A69E"},
	{"","0xFFB9B184"},
	
	},	

	FemaleHairSlave = 
	{
	{"hair_female_shaggy","0xFF554838"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFA7856A"},
	{"hair_female_shaggy","0xFFB55239"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFB7A69E"},
	{"hair_female_shaggy","0xFFB9B184"},

	{"hair_female_buzzcut","0xFF554838"},
	{"hair_female_buzzcut","0xFFA56B46"},
	{"hair_female_buzzcut","0xFFA7856A"},
	{"hair_female_buzzcut","0xFFB55239"},
	{"hair_female_buzzcut","0xFFA56B46"},
	{"hair_female_buzzcut","0xFFB7A69E"},
	{"hair_female_buzzcut","0xFFB9B184"},

	{"hair_cultist_female","0xFF554838"},
	{"hair_cultist_female","0xFFA56B46"},
	{"hair_cultist_female","0xFFA7856A"},
	{"hair_cultist_female","0xFFB55239"},
	{"hair_cultist_female","0xFFA56B46"},
	{"hair_cultist_female","0xFFB7A69E"},
	{"hair_cultist_female","0xFFB9B184"},
	},

	FemaleHairBeggar = 
	{
	{"hair_female_shaggy","0xFF554838"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFA7856A"},
	{"hair_female_shaggy","0xFFB55239"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFB7A69E"},
	{"hair_female_shaggy","0xFFB9B184"},

	{"hair_female","0xFF554838"},
	{"hair_female","0xFFA56B46"},
	{"hair_female","0xFFA7856A"},
	{"hair_female","0xFFB55239"},
	{"hair_female","0xFFA56B46"},
	{"hair_female","0xFFB7A69E"},
	{"hair_female","0xFFB9B184"},

	{"hair_female_pigtails","0xFF554838"},
	{"hair_female_pigtails","0xFFA56B46"},
	{"hair_female_pigtails","0xFFA7856A"},
	{"hair_female_pigtails","0xFFB55239"},
	{"hair_female_pigtails","0xFFA56B46"},
	{"hair_female_pigtails","0xFFB7A69E"},
	{"hair_female_pigtails","0xFFB9B184"},
	
	{"hair_female_bun","0xFF554838"},
	{"hair_female_bun","0xFFA56B46"},
	{"hair_female_bun","0xFFA7856A"},
	{"hair_female_bun","0xFFB55239"},
	{"hair_female_bun","0xFFA56B46"},
	{"hair_female_bun","0xFFB7A69E"},
	{"hair_female_bun","0xFFB9B184"},
	},

	FemaleHairVillage = 
	{
	{"hair_female","0xFF554838"},
	{"hair_female","0xFFA56B46"},
	{"hair_female","0xFFA7856A"},
	{"hair_female","0xFFB55239"},
	{"hair_female","0xFFA56B46"},
	{"hair_female","0xFFB7A69E"},
	{"hair_female","0xFFB9B184"},

	{"hair_female_pigtails","0xFF554838"},
	{"hair_female_pigtails","0xFFA56B46"},
	{"hair_female_pigtails","0xFFA7856A"},
	{"hair_female_pigtails","0xFFB55239"},
	{"hair_female_pigtails","0xFFA56B46"},
	{"hair_female_pigtails","0xFFB7A69E"},
	{"hair_female_pigtails","0xFFB9B184"},

	{"hair_female_bob","0xFF554838"},
	{"hair_female_bob","0xFFA56B46"},
	{"hair_female_bob","0xFFA7856A"},
	{"hair_female_bob","0xFFB55239"},
	{"hair_female_bob","0xFFA56B46"},
	{"hair_female_bob","0xFFB7A69E"},
	{"hair_female_bob","0xFFB9B184"},

	{"hair_female_bun","0xFF554838"},
	{"hair_female_bun","0xFFA56B46"},
	{"hair_female_bun","0xFFA7856A"},
	{"hair_female_bun","0xFFB55239"},
	{"hair_female_bun","0xFFA56B46"},
	{"hair_female_bun","0xFFB7A69E"},
	{"hair_female_bun","0xFFB9B184"},

	{"hair_female_ponytail","0xFF554838"},
	{"hair_female_ponytail","0xFFA56B46"},
	{"hair_female_ponytail","0xFFA7856A"},
	{"hair_female_ponytail","0xFFB55239"},
	{"hair_female_ponytail","0xFFA56B46"},
	{"hair_female_ponytail","0xFFB7A69E"},
	{"hair_female_ponytail","0xFFB9B184"},

	{"hair_female_shaggy","0xFF554838"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFA7856A"},
	{"hair_female_shaggy","0xFFB55239"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFB7A69E"},
	{"hair_female_shaggy","0xFFB9B184"},
	},	

	FemaleHairFounders = 
	{
	{"hair_female","0xFFB55239"},
	{"hair_female","0xFFFF7F00"},
	{"hair_female","0xFF339933"},
	{"hair_female","0xFFB55239"},
	{"hair_female","0xFFA56B46"},
	{"hair_female","0xFFB7A69E"},
	{"hair_female","0xFFB9B184"},
	{"hair_female","0xFF191919"},
	{"hair_female","0xFF339933"},
	{"hair_female","0xFF3399FF"},
	{"hair_female","0xFFFF66A3"},

	{"hair_female_pigtails","0xFFB55239"},
	{"hair_female_pigtails","0xFFFF7F00"},
	{"hair_female_pigtails","0xFF339933"},
	{"hair_female_pigtails","0xFFB55239"},
	{"hair_female_pigtails","0xFFA56B46"},
	{"hair_female_pigtails","0xFFB7A69E"},
	{"hair_female_pigtails","0xFFB9B184"},
	{"hair_female_pigtails","0xFF191919"},
	{"hair_female_pigtails","0xFF339933"},
	{"hair_female_pigtails","0xFF3399FF"},
	{"hair_female_pigtails","0xFFFF66A3"},

	{"hair_female_bob","0xFFB55239"},
	{"hair_female_bob","0xFFFF7F00"},
	{"hair_female_bob","0xFF339933"},
	{"hair_female_bob","0xFFB55239"},
	{"hair_female_bob","0xFFA56B46"},
	{"hair_female_bob","0xFFB7A69E"},
	{"hair_female_bob","0xFFB9B184"},
	{"hair_female_bob","0xFF191919"},
	{"hair_female_bob","0xFF339933"},
	{"hair_female_bob","0xFF3399FF"},
	{"hair_female_bob","0xFFFF66A3"},

	{"hair_female_bun","0xFFB55239"},
	{"hair_female_bun","0xFFFF7F00"},
	{"hair_female_bun","0xFF339933"},
	{"hair_female_bun","0xFFB55239"},
	{"hair_female_bun","0xFFA56B46"},
	{"hair_female_bun","0xFFB7A69E"},
	{"hair_female_bun","0xFFB9B184"},
	{"hair_female_bun","0xFF191919"},
	{"hair_female_bun","0xFF339933"},
	{"hair_female_bun","0xFF3399FF"},
	{"hair_female_bun","0xFFFF66A3"},

	{"hair_female_ponytail","0xFFB55239"},
	{"hair_female_ponytail","0xFFFF7F00"},
	{"hair_female_ponytail","0xFF339933"},
	{"hair_female_ponytail","0xFFB55239"},
	{"hair_female_ponytail","0xFFA56B46"},
	{"hair_female_ponytail","0xFFB7A69E"},
	{"hair_female_ponytail","0xFF191919"},
	{"hair_female_ponytail","0xFF339933"},
	{"hair_female_ponytail","0xFF3399FF"},
	{"hair_female_ponytail","0xFFFF66A3"},

	{"hair_female_shaggy","0xFFB55239"},
	{"hair_female_shaggy","0xFFFF7F00"},
	{"hair_female_shaggy","0xFF339933"},
	{"hair_female_shaggy","0xFFB55239"},
	{"hair_female_shaggy","0xFFA56B46"},
	{"hair_female_shaggy","0xFFB7A69E"},
	{"hair_female_shaggy","0xFFB9B184"},
	{"hair_female_shaggy","0xFF191919"},
	{"hair_female_shaggy","0xFF339933"},
	{"hair_female_shaggy","0xFF3399FF"},
	{"hair_female_shaggy","0xFFFF66A3"},
	},	

	MaleHairFounders = 
	{
	{"hair_male","0xFF554838"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFFA7856A"},
	{"hair_male","0xFFB55239"},
	{"hair_male","0xFFA56B46"},
	{"hair_male","0xFF191919"},
	{"hair_male","0xFFB55239"},

	{"hair_male_bangs","0xFF554838"},
	{"hair_male_bangs","0xFFA56B46"},
	{"hair_male_bangs","0xFFA7856A"},
	{"hair_male_bangs","0xFFB55239"},
	{"hair_male_bangs","0xFFA56B46"},
	{"hair_male_bangs","0xFF191919"},
	{"hair_male_bangs","0xFFB55239"},

	{"hair_male_buzzcut","0xFF554838"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFFA7856A"},
	{"hair_male_buzzcut","0xFFB55239"},
	{"hair_male_buzzcut","0xFFA56B46"},
	{"hair_male_buzzcut","0xFF191919"},
	{"hair_male_buzzcut","0xFFB55239"},

	{"hair_male_messy","0xFF554838"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFFA7856A"},
	{"hair_male_messy","0xFFB55239"},
	{"hair_male_messy","0xFFA56B46"},
	{"hair_male_messy","0xFF191919"},
	{"hair_male_messy","0xFFB55239"},

	{"hair_male_nobleman","0xFF554838"},
	{"hair_male_nobleman","0xFFA56B46"},
	{"hair_male_nobleman","0xFFA7856A"},
	{"hair_male_nobleman","0xFFB55239"},
	{"hair_male_nobleman","0xFFA56B46"},
	{"hair_male_nobleman","0xFF191919"},
	{"hair_male_nobleman","0xFFB55239"},

	{"hair_male_rougish","0xFF554838"},
	{"hair_male_rougish","0xFFA56B46"},
	{"hair_male_rougish","0xFFA7856A"},
	{"hair_male_rougish","0xFFB55239"},
	{"hair_male_rougish","0xFFA56B46"},
	{"hair_male_rougish","0xFF191919"},
	{"hair_male_rougish","0xFFB55239"},

	{"hair_male_sideundercut","0xFF554838"},
	{"hair_male_sideundercut","0xFFA56B46"},
	{"hair_male_sideundercut","0xFFA7856A"},
	{"hair_male_sideundercut","0xFFB55239"},
	{"hair_male_sideundercut","0xFFA56B46"},
	{"hair_male_sideundercut","0xFF191919"},
	{"hair_male_sideundercut","0xFFB55239"},
	
	{"hair_male_undercut","0xFF554838"},
	{"hair_male_undercut","0xFFA56B46"},
	{"hair_male_undercut","0xFFA7856A"},
	{"hair_male_undercut","0xFFB55239"},
	{"hair_male_undercut","0xFFA56B46"},
	{"hair_male_undercut","0xFF191919"},
	{"hair_male_undercut","0xFFB55239"},
	
	{"hair_male_windswept","0xFF554838"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFFA7856A"},
	{"hair_male_windswept","0xFFB55239"},
	{"hair_male_windswept","0xFFA56B46"},
	{"hair_male_windswept","0xFF191919"},
	{"hair_male_windswept","0xFFB55239"},

	{"","0xFF554838"},
	{"","0xFFA56B46"},
	{"","0xFFA7856A"},
	{"","0xFFB55239"},
	{"","0xFFA56B46"},
	{"","0xFFB7A69E"},
	{"","0xFFB55239"},
	
	},	
	FoundersChests =  { {"founders_chest_base","0xFFFFFF00"},
						{"founders_chest_base","0xFFFF00FF"},
						{"founders_chest_base","0xFF00FFFF"},
						{"founders_chest_base","0xFFFF0000"},
						{"founders_chest_base","0xFF00FF00"},
						{"founders_chest_base","0xFF0000FF"}, 
						{"founders_chest2_base","0xFFFFFF00"},
						{"founders_chest2_base","0xFFFF00FF"},
						{"founders_chest2_base","0xFF00FFFF"},
						{"founders_chest2_base","0xFFFF0000"},
						{"founders_chest2_base","0xFF00FF00"},
						{"founders_chest2_base","0xFF0000FF"},
					    {"founders_chest3_base","0xFFFFFF00"},
					    {"founders_chest3_base","0xFFFF00FF"},
					    {"founders_chest3_base","0xFF00FFFF"},
					    {"founders_chest3_base","0xFFFF0000"},
					    {"founders_chest3_base","0xFF00FF00"},
					    {"founders_chest3_base","0xFF0000FF"},},

	FoundersLegs =  { 	{"founders_legs_base","0xFFFFFF00"},
						{"founders_legs_base","0xFFFF00FF"},
						{"founders_legs_base","0xFF00FFFF"},
						{"founders_legs_base","0xFFFF0000"},
						{"founders_legs_base","0xFF00FF00"},
						{"founders_legs_base","0xFF0000FF"},
						{"founders_legs2_base","0xFFFFFF00"},
						{"founders_legs2_base","0xFFFF00FF"},
						{"founders_legs2_base","0xFF00FFFF"},
						{"founders_legs2_base","0xFFFF0000"},
						{"founders_legs2_base","0xFF00FF00"},
						{"founders_legs2_base","0xFF0000FF"},
					    {"founders_legs3_base","0xFFFFFF00"},
					    {"founders_legs3_base","0xFFFF00FF"},
					    {"founders_legs3_base","0xFF00FFFF"},
					    {"founders_legs3_base","0xFFFF0000"},
					    {"founders_legs3_base","0xFF00FF00"},
					    {"founders_legs3_base","0xFF0000FF"}, },

	ShortNames = {"Uadi","Thraz","Inall","Veat","Honst","Noom","Otasa",
					"Enysa","Urody","Irv","Blard","Eelde","Torn",
					"Amora","Hourd","Kalrr","Etr","Kas","Oldk","Igary",
					"Cric","Schir","Morgh","Eato","Garlt","Yero","Itb",
					"Essr","Oesta","Ieta","Eono","Irl","Troum","Drack",
					"Elmrr","Kals","Drald","Onyu","Dral","Urake","Ymose",
					"Ghaf","Enc","Oeme","Yalei","Oaleo","Utia","Roip",
					"Ingsh","Oesty","Nos","Chroh","Endd","Emss","Yacky",
					"Dret","Lerd","Vof","Itai","Aentha","Morz","Irany",
					"Tiash","Abele","Epolo","Kon","Skelnd","Onale","Lak",
					"Neyl","Iusta","Than","Uomu","Neiy","Athnn","Enll",
					"Risck","Yskeli","Ashp","Utury","Tinq","Yquay","Deys",
					"Orada","Yessi","Eaugho","Taih","Voel","Mort","Veur",
					"Yhata","Ihiny","Alee","Ieno","Itr","Therr","Ytaio",
					"Queck","Nysz","Denn","Belf","Alend","Overy","Rich",
					"Ryng","Swaeck","Abelo","Iemi","Yomi","Etaie","Quel",
					"Soirt","Yuski","Chriat","Oosa","Cerph","Orilo"},

	MinerNames = {"Uadi the Miner","Thraz the Miner","Inall the Miner","Veat the Miner","Honst the Miner","Noom the Miner","Otasa the Miner",
					"Enysa the Miner","Urody the Miner","Irv the Miner","Blard the Miner","Eelde the Miner","Torn the Miner",
					"Amora the Miner","Hourd the Miner","Kalrr the Miner","Etr the Miner","Kas the Miner","Oldk the Miner","Igary the Miner",
					"Cric the Miner","Schir the Miner","Morgh the Miner","Eato the Miner","Garlt the Miner","Yero the Miner","Itb the Miner",
					"Essr the Miner","Oesta the Miner","Ieta the Miner","Eono the Miner","Irl the Miner","Troum the Miner","Drack the Miner",
					"Elmrr the Miner","Kals the Miner","Drald the Miner","Onyu the Miner","Dral the Miner","Urake the Miner","Ymose the Miner",
					"Ghaf the Miner","Enc the Miner","Oeme the Miner","Yalei the Miner","Oaleo the Miner","Utia the Miner","Roip the Miner",
					"Ingsh the Miner","Oesty the Miner","Nos the Miner","Chroh the Miner","Endd the Miner","Emss the Miner","Yacky the Miner",
					"Dret the Miner","Lerd the Miner","Vof the Miner","Itai the Miner","Aentha the Miner","Morz the Miner","Irany the Miner",
					"Tiash the Miner","Abele the Miner","Epolo the Miner","Kon the Miner","Skelnd the Miner","Onale the Miner","Lak the Miner",
					"Neyl the Miner","Iusta the Miner","Than the Miner","Uomu the Miner","Neiy the Miner","Athnn the Miner","Enll the Miner",
					"Risck the Miner","Yskeli the Miner","Ashp the Miner","Utury the Miner","Tinq the Miner","Yquay the Miner","Deys the Miner",
					"Orada the Miner","Yessi the Miner","Eaugho the Miner","Taih the Miner","Voel the Miner","Mort the Miner","Veur the Miner",
					"Yhata the Miner","Ihiny the Miner","Alee the Miner","Ieno the Miner","Itr the Miner","Therr the Miner","Ytaio the Miner",
					"Queck the Miner","Nysz the Miner","Denn the Miner","Belf the Miner","Alend the Miner","Overy the Miner","Rich the Miner",
					"Ryng the Miner","Swaeck the Miner","Abelo the Miner","Iemi the Miner","Yomi the Miner","Etaie the Miner","Quel the Miner",
					"Soirt the Miner","Yuski the Miner","Chriat the Miner","Oosa the Miner","Cerph the Miner","Orilo the Miner"},

	MaleNpcNames = {"Abaet","Abarden","Acamen","Achard","Adeen","Aerden","Aghon","Agnar","Ahburn","Ahdun","Airen","Airis","Aldaren","Alderman","Alkirk","Allso","Amitel","Anfar","Anumil","Asden","Asen","Aslan","Atgur","Atlin","Auden","Ault","Aysen","Bacohl","Badeek","Balati","Baradeer","Basden","Bayde","Bedic","Beeron","Beson","Besur","Bewul","Biedgar","Biston","Bithon","Boaldelr","Bolrock","Breanon","Bredere","Bredock","Breen","Bristan","Buchmeid","Busma","Buthomar","Caelholdt","Cainon","Camchak","Camilde","Casden","Cayold","Celorn","Celthric","Cerdern","Cespar","Cevelt","Chamon","Chidak","Cibrock","Ciroc","Codern","Connell","Cordale","Cosdeer","Cuparun","Cydare","Cylmar","Cyton","Daburn","Daermod","Dakamon","Dakkone","Dalmarn","Dapvhir","Darkkon","Darko","Darmor","Darpick","Dask","Deathmar","Derik","Derrin","Dessfar","Dinfar","Doceon","Dochrohan","Dorn","Dosoman","Drakone","Drandon","Dritz","Drophar","Dryn","Duba","Duran","Durmark","Dyfar","Dyten","Eard","Eckard","Efar","Egmardern","Ekgamut","Eli","Elson","Elthin","Endor","Enidin","Enro","Erikarn","Eritai","Escariet","Etar","Etburn","Ethen","Etmere","Eythil","Faoturk","Faowind","Fenrirr","Fetmar","Ficadon","Fickfylo","Firedorn","Firiro","Folmard","Fraderk","Fydar","Fyn","Gafolern","Gai","Galiron","Gametris","Gemardt","Gemedern","Gerirr","Geth","Gibolock","Gibolt","Gom","Gosford","Gothikar","Gresforn","Gryn","Gundir","Guthale","Gybol","Gyin","Halmar","Harrenhal","Hectar","Hecton","Hermenze","Hermuck","Hildale","Hildar","Hydale","Hyten","Iarmod","Idon","Ieserk","Ikar","Illilorn","Illium","Ipedorn","Irefist","Isen","Isil","Jackson","Jalil","Janus","Jayco","Jesco","Jespar","Jex","Jib","Jin","Juktar","Jun","Justal","Kafar","Kaldar","Keran","Kesad","Kethren","Kib","Kiden","Kilbas","Kildarien","Kimdar","Kip","Kirder","Kolmorn","Kyrad","Lackus","Lacspor","Lafornon","Lahorn","Ledale","Leit","Lephidiles","Lerin","Letor","Lidorn","Liphanes","Loban","Ludokrin","Luphildern","Lurd","Macon","Madarlon","Marderdeen","Mardin","Markdoon","Marklin","Mathar","Medarin","Mellamo","Meowol","Meridan","Merkesh","Mes'ard","Mesophan","Mezo","Michael","Mickal","Migorn","Miphates","Mi'talrythin","Modric","Modum","Mufar","Mujarin","Mythik","Mythil","Nadeer","Nalfar","Naphates","Neowyld","Nikpal","Nikrolin","Niro","Noford","Nuthor","Nuwolf","Nythil","O’tho","Ocarin","Occhi","Odaren","Ohethlic","Okar","Omarn","Orin","Othelen","Oxbaren","Padan","Palid","Peitar","Pelphides","Pendus","Perder","Phairdon","Phemedes","Phoenix","Picon","Picumar","Pildoor","Ponith","Poran","Prothalon","Puthor","Qeisan","Qidan","Quid","Quiss","Qysan","Radag'mal","Randar","Rayth","Reaper","Reth","Rethik","Rhithin","Rhysling","Rikar","Rismak","Ritic","Rogeir","Rogoth","Rophan","Rydan","Ryfar","Ryodan","Rysdan","Rythern","Sabal","Sadareen","Samon","Samot","Scoth","Scythe","Sed","Sedar","Senthyril","Serin","Seryth","Sesmidat","Setlo","Shade","Shane","Shard","Shillen","Silco","Sil'forrin","Silpal","Soderman","Sothale","Stenwulf","Steven","Suth","Sutlin","Syth","Sythril","Talberon","Telpur","Temilfist","Tempist","Tespar","Tessino","Thiltran","Tholan","Tibolt","Ticharol","Tithan","Tobale","Tol’","Tolle","Tolsar","Tothale","Tousba","Tuk","Tuscanar","Tyden","Uerthe","Ugmar","Undin","Updar","Vaccon","Vacone","Valynard","Vectomon","Vespar","Vethelot","Vider","Vigoth","Vildar","Vinald","Virde","Voltain","Voudim","Vythethi","Wak’dern","Walkar","Wekmar","Werymn","William","Willican","Wiltmar","Wishane","Wrathran","Wraythe","Wyder","Wyeth","Xander","Xavier","Xex","Xithyl","Y’reth","Yabaro","Yesirn","Yssik","Zak","Zakarn","Zeke","Zerin","Zidar","Zigmal","Zilocke","Zio","Zotar","Zutar","Zyten"},
	FemaleNpcNames = {"Acele","Acholate","Ada","Adiannon","Adorra","Ahanna","Akara","Akassa","Akia","Amaerilde","Amara","Amarisa","Amarizi","Ana","Andonna","Ani","Annalyn","Archane","Ariannona","Arina","Arryn","Asada","Awnia","Ayne","Basete","Bathelie","Bethe","Brana","Brianan","Bridonna","Brynhilde","Calene","Calina","Celestine","Celoa","Cephenrene","Chani","Chivahle","Chrystyne","Corda","Cyelena","Dalavesta","Desini","Dylena","Ebatryne","Ecematare","Efari","Enaldie","Enoka","Enoona","Errinaya","Fayne","Frederika","Frida","Gene","Gessane","Gronalyn","Gvene","Gwethana","Halete","Helenia","Hildandi","Hyza","Idona","Ikini","Ilene","Illia","Iona","Jessika","Jezzine","Justalyne","Kassina","Kilayox","Kilia","Kilyne","Kressara","Laela","Laenaya","Lelani","Lenala","Linovahle","Linyah","Lloyanda","Lolinda","Lyna","Lynessa","Mehande","Melisande","Midiga","Mirayam","Mylene","Nachaloa","Naria","Narisa","Nelenna","Niraya","Nymira","Ochala","Olivia","Onathe","Ondola","Orwyne","Parthinia","Pascheine","Pela","Peri’el","Pharysene","Philadona","Prisane","Prysala","Pythe","Q’ara","Q’pala","Quasee","Rhyanon","Rivatha","Ryiah","Sanala","Sathe","Senira","Sennetta","Sepherene","Serane","Sevestra","Sidara","Sidathe","Sina","Sunete","Synestra","Sythini","Szene","Tabika","Tabithi","Tajule","Tamare","Teresse","Tolida","Tonica","Treka","Tressa","Trinsa","Tryane","Tybressa","Tycane","Tysinni","Undaria","Uneste","Urda","Usara","Useli","Ussesa","Venessa","Veseere","Voladea","Vysarane","Vythica","Wanera","Welisarne","Wellisa","Wesolyne","Wyeta","Yilvoxe","Ysane","Yve","Yviene","Yvonnette","Yysara","Zana","Zathe","Zecele","Zenobia","Zephale","Zephere","Zerma","Zestia","Zilka","Zoura","Zrye","Zyneste","Zynoa"},
	MalePlayerNames = {"Austin","Dan","Supreem","DukeGorilla","TonyTheHutt","Drayak","Zarcul","Troop LoD","dbltnk LoD","Ezekiel","Jairone","Obs LoD","BannerKC LOD","veeego","BlackLung LoD","Harry LoD","Dung Fist","Shibby","Rumble","Darknight","Foghladha","AlexSage","Tombles","Netnet","bensku","Gundehar","ColdMarrow","Auren Stark","Balkazarno","Rainman LoD","Lavos","Grimskab","Nexid","Mark Trail","IceCrow","Austin","Toad","Ursoc","Sarl","Freerk","TheFreshness","SkullLoD","SteelReign","Wayist","Freq","Bhais","sklurb","Jitsu","Laufer LoD","Tabarnak LoD","Starfire3D","Oliver","Theos","Nitroman","Kal","robl","Gunga Din","Conbro","Dargron","Mystikal","FLIPSKU","Zeph","Min","Sir Bob","Helis","Deathreign","TonyTheHutt","Fror","Harald Blackfist","Noobzilla","Corvyn","Crucible","Howler","BaGz","RaVaGe","tryerino","splixx","Dargron2","Zerdoza","mindseye","Sekkerhund","Soth","Gone","Schwarze Schaf","Mengbilar","Alpha","Lavender","Zerdozer","Slaughterjoe","Codex","Ravenclaw","Papi","Zaxion","Liberty","Raith","Dax","Chargul","Amero","Grim","Macros","Deimos","Majister","Vanak","Alien","Rada Lorkaaz","Tony","Yorlik","Brage","adbos","Zerojr","Behanglul","Lagardere","Poukah","Otto Sandrock","TEEM","SatelliteMind","Idontknow","Jazard","Myrkrid Ashen","Ursoc","Ugliness","Amishphenom","Ragestar","Ainz","Brave SirRobin","hierba","Lore","Ranghar","Indeed","Rinnsu","Hawkfire","Dago","Shadowpain","Shadowhunt","Hugo","Rorick","Gurney","Odin","Knuddenstore","Runesabre","Tabulazero","Hawkfyre II","Cassyr Dubois","Lufari","Photosozo","Finvega","Caerbanog","Fletch","Asmorex","Coins","Schattig","Tofu","JooGar","Caerbanog VanRurik","Zabalus","Abimor","Cookie","CookieTM","Kaid","Raja","Dunkelzwerg","Aokis","Verdorben","Xerxes ","Hilip","Chester","Kerbox","Hexplate","Ness199X","SquashSash","squid","Newbatonium","Slik","Magnus","deatrider","Enarial","Betarays","Yoofaloof","blasteron","Turk Shakespeare","Tintagil","Node","Jtoriko","Fortisq","aada","Lordy","HKPackee","Sunkara","Eden","Mayushi","Phearnun","Drexy","Solaris","Cammoo","Supercub013","Hessian","Rooster Castille","Misneach","Samuel","ReigekiKai","Kush Monstar","Certhas","Matt","Drydenn","Arconious","Nazrudin","Tharalik","Gnerix","Asmodai","Velit","Belador","Maganis","NiTe","aumua","Kourgath","Calheb","Deege","Raldor Santoro","Taelyn","Tokee","Exiled","Huart","Apronks","Icky","Lenart","Bidimus","Ekul","Auxilium","Dwyane Wade","Stucky","Sparda","Merif","Highfather","Kysper","migdr","Lester","Alipheese","TheChester","Nazrudin","Padag","Jasc Onine","Rapolda","Girion Telchar","Dirlettia","Parallaxe","Ecksesive","Sunroc","Trok","Cear Dragon","Gand","vidocq","Siodacain","Genoce","Koen","Arkii","Auzze","DocSteal","Eidenuul","Kintaro","Rylok","Splinter","Gnar","Tynian","Lauro","Drake","Sheltem","Trabbie","Ajax","Brauhm","Voldemort","Abaddon","Tarvok","Sebi Vayah","Whiskeypops","Graycloud","Nalahir","Raven","Kaelem","Colge","Tetravus","Miaobao","Gaston","Gondalf" },
	FemalePlayerNames = {"Kerrigan","Adida","Karma LoD","Danue Sunrider","verve","Achira","Adaline","Amarlia","Amoren","Meldor","Elinari","Selrina","Isha","Prana","Emy","Dixen","squid friend","Jebra","Anzha","Humblemisty","Cyliena","Hermannie","Sea","Ceres","viv","Sanja","Kanyin","Kitty","Eiahn","Miroquin","Tala","Swizzle","Culaan","Marean Lumia","Heather","Celinda Sheridan","JolaughnLLTS","Avanah","Netherwynn","Sylanavan","Aurelia","catsparkle","Felina Covenant","NickyB","Ahina","SilverSong","Sumsare","Hussy","Ali","Seylara Vigdisdottir","Brigitte"},
}