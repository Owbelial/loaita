require 'base_ai_mob'
require 'base_ai_intelligent'

this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"SetHue")

RegisterEventHandler(EventType.Message, "HasDiedMessage",
    function(killer)
        if (not this:HasObjVar("IsGhost")) then
    	   --MoveEquipmentToGround(this,this:GetObjVar("DoNotSpawnChest"))
        else
        	--DFB TODO: Create ghost ectoplasm here.
        end
    	--this:ScheduleTimerDelay(TimeSpan.FromSeconds(0.5),"DestroyWraith")
    	PlayEffectAtLoc("VoidPillar",this:GetLoc(),2)
    end)

RegisterEventHandler(EventType.Timer,"DestroyWraith",function ( ... )
	this:Destroy()
end)

RegisterEventHandler(EventType.Timer,"SetHue",
function()
	if (this:HasObjVar("IsGhost")) then
		this:SetObjVar("IsGhost",true)
		this:SetCloak(true)
		--this:GetEquippedObject("Chest"):SetHue("0xC100FFFF")
		--this:GetEquippedObject("Legs"):SetHue("0xC100FFFF")
	end
end)
