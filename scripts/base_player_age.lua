function OnAgeLoad()
	this:SetObjVar("LoginTime",DateTime.UtcNow)
end


RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		OnAgeLoad()
	end)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function ( ... )
		if not(this:HasObjVar("CreationDate")) then 
			this:SetObjVar("CreationDate",DateTime.UtcNow)
		end

		OnAgeLoad()
	end)
--[[
function GetPlayedTime()
	local playedTimeSecs = this:GetObjVar("PlayedTime") or 0
	local currentPlayed = DateTime.UtcNow - (this:GetObjVar("LoginTime") or DateTime.UtcNow)
	--DebugMessage("Played",DateTime.UtcNow:ToString(),this:GetObjVar("LoginTime"):ToString())
	return playedTimeSecs + currentPlayed.TotalSeconds
end

RegisterEventHandler(EventType.UserLogout,"", 
	function ( ... )
		this:SetObjVar("PlayedTime",GetPlayedTime())
	end)
]]