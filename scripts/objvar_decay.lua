DEFAULT_DECAYTIME = 300

function ScheduleDecay(decayTime)
	if(decayTime == nil) then
		decayTime = this:GetObjVar("ObjVarDecayTime")		
	end

	if( decayTime == nil ) then
		decayTime = DEFAULT_DECAYTIME
	end

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(decayTime), "objvar_decay")
end

RegisterSingleEventHandler(EventType.ModuleAttached, "objvar_decay", 
	function()
		ScheduleDecay()
	end)

RegisterEventHandler(EventType.Message, "UpdateObjVarDecay", 
	function(decayTime)
		ScheduleDecay(decayTime)
	end)

RegisterEventHandler(EventType.Timer, "objvar_decay", 
	function()
		this:DelObjVar(this:GetObjVar("ObjVarDel"))
		this:DelObjVar("ObjVarDel")
		this:DelModule(GetCurrentModule())
	end)