require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_padding")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_padding", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)
		
		if(enName ~= "Padding") then return end
		AlterEquipmentBonusStat(this, "BonusBashingResist", 1)
		AlterEquipmentBonusStat(this, "BonusAbsorption", 1)
			this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)