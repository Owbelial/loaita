require 'incl_enhancement'

mAvailEnhancements = {}

mUserEnhancementMenu = {}

function ValidateUse(user)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( this:TopmostContainer() == user ) then
		return true
	end
	--TODO Add Distance Check
	return true
end

function DoEnhance(target, myEnhancement,user)
	if (myEnhancement == nil) then 
		--DebugMessage("Invalid Enhancement")
		return 
	end
	mUserEnhancementMenu[user] = nil
	local mySuccess = AddEnhancement(target, myEnhancement, this, user)
	if(mySuccess == true) then
	user:SystemMessage("[FA0C0C] Enhancement Successful.")
		return
	end
	user:SystemMessage("[FA0C0C] Enhancement Failed.")
	return
end

 
function HandleEnhanceSelection(user,buttonStr)
	--DebugMessage("Context Menu Received")	

	if( user == nil or not(user:IsValid()) ) then
		return
	end

	if( buttonStr == "" ) then return end

	local menuIndex = buttonStr
	
	if( menuIndex == nil or menuIndex == 0 ) then return end

	--DebugMessage(mTargItem:GetName())
	local enhancemenu = mUserEnhancementMenu[user]
	if(buttonStr == "ListEnhancements") then
		ShowEnhancementListWindow(user)
		mUserEnhancementMenu[user] = nil
		return
	end
	local targItem = enhancemenu.Target
	--DebugMessage("Target " ..tostring(targItem))
	if(targItem ~= nil) then
		DoEnhance(targItem, enhancemenu[menuIndex], user)
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "enhanceTarget", 
	function(target,user)
		local ValidEnhancements = {}
		menuItems = {}
		i = 0
		for k, v in pairs(mAvailEnhancements) do
			if(IsValidForItem(target,v)) then				
				i = i + 1
				table.insert(menuItems,"("..i..") "..v)
				ValidEnhancements[i] = v
				--DebugMessage(v .. " Added")
			end
		end
		ValidEnhancements[i+1] = "ListEnhancements"
		mUserEnhancementMenu[user] = ValidEnhancements
		mUserEnhancementMenu[user].Target = target
		table.insert(menuItems,"ListEnhancements")
		
		ButtonMenu.Show{
			TargetUser = user,
			ResponseObj = this,
			DialogId = "enhancement_station",
			TitleStr = "Enhance Item",
			Buttons = menuItems,
			ResponseFunc = HandleEnhanceSelection,
		}		
	end)

RegisterEventHandler(EventType.Message, "UseObject", 
	function(user,useType)		
		if not (useType == "Enhance") then
			return
		end

		if not(ValidateUse(user) ) then
			return
		end

		user:SystemMessage("What do you wish to work on?")
		user:RequestClientTargetGameObj(this, "enhanceTarget")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "enhancement_station", 
	function()
		mAvailEnhancements = initializer.AvailableEnhancements	or {}
		this:SetObjVar("AvailEnhancementsDict", mAvailEnhancements)

		AddUseCase(this,"Enhance",true)		
		SetTooltipEntry(this,"enhancement_station_desc","Can be used to enhance specific items.\n")			
	end)


function ShowEnhancementListWindow(user)
	local newWindow = DynamicWindow("EnhancementListWindow","Enhancement Info   ",260,360)
	local curIndex = 0
	local curRow = 0

	--DebugMessage("Menu Size: " .. optSize)
	local x = 30
	local y = 10
	for i,j in pairs (mAvailEnhancements) do
		local yVal = y + (curRow * 35)
		local infoTab = enhancementTable[j]
		if(infoTab ~= nil) then
			local enhanceDesc = enhancementTable[j].EnhancementDisplayEffect or ""
			local enhanceResReq = enhancementTable[j].ResourcesRequired or {}
			local resReqStr = ""
			for l,m in pairs (enhanceResReq) do
				resReqStr = resReqStr .. "\n"..tostring(l) .. ": " .. tostring(m)
			end
			local butInf = enhanceDesc .. "\n" .. resReqStr
				newWindow:AddButton(x, yVal, k, j, 200, 0, butInf, "", true)
				curRow = curRow + 1
		end
		user:OpenDynamicWindow(newWindow)
	end
end

if(initializer == nil) then
	mAvailEnhancements = this:GetObjVar("AvailEnhancementsDict") or {}	
end
