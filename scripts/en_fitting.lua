require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_fitting")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_fitting", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Fitting") then return end
		AlterEquipmentBonusStat(this, "BonusSwingSpeedModifier", -1)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)