
function DyeRemoverDialogReponse(user,buttonId)	
	buttonId = tonumber(buttonId)
	target = this:GetObjVar("DyeTarget")

	if( target == nil or not target:IsValid()) then
		return
	end

	if (buttonId == 0 and ValidateUse(user,target)) then	
		target:SetHue("0xFFFFFF")
		user:SystemMessage("You have successfully undyed "..target:GetName()..".")
		this:Destroy()	
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "dyeClothing",
	function(target,user)

		if (not ValidateUse(user,target)) then return end

		this:SetObjVar("DyeTarget",target)

		ClientDialog.Show{
			TargetUser = user,
			DialogId = "DyeClothing",
			TitleStr = "Dye Clothing",
			DescStr = "Do you wish to remove the dye on the "..target:GetName().."?",
			ResponseFunc = DyeRemoverDialogReponse
		}
	end
)

function ValidateUse(user,target)
	if( user == nil or not(user:IsValid()) ) then
		return false
	end

	if( target == nil or not target:IsValid()) then
		return false
	end

	if( this:TopmostContainer() ~= user ) then
		user:SystemMessage("[$1782]")
		return false
	end

	local isDyeable = false
	local tags = target:GetObjectTags()
	for i,j in pairs(tags) do
		if (j == "Head" or j == "Chest" or j =="Legs" or j == "Shield") then
			isDyeable = true
		end
	end

	if (not isDyeable) then
		user:SystemMessage("[$1783]")
		return false
	end

	return true
end

RegisterEventHandler(EventType.Message, "UseObject", 
    function(user,usedType)
    	if(usedType ~= "Use" and usedType ~= "Apply") then return end

		this:SetObjVar("DyeTarget",target)

		newHue = colorcode

		user:SystemMessage("What do you wish to undye?")
		--DebugMessage("RequestClientTargetGameObj")
		user:RequestClientTargetGameObj(this, "dyeClothing")
	end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(),
	function ( ... )
		SetTooltipEntry(this,"dye","Can be used to remove dye from certain items.")
        AddUseCase(this,"Apply",true,"HasObject")
	end)