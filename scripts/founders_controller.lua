-- DAB TODO: Need event handlers for players entering / leaving a region
AddView("FoundersAreaProtection",SearchMulti({SearchUser(),SearchRegion("FoundersArea",true)}))
AddView("VIPAreaProtection",SearchMulti({SearchUser(),SearchRegion("VIPArea",true)}))

RegisterEventHandler(EventType.EnterView,"FoundersAreaProtection",function (user)
	if not IsFounder(user) then
        PlayEffectAtLoc("TeleportToEffect",user:GetLoc())
		user:SetWorldPosition(Loc(27.41497, -0.03605413, 15.69382))
        PlayEffectAtLoc("TeleportFromEffect",Loc(27.41497, -0.03605413, 15.69382))
	end 
end)

RegisterEventHandler(EventType.EnterView,"VIPAreaProtection",function (user)
	if not IsCollector(user) then
        PlayEffectAtLoc("TeleportToEffect",user:GetLoc())
		user:SetWorldPosition(Loc(27.41497, -0.03605413, 15.69382))
        PlayEffectAtLoc("TeleportFromEffect",Loc(27.41497, -0.03605413, 15.69382))
	end 
end)