require 'incl_enhancement'

RegisterEventHandler(EventType.Timer, "RemoveEnhanceScript", 
	function()
		this:DelModule("en_hardening")
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "en_hardening", 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(1000), "RemoveEnhanceScript")
	end)

RegisterEventHandler(EventType.Message, "PerformEnhancementMessage",
    function(enName, enhancementUser)

		if(enName ~= "Hardening") then return end
		AlterEquipmentBonusStat(this, "BonusAbsorption", 3)
		this:SendMessage("UpdateTooltip")
		this:FireTimer("RemoveEnhanceScript")
	end)