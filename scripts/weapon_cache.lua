

-- caching some weapon information to local memory space since this only change when weapons are changed, 
	--- but the data is read from a lot.
_Weapon = {
	LeftHand = {
		Object = nil,
		Class = nil,
		Type = nil,
		DamageType = nil,
		IsRanged = false,
		DrawSpeed = nil,
		ShieldType = nil,
	},
	RightHand = {
		Object = nil,
		Class = nil,
		Type = nil,
		DamageType = nil,
		IsRanged = false,
		DrawSpeed = nil,
	}
}
-- function to update the cached weapon
function UpdateWeaponCache(slot, weaponObj)
	-- when weaponObj is explicity false, we can skip GetEquippedObject
	if ( weaponObj == false ) then
		weaponObj = nil
	else
		weaponObj = weaponObj or this:GetEquippedObject(slot)
	end

	if ( slot == "LeftHand" and weaponObj ) then
		_Weapon.LeftHand = {
			Object = weaponObj,
			ShieldType = GetShieldType(weaponObj)
		}
		-- shields need nothing more
		if ( _Weapon.LeftHand.ShieldType ) then return end
	end

	local weaponType = nil
	if ( IsPlayerCharacter(this) ) then
		weaponType = Weapon.GetType(weaponObj)
	else
		-- mobs are balanced to default to BareHand
		weaponType = this:GetObjVar("AI-WeaponType") or "BareHand"
	end

	_Weapon[slot] = {
		Object = weaponObj,
		Type = weaponType,
		Class = Weapon.GetClass(weaponType),
		DamageType = Weapon.GetDamageType(weaponType),
		IsRanged = Weapon.IsRanged(weaponType),
		DrawSpeed = Weapon.GetStat(weaponType, "DrawSpeed"),
		Range = this:GetObjVar("WeaponRange") or Weapon.GetRange(weaponType),
	}

	--DebugTable(_Weapon[slot])
end