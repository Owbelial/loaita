

function GetEnhancements(eItem)
	local eList = eItem:GetObjVar("EnhancementList")
	if(eList == nil) then eList = {} end
	return eList
end

function GetNumEnhancements(eItem)
	local eList = GetEnhancements(eItem)
	local eCount = eList.Count or 0
	return eCount
end

function GetEnhancementSlotsUsed(eItem)
	local eList = GetEnhancements(eItem)
	local sUsed = 0
	for e, v in pairs(eList) do
		if(e ~= "Count") then
			if(eList[e].Slots ~= nil) then
				sUsed = sUsed + eList[e].Slots
			else
				sUsed = sUsed + 1
			end
		end
	end
	return sUsed
end

function HasSkillForEnhancement(enhancer, eItem, eName, ignoreInstances)
	local enData = AllEnhancementData[eName]
	if(enData == nil) then 
		--D*ebugMessage("Invalid EData")
		return nil
	end
	local skillsReq = enData.SkillRequired
	local skillGainPerInst = enData.SkillGainPerInstance or {}
	if(skillsReq == nil) then return true end
	local eInstances = GetEnhancementInstances(eItem, eName)
	for i,v in pairs(skillsReq) do
		local curSkillLev = GetSkillLevel(enhancer,i) or 0
		local skillAmtReq = v
		local addSkillPerInst = skillGainPerInst[i] or 0
		local addSkillFac = addSkillPerInst * eInstances
		--D*ebugMessage("SKill: " .. curSkillLev .. " Req: " .. (addSkillFac + skillAmtReq))
		if((curSkillLev < (addSkillFac + skillAmtReq)) or ((ignoreInstances ~= nil) and (ignoreInstances > eInstances))) then
			return false
		end

	end
	return true
end







function RequestAddEnhancement(enhancer, eItem, eName)
	--D*ebugMessage("Enhancer" .. tostring(enhancer) .. " Item: " ..tostring(eItem) .. " Enhancement: " ..tostring(eName))
	if(CanAddEnhancementToItem(eItem, eName)) then
		--D*ebugMessage("AddingEnhancement")
		AddEnhancement(enhancer,eItem,eName)
	else
		enhancer:SystemMessage("[$1640]")
	end

end

function AddEnhancement(enhancer, eItem, eName)
	--D*ebugMessage("Trying Add")
	local eList = GetEnhancements(eItem)
	local eCount = eList.Count or 0
	local eInstances = 0

	--D*ebugTable(eList)
	if(eList[eName] ~= nil) then
		eInstances = eList[eName].Instances or 1
	else
		eList[eName] = {}
	end
	eInstances = eInstances + 1
	local eInfo = GetEnhancementInfo(eName, eList[eName], eInstances)
	--eList[eName] = eInfo
	eList[eName].Instances = eInstances
	eCount = eCount + 1
	eList.Count = eCount
	local slots = GetEnhancementSlotsRequired(eName, eInstances)
	local curSlots = eList[eName].Slots or 0
	local newSlots = curSlots + slots
	eList[eName].Slots = newSlots
	--D*ebugMessage("NEWSLOTS: " ..newSlots)
	local result = ApplyEnhancementBonusesToItem(eItem, eName, eInstances)
	if(result) then
		eItem:SetObjVar("EnhancementList", eList)
		SetTooltipEntry(eItem,"Enhanced","[F2BD0C]Enhanced[-]")
		eItem:SendMessage("UpdateTooltip")
		enhancer:SystemMessage("[0AFC0C] Successfully enhanced "..eItem:GetName().." with "..AllEnhancementData[eName].EnhancementDisplayName ..": ".. AllEnhancementData[eName].EnhancementDisplayEffect.." .")
		enhancer:SystemMessage("[0AFC0C] Successfully enhanced "..eItem:GetName().." .","event")
	else
		enhancer:SystemMessage("[FA0COC] Enhancement was not successful!.")
	end
	enhancer:SendMessage("EnhanceActivityComlete", result)
end

function GetEnhancementInfo(eName, eInstance)
	return { ["Name"] = eName}
end


function ApplyEnhancementBonusesToItem(eItem, eName, eInstances)
	local eInfo = AllEnhancementData[eName]
	if(eName == nil) then return false end
	local applyBonuses = {}
	local failOut = false
	local eBonuses = eInfo.EnhancementBonuses
	if(eBonuses ~= nil) then
		for i,v in pairs(eBonuses) do
			local curVal = eItem:GetObjVar(i) or 0
			local addVal = eBonuses[i].ValuePerInstance
			local valCap = eBonuses[i].ValueCap
			local newVal = curVal + addVal
			--D*ebugMessage( "Cur: " .. curVal .. " add: " ..addVal .. " Cap: " ..valCap) 
			if(newVal <= valCap) and ((-1 * valCap) <= (-1 *newVal)) then
				applyBonuses[i] = newVal
			else
				failOut = true
				break
			end

		end
		if(failOut) then return false end

	end
	local enhancementScripts = eInfo.EnhancementScripts 
	if(enhancementScripts ~= nil) then
		for k,l in pairs(enhancementScripts) do
			if not(eItem:HasModule(k))  then
				eItem:AddModule(k)
			end

				eItem:SendMessage(k .. "init" , eInstances)
	
		end
	end
	local eInstBonusInfo = eInfo.AdditionalBonuses
	if(eInstBonusInfo ~= nil) then
		eInstInfo = eInstBonusInfo.InstanceMin
		local addBon = {}
		if(eInstInfo ~= nil) then
			for w,x in pairs(eInstInfo) do
				if(w <= eInstances) then
					if(eInstInfo[w].Bonuses ~= nil) then
						for a,b in pairs(eInstInfo[w].Bonuses) do
							local myBon = applyBonuses[a] or 0
							myBon = myBon + b
							applyBonuses[a] = myBon
						end
					end
					if(eInstInfo[w].Multipliers ~= nil) then
						for c,d in pairs(eInstInfo[w].Multipliers) do
							local myBon = eItem:GetObjVar(c) or 1
							myBon = myBon * d
							eItem:SetObjVar(c,myBon)
						end
					end
				if(eInstInfo[w].Scripts ~= nil) then
						for e,f in pairs(eInstInfo[w].Scripts) do
							if(not eItem:HasModule(e)) then
								eItem:AddModule(e)
							end
							eItem:SendMessage(e .. "init", eInstances)
						end
					end
				end
			end
		end
	end
	for m,n in pairs(applyBonuses) do
		eItem:SetObjVar(m,n)
	end
	return true
end

function GetAvailableEnhancementsForItem(eItem)
	if(eItem == nil) then return end
	local eAvail = {}
	for i, v in pairs (AllEnhancementData) do
		--D*ebugMessage("Evaluating: " .. i)
		if(EnhancementIsValidForItem(eItem, i)) then
			--D*ebugMessage("Adding: " ..i)
			eAvail[i] = true
		end
	end
	return eAvail
end
function EnhancementIsValidForItem(eItem, eName)
	local eData = AllEnhancementData[eName]
	if(eData == nil) then 
		--D*ebugMessage("No Data for: " .. tostring(eName))
		return false 
	end
	local eClasses = eData.EnhancementItemClass or {}
	local iClass = GetEquipmentClass(eItem)
	local validClass = eClasses[iClass]
	--D*ebugMessage("ValidClass : ".. tostring(validClass))
	if(validClass ~= true) then
		--D*ebugMessage("Invalid Item Class: " .. iClass .. " for " ..eName)
		return false
	end
	local eSubClasses = eData.EnhancementItemSubClass

	if(eSubClasses ~= nil) then
		local iSubClasses = GetItemSubClasses(eItem) or {}
		for i,v in pairs (eSubClasses) do
			--D*ebugTable(eSubClasses)
			--D*ebugMessage("CurSubclass: " ..tostring(iSubClasses[i]))
			if (v and (iSubClasses[i] == nil or iSubClasses[i] == false)) or (v == false and (iSubClasses[i] == nil or iSubClasses[i] == false)) then 
						--D*ebugMessage("SubClass not found in item: " .. i .. " under: " ..eName )

				return 	false 
			end
		end
		--D*ebugMessage("Valid Subclasses")

	end
	--D*ebugMessage("CLASS: " .. iClass)
	local eDamClass = eData.EnhancementItemDamClass
	if(eDamClass ~= nil) then
		if(iClass ~= "WeaponClass") then return false end
		local iDamClass = GetWeaponDamageType(eItem)
		--D*ebugTable(eDamClass)
		--D*ebugMessage("IDAM: " ..iDamClass)
		if(eDamClass[iDamClass] ~= true) then return false end
	end
	return true
end


function CanAddEnhancementToItem(eItem, eName)
	if not(EnhancementIsValidForItem(eItem, eName)) then 
		return false 
	end
	
	if not(CanAddEnhancementInstance(eItem, eName)) then
		return false
	end
	return true

end

function CanAddEnhancementEffect(eItem, eName)
	local eInfo = AllEnhancementData[eName]
	local eBon = eInfo.EnhancementBonuses
	for a,b in pairs(eBon) do
		local curVal = eItem:GetObjVar(a) or 0
		local addVal = eBon[a].ValuePerInstance or 0
		local valCap = eBon[a].ValueCap
		if(curVal + addVal > valCap)  or ((-1 * ( curVal + addVal)) < (-1 * valCap)) then return false end
	end
	return true
end
function GetEnhancementInstances(eItem, eName)
	local eInstances = 0
	local eList = GetEnhancements(eItem)
	if(eList[eName] ~= nil) then
		eInstances = eList[eName].Instances or 1
	end 
	--D*ebugMessage(eName .. " Instances: " ..eInstances)
	return eInstances
end
function CanAddEnhancementInstance(eItem, eName)
	local eInstances = GetEnhancementInstances(eItem,eName)
	local maxInstances = GetMaxEnhancementInstances(eName) or 0
	if(eInstances > maxInstances) then
		return false
	end

	local slotsNeeded = GetEnhancementSlotsRequired(eName, eInstances)
	local availSlots = GetEnhancementSlotsAvailable(eItem)
	if(slotsNeeded > availSlots) then
		return false
	end
	return true
end

function GetMaxEnhancementInstances(eName)
	local eData = AllEnhancementData[eName]
	if(eData == nil) then return 0 end
	local maxInst = eData.EnhancementMaxInstances or 0
	return maxInst
end
function GetEnhancementSlotsRequired(eName, eInstances)
	local slotsPerInstance = GetEnhancementData(eName, "EnhancementSlotRequiredPerInstance")
	local bonusCostPerInstance = 0
	if(eInstances > 1) then 
		bonusCostPerInstance = GetEnhancementData(eName, "BonusSlotCostPerInstance") or 0
	end
	return math.floor(slotsPerInstance + bonusCostPerInstance)
end

function GetEnhancementSlotsAvailable(eItem)
	local maxSlots = GetMaxSlots(eItem) or 0
	local usedSlots = GetEnhancementSlotsUsed(eItem) or 0 
	--D*ebugMessage("U/M: " .. usedSlots .. "/" .. maxSlots)
	return math.max(0, maxSlots - usedSlots)
end

function GetMaxSlots(eItem)
	local toRet = GetBaseNumSlots(eItem)
	--D*ebugMessage("Max Slots: " ..toRet)
return toRet
end
function GetEnhancementData(eName, eInfo)
	local eBase = AllEnhancementData[eName]
	if(eBase == nil) then return nil end
	local eRet = eBase[eInfo]
	return eRet
end


function ConsumeResourcesForEnhancement(enhancer,eName)
	local enhancementResources = GetEnhancementData(eName, "ResourcesRequired")
	--D*ebugMessage("Checking for Consume")
	if(enhancementResources == nil) then return false end
	--D*ebugMessage("Consuming")
	return ConsumeResources(enhancementResources, this, "enhancement_controller")
end

function HasResourcesForEnhancement(enhancer,eName)
	local enhancementResources = GetEnhancementData(eName, "ResourcesRequired")
	if(enhancementResources == nil) then return true end
	return HasResources(enhancementResources, this)
end

