-- TODO: Store this information in a more consistent manner

-- Regen rate is in units per second

function GetResourceSourceId(objRef)
	local sourceId = objRef:GetSharedObjectProperty("ResourceSourceId")

	-- for dynamics you can also use an objvar to define source id
	if( not(objRef:IsPermanent()) and objRef:HasObjVar("ResourceSourceId") ) then
		sourceId = objRef:GetObjVar("ResourceSourceId")
	end

	return sourceId
end

function GetResourceSourceInfo(objRef)
	local sourceId = GetResourceSourceId(objRef)

	if( sourceId ~= nil ) then
		return ResourceData.ResourceSourceInfo[sourceId]
	end
	--LuaDebugCallStack("[ResourceSourceInfo] sourceId is nil!")
end

function IsResourceDepleted(objRef)
	--DebugMessage("Alpha")
	if( not(objRef:IsPermanent()) and objRef:HasObjVar("HandlesHarvest") ) then
		--DebugMessage("Beta")
		if (objRef:HasObjVar("HarvestCount")) then
			--DebugMessage("Count: " .. objRef:GetObjVar("HarvestCount"))
			return objRef:GetObjVar("HarvestCount") <= 0
		else
			return true
		end
	else		
		--DebugMessage("Gamma")
		-- get the resource source info (has total resource count)
		local sourceInfo = GetResourceSourceInfo(objRef)
		if (sourceInfo == nil or sourceInfo.Count == nil ) then return true end


		local depletion = 0
		-- permanent depletion is stored on the map resource controller
		if(objRef:IsPermanent()) then
			--DebugMessage("Rectuli")
			local mapController = FindObjectWithTag("MapResourceController")
			--DebugMessage("mapController is "..tostring(mapController))
			if(mapController) then
				local sourceData = mapController:GetObjVar("permanentSourceData")
				--DebugMessage("TABLE DUMP:"..DumpTable(sourceData))
				--DebugMessage(sourceData,sourceData[objRef]," is the CHECK")
				if (sourceData == nil or sourceData[objRef] == nil) then 
					--DebugMessage("Here first")
					depletion = 0
				else
					--DebugMessage("Nope here")
					depletion = sourceData[objRef].Depletion or 0
				end
			end
		-- dynamic depletion is stored in a table on the object
		else
			--DebugMessage("Zeta")
			local sourceData = objRef:GetObjVar("ResourceSourceData")
			if (sourceData == nil) then 
				depletion = 0
			else
				depletion = sourceData.Depletion or 0
			end
		end
		--DebugMessage("Depletion = ",depletion)
		--DebugMessage(sourceInfo.Count.." is sourceInfo.count)")
		return (sourceInfo.Count <= depletion)
	end
end

function GetRequiredTool(objRef)
	local requiredTool = nil
	if( not(objRef:IsPermanent()) and objRef:HasObjVar("HarvestToolType") ) then
		requiredTool = objRef:GetObjVar("HarvestToolType")
		--DebugMessage("Attempted to harvest non-permanent object")
	else
		local sourceId = GetResourceSourceId(objRef)
		--DebugMessage("sourceId is "..tostring(sourceId).." Id is ".. objRef.Id)
		if( sourceId ~= nil and ResourceData.ResourceSourceInfo[sourceId] ~= nil ) then
			-- tool required
			requiredTool = ResourceData.ResourceSourceInfo[sourceId].ToolType
			--DebugMessage("ToolType is " .. tostring(requiredTool))
		end
	end

	return requiredTool
end

function GetHarvestToolInBackpack(user,toolType)
	local backpackObj = user:GetEquippedObject("Backpack")  
	if( backpackObj ~= nil ) then
		--DebugMessage("Tooltype is "..tostring(toolType))
		local packObj = FindItemInContainerRecursive(backpackObj,
        function(item)            
            return item:GetObjVar("ToolType") == toolType
        end)
		--DFB HACK: Check the equipped object to see if it's a tool also.
		local equippedObj = user:GetEquippedObject("RightHand")
		if (equippedObj ~= nil and equippedObj:GetObjVar("ToolType") == toolType) then
			packObj = equippedObj
		end

		return packObj
	end
end

function GetResourceDisplayName(resourceType)
	if(ResourceData.ResourceInfo[resourceType] ~= nil
			and ResourceData.ResourceInfo[resourceType].DisplayName ~= nil) then
		return ResourceData.ResourceInfo[resourceType].DisplayName
	else
		return resourceType
	end
end