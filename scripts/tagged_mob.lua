
-- Apply this to the loot container of the mob ( backpack )
_RegenColor = "Color=00FFFF"


_TopMost = this:TopmostContainer() or this
_players = {}

_playingLocalEffect = false
_playingGlobalEffect = false

function ClearLocalEffect()
    if ( _playingLocalEffect ) then
        for i=1,#_players do
            local player = _players[i]
            if ( player:IsValid() ) then
                player:StopLocalEffect(_TopMost, "RegenEffect", 2.0)
            end
        end
        _playingLocalEffect = false
    end
end

function ClearGlobalEffect()
    if ( _playingGlobalEffect ) then
        _TopMost:StopEffect("RegenEffect", 2.0)
        _playingGlobalEffect = false
    end
end

RegisterEventHandler(EventType.Message, "Tag", function(player)
    if ( player and player:IsValid() ) then
        table.insert(_players, player)
        -- make it sparkle
        player:PlayLocalEffect(_TopMost,"RegenEffect",0,_RegenColor)
        _playingLocalEffect = true
        -- in 1 minute it's free for all
        if not( this:HasTimer("TagClearTimer") ) then
            this:ScheduleTimerDelay(TimeSpan.FromMinutes(1), "TagClearTimer")
        end
    end
end)

RegisterEventHandler(EventType.Timer, "TagClearTimer", function()
    _TopMost:DelObjVar("Tag")
    ClearLocalEffect()
    if ( #this:GetContainedObjects() > 0 ) then
        -- there's still stuff left, make it sparkle for everyone
        _TopMost:PlayEffect("RegenEffect", 0, _RegenColor)
        _playingGlobalEffect = true
    end
end)

RegisterEventHandler(EventType.ContainerItemRemoved, "", function(contObj)
    CallFunctionDelayed(TimeSpan.FromMilliseconds(1), function()
        if ( #this:GetContainedObjects() == 0 ) then
            -- remove the tag timer, it's empty.
            if ( this:HasTimer("TagClearTimer") ) then
                this:RemoveTimer("TagClearTimer")
            end
            -- clear any/all effects, the mob is empty.
            ClearLocalEffect()
            ClearGlobalEffect()
        end
    end)
end)