RegisterEventHandler(EventType.Message, "UseObject", 
	function(user,usedType)
		if(usedType == "Ignite/Extinguish") then
			local isLit = this:GetSharedObjectProperty("IsLit")
			this:SetSharedObjectProperty("IsLit",not isLit)
		end
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "lightable",
	function()
        AddUseCase(this,"Ignite/Extinguish",false)        
	end)