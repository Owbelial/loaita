-- Locks the chest until all the mobs are defeated

RegisterSingleEventHandler(EventType.ModuleAttached,"buried_treasure_lock",
	function ( ... )
		local mobTables = initializer.MobTables
		local spawnCount = 0 
		if(mobTables and #mobTables > 0) then
			local mobSpawns = mobTables[math.random(#mobTables)]
			for i, mobTemplate in pairs(mobSpawns) do
				local spawnLoc = GetNearbyPassableLoc(this,360,2,5)
				if(spawnLoc) then
					this:PlayEffect("TeleportFromEffect")
					CreateObj(mobTemplate, spawnLoc, "mob_spawned")
					spawnCount = spawnCount + 1
				end
			end
		end

		if(spawnCount == 0) then
			this:DelModule("buried_treasure_lock")
		end

		if(this:HasModule("decay")) then
			this:DelModule("decay")
		end

		this:PlayObjectSound("DoorLock")
		this:SendMessage("Lock")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"CheckEventComplete")
		this:ScheduleTimerDelay(TimeSpan.FromMinutes(5),"EndEvent")
	end)

RegisterEventHandler(EventType.CreatedObject, "mob_spawned",
	function(success,objRef)
		if(success) then
			AddToListObjVar(this,"TreasureMobs",objRef)
		end
	end)

RegisterEventHandler(EventType.Timer,"CheckEventComplete",
	function()
		local livingMobs = 0
	    local allMobs = this:GetObjVar("TreasureMobs")
		for i,mobRef in pairs(allMobs) do
			if(mobRef and mobRef:IsValid() and not(IsDead(mobRef))) then
				livingMobs = livingMobs + 1
			end
		end

		if(livingMobs > 0) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"CheckEventComplete")
		else
			this:PlayEffect("TeleportToEffect")
			this:PlayObjectSound("DoorUnlock")
			this:SendMessage("Unlock")
			this:DelModule("buried_treasure_lock")
		end
	end)

RegisterEventHandler(EventType.Timer,"EndEvent",
	function()
		local allMobs = this:GetObjVar("TreasureMobs")
		for i,mobRef in pairs(allMobs) do
			if(mobRef and mobRef:IsValid() and not(IsDead(mobRef))) then				
				mobRef:Destroy()
			end
		end

		this:Destroy()
	end)